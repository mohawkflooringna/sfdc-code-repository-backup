public with sharing class hybrisPostHandler_CC{
    
    public static HttpResponse processHttpRequest(String callOutName, String reqBody, String contentType) {
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint('callout:'+callOutName); 
        req.setHeader('Content-Type', contentType);
        req.setBody(reqBody);
         
        Http http = new Http();
        HTTPResponse res = http.send(req);
        System.debug(res.getBody());
        return res;
    }
    
    @AuraEnabled
    public static String doCreateQuotePost(String relAcId, String contactId) {  
        
        Id oppId;
        String resBody = null;
        system.debug('contactId :'+contactId);
        Related_Account__c dealerAc = [SELECT Account__c,
                                       Opportunity__c, 
                                       Account__r.Global_Account_Number__c,
                                       Account__r.Name,
                                       Account__r.ShippingStreet,
                                       Account__r.ShippingCity,                                                     
                                       Account__r.ShippingStateCode 
                                       FROM Related_Account__c 
                                       WHERE id =: relAcId];         // AND Project_Role__c =:UtilityCls.DEALER
        
        // 04/13/2017
        String contactEmail = '';
        if(UtilityCls.isStrNotNull(contactId)){
            contactEmail = [SELECT Id, Name, FirstName, LastName, Email, Contact_Phone__c, AccountId
                            FROM Contact WHERE 
                            Id =: contactId].Email;
        }
        
        if(dealerAc.Opportunity__c != null){
            List<Related_Account__c> endSpecUserRelAcc ;
            String endUserEmailId = '',specifierEmailId = '';
            
            Try
            {
                endSpecUserRelAcc = [SELECT Account__c,
                                     Opportunity__c, Project_Role__c ,
                                     Account__r.Global_Account_Number__c,
                                     Contact__r.Email
                                     FROM Related_Account__c 
                                     WHERE Opportunity__c =: dealerAc.Opportunity__c and Project_Role__c IN ('End User','Specifier') ];
                
            }catch(Exception ex)
            {
                system.debug('ex '+ ex.getMessage());
            }
            for(Related_Account__c relAcc : endSpecUserRelAcc)
            {
                if(relAcc.Project_Role__c == 'End User')
                    endUserEmailId = relAcc.Contact__r.Email;
                if(relAcc.Project_Role__c == 'Specifier')
                    specifierEmailId = relAcc.Contact__r.Email;
                
            }
            
            oppId = dealerAc.Opportunity__c;
            
            Opportunity opp = [SELECT Id, AccountId, Account.Name, 
                               Account.Global_Account_Number__c,
                               Account.ShippingCity,
                               Account.ShippingStreet,
                               Account.ShippingStateCode,
                               Market_Segment__c,
                               CloseDate,GPO__c,GSA__c, 
                               Description 
                               From Opportunity WHERE Id =:oppId];
            
            // 04/13/2017
            List<OpportunityLineItem> productList = [SELECT Product2.Id, Quantity, Product2.UOM__c, Product2.External_id__c
                                                     FROM OpportunityLineItem WHERE OpportunityId =: oppId];
            
            String[] tempStr = String.valueOf(opp.CloseDate).split(' ');
            String closeDate = tempStr[0];
            
            Integer quantity = 0;
            
            // <contact>admin@mohawk.com</contact> received on 04/12/2017 need to 
            String body = '<b2bQuote>'   
                +'<projectID>'+oppId+'</projectID>' 
                +'<b2bUnitAccount>'+dealerAc.Account__r.Global_Account_Number__c+'</b2bUnitAccount>'
                
                //start Added By LJ US-30976 12-15-2017
                //+'<user>'+UserInfo.getUserEmail()+'</user>' //Commented by MB - 2/22/18
                +'<user>'+UserInfo.getUserName()+'</user>' // Added by MB - 2/22/18
                +'<contact>'+ contactEmail+'</contact>'
                +'<endUser>'+endUserEmailId+'</endUser>'
                +'<specifier>'+specifierEmailId+'</specifier>'
                //End Added By LJ US-30976 12-15-2017  
                
                +'<marketingSegment>'+opp.Market_Segment__c+'</marketingSegment>'  
                +'<headerComments>'+opp.Description+'</headerComments>' 
                +'<expirationDate>'+closeDate+'</expirationDate>'
                +'<salespersonEmailAddress>'+UserInfo.getUserEmail()+'</salespersonEmailAddress>'
                
                //start Added By LJ US-30976 12-15-2017
                +'<secureToken></secureToken>'   
                
                +'<gpo>'+opp.GPO__c+'</gpo>'
                +'<gsa>'+opp.GSA__c+'</gsa>'
                //End Added By LJ US-30976 12-15-2017
                
                /*Commented By LJ US-30976 12-15-2017
+'<jobLocation>'+opp.Account.ShippingStreet+'</jobLocation>'   
+'<jobLocationCity>'+opp.Account.ShippingCity+'</jobLocationCity>'   
+'<jobLocationState>'+opp.Account.ShippingStateCode+'</jobLocationState>'   

+'<specifyingCompany>'+dealerAc.Account__r.Name+'</specifyingCompany>'   
+'<specifyingCompanyLocation>'+dealerAc.Account__r.ShippingStreet+'</specifyingCompanyLocation>'   
+'<specifyingCompanyState>'+dealerAc.Account__r.ShippingStateCode +'</specifyingCompanyState>'   
*/
                +'<productLineInfo>';   
            system.debug('productList :'+productList);
            for(OpportunityLineItem prod : productList) {
                quantity = 0;
                Decimal tempQuantity = prod.Quantity;
                quantity = tempQuantity.intValue();
                
                body += '<productLine>'   
                    +'<productId>'+prod.Product2.External_id__c+'</productId>'   
                    +'<quantityUOM>'+prod.Product2.UOM__c+'</quantityUOM>'   
                    +'<quantity>'+quantity+'</quantity>'   
                    +'</productLine>';
            }
            
            body += '</productLineInfo>'   
                +'</b2bQuote>';   
            
            system.debug('@@@@body@@'+body);
            
            HttpResponse httpRes = processHttpRequest('Hybris_Create_Quote_New', body, 'application/xml');
            System.debug('Hybris_Create_Quote_New');
            // HttpResponse httpRes = processHttpRequest('Hybris_Quote_Creation', body, 'application/xml');
            resBody = createQuote(httpRes);
            system.debug('******resBody****'+resBody);
            //resBody = Json.serialize(resBody);
        }//end If
        
        return  resBody;
    }//End of Method 
    
    
    public static String createQuote(HttpResponse resBody){
        
        system.debug('****resBody**'+resBody);
        Dom.Document doc = new Dom.Document();
        doc.load(String.valueOf(resBody.getBody()));
        //Dom.Document doc = resBody.getBodyDocument();
        
        system.debug('****resBody**'+resBody.getBody());
        system.debug('****resBody**'+doc);
        //Retrieve the root element for this document.
        Dom.XMLNode rootElet = doc.getRootElement();
        String responseStatus = rootElet.getChildElement('ResponseStatus', null).getText();
        
        System.debug(rootElet);
        System.debug(responseStatus);
        
        ResultHandler result = new ResultHandler();
        result.responseStatus = responseStatus;
        
        System.debug('**Child Nodes ' + rootElet.getChildElements());
        
        if(responseStatus == 'Failure'){
            //String errorMessage = rootElet.getChildElement('errorMessage', null).getText();
            //result.message = errorMessage;
        }
        
        System.debug('**httpStatus ' + result.responseStatus);
        System.debug('**httpMessage ' + result.message);
        
        //Return reslut in JSON format
        return Json.serialize(result); 
        
    }
    
    @AuraEnabled
    public static List<Related_Account__c> getRelatedAccList(Id opportunityId) {
        System.debug(opportunityId+'----');
        return [SELECT Id, Account__r.Name, Account__c FROM Related_Account__c WHERE Project_Role__c IN ('Dealer', 'End User') AND Opportunity__c =: opportunityId];
    }
    
    Class ResultHandler{
        @AuraEnabled
        Public String responseStatus {get; set;}
        @AuraEnabled
        Public String message {get; set;}
        
    }
    
    @AuraEnabled
    public static List<Contact> getAccountContacts(Id accId) {
        
        return [SELECT Id, Name, FirstName, LastName, Email, Contact_Phone__c, AccountId
                FROM Contact WHERE 
                AccountId =: accId];
    }
    
    
}