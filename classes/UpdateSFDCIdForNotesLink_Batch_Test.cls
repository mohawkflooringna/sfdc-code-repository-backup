/**************************************************************************

Name : UpdateSFDCIdForNotesLink_Batch_Test
===========================================================================
Purpose : Test class for UpdateSFDCIdForNotesLink_Batch
===========================================================================
History:
--------
VERSION    AUTHOR         DATE             DETAIL          DESCRIPTION
2.0		Susmitha         2/March/2018      Created
***************************************************************************/
@isTest
private class UpdateSFDCIdForNotesLink_Batch_Test {
    
    static testMethod void testMethod1() {
        List<Account> accountlst=utility_test.createAccounts(false, 1);
        accountlst[0].SAP_Account_GUID__c='Test guid';
        insert accountlst;
        Content_Note_Link__c c=new Content_Note_Link__c ();
        
        c.GUID__c='Test guid';
        c.Note_ID__c='Test Note';
        c.Object_ID__c='Test';
        c.Object_Type__c='Account';
        insert c;
        UpdateSFDCIdForNotesLink_Batch  cls = new UpdateSFDCIdForNotesLink_Batch ();
        String jobId = database.executeBatch(cls);
        system.assert(jobiD!=null);
    }
    static testMethod void testMethod2() {
        List<Opportunity> opplst=utility_test.createOpportunities(false, 1);
        opplst[0].SAP_Opportunity_GUID__c='Test guid';
        insert opplst;
        Content_Note_Link__c c=new Content_Note_Link__c ();
        c.GUID__c='Test guid';
        c.Note_ID__c='Test Note';
        c.Object_ID__c='Test';
        c.Object_Type__c='Project';
        insert c;
        UpdateSFDCIdForNotesLink_Batch  cls = new UpdateSFDCIdForNotesLink_Batch ();
        String jobId = database.executeBatch(cls);
        system.assert(jobiD!=null);
        
    }
    static testMethod void testMethod3() {
        List<Contact> contlst=utility_test.createContacts(false, 1);
        contlst[0].SAP_Contact_GUID__c='Test guid';
        insert contlst;
        Content_Note_Link__c c=new Content_Note_Link__c ();
        c.GUID__c='Test guid';
        c.Note_ID__c='Test Note';
        c.Object_ID__c='Test';
        c.Object_Type__c='Contact';
        insert c;
        UpdateSFDCIdForNotesLink_Batch  cls = new UpdateSFDCIdForNotesLink_Batch ();
        String jobId = database.executeBatch(cls);
        system.assert(jobiD!=null);
        
    }
    static testMethod void testMethod4() {
        List<Account> accountlist=utility_test.createAccounts(true, 1);
        List<Task> taskList=utility_test.createTasks(false, 1, accountlist);
        taskList[0].SAP_Appointment_GUID__c='Test guid';
        insert taskList;
        Content_Note_Link__c c=new Content_Note_Link__c ();
        c.GUID__c='Test guid';
        c.Note_ID__c='Test Note';
        c.Object_ID__c='Test';
        c.Object_Type__c='Activity';
        insert c;
        UpdateSFDCIdForNotesLink_Batch  cls = new UpdateSFDCIdForNotesLink_Batch ();
        String jobId = database.executeBatch(cls);
        system.assert(jobiD!=null);
        
    }
    static testMethod void testMethod5() {
        List<Account> accountlist=utility_test.createAccounts(true, 1);
        List<Event> EvtList=utility_test.createEvents(false, 1, accountlist);
        EvtList[0].SAP_Appointment_GUID__c='Test guid';
        insert EvtList;
        Content_Note_Link__c c=new Content_Note_Link__c ();
        c.GUID__c='Test guid';
        c.Note_ID__c='Test Note';
        c.Object_ID__c='Test';
        c.Object_Type__c='Activity';
        insert c;
        UpdateSFDCIdForNotesLink_Batch  cls = new UpdateSFDCIdForNotesLink_Batch ();
        String jobId = database.executeBatch(cls);
        system.assert(jobiD!=null);
        
    }
}