/**************************************************************************

Name : SampleDisplayInventoryInsert_Batch_Test
===========================================================================
Purpose : Uitlity class to SampleDisplayInventoryInsert_Batch test data 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Sasi Naik    2/May/2017    Create  

***************************************************************************/
@isTest
private class SampleDisplayInventoryInsert_Batch_Test {
    static List<Product2> prod2List;
    static List<Account> resAccListForInvoicing;
    
    @isTest
    static void init(){
        List<Product2>Prod2List = Utility_Test.createProduct2(false, 1);
            for( Integer i=0; i<Prod2List.size(); i++ ){
                Prod2List[i].RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get(UtilityCls.LABELREDIDENTALDISPLAY).getRecordTypeId();
                Prod2List[i].Buying_Group__c = 'Open Line (OL)';
                Prod2List[i].External_Id__c = '1234'+i;
                Prod2List[i].IsActive = true;
                Prod2List[i].Trackable_Display_asset__C = true;
            }
            insert Prod2List;
            
            List<Account>resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for( Integer i=0; i < resAccListForInvoicing.size(); i++ ){
                resAccListForInvoicing[i].Business_Type__c = 'Residential';
                resAccListForInvoicing[i].Customer_Group__c = 'Open Line (OL)';
            }
            insert resAccListForInvoicing;
            
            
            Product_Customer_Group_Relationship__c pcgr = new Product_Customer_Group_Relationship__c();
            pcgr.Product__c = Prod2List[0].Id;
            pcgr.Customer_Group_Number__c = '000000';
            pcgr.External_Id__c = '12340';
            insert pcgr;
    }
    
    static testMethod void testMethod1() {
        System.runAs( Utility_Test.ADMIN_USER ) {            
            test.startTest();
            init();
            SampleDisplayInventoryInsert_Batch  cls = new SampleDisplayInventoryInsert_Batch(null);
            database.executeBatch(cls,10);
            
            String sch = '0 0 2 * * ?'; 
            system.schedule('Test SampleDisplayInventoryInsert_Batch', sch, cls); 
            test.stopTest();
        }
    }
}