public without sharing class Multiday_Visit_WindowsHandler{
    
    public static void isAfterInsertMultiDay( List<mamd__Multiday_Visit_Windows__c>newList ){
        List<SObject>ListToInsert = new List<SObject>();
        set<string>recordIds = new set<string>();
        if( newList != null && newList.size() > 0 ){
            set<string>accountIds = new set<string>();            
                        
            for( mamd__Multiday_Visit_Windows__c MVW : newList ){
                if( MVW.mamd__VW_Account__c != null && MVW.VW_Mohawk_Account_Team__c == null ){
                    accountIds.add( MVW.mamd__VW_Account__c );
                    recordIds.add( MVW.id );
                }
            }
            
            if( accountIds != null && accountIds.size() > 0 && recordIds != null && recordIds.size() > 0 ){
                Map<id,List<id>>AccountMATMap = new Map<id,List<id>>();
                for( Mohawk_Account_Team__c MAT : [SELECT id,Account__c FROM Mohawk_Account_Team__c WHERE Account__c IN : accountIds] ){
                    if( AccountMATMap.containsKey( MAT.Account__c ) ){
                        AccountMATMap.get( MAT.Account__c ).add( MAT.id );
                    }else{
                        AccountMATMap.put( MAT.Account__c,New List<id>{ MAT.id } );
                    }
                }
                system.debug( '::::recordIds::::' + recordIds );
                string soql = queryString() + ' WHERE id IN : recordIds';
                system.debug( ':::::soql::::' + soql );
                //List<mamd__Multiday_Visit_Windows__c>listToWorkOn = queryMultiDay( 'id IN : recordIds' );    
                List<mamd__Multiday_Visit_Windows__c>listToWorkOn = database.query( soql );
                for( mamd__Multiday_Visit_Windows__c MVW : listToWorkOn  ){
                    if( AccountMATMap != null && AccountMATMap.containsKey( MVW.mamd__VW_Account__c ) ){
                        for( Id MATID : AccountMATMap.get( MVW.mamd__VW_Account__c ) ){
                            SObject newRecord = new mamd__Multiday_Visit_Windows__c();            
                            for( string str : allFields() ){
                                if( str.Contains('__c') && str != 'VW_Mohawk_Account_Team__c' ){
                                    newRecord.put( str , MVW.get( str ) );
                                }else{
                                    newRecord.put( 'VW_Mohawk_Account_Team__c' , MATID );
                                }
                            }
                            newRecord.put('mamd__VW_Account__c' , null);
                            ListToInsert.add( newRecord );
                        }
                    }
                }
                
                if( !ListToInsert.isEmpty() ){
                    system.debug( ':::::B ListToInsert::::' + ListToInsert );
                    insert ListToInsert;
                    //system.debug( ':::::A ListToInsert::::' + ListToInsert );
                }
            }
            
        }        
    }
    
    public static void isAfterUpdateMultiDay( List<mamd__Multiday_Visit_Windows__c>newList ){
    
        Map<id,List<mamd__Multiday_Visit_Windows__c>>accMVWMap = new Map<id,List<mamd__Multiday_Visit_Windows__c>>();
        Set<string>daysSet = new Set<string>();
        List<mamd__Multiday_Visit_Windows__c>updateMVW = new List<mamd__Multiday_Visit_Windows__c>();
        
        for( mamd__Multiday_Visit_Windows__c MVW : newList ){
            if( MVW.mamd__VW_Account__c != null ){
                if( accMVWMap.ContainsKey( MVW.mamd__VW_Account__c ) ){
                    accMVWMap.get( MVW.mamd__VW_Account__c ).add( MVW );
                }else{
                    accMVWMap.put( MVW.mamd__VW_Account__c,new List<mamd__Multiday_Visit_Windows__c>{ MVW } );
                }
                daysSet.add( MVW.mamd__Days_of_Week__c );
            }
        }
        system.debug( ':::accMVWMap::::' + accMVWMap );
        if( accMVWMap != null && accMVWMap.size() > 0 ){
            Map<id,id>MATACCMap = new Map<id,id>();
            Set<string>MATIds = new Set<string>();
            for( Mohawk_Account_Team__c MAT : [SELECT id,Account__c FROM Mohawk_Account_Team__c WHERE Account__c IN : accMVWMap.keySet() ] ){
                MATACCMap.put( MAT.id,MAT.Account__c );
                MATIds.add( MAT.id );
            }
            
            string soql = queryString() + ' WHERE VW_Mohawk_Account_Team__c IN : MATIds AND mamd__Days_of_Week__c IN : daysSet';
            system.debug( '::::soql:::' + soql );
            //List<mamd__Multiday_Visit_Windows__c>listToWorkOn = queryMultiDay( 'VW_Mohawk_Account_Team__c IN : MATACCMap.keySet() AND mamd__Days_of_Week__c : daysSet' );
            List<mamd__Multiday_Visit_Windows__c>listToWorkOn = database.query( soql );
            for( mamd__Multiday_Visit_Windows__c MVW_UP : listToWorkOn ){
                if( MVW_UP.VW_Mohawk_Account_Team__c != null ){
                    for( mamd__Multiday_Visit_Windows__c MVW : accMVWMap.get( MATACCMap.get( MVW_UP.VW_Mohawk_Account_Team__c ) ) ){
                        if( MVW_UP.mamd__Days_of_Week__c ==  MVW.mamd__Days_of_Week__c ){
                            MVW_UP.mamd__Time_Window_Start_1__c = MVW.mamd__Time_Window_Start_1__c;
                            MVW_UP.mamd__Time_Window_End_1__c = MVW.mamd__Time_Window_End_1__c;
                            MVW_UP.mamd__Time_Window_Start_2__c = MVW.mamd__Time_Window_Start_2__c;
                            MVW_UP.mamd__Time_Window_End_2__c = MVW.mamd__Time_Window_End_2__c;
                            updateMVW.add( MVW_UP );
                        }
                    }
                }
            }
        
            if( !updateMVW.isEmpty() )
                update updateMVW;
        }
    }
    
    public static void isAfterDeleteMultiDay( List<mamd__Multiday_Visit_Windows__c>oldList ){
        Map<id,List<mamd__Multiday_Visit_Windows__c>>accMVWMap = new Map<id,List<mamd__Multiday_Visit_Windows__c>>();
        Set<string>daysSet = new Set<string>();
        List<mamd__Multiday_Visit_Windows__c>deleteMVW = new List<mamd__Multiday_Visit_Windows__c>();
        
        for( mamd__Multiday_Visit_Windows__c MVW : oldList ){
            if( MVW.mamd__VW_Account__c != null ){
                if( accMVWMap.ContainsKey( MVW.mamd__VW_Account__c ) ){
                    accMVWMap.get( MVW.mamd__VW_Account__c ).add( MVW );
                }else{
                    accMVWMap.put( MVW.mamd__VW_Account__c,new List<mamd__Multiday_Visit_Windows__c>{ MVW } );
                }
                daysSet.add( MVW.mamd__Days_of_Week__c );
            }
        }
        system.debug( ':::accMVWMap::::' + accMVWMap );
        system.debug( '::::daysSet:::::' + daysSet );
        if( accMVWMap != null && accMVWMap.size() > 0 ){
            Map<id,id>MATACCMap = new Map<id,id>();
            Set<string>MATIds = new Set<string>();
            for( Mohawk_Account_Team__c MAT : [SELECT id,Account__c FROM Mohawk_Account_Team__c WHERE Account__c IN : accMVWMap.keySet() ] ){
                MATACCMap.put( MAT.id,MAT.Account__c );
                MATIds.add( MAT.id );
            }
            system.debug( '::::MATACCMap::::' + MATACCMap );
            system.debug( '::::MATIds::::' + MATIds );
            string soql = queryString() + ' WHERE VW_Mohawk_Account_Team__c IN : MATIds AND mamd__Days_of_Week__c IN : daysSet';            
            List<mamd__Multiday_Visit_Windows__c>listToWorkOn = database.query( soql );
            system.debug( '::::listToWorkOn::::' + listToWorkOn.size() );
            
            for( mamd__Multiday_Visit_Windows__c MVW_UP : listToWorkOn ){
                if( MVW_UP.VW_Mohawk_Account_Team__c != null ){
                    for( mamd__Multiday_Visit_Windows__c MVW : accMVWMap.get( MATACCMap.get( MVW_UP.VW_Mohawk_Account_Team__c ) ) ){
                        if( MVW_UP.mamd__Days_of_Week__c ==  MVW.mamd__Days_of_Week__c ){
                            deleteMVW.add( MVW_UP );
                        }
                    }
                }
            }
            system.debug( ':::deleteMVW::::' + deleteMVW );
            if( !deleteMVW.isEmpty() )
                delete deleteMVW;
        }
    }
    
    public static string queryString(){
        String query = ' SELECT ' +
                        String.join( allFields(), ',' ) +
                        ' FROM mamd__Multiday_Visit_Windows__c ';
        return query;
    }
    
    public static List<string>allFields(){
        return new List<String>( mamd__Multiday_Visit_Windows__c.SObjectType.getDescribe().fields.getMap().keySet() ); 
    }
}