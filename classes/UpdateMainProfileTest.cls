/**************************************************************************


Name : OutBoundDeletion_Test 
===========================================================================
Purpose :  Test class to UpdateMainProfileTest test data 
===========================================================================
History:
--------
VERSION    AUTHOR              DATE           DETAIL          DESCRIPTION
1.0        Nagendra Singh     29/Jan/2019       

***************************************************************************/
@isTest
public class UpdateMainProfileTest {
    
    static List<Account> resAccListForInvoicing;
    static List<Territory__c> terrList;
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static Id resiNonInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Non-Invoicing');
    static Id resiInvRTId    = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
    
    static testMethod void test_CheckMainProfileCheckBox_Insert() {
        System.runAs(Utility_Test.ADMIN_USER) {        
            test.startTest(); 
            //init();
                      
            Utility_Test.getTeamCreationCusSetting(); 
            AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
            APSC.Name = 'Two Team Account';
            APSC.Exclude_Chain_Numbers_1__c = '11';
            APSC.Exclude_Chain_Numbers_2__c = '12';
            insert APSC;
            
            List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false); // Added by MB - 07/10/17
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1); 
            //resAccListForInvoicing = Utility_Test.createAccountsForInvoicing(true, 1); // Added by MB - 07/07/17
            terrList = Utility_Test.createTerritories(true, 2);
            system.assert(terrList.size()>0);
            mohAccTeam = Utility_Test.createMohawkAccountTeam(true, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId, accProfile, 'Horizon');
            for (Mohawk_Account_Team__c mat : mohAccTeam){
                mat.Main_Profile__c = false;  
            }
            //  update mohAccTeam;
            List<Account> acList = new List<Account>();
            for(Integer i = 0; i < 1; i++){
                
                acList.add(new Account(Name = 'Residential Account for Invoicing'+i, 
                                       Business_Type__c = 'Residential', Customer_Group__c = 'Mohawk Aligned;Independent Aligned;Open Line',
                                       Role_C__c = 'End User', BillingPostalCode = '100'+i, ShippingPostalCode = '100'+i, Facilities__c= '0-10',
                                       ShippingCountry = 'United States', ShippingCity= 'New Year', ShippingStreet = 'Street',
                                       Global_Account_Number__c = 'R20000'+i, CAMS_Account_Number__c = 'R.900124.1'+i, Chain_Number__c = 'R.900124',
                                       RecordTypeId = UtilityCls.getRecordTypeInfo('Account', UtilityCls.INVOICING_ACCOUNT) ));                 
            }
            
            insert acList;
            
            //  List<Account> resAccListForInvoicing1 = Utility_Test.createResidentialAccountsForInvoicing(true, 1); 
            List<Mohawk_Account_Team__c> mohAccTeam3  = Utility_Test.createMohawkAccountTeam(true, 1, acList, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId, accProfile, 'Aladdin');
            
            List<Mohawk_Account_Team__c> lstMHK = [SELECT createdDate,Main_Profile__c From Mohawk_Account_Team__c WHERE Id=:mohAccTeam[0].id];
            System.assert(lstMHK[0].Main_Profile__c ==true);
            //String jobIdSch = System.schedule('Test my class', '0 0 0 1 JAN,APR,JUL,OCT ? *', new updateMATForBI_Batch());
            
            test.stopTest();
        }
    }
    
    
    
    static testMethod void testCheck_MainProfile_updatedelete() {
        System.runAs(Utility_Test.ADMIN_USER) {        
            test.startTest(); 
            //init();
            
            Utility_Test.getTeamCreationCusSetting(); 
            AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
            APSC.Name = 'Two Team Account';
            APSC.Exclude_Chain_Numbers_1__c = '11';
            APSC.Exclude_Chain_Numbers_2__c = '12';
            insert APSC;
            
            
            List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false); // Added by MB - 07/10/17
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1); 
            //resAccListForInvoicing = Utility_Test.createAccountsForInvoicing(true, 1); // Added by MB - 07/07/17
            terrList = Utility_Test.createTerritories(true, 2);
            system.assert(terrList.size()>0);
            mohAccTeam = Utility_Test.createMohawkAccountTeam(true, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId, accProfile, 'Horizon');
            for (Mohawk_Account_Team__c mat : mohAccTeam){
                mat.Main_Profile__c = false;  
            }
            update mohAccTeam;
            List<Account> acList = new List<Account>();
            for(Integer i = 0; i < 1; i++){
                
                acList.add(new Account(Name = 'Residential Account for Invoicing'+i, 
                                       Business_Type__c = 'Residential', Customer_Group__c = 'Mohawk Aligned;Independent Aligned;Open Line',
                                       Role_C__c = 'End User', BillingPostalCode = '100'+i, ShippingPostalCode = '100'+i, Facilities__c= '0-10',
                                       ShippingCountry = 'United States', ShippingCity= 'New Year', ShippingStreet = 'Street',
                                       Global_Account_Number__c = 'R20000'+i, CAMS_Account_Number__c = 'R.900124.1'+i, Chain_Number__c = 'R.900124',
                                       RecordTypeId = UtilityCls.getRecordTypeInfo('Account', UtilityCls.INVOICING_ACCOUNT) ));                 
            }
            
            insert acList;
            
            //  List<Account> resAccListForInvoicing1 = Utility_Test.createResidentialAccountsForInvoicing(true, 1); 
            List<Mohawk_Account_Team__c> mohAccTeam3  = Utility_Test.createMohawkAccountTeam(true, 1, acList, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId, accProfile, 'Aladdin');
            delete mohAccTeam;
            
            List<Mohawk_Account_Team__c> lstMHK = [SELECT createdDate,Main_Profile__c From Mohawk_Account_Team__c WHERE Id=:mohAccTeam3[0].id];
            System.assert(lstMHK[0].Main_Profile__c ==true);
            
            test.stopTest();
        }
    }
    
}