/***************************************************************************

Name : generalDropDownController 

===========================================================================
Purpose :  Controller for generalDropDown lightning component
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha      15/Jan/2018     Created 
***************************************************************************/
public class generalDropDownController 
{
    // List of account profile settings
    public class sObjectListWrapper
    {
        @AuraEnabled
        public List <Account_profile_settings__c> sObjectList;
        
        @AuraEnabled
        public Boolean isError;
    }
    
    
    /*Method which return wrapper class which holds list of sObject*/    
    @AuraEnabled
    public static sObjectListWrapper getsObjectList(String objectName,String IdValue) 
    {
        sObjectListWrapper accWrapper = new sObjectListWrapper();
        accWrapper.sObjectList = getObjectList(objectName,IdValue);
        accWrapper.isError = false;
        return accWrapper;
    }
    
    public static List < Account_profile_settings__c > getObjectList(String objectName,String IdValue ) {        
        List<Account_profile__c> accProf = new List<Account_profile__c>();        
        accProf=[SELECT ID,  Primary_Business__c FROM Account_Profile__c WHERE ID =:IdValue Limit 1];
        System.debug(LoggingLevel.INFO, '*** objectName: ' + objectName);
        List<Account_profile_settings__c> returnList = new List<Account_profile_settings__c>();
        list<Account_profile_settings__c> lstOfRecords=[select Id,Name FROM  Account_profile_settings__c];
        for(Account_profile_settings__c  obj : lstOfRecords){
            if(obj.Id==accProf[0].Primary_Business__c){
                returnList.add(obj);
            }
        }
        for(Account_profile_settings__c  obj : lstOfRecords){
            if(obj.Id!=accProf[0].Primary_Business__c){
                returnList.add(obj);
            }
        }
        return returnList;
        
    }
    
    @AuraEnabled
    public static sObject getObject(String IdValue,String objectName ) {        
        List<Account_profile_settings__c> lstOfRecords = new List<Account_profile_settings__c>();
        lstOfRecords=[SELECT 
                      ID,
                      NAME,
                      Builder_Business_Split_1Team__c, 
                      Builder_Business_Split_2_Team__c, 
                      Multi_Family_Business_Split_1team__c, 
                      Multi_Family_Business_Split_2Team__c,
                      Retail_Business_Split_1Team__c, 
                      Retail_Business_SPlit_2Team__c,
                      Distribution_Carpet__c,
                      Distribution_Cushion__c,
                      Distribution_Hardwood__c,
                      Distribution_Laminate__c,
                      Distribution_Tile__c,
                      Distribution_Resilient__c, 
                      Retail__c,
                      Builder__c,
                      Multi_Family__c,
                      Aladdin_Commercial__c,
                      Retail_Multi_channel_split__c,
                      Builder_Multi_Family_Multi_Channel_Split__c,
                      Retail_1_channel_split__c,
                      Builder_Multi_Family_1_Channel_Split__c                
                      FROM Account_profile_settings__c WHERE ID=:IdValue LIMIT 1 ];
        system.debug('rec'+lstOfRecords[0]);
        return lstOfRecords[0];
    }
    @AuraEnabled
    public static Account_Profile__c getAllDistributions(Id recId){
        system.debug('test :::'+recId);
        return [SELECT R_Distribution_Tile__c, 
                R_Distribution_Resilient__c,
                R_Distribution_Laminate__c,
                R_Distribution_Hardwood__c,
                R_Distribution_Cushion__c,
                R_Distribution_Carpet__c,
                B_Distribution_Tile__c,
                B_Distribution_Resilient__c,
                B_Distribution_Laminate__c,
                B_Distribution_Hardwood__c,
                B_Distribution_Cushion__c,
                B_Distribution_Carpet__c,
                R_Business_Split__c,
                B_Business_Split__c,
                Primary_Business__r.Name,
                Primary_Business__c,
                Annual_Retail_Sales__c,
                Annual_Retail_Sales_Non_Flooring__c,
                Two_Sales_Teams__c
                 
                FROM Account_Profile__c WHERE Id =:recId Limit 1];
    }
    
}