@isTest
private class MATDeletefromCAMS_BatchTest {
    static List<Account> resAccListForInvoicing;
    static List<Territory__c> terrList;
    static List<Mohawk_Account_Team__c> mohAccTeam;
    
   
    
    static Id resiInvRTId    = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
    
    @TestSetup
    public static void init(){   
        Utility_Test.getTeamCreationCusSetting();
        Utility_Test.getDefaultConfCusSetting();
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
        
        AP_Trigger_Settings__c APSC1 = new AP_Trigger_Settings__c();
        APSC1.Name = 'Chain Number Exclude';
        APSC1.Exclude_Chain_Numbers_1__c = '10';
        APSC1.Exclude_Chain_Numbers_2__c = '13';
        insert APSC1;
                              
        MAT_Batch_Setting__c setting = new MAT_Batch_Setting__c();
            setting.Last_Run_At__c = SYstem.today().adddays(-5);
            setting.MinutesDelay__c = 5;
            setting.Batch_Size__c = 200;
            setting.Name='MATDeletefromCAMS_Batch';
            setting.Batch_Query__c = 'SELECT id,name, Territory_User_Number__c from Mohawk_Account_Team__c where User__c != null';
            insert setting;
    }
    
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();  
            terrList = Utility_Test.createTerritories(false, 2);
            for(Territory__c ter : terrList){
                ter.type__c='Builder / Multi-Family';
            }
            insert terrList;
            User u=Utility_Test.getTestUser('TestUser',null);

           
            
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            insert resAccListForInvoicing;

            system.debug('$$$$$$$$$$Residential User'+Utility_Test.RESIDENTIAL_USER);
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
            for(Mohawk_Account_Team__c mat:mohAccTeam){
                mat.Account__c=resAccListForInvoicing[0].id;
            }
            insert mohAccTeam;
           
            system.assert(mohAccTeam!=null);
            

            MATDeletefromCAMS_Batch  cls = new MATDeletefromCAMS_Batch ();
            String jobIdShare=Database.executeBatch(cls);
            System.assert(jobIdShare != null);
            String sch = '0 0 23 * * ?';
            system.schedule('Test SCH Check', sch, cls); 
            
            test.stopTest();
            
        }
        
    }
}