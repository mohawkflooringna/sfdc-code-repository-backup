public class MHKAccountTeamTriggerAsync {
   @future
  public static void updateMohawkAccountTeamMemberIdsAsync(Set<id> AccIds, Set<id> matids, Boolean isInsert,Boolean isDelete ) {
       Map<Id,Set<Id>> AccUserIdsMap=new Map<Id,Set<id>>();
       List<Account> AccountListToUpdate= [SELECT ID,Mohawk_Team_Members__c,(SELECT ID,Account__c,User__c FROM Mohawk_Account_Teams__r) FROM Account WHERE ID IN:AccIds];
               
       for(Account acc : AccountListToUpdate){
                                                   
               for(Mohawk_Account_Team__c each: acc.Mohawk_Account_Teams__r){
                     if(AccUserIdsMap.containsKey(each.Account__c)){
                            AccUserIdsMap.get(each.Account__c).add(each.User__c);
                       }else{
                            set<id> acTeamList = new set<id>();
                            acTeamList.add(each.User__c);
                            AccUserIdsMap.put(each.Account__c, acTeamList);
                       }
              }
        }
        
        
        for(Account a :AccountListToUpdate){
            String str='';
            if(AccUserIdsMap.containsKey(a.id)){
                for(id usId:AccUserIdsMap.get(a.id)){
                     str+=usId+';';
                }
              str = str.removeEnd(';');   //remove ; from  end if present  
              str = str.removeStart(';');   //remove ; from  Start if present            

            }
            a.Mohawk_Team_Members__c=str;
        }
    
        if(AccountListToUpdate.size()>0){
            UtilityCls.AsycupdateFromMAT = true; // To prevent recurssion
            Database.update(AccountListToUpdate,false);
        }
      //System.debug('MHK TEam'+[SELECT ID, Account__c, Territory_User_Number__c ,recordtype.name  from mohawk_account_team__c]);
      // MAT to Territory Account Assignments
      List<mohawk_account_team__c> MATList= [SELECT ID, Account__c, Territory_User_Number__c  from mohawk_account_team__c WHERE ID IN:matids and recordtype.name != 'Commercial' all rows ];
      if(isInsert && !MATList.isempty()){
          MATtoTerritoryAccountService.createTerritoryAccounts(MATList);
      }
      
      if(isDelete && !MATList.isempty()){
          MATtoTerritoryAccountService.deleteTerritoryAccounts(MATList);
      }
  }
}