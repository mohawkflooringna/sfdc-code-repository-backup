/**************************************************************************

Name : Rest_MATAccountProfile_Test

===========================================================================
Purpose : This class is used for Rest_MATAccountProfile
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0		  Susmitha       28/Feb/2018     Created
***************************************************************************/
@isTest
private class Rest_MATAccountProfile_Test {
    static testMethod void testMethod1() {
        test.startTest();
        
        
        List<Account> AccountList=Utility_Test.createAccounts(true,1);
        List<Territory__c> terrList= Utility_Test.createTerritories(true,1);
        List<My_Account_Plan__c> MyAccountPlanList=Utility_Test.createMyAccountPlan(false,1);
        for(My_Account_Plan__c macp: MyAccountPlanList){
            macp.Account__c=AccountList[0].id;
            macp.Fiscal_Year__c=String.valueOf(Date.Today().Year());
            macp.Segments__c='Healthcare';
        }
        insert MyAccountPlanList;
        system.assert(MyAccountPlanList.size()>0);

        User u=new User(Id=userinfo.getuserid());
        
        List<Mohawk_Account_Team__c> mohawkAccTeam=Utility_Test.createMohawkAccountTeam(FALSE,1,AccountList,terrList,u);
        mohawkAccTeam[0].Chain_Number_UserId__c='R.6765434';
        insert mohawkAccTeam;
        system.assert(mohawkAccTeam.size()>0);

        Rest_MATAccountProfile.SaveContract(mohawkAccTeam[0]);
        
        test.stopTest();
    }
    static testMethod void testMethod2() {
        test.startTest();
        
        
        List<Account> AccountList=Utility_Test.createAccounts(true,1);
        List<Territory__c> terrList= Utility_Test.createTerritories(true,1);
        List<My_Account_Plan__c> MyAccountPlanList=Utility_Test.createMyAccountPlan(false,1);
        for(My_Account_Plan__c macp: MyAccountPlanList){
            macp.Account__c=AccountList[0].id;
            macp.Fiscal_Year__c=String.valueOf(Date.Today().Year());
            macp.Segments__c='Healthcare';
        }
        insert MyAccountPlanList;
        User u=new User(Id=userinfo.getuserid());
        
        List<Mohawk_Account_Team__c> mohawkAccTeam=Utility_Test.createMohawkAccountTeam(FALSE,1,AccountList,terrList,u);
        mohawkAccTeam[0].Chain_Number_UserId__c=null;
        insert mohawkAccTeam;
        system.assert(mohawkAccTeam.size()>0);

        Rest_MATAccountProfile.SaveContract(mohawkAccTeam[0]);
        
        test.stopTest();
    }
    static testMethod void testMethod3() {
        test.startTest();
        
        
        List<Account> AccountList=Utility_Test.createAccounts(true,1);
        List<Territory__c> terrList= Utility_Test.createTerritories(true,1);
        List<My_Account_Plan__c> MyAccountPlanList=Utility_Test.createMyAccountPlan(false,1);
        for(My_Account_Plan__c macp: MyAccountPlanList){
            macp.Account__c=AccountList[0].id;
            macp.Fiscal_Year__c=String.valueOf(Date.Today().Year());
            macp.Segments__c='Healthcare';
        }
        insert MyAccountPlanList;
        User u=new User(Id=userinfo.getuserid());
        
        List<Mohawk_Account_Team__c> mohawkAccTeam=Utility_Test.createMohawkAccountTeam(FALSE,1,AccountList,terrList,u);
        insert mohawkAccTeam;
        system.assert(mohawkAccTeam.size()>0);
        mohawkAccTeam[0].Chain_Number_UserId__c='test chain num';
        
        Rest_MATAccountProfile.SaveContract(mohawkAccTeam[0]);
        
        test.stopTest();
    }
}