/**************************************************************************

Name : accountProfileCustomDetailControllerTest

===========================================================================
Purpose : This tess class is used for accountProfileCustomDetailController
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Radhika       29/May/2018     Created         
***************************************************************************/
@isTest
public class accountProfileCustomDetailControllerTest {
    
    static testMethod void getPickListValuesIntoList() 
    {
        List<string>pickListValuesList = new List<string>();
        Schema.DescribeFieldResult fieldResult = AP_Section__mdt.Section_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
       
        List<String> result = accountProfileCustomDetailController.getPickListValuesIntoList();
        System.assertEquals(pickListValuesList, result);
    }
    static testMethod void getAccountProfileDetail()
    {
        Account_Profile__c apRec = new Account_Profile__c();
        apRec.Name = 'test record';
        apRec.Multi_Channel__c = true;
        insert apRec;
        accountProfileCustomDetailController.getAccountProfileDetail(apRec.id);
        List<accountProfileCustomDetailController.expendableSectionWrapper> testWrapList = 
        accountProfileCustomDetailController.createExpandableSections('Retail','Total','DESKTOP',apRec.id);
    }
    static testMethod void createExpandableSections1()
    {
        Account_Profile_Settings__c apsRec = new Account_Profile_Settings__c(name='Retail');
        insert apsRec;
        Account_Profile__c apRec = new Account_Profile__c();
        apRec.Name = 'test record';
        apRec.Multi_Channel__c = true;
        apRec.Primary_Business__c = apsRec.id;
        insert apRec;
        List<accountProfileCustomDetailController.expendableSectionWrapper> testWrapList = 
        accountProfileCustomDetailController.createExpandableSections('Retail','Total','DESKTOP',apRec.id);
        List<accountProfileCustomDetailController.tableSectionContentWrapper> tableWrapList = new List<accountProfileCustomDetailController.tableSectionContentWrapper>();
        for(accountProfileCustomDetailController.expendableSectionWrapper ep : testWrapList)
        {
            tableWrapList.addAll(ep.tableSectionContent);
        }
        accountProfileCustomDetailController.tableWrapper twp = 
            accountProfileCustomDetailController.createTableSectionsContent(JSON.serialize(tableWrapList),apRec.id,'Total Overview');
    }
    static testMethod void createExpandableSections2()
    {
       Account_Profile_Settings__c apsRec = new Account_Profile_Settings__c(name='Retail');
        insert apsRec;
        Account_Profile__c apRec = new Account_Profile__c();
        apRec.Name = 'test record';
        apRec.Multi_Channel__c = true;
        apRec.Primary_Business__c = apsRec.id;
        insert apRec;
        List<accountProfileCustomDetailController.expendableSectionWrapper> testWrapList = 
        accountProfileCustomDetailController.createExpandableSections('Retail','Total','MOBILE',apRec.id);
        List<accountProfileCustomDetailController.tableSectionContentWrapper> tableWrapList = new List<accountProfileCustomDetailController.tableSectionContentWrapper>();
        for(accountProfileCustomDetailController.expendableSectionWrapper ep : testWrapList)
        {
            tableWrapList.addAll(ep.tableSectionContent);
        }
        accountProfileCustomDetailController.tableWrapper twp = 
            accountProfileCustomDetailController.createTableSectionsContent(JSON.serialize(tableWrapList),apRec.id,'Carpet Breakdown');
  
    }
    static testMethod void createBlockSectionsContent()
    {
        List<AP_Section_Fields__mdt> apSectionFieldsmdt = returnAPSectionFieldRecords('m0K2C000000CaVi');
        Account_Profile_Settings__c apsRec = new Account_Profile_Settings__c(name='Retail');
        insert apsRec;
        Account_Profile__c apRec = new Account_Profile__c();
        apRec.Name = 'test record';
        apRec.Multi_Channel__c = true;
        apRec.Primary_Business__c = apsRec.id;
        insert apRec;
        List<accountProfileCustomDetailController.blockSectionContentWrapper> testWrapList = 
        accountProfileCustomDetailController.createBlockSectionsContent(apSectionFieldsmdt,apRec.id);
        
    }
    static List<AP_Section_Fields__mdt> returnAPSectionFieldRecords(String parentSectionName)
    {
        List<AP_Section_Fields__mdt> apSectionFieldsmdt = [ SELECT DeveloperName,Alignment__c,AP_Section__c,Field_Name__c,isShow__c,
                                      Label_L__c,Label_M__c,Label_S__c,Length__c,Sequence__c,Type__c FROM AP_Section_Fields__mdt where AP_Section__c= :parentSectionName];
        return apSectionFieldsmdt;
    }
}