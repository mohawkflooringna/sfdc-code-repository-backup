/***************************************************************************

Name : HybrisPostHandler_CC_Test 

===========================================================================
Purpose :  Test class for HybrisPostHandler_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand        30/March/2017     Created 
***************************************************************************/
@isTest
private class HybrisPostHandler_CC_Test {
    
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
    }
    
    static testMethod void testMethod1() {
        init();
        
        List<Account> accForNonInvoicingList2 = Utility_Test.createAccountsForNonInvoicing(false, 1); 
        List<Account> accList =  Utility_Test.createAccounts(true, 1);
        
        for(Integer i=0;i<accForNonInvoicingList2.size();i++){
            accForNonInvoicingList2[i].Name = 'Teat comm RelAc2';
            accForNonInvoicingList2[i].Business_Type__c = 'Commercial';
        }
        insert accForNonInvoicingList2;
        system.assert(accForNonInvoicingList2!=null);
        List<Opportunity> oppList  = Utility_Test.createOpportunities(true, 1, accList);
        List<Related_Account__c> relAccList = Utility_Test.createRelatedAccountList(true, 1, accForNonInvoicingList2, oppList);
        String relAcId = String.valueOf(relAccList.get(0).id);
        system.assert(relAcId!=null);
        
        Test.setMock(HttpCalloutMock.class, new HybrisPostHandlerCalloutMock_Test());
        
        test.startTest();
        hybrisPostHandler_CC.getRelatedAccList(oppList[0].Id);
        hybrisPostHandler_CC.getAccountContacts(accForNonInvoicingList2[0].Id);
        system.assert(relAcId!=null);
        
        System.assertNotEquals(hybrisPostHandler_CC.doCreateQuotePost(relAcId, null), null);
        test.stopTest();
    }
    
}