public class distributionInAccountProfileController {
    @AuraEnabled
    public static Account_Profile__c getAllDistributions(Id recId){
        return [SELECT Primary_Business__r.Name,
                Primary_Business__c,
                Annual_Retail_Sales__c,
                Annual_Retail_Sales_Non_Flooring__c,
                Two_Sales_Teams__c,
                Cut_Order__c,
                Competitor_Alignment__c,
                If_Other_Alignment_Please_Specify__c,
                Stocking__c,
                Number_of_Selling_Locations__c,
                Number_of_Sales_Associates__c,
                Retail__c,
                Builder__c,
                Multi_Family__c,
                Aladdin_Commercial__c,
                Product_Category_Allocation_Carpet__c,
                Product_Category_Allocation_Cushion__c,
                Product_Category_Allocation_Laminate__c,
                Product_Category_Allocation_Resilient__c,
                Product_Category_Allocation_Tile__c,
                Product_Category_Allocation_Wood__c,
                Multi_Channel__c,
                Chain_Number__c
                FROM Account_Profile__c WHERE Id =:recId Limit 1];
    }
    @AuraEnabled
    //public static void updateDistributions(Account_Profile__c ap){
    public static boolean updateDistributions( Account_Profile__c ap ){
        boolean isError = false;
        try{
            update ap;
			return isError;
        }
        catch(exception ex){
            isError = true;
            UtilityCls.createExceptionLog(ex,  'distributionInAccountProfileController', 'updateDistributions', 'logCode', '', '');
            system.debug('exception:::'+ex);
            return isError;
        }        
    }
    @AuraEnabled
    public static boolean isChainNumberExist( Account_Profile__c ap ){
        system.debug( ':::CHAIN NUMBER:::' + ap.Chain_Number__c );
        Set<string>chainSet1 = new Set<string>();
        Set<string>chainSet2 = new Set<string>();
        for( AP_Trigger_Settings__c ATS : [ SELECT id,Exclude_Chain_Numbers_1__c,Exclude_Chain_Numbers_2__c FROM AP_Trigger_Settings__c WHERE Name =: 'Two Team Account' ] ){
            chainSet1.addall( ATS.Exclude_Chain_Numbers_1__c.split(';') );
            chainSet2.addall( ATS.Exclude_Chain_Numbers_2__c.split(';') );
            
        }
        system.debug( '::SET 1:::'+chainSet1 );
        system.debug( '::SET 2:::'+chainSet2 );
        system.debug( '::CONDITION 1 CHECK:::'+chainSet1.contains( ap.Chain_Number__c ) );
        system.debug( '::CONDITION 2 CHECK:::'+chainSet2.contains( ap.Chain_Number__c ) );
        if( chainSet1.contains( ap.Chain_Number__c ) || chainSet2.contains( ap.Chain_Number__c ) )
            return true;
        else
        	return false;
    }
    
    
    private static Account_Profile__c getAPforRollUp( string recordId ){
        Account_profile__c ap = [ SELECT id
                ,Product_Category_Allocation_Carpet__c
                ,Product_Category_Allocation_Cushion__c
                ,Product_Category_Allocation_Wood__c
                ,Product_Category_Allocation_Laminate__c
                ,Product_Category_Allocation_Tile__c
                ,Product_Category_Allocation_Resilient__c
                FROM Account_Profile__c WHERE id =: recordId ];
        
        	ap.R_GOpp_A__c = 0;
            ap.R_Yellow_Opportunity_Aladdin__c = 0;
            ap.R_ROpp_A__c = 0;
            ap.B_GOpp_A__c = 0;
            ap.B_Yellow_Opportunity_Aladdin__c = 0;
            ap.B_ROpp_A__c = 0;
            ap.R_GOpp_Cus_A__c = 0;
            ap.R_YOpp_Cus_Ala__c = 0;
            ap.R_ROpp_Cus_A__c = 0;
            ap.B_GOpp_Cus_A__c = 0;
            ap.B_YOpp_Cus_Ala__c = 0;
            ap.B_ROpp_Cus_A__c = 0;
            ap.R_GOpp_Hor__c = 0;
            ap.R_Yellow_Opportunity_Horizon__c = 0;
            ap.R_ROpp_Hor__c = 0;
            ap.B_GOpp_Hor__c = 0;
            ap.B_Yellow_Opportunity_Horizon__c = 0;
            ap.B_ROpp_Hor__c = 0;
            ap.R_GOpp_Cus_Hor__c = 0;
            ap.R_YOpp_Cus_Hor__c = 0;
            ap.R_ROpp_Cus_Hor__c = 0;
            ap.B_GOpp_Cus_Hor__c = 0;
            ap.B_YOpp_Cus_Hor__c = 0;
            ap.B_ROpp_Cus_HoB__c = 0;
            ap.R_GOpp_Kar__c = 0;
            ap.R_Yellow_Opportunity_Karastan__c = 0;
            ap.R_ROpp_K__c = 0;
            ap.B_GOpp_Kar__c = 0;
            ap.B_Yellow_Opportunity_Karastan__c = 0;
            ap.B_ROpp_K__c = 0;
            ap.R_GOpp_Cus_Kar__c = 0;
            ap.R_YOpp_Cus_Kar__c = 0;
            ap.R_ROpp_Cus_K__c = 0;
            ap.B_GOpp_Cus_Kar__c = 0;
            ap.B_YOpp_Cus_Kar__c = 0;
            ap.B_ROpp_Cus_K__c = 0;
            ap.R_GOpp_AC__c = 0;
            ap.R_Yellow_Opportunity_Aladdin_Comm__c = 0;
            ap.R_ROpp_AC__c = 0;
            ap.B_GOpp_AC__c = 0;
            ap.B_Yellow_Opportunity_Aladdin_Comm__c = 0;
            ap.B_ROpp_AC__c = 0;
            ap.R_GOpp_Cus_AC__c = 0;
            ap.R_YOpp_Cus_Ala_Comm__c = 0;
            ap.R_ROpp_Cus_AC__c = 0;
            ap.B_GOpp_Cus_AC__c = 0;
            ap.B_YOpp_Cus_Ala_Comm__c = 0;
            ap.B_ROpp_Cus_AC__c = 0;
            ap.R_GOpp_RC__c = 0;
            ap.R_Yellow_Opportunity_Resilient_Comm__c = 0;
            ap.R_Red_Opportunity_Resilient_Comm__c = 0;
            ap.B_GOpp_RC__c = 0;
            ap.B_Yellow_Opportunity_Resilient_Comm__c = 0;
            ap.B_Red_Opportunity_Resilient_Comm__c = 0;
            ap.R_GOpp_Car_PP__c = 0;
            ap.R_YOpp_Car_PP__c = 0;
            ap.R_ROpp_Car_PP__c = 0;
            ap.B_GOpp_Car_PP__c = 0;
            ap.B_YOpp_Car_PP__c = 0;
            ap.B_ROpp_Car_PP__c = 0;
            ap.R_GOpp_Cus_PP__c = 0;
            ap.R_YOpp_Cus_PP__c = 0;
            ap.R_ROpp_Cus_PP__c = 0;
            ap.B_GOpp_Cus_PP__c = 0;
            ap.B_YOpp_Cus_PP__c = 0;
            ap.B_ROpp_Cus_PP__c = 0;
            ap.R_GOpp_Har_PP__c = 0;
            ap.R_YOpp_Har_PP__c = 0;
            ap.R_ROpp_Har_PP__c = 0;
            ap.B_GOpp_Har_PP__c = 0;
            ap.B_YOpp_Har_PP__c = 0;
            ap.B_ROpp_Har_PP__c = 0;
            ap.R_GOpp_Lam_PP__c = 0;
            ap.R_YOpp_Lam_PP__c = 0;
            ap.R_ROpp_Lam_PP__c = 0;
            ap.B_GOpp_Lam_PP__c = 0;
            ap.B_YOpp_Lam_PP__c = 0;
            ap.B_ROpp_Lam_PP__c = 0;
            ap.R_GOpp_Tile_PP__c = 0;
            ap.R_YOpp_Tile_PP__c = 0;
            ap.R_ROpp_Tile_PP__c = 0;
            ap.B_GOpp_Tile_PP__c = 0;
            ap.B_YOpp_Tile_PP__c = 0;
            ap.B_ROpp_Tile_PP__c = 0;
            ap.R_GOpp_Res_PP__c = 0;
            ap.R_YOpp_Res_PP__c = 0;
            ap.R_ROpp_Res_PP__c = 0;
            ap.B_GOpp_Res_PP__c = 0;
            ap.B_YOpp_Res_PP__c = 0;
            ap.B_ROpp_Res_PP__c = 0;
            ap.R_GOpp_Har_RW__c = 0;
            ap.R_YOpp_Har_RW__c = 0;
            ap.R_ROpp_Har_RW__c = 0;
            ap.B_GOpp_Har_RW__c = 0;
            ap.B_YOpp_Har_RW__c = 0;
            ap.B_ROpp_Har_RW__c = 0;
            ap.R_GOpp_Lam_RL__c = 0;
            ap.R_YOpp_Lam_RL__c = 0;
            ap.R_ROpp_Lam_RL__c = 0;
            ap.B_GOpp_Lam_RL__c = 0;
            ap.B_YOpp_Lam_RL__c = 0;
            ap.B_ROpp_Lam_RL__c = 0;
            ap.R_GOpp_Tile_RT__c = 0;
            ap.R_YOpp_Tile_RT__c = 0;
            ap.R_ROpp_Tile_RT__c = 0;
            ap.B_GOpp_Tile_RT__c = 0;
            ap.B_YOpp_Tile_RT__c = 0;
            ap.B_ROpp_Tile_RT__c = 0;
            ap.R_GOpp_RR__c = 0;
            ap.R_Yellow_Opportunity_Resilient_Retail__c = 0;
            ap.R_Red_Opportunity_Resilient_Retail__c = 0;
            ap.B_GOpp_RR__c = 0;
            ap.B_Yellow_Opportunity_Resilient_Retail__c = 0;
            ap.B_Red_Opportunity_Resilient_Retail__c = 0;
            ap.R_GOpp_Car__c = 0;
            ap.R_Yellow_Opportunity_Carpet__c = 0;
            ap.R_ROpp_Car__c = 0;
            ap.B_GOpp_Car__c = 0;
            ap.B_Yellow_Opportunity_Carpet__c = 0;
            ap.B_ROpp_Car__c = 0;
            ap.R_GOpp_Cus__c = 0;
            ap.R_Yellow_Opportunity_Cushion__c = 0;
            ap.R_ROpp_Cus__c = 0;
            ap.B_GOpp_Cus__c = 0;
            ap.B_Yellow_Opportunity_Cushion__c = 0;
            ap.B_ROpp_Cus__c = 0;
            ap.R_GOpp_Har__c = 0;
            ap.R_Yellow_Opportunity_Hardwood__c = 0;
            ap.R_ROpp_Har__c = 0;
            ap.B_GOpp_Har__c = 0;
            ap.B_Yellow_Opportunity_Hardwood__c = 0;
            ap.B_ROpp_Har__c = 0;
            ap.R_GOpp_L__c = 0;
            ap.R_Yellow_Opportunity_Laminate__c = 0;
            ap.R_ROpp_L__c = 0;
            ap.B_GOpp_L__c = 0;
            ap.B_Yellow_Opportunity_Laminate__c = 0;
            ap.B_ROpp_L__c = 0;
            ap.R_GOpp_T__c = 0;
            ap.R_Yellow_Opportunity_Tile__c = 0;
            ap.R_Red_Opportunity_Tile__c = 0;
            ap.B_GOpp_T__c = 0;
            ap.B_Yellow_Opportunity_Tile__c = 0;
            ap.B_Red_Opportunity_Tile__c = 0;
            ap.R_GOpp_R__c = 0;
            ap.R_Yellow_Opportunity_Resilient__c = 0;
            ap.R_ROpp_R__c = 0;
            ap.B_GOpp_R__c = 0;
            ap.B_Yellow_Opportunity_Resilient__c = 0;
            ap.B_ROpp_R__c = 0;
       
        return ap;
    }
    @AuraEnabled
    public static void rollupMat( string recordId ){
        Account_Profile__c ap = getAPforRollUp( recordId );
       // system.debug( ':::DIS CALL:::' + ap );
     
        MohawkAccountTeamService.updateFromEvent = true;
        update MohawkAccountTeamService.calculateRollupfromMAT( ap );
    }
    
    @AuraEnabled
    public static lightningTableWrapper getFieldHistory( string recordId ){
        lightningTableWrapper ltw = new lightningTableWrapper();        
        set<string>fieldName = new set<string>{
            'Primary_Business__c',
                'Annual_Retail_Sales__c',
                'Cut_Order__c',
                'Stocking__c',
                'Annual_Retail_Sales_Non_Flooring__c',
                'Competitor_Alignment__c',
                'If_Other_Alignment_Please_Specify__c',
                'Number_of_Sales_Associates__c',
                'Number_of_Selling_Locations__c',
                'Retail__c',
                'Builder__c',
                'Multi_Family__c',
                'Aladdin_Commercial__c',
                'Product_Category_Allocation_Carpet__c',
                'Product_Category_Allocation_Cushion__c',
                'Product_Category_Allocation_Laminate__c',
                'Product_Category_Allocation_Resilient__c',
                'Product_Category_Allocation_Tile__c',
                'Product_Category_Allocation_Wood__c'
                };
                    
                    ltw.total = [ Select count() from Account_Profile__History WHERE Field IN : fieldName AND parentId =: recordId ];    
        List<dataWrapper> dataList = new List<dataWrapper>();
        
        for( Account_Profile__History APH : [ Select Field, OldValue, NewValue, CreatedDate,CreatedBy.Name from Account_Profile__History WHERE Field IN : fieldName AND parentId =: recordId ORDER BY CreatedDate desc] ){
            dataList.add( new dataWrapper( APH ) );  
        }
        ltw.sObjectrecords = dataList;
        return ltw;
    }
    
    public class lightningTableWrapper {
        @AuraEnabled
        public Integer page { get;set; }
        
        @AuraEnabled
        public Integer total { get;set; }
        
        @AuraEnabled
        public List<dataWrapper> sObjectrecords { get;set; }
    }
    
    public class dataWrapper {
        @AuraEnabled
        public string ChangeDate {get;set;}
        
        @AuraEnabled
        public string ChangeField {get;set;}
        
        @AuraEnabled
        public string UserName {get;set;}
        
        @AuraEnabled 
        public string OldValue {get;set;}
        
        @AuraEnabled
        public string NewValue {get;set;}
        
        public dataWrapper( Account_Profile__History AH ){
            Map<string,string> fieldLabel = fieldLabelMap();
            
            if( fieldLabel.get( AH.Field.toLowerCase() ) != 'primary business' ){
                this.ChangeDate = AH.createdDate.format('MM/dd/YYYY');
                this.UserName = AH.CreatedBy.Name;
                this.ChangeField = fieldLabel.get( AH.Field.toLowerCase() );
                this.OldValue = string.valueof( AH.OldValue );
                this.NewValue = string.valueof( AH.NewValue );
            }else{
                Map<string,string>APSOldMap = distributionInAccountProfileController.APSMap( string.valueof( AH.OldValue) );
                Map<string,string>APSNewMap = distributionInAccountProfileController.APSMap( string.valueof( AH.NewValue) );
                if( APSOldMap.get( string.valueof( AH.OldValue ) ) != null ){
                    this.ChangeDate = AH.createdDate.format('MM/dd/YYYY');
                    this.UserName = AH.CreatedBy.Name;
                    this.ChangeField = fieldLabel.get( AH.Field.toLowerCase() );
                    this.OldValue = APSOldMap.get( string.valueof( AH.OldValue ) );
                    this.NewValue = APSNewMap.get( string.valueof( AH.NewValue ) );
                }
            }
        }
    }
    
    public static Map<string,string> fieldLabelMap(){
        Map<string,string>returnMap = new Map<string,string>();
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType APHSchema = schemaMap.get('Account_Profile__c');
        Map<String, Schema.SObjectField> fieldMap = APHSchema.getDescribe().fields.getMap();
        for ( String fieldName: fieldMap.keySet() ) {
            returnMap.put( fieldName,fieldMap.get(fieldName).getDescribe().getLabel() );
            //system.debug(fieldName + ':::::::' + fieldMap.get(fieldName).getDescribe().getLabel());
        }        
        return returnMap;
    }
    
    public static Map<string,string> APSMap( string APSRecordId ){
        Map<string,string>returnMap = new Map<string,string>();
        for( Account_Profile_Settings__c APS : [ SELECT id,Name FROM Account_Profile_Settings__c WHERE id =: APSRecordId ] ){
            returnMap.put( APS.id,APS.Name );
        }
        return returnMap;
    }
    
    @AuraEnabled
    public static List<Picklist_Value__c> createAnnualSalesPickList(){
        return [ SELECT Key__c,Value__c FROM Picklist_Value__c ORDER BY Value__c ASC];
    } 
}