public class DisplayMATController {
   @AuraEnabled
    public static List<Mohawk_Account_Team__c> getMAT(STring accountProfileId){
        return [select id, DetailOverview__c, User__c, BMF_Team_Member__c, Product_Types__c, Brands__c, User_Name__c  from Mohawk_Account_Team__c
                       where account_profile__C =: accountProfileId and main_profile__c = true and user__c != :userinfo.getuserID()];
                
    }
}