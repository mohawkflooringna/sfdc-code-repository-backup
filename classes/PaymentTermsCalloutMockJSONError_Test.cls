/**************************************************************************

Name : PaymentTermsCalloutMockJSONError_Test

===========================================================================
Purpose :  the JSON format for OFREIGHTDATA is empty
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        14/Feb/2017     Created 
***************************************************************************/
@isTest
global class PaymentTermsCalloutMockJSONError_Test implements HttpCalloutMock{
    
    global HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setStatus('OK');
        res.setStatusCode(200);
      res.setBody('{'

              +'"MAIN": {'

                +'"IPACCOUNT": "R.018129.0000",'

                +'"OACCOUNTNAME": "J A Lotourneau & Fils Inc",'

                +'"ODATETIME": "*February 10 - 2017, 01:47pm EST*",'

                +'"OMESSAGE": null,'

                +'"OINFOMESSAGE": "If you have a question regarding Terms, please contact Mohawk Financial Services at (800) 427-4900.",'

                +'"OTERMSDATA": ['

                  +'{'

                    +'"TACTICALCAT": "Soft Surface",'

                    +'"PRODTYPEDESC": "Residential",'

                    +'"TERMSDESC": "5/30 N40",'

                    +'"NONSTDIND": "Y"'

                  +'},'

                  +'{'

                    +'"TACTICALCAT": "Hard Surface",'

                    +'"PRODTYPEDESC": "Mainstreet Commercial",'

                    +'"TERMSDESC": "5/30 N40",'

                    +'"NONSTDIND": "Y"'

                  +'},'

                  +'{'

                    +'"TACTICALCAT": "Soft Surface",'

                    +'"PRODTYPEDESC": "Carpet Tile",'

                    +'"TERMSDESC": "5/20 3/30 Net 40",'

                    +'"NONSTDIND": "N"'

                  +'},'

                  +'{'

                    +'"TACTICALCAT": "Cushion",'

                    +'"PRODTYPEDESC": "Image",'

                    +'"TERMSDESC": null,'

                    +'"NONSTDIND": "N"'

                  +'},'

                  +'{'

                    +'"TACTICALCAT": "Soft Surface",'

                    +'"PRODTYPEDESC": "Karastan Woven",'

                    +'"TERMSDESC": "Net 30 Days",'

                    +'"NONSTDIND": "N"'

                  +'},'

                  +'{'

                    +'"TACTICALCAT": "Soft Surface",'

                    +'"PRODTYPEDESC": "Karastan Tufted",'

                    +'"TERMSDESC": "5/30 N40",'

                    +'"NONSTDIND": "Y"'

                  +'},'

                  +'{'

                    +'"TACTICALCAT": "Soft Surface",'

                    +'"PRODTYPEDESC": "Helios",'

                    +'"TERMSDESC": "Net 30 Days",'

                    +'"NONSTDIND": "N"'

                  +'},'
                +']'

              +'}'

            +'}');
          
          return res;
    }
}