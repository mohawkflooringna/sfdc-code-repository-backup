/**************************************************************************

Name : AssignMATParentAccountToChildAcctsB_test
===========================================================================
Purpose : Test class for AssignMATParentAccountToChildAcctsBatch class 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha       10/March/2018    Create  
***************************************************************************/
@isTest(seeAllData=false)
private class AssignMATParentAccountToChildAcctsB_test {
    
    static testMethod void testMethod1() {
        list<Account> parentAccs = Utility_Test.createAccounts(true, 2);
        List<Territory__c>  terrList = Utility_Test.createTerritories(true, 2);
        
        list<Mohawk_Account_team__c> mohAccTeamList = Utility_Test.createMohawkAccountTeam(true, 2, parentAccs, terrList, Utility_Test.ADMIN_USER);
        list<Account> childAccs = Utility_Test.createResidentialAccountsForInvoicing(false, 2);
        
        integer i=0;
        for(Account a :childAccs){
            a.parentId = parentAccs[i].id;
            i++;
        }
        childAccs[0].parentId=parentAccs[1].id;
        insert childAccs;
        test.startTEST();
        
        AssignMATParentAccountToChildAcctsBatch rollupbatch = new AssignMATParentAccountToChildAcctsBatch();
        
        String jobId = Database.executeBatch(rollupbatch,200); 
        
        
        
        test.stoptest();

            
       /*
        delete mohAccTeamList[0];
        AssignMATParentAccountToChildAcctsBatch rollupbatch2 = new AssignMATParentAccountToChildAcctsBatch();
        
        String jobId1 = Database.executeBatch(rollupbatch2,200); 
        
        
        
     */


        }
    static testMethod void testMethod2() {
        list<Account> parentAccs = Utility_Test.createAccounts(true, 2);
        List<Territory__c>  terrList = Utility_Test.createTerritories(true, 2);
        
        list<Mohawk_Account_team__c> mohAccTeamList = Utility_Test.createMohawkAccountTeam(true, 2, parentAccs, terrList, Utility_Test.ADMIN_USER);
        list<Account> childAccs = Utility_Test.createResidentialAccountsForInvoicing(false, 2);
        system.assert(terrList!=null);
        system.assert(parentAccs!=null);

        integer i=0;
        for(Account a :childAccs){
            a.parentId = parentAccs[i].id;
            i++;
        }
        childAccs[0].parentId=parentAccs[1].id;
        insert childAccs;
        list<Mohawk_Account_team__c> childMohAccTeamList = Utility_Test.createMohawkAccountTeam(false, 2, childAccs, terrList, Utility_Test.ADMIN_USER);
        for(Mohawk_Account_team__c cm : childMohAccTeamList){
            cm.Parent_AT_Member__c=true;
        }
        insert childMohAccTeamList;
        system.assert(childAccs!=null);
delete mohAccTeamList[1];
        test.startTEST();
        
        AssignMATParentAccountToChildAcctsBatch rollupbatch = new AssignMATParentAccountToChildAcctsBatch();
        
        String jobId = Database.executeBatch(rollupbatch,200); 
        
        
        system.assert(jobId!=null);
        test.stoptest();

            
       /*
        delete mohAccTeamList[0];
        AssignMATParentAccountToChildAcctsBatch rollupbatch2 = new AssignMATParentAccountToChildAcctsBatch();
        
        String jobId1 = Database.executeBatch(rollupbatch2,200); 
        
        
        
     */


        }
    static testMethod void testMethod3() {
        list<Account> parentAccs = Utility_Test.createAccounts(true, 2);
        List<Territory__c>  terrList = Utility_Test.createTerritories(true, 2);
        
        list<Mohawk_Account_team__c> mohAccTeamList = Utility_Test.createMohawkAccountTeam(true, 2, parentAccs, terrList, Utility_Test.ADMIN_USER);
        list<Account> childAccs = Utility_Test.createResidentialAccountsForInvoicing(false, 2);
        system.assert(terrList!=null);
        system.assert(parentAccs!=null);

        integer i=0;
        for(Account a :childAccs){
            a.parentId = parentAccs[i].id;
            i++;
        }
        childAccs[0].parentId=parentAccs[1].id;
        insert childAccs;
        list<Mohawk_Account_team__c> childMohAccTeamList = Utility_Test.createMohawkAccountTeam(false, 2, childAccs, terrList, Utility_Test.ADMIN_USER);
        system.assert(childAccs!=null);
        childMohAccTeamList[1].Parent_AT_Member__c=true;
insert childMohAccTeamList;
        test.startTEST();
        
        AssignMATParentAccountToChildAcctsBatch rollupbatch = new AssignMATParentAccountToChildAcctsBatch();
        
        String jobId = Database.executeBatch(rollupbatch,200); 
        
        
        system.assert(jobId!=null);
        test.stoptest();

            
       /*
        delete mohAccTeamList[0];
        AssignMATParentAccountToChildAcctsBatch rollupbatch2 = new AssignMATParentAccountToChildAcctsBatch();
        
        String jobId1 = Database.executeBatch(rollupbatch2,200); 
        
        
        
     */


        }
}