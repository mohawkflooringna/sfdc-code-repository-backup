/**
 * Class Name: Rest_matDeleteFromCAMS
 * Author: Mudit Saxana - Mohawk
 * Date: Feb 22, 2018
 * Description: This class is used to delete the mat Delete from CAMS using API
 **/
@RestResource(urlMapping = '/v1/matDeleteFromCAMS/*')
global without sharing class Rest_matDeleteFromCAMS {

    @HttpPost
    // Http call delete
    global static response matDelete(Mohawk_Account_Team__c mat) {
        Response result = new response();

        String message= '';
        Boolean delflag = false;
        Boolean updflag = false;
        Mohawk_Account_Team__c oldmat;
        
        if (mat.External_Id__c != null) {
           
            list < Mohawk_Account_Team__c > matList = [Select Id, External_Id__c,User__c, RecordType.name, Territory_User_Number__c from Mohawk_Account_Team__c where External_Id__c =: mat.External_Id__c LIMIT 1];
            oldmat = (matList != null && matList.size() >= 1) ? matList[0] : null;
            result.success = result.success == null ? '' : result.success;
            
            if (oldmat == null) {
                result.success = 'false';
                message= 'mat is not found in Salesforce.';
            }
            
            // Commercial Mohawk Account Team
            if (oldmat != null && oldmat.RecordType.name == UtilityCls.COMMERCIAL) {
                delflag= true;
            }
            // Residential Mohawk Account Team
            if (oldmat != null && oldmat.RecordType.name != UtilityCls.COMMERCIAL) {

                if (oldmat.User__c != null) {
                    if (oldmat.Territory_User_Number__c != null) {
                        set < string > territoryUserNumberset = new set < string > ();
                        territoryUserNumberset.addAll(oldmat.Territory_User_Number__c.split(';'));

                        if (!territoryUserNumberset.contains(mat.Territory_User_Number__c)) {
                            result.success = 'false';
                            message= 'Territory User -' + mat.Territory_User_Number__c + '- doesnt match';
                        }
                
                        if (territoryUserNumberset.size() == 1 && result.success != 'false') {
                              oldmat.Territory_User_Number__c = '';
                              oldmat.Product_Types__c = '';
                              oldmat.Brands__c = '';
                              updflag = true;                                   
                          
                        } else if (territoryUserNumberset.size() > 1 && result.success != 'false') {

                            if (territoryUserNumberset.contains(mat.Territory_User_Number__c)) {
                                territoryUserNumberset.remove(mat.Territory_User_Number__c);
                                oldmat.Territory_User_Number__c = String.join(new List <String> (territoryUserNumberset), ';');
                                updflag = true;
                            }
                        }
                    } else {
                        result.success = 'false';
                        message = 'Territory User : ' + mat.Territory_User_Number__c + ' - doesnt match with mat';
                    }
                } else if (oldmat != null && oldmat.User__c == null) {
                    //Open position
                    delflag = true;
                }
            }
        } else {
            result.success = 'false';
            message= 'External Id not found.';
        }

        if (result.success != 'false' && (delflag || updflag)) {
            try {
                if (delflag && Schema.sObjectType.Mohawk_Account_Team__c.isDeletable()) {                  
                    delete oldmat;
                    result.message = 'CAMS Id - ' + mat.External_Id__c + ' - Deleted successfully.!';
                }
                if (updflag && Schema.sObjectType.Mohawk_Account_Team__c.isupdateable()) {
                    update oldmat;
                    result.message = ' CAMS Id - ' + mat.External_Id__c + ' - Updated successfully.!';
                }
                result.success = 'true';
                result.errorCode = 'success';
            } catch (dmlException ex) {
                result.success = 'false';
                result.message= 'CAMS id - ' + mat.External_Id__c + '- ' + ex.getMessage();
                result.errorCode = 'Error';
            }
        } else {
            result.message= 'CAMS id - ' + mat.External_Id__c + '- ' + message;
            result.errorCode = 'Error';
        }
        return result;

    }

    global class response {
        global String success;
        global String message;
        global string errorCode;
    }

}