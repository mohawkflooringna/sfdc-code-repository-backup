/**************************************************************************

Name : RetrievePaymentTerms_CC_Test 

===========================================================================
Purpose :   This class is used for RetrievePaymentTerms_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        13/Feb/2017     Created 
***************************************************************************/
@isTest
private class RetrievePaymentTerms_CC_Test { 
    static List<Account> accList;
    
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
        accList =  Utility_Test.createAccounts(true, 1);
    }
    
    static testMethod void testSendRequest() {
        init();
        
        for(Account ac : accList){
            ac.Global_Account_Number__c = 'R.018129.0000';
            ac.CAMS_Account_number__c='R.018129.0000';
        }
        
        UPDATE accList;
        
        test.startTest();
        system.assert(accList!=null);
        Test.setMock(HttpCalloutMock.class, new PaymentTermsCalloutMock_Test());
        System.assertNotEquals(RetrievePaymentTerms_CC.handlePaymentTerms(accList[0].id), null);
        RetrievePaymentTerms_CC.OtermsDataList childCls=new RetrievePaymentTerms_CC.OtermsDataList('Test1','Test2','Test3','Test4');
        RetrievePaymentTerms_CC.PaymentTermsViewModel chls=new RetrievePaymentTerms_CC.PaymentTermsViewModel();
        chls.iPAccount='Test';
        chls.oAccountName='Test';
        chls.oDateTime='Test';
        chls.oInformessage='Test';
        chls.oMessage='Test';
        List<RetrievePaymentTerms_CC.OtermsDataList> testLis=new list<RetrievePaymentTerms_CC.OtermsDataList>();
        chls.softSurfaceList=testLis;
        chls.cushionList=testLis;
        chls.hardsurfaceList=testLis;

        
        test.stopTest();
    }
    
    static testMethod void testWithWrongGlobalAccountNumber() {
        init(); 
        //update Global_Account_Number__c on account 
        for(Account ac : accList){
            ac.Global_Account_Number__c = 'R.0181';
        }
        
        UPDATE accList;
        
        test.startTest();
        system.assert(accList!=null);
        
        Test.setMock(HttpCalloutMock.class, new PaymentTermsCalloutMock_Test());
        System.assertNotEquals(RetrievePaymentTerms_CC.handlePaymentTerms(accList[0].id), null);
        test.stopTest();
    }
    
    static testMethod void testWithException() {
        init(); 
        //update Global_Account_Number__c on account 
        for(Account ac : accList){
            ac.Global_Account_Number__c = 'R.0181';
        }
        
        UPDATE accList;
        
        test.startTest();
        system.assert(accList!=null);
        
        System.assertNotEquals(RetrievePaymentTerms_CC.handlePaymentTerms(accList[0].id), null);
        test.stopTest();
    }
    
    static testMethod void testWithOutGlobalAccountNumber() {
        init();         
        
        test.startTest();
        system.assert(accList!=null);
        
        Test.setMock(HttpCalloutMock.class, new PaymentTermsCalloutMock_Test());
        System.assertNotEquals(RetrievePaymentTerms_CC.handlePaymentTerms(accList[0].id), null);
        test.stopTest();
    }
    
    static testMethod void testWithJSONError() {
        init();         
        
        test.startTest();
        system.assert(accList!=null);
        
        Test.setMock(HttpCalloutMock.class, new PaymentTermsCalloutMockJSONError_Test());
        System.assertNotEquals(RetrievePaymentTerms_CC.handlePaymentTerms(accList[0].id), null);
        test.stopTest();
    }
    
    static testMethod void testWithIdNull() {
        test.startTest();
        system.assert(accList==null);
        
        Test.setMock(HttpCalloutMock.class, new PaymentTermsCalloutMock_Test());
        System.assertNotEquals(RetrievePaymentTerms_CC.handlePaymentTerms(null), null);
        test.stopTest();
    }
    
    static testMethod void testWithDataEmpty() {
        init(); 
        //update Global_Account_Number__c on account 
        for(Account ac : accList){
            ac.Global_Account_Number__c = 'R.102318.0000';
        }
        
        UPDATE accList;
        
        test.startTest();
        system.assert(accList!=null);
        
        Test.setMock(HttpCalloutMock.class, new PaymentTermsCalloutMockDataEmpty_Test());
        System.assertNotEquals(RetrievePaymentTerms_CC.handlePaymentTerms(accList[0].id), null);
        test.stopTest();
    }
}