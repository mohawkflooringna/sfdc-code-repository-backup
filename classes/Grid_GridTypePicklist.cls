global class Grid_GridTypePicklist extends VisualEditor.DynamicPickList{
    
    global override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('Price Grid', 'Price_Grid');
        return defaultValue;
    }
    
    global override VisualEditor.DynamicPickListRows getValues() {
        VisualEditor.DynamicPickListRows  picklistValues = new VisualEditor.DynamicPickListRows();
        for(Grid_Type__mdt gridType : [SELECT Id, Grid_Type__c, DeveloperName FROM Grid_Type__mdt]){
            picklistValues.addRow(new VisualEditor.DataRow(gridType.Grid_Type__c, gridType.DeveloperName));
        }
        return picklistValues;
    }
}