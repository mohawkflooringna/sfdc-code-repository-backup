/**************************************************************************

Name : genericFormBuilderController_Test
DESC : This tess class is used for genericFormBuilderController
CreatedBy: Susmitha
Date: Aug 22,2018

***************************************************************************/
@isTest
private class genericFormBuilderController_Test {
    
    private static testmethod void testMethod1() {
        Test.startTest();
        System.runAs(Utility_Test.ADMIN_USER){
            genericFormBuilderController.getFields('Competitor_Products__c','Cushion');
        }
        Test.stopTest();
    }
}