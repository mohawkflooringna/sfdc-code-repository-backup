@isTest
public class LinkOpportunityController_Test {
    @isTest
    private static void LinkOpportunityControllerTest1()
    {
    LinkOpportunityController controller = new LinkOpportunityController() ;
        
        LinkOpportunityController loppCon = new LinkOpportunityController();
        loppCon.save();
        loppCon.onCancel();
        
    }
    
@isTest
private static void LinkOpportunityControllerTest2()
    {	
        account acc = new account();
        acc.Name = 'testAcc';
        insert acc;
        system.assertEquals('testAcc', acc.Name);
        
        opportunity opp = new opportunity();
        opp.Name = 'testOpp2';
        opp.CloseDate = Date.newInstance(2018, 17, 9);
        opp.CurrencyIsoCode = 'USD';
        opp.Location__c = 'USA';
        opp.Status__c = 'In Process';
        opp.AccountId = acc.Id;
        opp.StageName = 'Bidding';
        opp.Market_Segment__c = 'Government';
        insert opp;
        system.assertEquals('testOpp2', opp.Name);
        
        Test.StartTest();
        LinkOpportunityController loppCon = new LinkOpportunityController();
    	loppCon.Quote.OpportunityId = opp.ID;
        ApexPages.currentPage().getParameters().put('id', String.valueOf(opp.ID));
        loppCon.save();
        loppCon.onCancel();
        Test.StopTest();
    }


}