/**************************************************************************

Name : IncludeSALInMail_CC

===========================================================================
Purpose : Class for Appoinment send email along with Sales Action List based on flag available on appoinment object
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand          22/Dec/2016     Created
2.0        Anand          27/Dec/2016     Updated
3.0        Susmitha       27/Nov/2017     Updated
***************************************************************************/
public with sharing class IncludeSALInMail_CC {

    // only to get the Action List Record Types
    Public Static Id resSALRT;
    Public Static Id comSALRT;
    Public Static Id resFLRT;
    Public Static Id comFLRT;

    static {

        // Action List Residential Record type Ids
        resSALRT = UtilityCls.getRecordTypeInfo('Action_List__c', UtilityCls.RESIDENTIAL_SALES_ACTION_LIST);
        comSALRT = UtilityCls.getRecordTypeInfo('Action_List__c', UtilityCls.COMMERCIAL_SALES_ACTION_LIST);

        //Action List Fundamental Record type Ids
        resFLRT = UtilityCls.getRecordTypeInfo('Action_List__c', UtilityCls.RESIDENTIAL_FUNDAMENT_LIST);
        comFLRT = UtilityCls.getRecordTypeInfo('Action_List__c', UtilityCls.COMMERCIAL_FUNDAMENT_LIST);

    }

    @AuraEnabled
    public static String getEventContactEmail(String eventId) {// 05/08/2017
        list<String> eventConts = new list<string>();
        String eventEmail = '';

        for(EventRelation data : [SELECT id, Relationid FROM EventRelation WHERE iswhat=false and isparent=true and EventId =: eventId]) {
          eventConts.add(data.Relationid);
        }
        system.debug('eventConts' + eventConts);
        if(eventConts.size()>0){
          for(Contact data : [select id, name, email from contact where id in: eventConts ]){
            eventEmail += data.email + ',';
          }
          eventEmail = eventEmail.substring(0, eventEmail.length()-1);// remove the last ,
        }
        System.debug('eventEmail' + eventEmail);
        return eventEmail;
    }

    @AuraEnabled
    public static String setSubject(String eventId) {
        Event e = appoinmentData(eventId);
        return label.Meeting_with + ' ' + e.owner.Name + ' ' + label.From_Mohawk_Industries;
    }

    @AuraEnabled
    public static String setEmailBody(String eventId) {
        Event e = appoinmentData(eventId);
        String contactFName = '';
        if (e.whoId != null) {
            contactFName = [select Firstname FROM Contact WHERE Id = :e.whoid].FirstName ;
            contactFName = contactFName == null ? ' ' : contactFName + ',';
        }

        String body = label.Hello + ' ' + contactFName + '<br/><br/>' + label.Here_is_a_list_of_items;
        return body;
    }

    //Method to send email - General approach
    @AuraEnabled
    public static void sendMail(String mMail, String mSubject, String mbody, String attachmentString, String cc, String bcc) {

        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        // Step 2: Set list of people who should get the email
        List<String> sendTo = new List<String>();
        sendTo.add(mMail);
        sendTo = mMail.split(',');
        mail.setToAddresses(sendTo);


        if (cc != '') {
            List<String> sendCc = new List<String>();
            sendCc.add(cc);
            mail.setCcAddresses(sendCc);
        }

        if (bcc != '') {
            List<String> sendBcc = new List<String>();
            sendBcc.add(bcc);
            mail.setBccAddresses(sendBcc);
        }

        // Step 3: Set who the email is sent from
        mail.setReplyTo(Userinfo.getUserEmail());
        mail.setSenderDisplayName(Userinfo.getFirstName() + ' ' + Userinfo.getLastName());

        // Step 4. Set email contents - you can use variables!
        mail.setSubject(mSubject);
        mail.setHtmlBody(mbody);

        //Step 5. Add your email to the master list
        mails.add(mail);

        //Step 6: Send all emails in the master list
        Messaging.sendEmail(mails);
    }

    //Method to send email  - with Event object
    @AuraEnabled
    public static void sendMailMethod(String mMail, String mSubject, String mbody, String eventId, String attachmentString, String cc, String bcc) {

        Event evt = appoinmentData(eventId);
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        list<String> attachIdList = new list<String>();

        // Step 1: Create a new Email
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        // Step 2: Set list of people who should get the email
        List<String> sendTo = new List<String>();
        // sendTo.add(mMail);
        sendTo = mMail.split(',');
        mail.setToAddresses(sendTo);


        if (cc != '') {
            List<String> sendCc = new List<String>();
            sendCc.add(cc);
            mail.setCcAddresses(sendCc);
            
            //sendCc = cc.split(',');
        //mail.setCcAddresses(sendCc);
        }

        if (bcc != '') {
            List<String> sendBcc = new List<String>();
            //sendBcc.add(bcc);
            //mail.setBccAddresses(sendBcc);
            
            sendBcc = bcc.split(',');
        mail.setBccAddresses(sendBcc);
        }

        // Step 3: Set who the email is sent from
        mail.setReplyTo(Userinfo.getUserEmail());
        mail.setSenderDisplayName(Userinfo.getFirstName() + ' ' + Userinfo.getLastName());

        // Step 4. Set email contents - you can use variables!
        mail.setSubject(mSubject);
        mail.setHtmlBody(mbody);

      /*****************Attachment block - start*********************/
        //Send attachments only if inlclude attachments checkbox is checked
        if (attachmentString != '' && evt.Include_SAL_In_Mail__c == true) {
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();

            //Initialize email attachments with Selected Sales Action list's Files.
            for (ContentVersion file : salAttachments(eventId, '')) {
                if (attachmentString.contains(file.Id + '::')) {// allow attachment to mail if it is selected/not-removed in frontend
                    System.debug('sendMailMethod : Attachment : ' + file.Title);
                    String fileName = file.PathOnClient;//file.Title +'.'+ file.FileExtension; 
                    //If extention is snote then convert it as docx
                    if(file.FileExtension == 'snote'){
                          fileName =  file.Title +'.' + 'docx';
                    }
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    efa.setFileName(fileName);                  
                    efa.setBody(file.VersionData);
                    fileAttachments.add(efa);
                } 
            }

            mail.setFileAttachments(fileAttachments);
        }
      /*******************Attachment block - end************************/

        //Step 5. Add your email to the master list
        mails.add(mail);

        //Step 6: Send all emails in the master list
        Messaging.sendEmail(mails);
    }

    //Method to retrieve ContentVersions(Advanced files) related to Selected SALs. 
    @AuraEnabled
    Public static list<ContentVersion> salAttachments(String eventId, String fetch) {

        Event e = appoinmentData(eventId);
        Set<id> conDocIds = new Set<Id>();
        if (e.Id != null) {

            //Include attachments if 1)SAL Should not be private
            //2)(Yet to confirm)Residential Only - If Actin list record type should be Residential
            //Include attachments only if My_Appointment_List's Include_Attachments_In_Email__c is true
            for (ContentDocumentLink cdl : [
                    SELECT Id, ContentDocumentId
                    FROM ContentDocumentLink
                    WHERE LinkedEntityId in (
                            SELECT Action_List__c
                            FROM My_Appointment_List__c
                            WHERE Event_Action__c = :e.Event_Action__c
                            AND Action_List__r.Private__c = false
                            AND Include_Attachments_In_Email__c = true
                    )
            ]) {
                // only for reidential AND (Action_List__r.RecordTypeId =: resSALRT OR Action_List__r.RecordTypeId =:comSALRT)

                conDocIds.add(cdl.ContentDocumentId);
            }

            System.debug('@@@@@@conDocIds@@@@@@' + conDocIds);
        }//End of If

        List<ContentVersion> conVerList = new List<ContentVersion>();
        if (fetch != '' && fetch == 'id-name') {
            return [SELECT Id, Title,FileExtension, PathOnClient,FileType FROM ContentVersion where ContentDocumentId in :conDocIds];
        } else {
            return [SELECT Id, Title, VersionData ,PathOnClient,FileExtension, FileType FROM ContentVersion where ContentDocumentId in :conDocIds];
        }
    }


    @AuraEnabled
    Public static EmailTemplate dynamicTemplate(String templateId) {

        return [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate where Id = :templateId];
    }

    //Method to return My appointment list records based on Appoinment(Event) Include_SAL_In_Mail__c flag
    @AuraEnabled
    public static list<My_Appointment_List__c> sALItems(String eventId) {

        Event e = appoinmentData(eventId);

        //List<Event> e = (List<Event>)UtilityCls.getRecord('Event', eventId, 'Id,Event_Action__c,Include_SAL_In_Mail__c');

        if (e.Event_Action__c != null && e.Include_SAL_In_Mail__c == true) {


            return [
                    SELECT Id, Action_List__r.Name, Action_List__r.Description__c, Action_List__r.Customer_Group__c, Action_List__r.RecordType.Name
                    FROM My_Appointment_List__c
                    WHERE Event_Action__c = :e.Event_Action__c
                    AND (Action_List__r.RecordTypeId = :resSALRT OR Action_List__r.RecordTypeId = :comSALRT)
            ];
        } else {
            return null;
        }
    }//End of sALItems


    //Method to return My appointment list records based on Appoinment(Event) Include_SAL_In_Mail__c flag
    @AuraEnabled
    public static list<My_Appointment_List__c> fundametalItems(String eventId) {

        Event e = appoinmentData(eventId);

        if (e.Event_Action__c != null && e.Include_SAL_In_Mail__c == true) {

            return [
                    SELECT Id, Action_List__r.Name, Action_List__r.Description__c,
                            Action_List__r.Customer_Group__c, Action_List__r.RecordType.Name
                    FROM My_Appointment_List__c
                    WHERE Event_Action__c = :e.Event_Action__c
                    AND (Action_List__r.RecordTypeId = :resFLRT OR Action_List__r.RecordTypeId = :comFLRT)
            ];
        } else {
            return null;
        }
    }

    Class AppointmentResultHandler{
      @AuraEnabled
      Public Event eventData;
      @AuraEnabled
      Public List<Action_List__c> actionList;
      
    }

    /*@AuraEnabled
    public static String appointmentDataNew(String eventId) {// not in use

        Event eventData = appoinmentData(eventId);

        Set<Id> actionIds = new Set<Id>();
        List<My_Appointment_List__c> myAppList = [SELECT Id FROM My_Appointment_List__c WHERE Event_Action__c =: eventData.Event_Action__c];
        for(My_Appointment_List__c item : myAppList) {
            actionIds.add(item.Id);
        }

        List<Action_List__c> actionItems = [SELECT Id, Name FROM Action_List__c Where Id in: actionIds];

        AppointmentResultHandler finalResult = new AppointmentResultHandler();
        finalResult.eventData = eventData;
        finalResult.actionList = actionItems;

        return JSON.serialize(finalResult);
    }*/

    //Method to return My appointment list records based on Appoinment(Event) Include_SAL_In_Mail__c flag
    @AuraEnabled
    public static Event appoinmentData(String eventId) {

        Event eventData = [
                SELECT Id,
                        Subject,
                        whoId,
                        What.Name,
                        EndDateTime,
                        StartDateTime,
                        Event_Action__c,
                        Include_SAL_In_Mail__c,
                        Action_List_Items__c,
                        Objectives__c,
                        Description,
                        owner.Name
                FROM Event
                WHERE Id = :eventId
        ];

        Datetime startDate = eventData.StartDateTime;
        Datetime localStartDatetime = getLocalDateTime(eventData.StartDateTime);
        eventData.StartDateTime = localStartDatetime;
        Datetime localEndDateTime = getLocalDateTime(eventData.EndDateTime);
        eventData.EndDateTime = localEndDateTime;
        System.debug(eventData.StartDateTime);
//        System.debug(eventData);
        // return eventData;

        

        return eventData;

    }
    public static Datetime getLocalDateTime(Datetime z) {
        Datetime l = z.Date();
        l = l.addHours(z.hour());
        l = l.addMinutes(z.minute());
        l = l.addSeconds(z.second());

        return l;
    }
   
    


}