public class accountProfileCustomDetailController {
    public static final string AP_SECTION_OBJECT_NAME = 'AP_Section__mdt'; //CHANGE THIS AFTER
    public static final string AP_SECTION_FIELD_OBJECT_NAME = 'AP_Section_Fields__mdt';
    public static final List<string>AP_SECTION_OBJECT_FIELD_LIST = new List<string>{
        'MasterLabel','Always_Show__c',
		'Business_Type__c','Display_As__c',
		'ExpandableDefaultON__c','Parent_Section__c',
		'Section_Object__c','Sequence__c,Section_Type__c'
	};
                
	public static final List<string>AP_SECTION_FIELD_FIELD_LIST = new List<string>{
		'MasterLabel','Alignment__c',
		'AP_Section__c	','Field_Name__c',
		'isShow__c','Label_L__c','Label_M__c','Label_S__c',
		'Length__c','Sequence__c','Type__c'
	};
                            
	public static final string retationShipName = 'AP_Section_Fields1__r';
	public static final Map<string,string>sectionObjectMap = new Map<string,string>{
		'Account_Profile__c' => 'Account Profile',
		'Mohawk_Account_Team__c' => 'Mohawk Account Team'
	};
    
    @AuraEnabled
    public static List<string>getPickListValuesIntoList(){
        List<string>pickListValuesList = new List<string>();
        Schema.DescribeFieldResult fieldResult = AP_Section__mdt.Section_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList;
    }
    
    @AuraEnabled
    public static Account_Profile__c getAccountProfileDetail( string recordId ){
        return [ SELECT Id, Name,Multi_Channel__c,Primary_Business__r.Name FROM Account_Profile__c WHERE id =: recordId LIMIT 1];
    }
    
    @AuraEnabled
    public static Mohawk_Account_Team__c getMohawkAccountTeam( string recordId ){
        return [ SELECT Id, Name,Brands__c,Product_Types__c FROM Mohawk_Account_Team__c WHERE id =: recordId LIMIT 1];
    }
    
    /*********************************************************************
* This Method is use to get the Section Header and List of Fields.
* This Method is use in parent component. 
*********************************************************************/
    @AuraEnabled
    public static List<expendableSectionWrapper> createExpandableSections( string BusinessType,string sectionType,string device,string recordId  ){
        List<expendableSectionWrapper>expendableSectionWrapperList = new List<expendableSectionWrapper>();
        //PUTTING Mandatory SECTION
        //Identifier for Mandatory SECTION = Always_Show__c = true
		string sectionObject = sectionObjectMap.get( getObjectName( recordId ) );
		
		
        string mandatorySectionSoql = generateSOQLQuery( AP_SECTION_OBJECT_NAME,AP_SECTION_OBJECT_FIELD_LIST,'Section_Object__c =: sectionObject AND Always_Show__c = true',AP_SECTION_FIELD_FIELD_LIST,retationShipName,'' );
        for( AP_Section__mdt mandatorySection : database.query( mandatorySectionSoql ) ){        
            expendableSectionWrapper ESW = new expendableSectionWrapper();
            ESW.sectionSequence = string.valueof( mandatorySection.Sequence__c );
            ESW.sectionHeader = mandatorySection.MasterLabel;
            ESW.sectionDisplayType = mandatorySection.Display_As__c;
            ESW.isSectionDefaultOn = mandatorySection.ExpandableDefaultON__c;
            ESW.blockSectionContent = mandatorySection.AP_Section_Fields1__r;
            expendableSectionWrapperList.add( ESW );
        }
        //OTHER SECTIONS
        string whereCondition = 'Section_Object__c =: sectionObject';
        if( sectionObject == 'Account Profile' )
			whereCondition += ' AND Business_Type__c =: BusinessType';        
        //if( sectionObject == 'Mohawk Account Team' && BusinessType != null && BusinessType != '' )
            //whereCondition += ' AND Brand_Type__c =: BusinessType ';
		if( sectionType != null && sectionType != '' )
        	whereCondition += ' AND Section_Type__c =: sectionType';
        
        string otherSectionSoql = generateSOQLQuery( AP_SECTION_OBJECT_NAME,AP_SECTION_OBJECT_FIELD_LIST,whereCondition,AP_SECTION_FIELD_FIELD_LIST,retationShipName,'' ); 
        system.debug('***'+otherSectionSoql);    
        List<AP_Section__mdt>tableDataList = database.query( otherSectionSoql );
        system.debug('::::' + tableDataList);
        for( AP_Section__mdt otherSection : tableDataList ){
            if( otherSection.Parent_Section__c == null || otherSection.Parent_Section__c == '' ){
                expendableSectionWrapper ESW = new expendableSectionWrapper();	
                ESW.sectionSequence = string.valueof( otherSection.Sequence__c );
                ESW.sectionHeader = otherSection.MasterLabel;
                ESW.sectionDisplayType = otherSection.Display_As__c;
                ESW.isSectionDefaultOn = otherSection.ExpandableDefaultON__c;
                ESW.blockSectionContent = otherSection.AP_Section_Fields1__r;
                for( AP_Section__mdt childSection : tableDataList ){
                    if( childSection.Parent_Section__c == otherSection.MasterLabel ){
                        tableSectionContentWrapper TSCW = new tableSectionContentWrapper();
                        TSCW.listOfTotal = otherSection.AP_Section_Fields1__r;
                        TSCW.APSectionMasterLabel = childSection.MasterLabel;
                        TSCW.APSectionMasterSequence = childSection.Sequence__c;
                        TSCW.listOfFields = childSection.AP_Section_Fields1__r;
                        ESW.tableSectionContent.add( TSCW );
                    }
                }
                expendableSectionWrapperList.add( ESW );
            }
        }
        return expendableSectionWrapperList;
    }
    
    public class expendableSectionWrapper{
        @AuraEnabled
        public string sectionSequence;
        
        @AuraEnabled
        public string sectionHeader;
        
        @AuraEnabled
        public string sectionDisplayType;
        
        @AuraEnabled
        public boolean isSectionDefaultOn;
        
        @AuraEnabled
        public List<AP_Section_Fields__mdt>blockSectionContent;
        
        @AuraEnabled
        public List<tableSectionContentWrapper>tableSectionContent;
        
        public expendableSectionWrapper( ){
            this.blockSectionContent = new List<AP_Section_Fields__mdt>();
            this.tableSectionContent = new List<tableSectionContentWrapper>();
        }
    }
    
    public class tableSectionContentWrapper{
        @AuraEnabled
        public string APSectionMasterLabel;
        
        @AuraEnabled
        public decimal APSectionMasterSequence;
        
        @AuraEnabled
        public List<AP_Section_Fields__mdt>listOfTotal;
        
        @AuraEnabled
        public List<AP_Section_Fields__mdt>listOfFields;
        
        public tableSectionContentWrapper(){
            this.listOfTotal = new List<AP_Section_Fields__mdt>();
            this.listOfFields = new List<AP_Section_Fields__mdt>();
        }
    }
    
    @AuraEnabled
    public static List<blockSectionContentWrapper> createBlockSectionsContent( List<AP_Section_Fields__mdt>fields,string recordId ){
        List<blockSectionContentWrapper>contentList = new List<blockSectionContentWrapper>();
        if( fields != null && fields.size() > 0 ){
            List<string>fieldList = new List<string>();
            for( AP_Section_Fields__mdt APSF : fields ){
                if( APSF.Field_Name__c != null && APSF.Field_Name__c != '' )
                    fieldList.add( APSF.Field_Name__c );
            }
            Map<string,string>fetchData = sectionQuery( getObjectName( recordId ),fieldList,recordId );
            for( AP_Section_Fields__mdt APSF : fields ){
                contentList.add(
                    new blockSectionContentWrapper( APSF.Sequence__c,APSF.Label_L__c,APSF.Label_M__c,APSF.Label_S__c,fetchData.get( APSF.Field_Name__c ),APSF.Type__c )
                );
            }
            
        }
        return contentList;
    }
    
    public class blockSectionContentWrapper{
        @AuraEnabled
        public decimal fieldSequence;
        
        @AuraEnabled
        public string fieldLabel_L;
        
        @AuraEnabled
        public string fieldLabel_M;
        
        @AuraEnabled
        public string fieldLabel_S;
        
        @AuraEnabled
        public string fieldValue;
        
        @AuraEnabled
        public string fieldType;
        
        //FOR LOOKUP ONLY
        @AuraEnabled
        public string fieldID;
        
        public blockSectionContentWrapper( decimal fs,string fl_l,string fl_m,string fl_s,string fv,string ft ){
            this.fieldSequence = fs;
            this.fieldLabel_L = fl_l;
            this.fieldLabel_M = fl_m;
            this.fieldLabel_S = fl_s;
            this.fieldType = ft;
            if( ft == 'Lookup' ){
                this.fieldID = fv;
                this.fieldValue = getPrimaryBussinessName( fv );
            }else{
                this.fieldValue = (fv != null && fv != '' && fv.trim().length() > 0 ? fv : '0.00');
            }
        }
    }    
    
    @AuraEnabled
    public static tableWrapper createTableSectionsContent( string fieldsData,string recordId,string headerName ){
        List<Header> hdrs = new List<Header>();
        List<Row> rws = new List<Row>();
        tableWrapper gridData = new tableWrapper();
        
        List<string>totalFieldList = new List<string>();
        List<tableSectionContentWrapper> fields = (List<tableSectionContentWrapper>)JSON.deserialize(fieldsData,List<tableSectionContentWrapper>.class);
        
        if( fields != null && fields.size() > 0 ){
            if( headerName == 'Total Overview' || headerName == 'Overview' ){
                hdrs.add( new Header( 0,'Product Category','Product Category','Product Category' ) );    
            }else{
                hdrs.add( new Header( 0,'Brand Family','Brand Family','Brand Family' ) );
            }
            List<string>fieldList = new List<string>();
            List<string>duplicateHeaderCheck = new List<string>();
            
            for( tableSectionContentWrapper header : fields ){
                for( AP_Section_Fields__mdt fieldValue : header.listOfFields ){
                    if( !duplicateHeaderCheck.Contains( fieldValue.MasterLabel ) ){
                    	hdrs.add( new Header( fieldValue.Sequence__c,fieldValue.Label_L__c,fieldValue.Label_M__c,fieldValue.Label_S__c ) );
                        duplicateHeaderCheck.add( fieldValue.MasterLabel );
                    }
                    
					if( fieldValue.Field_Name__c != null && fieldValue.Field_Name__c != '' )
                        fieldList.add( fieldValue.Field_Name__c );
                }
                for( AP_Section_Fields__mdt totalFields : header.listOfTotal ){
                    if( !totalFieldList.Contains( totalFields.Field_Name__c ) )
                        totalFieldList.add( totalFields.Field_Name__c );
                }
            }
            
            Map<string,string>fetchData = sectionQuery( getObjectName( recordId ),fieldList,recordId );            
            for( tableSectionContentWrapper header : fields ){
                Row rw = New Row();                
                rw.cols = new List<Col>();
                rw.order = header.APSectionMasterSequence;
                rw.cols.add( new Col( 0,header.APSectionMasterLabel,hdrs[0].title_L,hdrs[0].title_M,hdrs[0].title_S,'Left','Text' ) );
                for( AP_Section_Fields__mdt fieldValue : header.listOfFields ){
                    rw.cols.add( new Col( fieldValue.Sequence__c,fetchData.get( fieldValue.Field_Name__c ),fieldValue.Label_L__c,fieldValue.Label_M__c,fieldValue.Label_S__c,fieldValue.Alignment__c,fieldValue.Type__c ) );  
                }
                rw.cols.sort();
                rws.add(rw);
            }
            
            //MAKING TOTAL ROW
            Row rw = New Row();
            rw.cols = new List<Col>();
			rw.order = fields.size() + 1;            
            rw.cols.add( new Col( 0,'Total','','','','Right','Text' ) );            
            Map<string,string>fetchTotalData = sectionQuery( getObjectName( recordId ),totalFieldList,recordId );
            List<string>duplicateTotalCheck = new List<string>();
            for( tableSectionContentWrapper totalRow : fields ){
                for( AP_Section_Fields__mdt totalCol : totalRow.listOfTotal ){
                    if( !duplicateTotalCheck.Contains( totalCol.Field_Name__c ) ){
                        rw.cols.add( new Col( totalCol.Sequence__c,fetchTotalData.get( totalCol.Field_Name__c ),totalCol.Label_L__c,totalCol.Label_M__c,totalCol.Label_S__c,totalCol.Alignment__c,totalCol.Type__c ) );
                        duplicateTotalCheck.add( totalCol.Field_Name__c );
                    }
                }    
            }
            rw.cols.sort();
            rws.add(rw);
            //MAKING TOTAL ROW
            
            
            gridData.rows = rws;
            gridData.headers = hdrs;
            gridData.headers.sort();
            gridData.rows.sort();
        }
        return gridData;
        
        
    }
	
    public class tableWrapper{
        @AuraEnabled
        public List<Header> headers;
        
        @AuraEnabled
        public List<Row> rows;
    }
    
    public class Header implements Comparable{
        @AuraEnabled
        public decimal sequence;
        @AuraEnabled
        public String title_L;
        @AuraEnabled
        public String title_M;
        @AuraEnabled
        public String title_S;
        
        public Header( decimal s, string tl,string tm,string ts ){
            this.sequence = s;
            this.title_L = tl;
            this.title_M = tm;
            this.title_S = ts;
        }
        
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(sequence - ((Header)objToCompare).sequence);               
        }
    }
    
    public class Row implements Comparable{
        @AuraEnabled
        public decimal order;
        @AuraEnabled
        public List<Col> cols;
        
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(order - ((Row)objToCompare).order);               
        }
    }
    
    public class Col implements Comparable{
        @AuraEnabled
        public decimal order;
        @AuraEnabled
        public String value;
        @AuraEnabled
        public String dataLabel_L;
        @AuraEnabled
        public String dataLabel_M;
        @AuraEnabled
        public String dataLabel_S;
        @AuraEnabled
        public String alignment;
        @AuraEnabled
        public String dataType;
        
        public Col( decimal o, string v,string dl_l,string dl_m,string dl_s,string al,string dt ){
            this.order = o;
            this.value = (v != null && v != '' && v.trim().length() > 0 ? v : '0');
            this.dataLabel_L = dl_l;
            this.dataLabel_M = dl_m;
            this.dataLabel_S = dl_s;
            this.alignment = al;
            this.dataType = dt;
        }
        
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(order - ((Col)objToCompare).order);               
        }
    }
    
	public static Map<string,string>sectionQuery( string objectName,List<string> fieldToQuery,string recordId ){
        Map<string,string>returnMap = new Map<string,string>();
        string soql = generateSOQLQuery( objectName,fieldToQuery,'id =: recordId',null,'','' );
        sObject AP = database.query( soql );
        for( string str : fieldToQuery ){
            returnMap.put( str,string.valueOf( AP.get( str ) ) );
        }
        return returnMap;
    }
    
    public static string generateSOQLQuery( string objectName,List<string>fields,string whereClause,List<string>childFieldList,string relationShipName,string childWhereClause ){
        string soql = 'SELECT ' + string.join(fields, ',');
        if( childFieldList != null && childFieldList.size() > 0 && relationShipName != null && relationShipName != '' ){
            soql += ',( SELECT ' + 
                string.join(childFieldList, ',') + 
                ' FROM ' + relationShipName + ( childWhereClause != null && childWhereClause != '' ? ' WHERE ' + childWhereClause : '') + ')';
        }
        soql += ' FROM ' + objectName;
        soql += ( whereClause != null && whereClause != '' ? ' WHERE '+whereClause : '' );
        
        return soql;
    }
    
    public static string getObjectName( id recordId ){
        return recordId.getSObjectType().getDescribe().getName();
    }
    
    
    
    public static string getPrimaryBussinessName( string recordId ){        
        return [ SELECT id,Name FROM Account_Profile_Settings__c WHERE id =: recordId ].Name;
    }
    
}