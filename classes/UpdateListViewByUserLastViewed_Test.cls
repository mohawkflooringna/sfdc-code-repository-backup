@isTest(seealldata = true)
public class UpdateListViewByUserLastViewed_Test {
    public static String sobjTypes = System.Label.SObjects_For_ListViews;
    public static List<String> sObjTypesList = sobjTypes.split(';');
    public static Integer userCount = 0;
    public static Integer offset = 0;
    public static List<User> totalUserList = Database.query(System.Label.List_View_SOQL_Query);
    public static Integer totalUserCount = totalUserList.size();
    
    public static testMethod void viewList12(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
    
    public static testMethod void viewList(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95 OFFSET 95';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
    
    public static testMethod void viewList1(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95 OFFSET 190';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
    
    public static testMethod void viewList2(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95 OFFSET 285';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
    
    public static testMethod void viewList3(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95 OFFSET 380';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
    
    public static testMethod void viewList4(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95 OFFSET 475';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
    
    
    public static testMethod void viewList5(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95 OFFSET 570';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
    
    public static testMethod void viewList6(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95 OFFSET 665';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
    
    public static testMethod void viewList7(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95 OFFSET 760';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
    
    public static testMethod void viewList8(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95 OFFSET 855';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
    
    public static testMethod void viewList9(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95 OFFSET 950';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
    
    public static testMethod void viewList10(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95 OFFSET 1045';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
    
    public static testMethod void viewList11(){
        Test.startTest();
        String query = System.Label.List_View_SOQL_Query + ' LIMIT 95 OFFSET 1140';
        List<User> userList = Database.query(query);
        userCount += userList.size();
        offset = userCount;
        for(User usr: userList){
            System.runAs(usr){
                for(List<ListView> listViews : [SELECT Id, Name FROM ListView WHERE SobjectType IN: sobjTypesList FOR VIEW]){
                }
            } 
        }
        Test.stopTest();
    }
}