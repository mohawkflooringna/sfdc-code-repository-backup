/***************************************************************************

Name : cloneQuickActionController 

===========================================================================
Purpose :  Controller for cloneQuickAction lightning component
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha      06/Dec/2017     Created 
2.0        Lakshman      11/Dec/2017     Modified
***************************************************************************/
public class cloneQuickActionController 
{
    //List of quotes and List of accounts
   public class accountAndQuoteWrapper
    {
        @AuraEnabled
        public List <Account> accList;
        @AuraEnabled
        public List <Quote> quoteList;
        @AuraEnabled
        public Quote quote;
        @AuraEnabled
        public String message;
        @AuraEnabled
        public Boolean isError;
    }
    
    
    /*Method which return wrapper class which holds list of accounts,quotes and quote*/    
    @AuraEnabled
    public static accountAndQuoteWrapper getDealerAndQuotes(String projectId, String quoteId) 
    {
        accountAndQuoteWrapper accWrapper = new accountAndQuoteWrapper();
        
         if(UtilityCls.isStrNotNull(projectId))
         {
             accWrapper.accList = getAccounts(projectId);
             accWrapper.quoteList = getQuotes(projectId);
             accWrapper.isError = false;
             
         }else if(UtilityCls.isStrNotNull(quoteId))
         {
             Quote quote = getQuote(quoteId);
             accWrapper.quote = quote;
             accWrapper.accList = getAccounts(quote.opportunityId);
             accWrapper.isError = false;
             
         }else
         {
             accWrapper.isError = true;
             accWrapper.message = 'Please give valid ids';
         }
             
        
        return accWrapper;
    }
    
    /*Method to return list of Accounts(Project related accounts)*/
    private static List < Account > getAccounts(String projectId) {
        system.debug('projectid'+projectId);
        if (projectId != null) {
            set<Id> AccId =new set<Id>();
            List < Related_Account__c > relatedAccList = [SELECT ID, Account__c, Opportunity__c FROM Related_Account__c WHERE Opportunity__c =: projectId];
            for(Related_Account__c relAcc :relatedAccList){
                AccId.add(relAcc.Account__c); 
            }
            List < Account > accountList = [SELECT Name, Id,Global_Account_Number__c FROM Account where id in:AccId];
            
            return accountList;
        } else {
            return null;
        }
        
    }
    
    /*Method to return list of quotes of particular project*/
    private static List < Quote > getQuotes(string projectId) {
        List < Quote > QuoteList = [SELECT Name, Id,External_Quote_Id__c,QuoteNumber,IsSyncing,ExpirationDate, opportunityId, Subtotal , TotalPrice, CreatedBy.Name FROM quote where opportunityId =: projectId limit 1000];
        
        return QuoteList;
        
    }
    /*Method to return Quote when quoteId is passed*/
    public static Quote getQuote(string quoteId) {
        if (quoteId != null) {
            Quote qte = [SELECT Name, Id,opportunityId,QuoteNumber,IsSyncing,ExpirationDate,External_Quote_Id__c FROM Quote where id =: quoteId limit 1];
            system.debug('opp id'+qte.opportunityId);
            return qte;
        } else
            return null;
        
    }
}