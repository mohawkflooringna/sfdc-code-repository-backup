@isTest
private class Quotes_CC_Test {
    public static List<Account> accList = new List<Account>();
    public static List<Opportunity> oppList = new List<Opportunity>();
    public static Id quoteId;
    public static String oppId = '';
    public static List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
    public static List<QuoteLineItem> quoteLineItemList = new List<QuoteLineItem>();
    public static List<Related_Account__c> relAccList = new List<Related_Account__c>();
    public static List<PriceBookEntry> pbEntryList = new List<PriceBookEntry>();
    public static Default_Configuration__c defConfig = Utility_Test.getDefaultConfCusSetting();
    public static PriceBook2 pb = new PriceBook2();
    public static Map<Id, List<Product2>> prodAccMap = new Map<Id, List<Product2>>();
    public static List<User> userList = new List<User>();
    
    @isTest
    public static void init(){
        
        /*List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
List<QuoteLineItem> quoteLineItemList = new List<QuoteLineItem>();
List<Related_Account__c> relAccList = new List<Related_Account__c>();
List<PriceBookEntry> pbEntryList = new List<PriceBookEntry>(); */
        //Default_Configuration__c defConfig = Utility_Test.getDefaultConfCusSetting();
        
        
        //String pbId = Test.getStandardPricebookId();
        String contId = '';
        String eUContId = '';
        userList = Utility_Test.createTestUsers(false, 1, 'Commercial Sales User');
        userList[0].Business_Type__c = UtilityCls.COMMERCIAL;
        insert userList;
        
        defConfig.Fire_Account_Trigger__c = false;
        defConfig.Fire_Opportunity_Trigger__c = false;
        defConfig.Fire_RelatedAccount_Trigger__c = false;
        defConfig.Quote_Trigger__c = true;
        update defConfig;
        
        accList = Utility_Test.createAccounts(false, 2);
        accList[0].Business_Type__c = UtilityCls.COMMERCIAL;
        accList[0].RecordTypeId = UtilityCls.getRecordTypeInfo('Account', 'Non-Invoicing');
        accList[0].OwnerId = userList[0].Id;
        accList[0].Role_C__c = 'End User';
        accList[1].OwnerId = userList[0].Id;
        accList[1].Strategic_Account__c = true;
        accList[1].Business_Type__c = UtilityCls.COMMERCIAL;
        accList[1].Role_C__c = 'Dealer';
        accList[1].RecordTypeId = UtilityCls.getRecordTypeInfo('Account', 'Invoicing');
        insert accList;
        
        List<Contact> contactList = Utility_Test.createContacts(true, 2, accList);
        for(Contact cont: contactList){
            if(cont.AccountId == accList[1].Id){
                //cont.AccountId = accList[1].Id;
                contId = cont.Id;
                cont.OwnerId = userList[0].Id;
            }else{
                eUContId = cont.Id;
            }
        }
        update contactList;
        
        
        pb.Name = 'Standard Price Book';
        insert pb;
        
        oppList = Utility_Test.createOpportunities(true, 2, accList);
        for(Opportunity opp: oppList){
            opp.RecordTypeId = UtilityCls.getRecordTypeInfo('Opportunity', 'Commercial Traditional');
            opp.StageName = 'Discovery';
            opp.Contact__c = contId;
            if(opp.AccountId == accList[0].Id){
                oppId = opp.Id;
                opp.Pricebook2Id = pb.Id;
                opp.Contact__c = eUContId;
                //opp.StageName = 'Discovery';
                opp.OwnerId = userList[0].Id;
                opp.Market_Segment__c = 'Healthcare';
                opp.Location__c = 'Atlanta';
            }
            System.debug('Opp :' + opp);
        }
        System.debug('OppId' + oppId);
        update oppList;
        
        List<Product2> Prod2List = Utility_Test.createProduct2(true, 3);
        List<Product_Relationship__c> prodRelList = new List<Product_Relationship__c>();
        
        List<Product2> prodList = new List<Product2>();
        Id prodId = Prod2List[0].Id;
        
        for(Integer i=0;i<Prod2List.size();i++){
            OpportunityLineItem oppLI = new OpportunityLineItem();
            QuoteLineItem quoteLI = new QuoteLineItem();
            Product2 prod = new Product2();
            PriceBookEntry pbEntry = new PriceBookEntry();
            if(i == 0){
                Prod2List[i].RecordTypeId = UtilityCls.getRecordTypeInfo('Product2', 'Commercial Product');
                Prod2List[i].Sub_Category__c = 'Modern Tile';
                //Prod2List[i].PriceBookId = SPriceBook;
                oppLi.OpportunityId = oppId;
                //oppLi.PricebookEntryId = SPriceBook;
                quoteLI.Product2Id = oppLi.Product2Id = Prod2List[i].Id;
                quoteLI.Quantity = oppLi.Quantity = 10.00;
                quoteLI.UnitPrice = oppLi.UnitPrice = 10.00;
                oppLineItemList.add(oppLI);
                quoteLineItemList.add(quoteLI);
            }else{
                Prod2List[i].RecordTypeId = UtilityCls.getRecordTypeInfo('Product2', 'Commercial Product Accessory');
                //Prod2List[i].Sub_Category__c = 'Modern Tile';
                Product_Relationship__c prodRel = new Product_Relationship__c();
                prodRel.Source_Product_Unique_Id__c = prodRel.Product__c = prodId;
                prodRel.Target_Product_Unique_Id__c = prodRel.Related_Product__c = Prod2List[i].Id;
                prodRel.Commercial_Accessory__c = true;
                prodRelList.add(prodRel);
                prod.Name = Prod2List[i].Name;
                prod.Id = Prod2List[i].Id;
                prodList.add(prod);
            }
            pbEntry.Product2Id = Prod2List[i].Id;
            pbEntry.Pricebook2Id = pb.Id;
            pbEntry.UnitPrice = 1.00;
            pbEntry.IsActive = true;
            pbEntryList.add(pbEntry);
        }
        update Prod2List;
        insert prodRelList;
        insert pbEntryList;
        insert oppLineItemList;
        prodAccMap.put(prodId, prodList);
        
        for(Account acc: accList){
            Related_Account__c relAcc = new Related_Account__c();
            relAcc.Opportunity__c = oppId;
            relAcc.Account__c = acc.Id;
            relAcc.RecordTypeId = UtilityCls.getRecordTypeInfo('Related_Account__c', 'Commercial');
            relAcc.Project_Role__c = acc.Role_C__c;
            relAccList.add(relAcc);
        }
        
        insert relAccList;
    }
    @isTest
    public static void methodForGetOppInfo(){
        init();
        Quotes_CC.getOppInfo(oppId);
    }
    
    @isTest
    public static void methodForCreateQuote(){
        init();
        Date expDate = Date.today();
        Quote quote = new Quote();
        Quotes_CC.QuoteInfo quoteInfo = new Quotes_CC.QuoteInfo();
        String jsonString = '';
        
        quoteInfo.quote.OpportunityId = null;
        quote.Market_Segment__c = 'Healthcare';
        quote.Job_Location__c = 'Atlanta';
        quote.Dealer__c = accList[1].Id;
        quote.End_User__c = accList[0].Id;
        quote.ExpirationDate = expDate.addDays(30);
        quote.RecordTypeId = UtilityCls.getRecordTypeInfo('Quote', 'Commercial');
        quote.Pricebook2Id = pb.Id;
        quoteInfo.quote = quote;
        
        quoteInfo.productAccessoryMap = prodAccMap;
        quoteInfo.fieldValuesMap.put('Stage', 'Discovery');
        quoteInfo.fieldValuesMap.put('Quoted', 'false');
        jsonString = JSON.serialize(quoteInfo);
        //String jsonString = '{"quote":'+quote+',"quoteLineItems":'+quoteLineItemList+'}';
        //With Line Items
        for(QuoteLineItem qli: quoteLineItemList){
            for(PriceBookEntry pbe: pbEntryList){
                if(qli.Product2Id == pbe.Product2Id){
                    qli.PriceBookEntryId = pbe.Id;
                }
            }
        }
        quoteInfo.quoteLineItems = quoteLineItemList;
        Quotes_CC.saveQuote(jsonString);
        
        quoteInfo.quote.OpportunityId = oppId;
        jsonString = JSON.serialize(quoteInfo);
        quoteInfo = Quotes_CC.saveQuote(jsonString);
        quoteId = quoteInfo.quote.Id;

        /*if(quoteInfo.quote.Id != null){
            Quotes_CC.createPDF(quoteInfo.quote.Id);
        }*/
        
        
        
        userList[0].CountryCode = 'CA';
        update userList;
        User usr = userList[0];
        System.runAs(usr){
            Quotes_CC.getOppInfo(oppId);
        }
        
        /*for(Opportunity opp: oppList){
            opp.StageName = 'Closed Lost';
            opp.Status__c = 'Closed';
        }
        update oppList;
        Test.startTest();
        methodforCreatePDF(oppId, quoteId);
        Test.stopTest();*/
    }
    
    @isTest
    public static void methodforExpiredQuote(){
        init();
        Quote q = new Quote();
        q.Market_Segment__c = 'Healthcare';
        q.Job_Location__c = 'Atlanta';
        q.Dealer__c = accList[1].Id;
        q.End_User__c = accList[0].Id;
        q.ExpirationDate = Date.today() - 1;
        q.RecordTypeId = UtilityCls.getRecordTypeInfo('Quote', 'Commercial');
        q.Pricebook2Id = pb.Id;
        q.OpportunityId = oppId;
        q.Status = 'Expired';
        q.Name = 'quote1';
        insert q;
        if(q.Id != null){
            Quotes_CC.createPDF(q.Id);
        }
        
        Opportunity opp = new Opportunity();
        opp.Id = oppId;
        opp.StageName = 'Closed Lost';
        opp.Status__c = 'Closed';
        update oppList;
        
        methodforCreatePDF(oppId, q.Id);
        
    }
    public static void methodforCreatePDF(Id oppId, Id quoteId){
        Quotes_CC.getOppInfo(oppId);
        if(quoteId != null){
            Quotes_CC.createPDF(quoteId);
        } 
    }
}