@isTest
public class DataConvertHelperTest {
    
    public static testMethod void testDataConvertHelper1 (){
        DataConvertHelper.mapContactFields(null,'','');
        Account acc = new Account();
        acc.Name = 'testac';
        insert acc;
        DataConvertHelper.mapContactFields(new rdcc__Insight_Contact__c(rdcc__Mailing_Country__c='XYZ',rdcc__Mailing_State__c='XYZ'),acc.id,'Yes');
        DataConvertHelper.mapContactFields(new rdcc__Insight_Contact__c(rdcc__Mailing_Country__c='India',rdcc__Mailing_State__c='Delhi'),acc.id,'No');
        DataConvertHelper.getMappedContactWithMandatoryFields(new rdcc__Insight_Contact__c(rdcc__Mailing_Country__c='India',rdcc__Mailing_State__c='Delhi'),acc.id);
    }
    
    @isTest public static void filledMandatoryFieldsTest(){
        //Creating a field type map to send to the function
        Map<String,String> fieldApiNmTypeMap = new Map<String,String>();
        Map<string,List<rdcc__ProjectContactRole__c>> conRoleMap = new Map<string,List<rdcc__ProjectContactRole__c>>();
        List<rdcc__ProjectContactRole__c> pcr = new List<rdcc__ProjectContactRole__c>();
        Map<String, Contact> insConIdConMap = new Map<String, Contact>();
        
        rdcc__Insight_Company__c insightCompany=new rdcc__Insight_Company__c();
        insightCompany.name='InsightCompanySample';
        insightCompany.rdcc__Company_ID__c='001';
        insert insightCompany;
        System.assertEquals('001', insightCompany.rdcc__Company_ID__c);
        
        rdcc__Insight_Contact__c contact = new rdcc__Insight_Contact__c();
        contact.rdcc__Contact_ID__c = '23132131';
        contact.Name = 'test';
        contact.rdcc__Last_Name__c = 'test';
        contact.rdcc__CMD_Comapany_Id__c = '001';
        contact.rdcc__Insight_Company__c  = insightCompany.id;
        insert contact;
        System.assertEquals('001', contact.rdcc__CMD_Comapany_Id__c);
        
        rdcc__ProjectContactRole__c prjconrl = new rdcc__ProjectContactRole__c();
        prjconrl.rdcc__Insight_Contact__c = contact.ID;
        insert prjconrl;
        pcr.add(prjconrl);
        conRoleMap.put(contact.rdcc__Contact_ID__c,pcr);
        
        /*for(Schema.SObjectField objField:UtilityClass.getFieldMap('Contact').values()){
            Schema.DescribeFieldResult dfr = objField.getDescribe();
            fieldApiNmTypeMap.put(dfr.getName(), dfr.getType().name());
        }*/
        
        Map<String,Schema.SObjectField> fieldMap = UtilityClass.getFieldMap('Contact');
        fieldApiNmTypeMap.put(fieldMap.get('Title').getDescribe().getName(), fieldMap.get('Title').getDescribe().getType().name());
        fieldApiNmTypeMap.put(fieldMap.get('LeadSource').getDescribe().getName(), fieldMap.get('LeadSource').getDescribe().getType().name());
        fieldApiNmTypeMap.put(fieldMap.get('Email').getDescribe().getName(), fieldMap.get('Email').getDescribe().getType().name());
        fieldApiNmTypeMap.put(fieldMap.get('MailingAddress').getDescribe().getName(), fieldMap.get('MailingAddress').getDescribe().getType().name());
        //Creating a record to be updated
        Contact c = new Contact();
        System.assertEquals(c.IsDeleted,false);
        System.assertEquals(c,c); // added on 8/6/2018
        //Updating object type
        DataConvertHelper.objectType = 'Contact';
        DataConvertHelper.filledMandatoryFields(c, fieldApiNmTypeMap);
        
        Contact con = new Contact();
        con.LastName = 'testConLName';
        con.Email = 'tst@test.com';
        con.rdcc__Contact_ID__c = '23132131';
        insert con;
        System.assertEquals('testConLName',con.LastName);
        
        insConIdConMap.put(con.rdcc__Contact_ID__c, con);
        DataConvertHelper.updateConRole(conRoleMap, insConIdConMap);
        
        // try
        
        
    }
    
     @isTest public static void testConvertAccount(){
        //Creating Config Settings Object
        Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Residential').getRecordTypeId(); 
        rdcc__Configurable_Settings__c configSett = new rdcc__Configurable_Settings__c();
        configSett.rdcc__Allow_Future_ConstructConnect_Updates__c = 'Yes';
        configSett.rdcc__Automated_SF_Account_Creation__c = 'Yes';
        configSett.rdcc__Allow_Future_Updates_to_Account_Contact__c = 'Yes';
        configSett.rdcc__Allow_Future_Updates_to_Account_Contact__c = 'No';
        configSett.rdcc__Contact_Record_Type__c = RecordTypeIdContact;
        /*for (id recTypeId: UtilityClass.getAllowedRecordTypeIds('Contact')){
            configSett.rdcc__Contact_Record_Type__c = recTypeId;
            break;
        }
        for (id recTypeId: UtilityClass.getAllowedRecordTypeIds('Account')){
            configSett.rdcc__Account_Record_Type__c = recTypeId;
            break;
        }*/
        configSett.rdcc__Allow_Future_Updates_to_Account_Contact__c = 'Yes';
        insert configSett;
        rdcc__Configurable_Settings__c check = [Select Id, IsDeleted from rdcc__Configurable_Settings__c WHERE Id = :configSett.Id ALL ROWS];  // added on 7/10/2018
        System.assertEquals(false, check.IsDeleted);  // added on 7/10/2018
        
        rdcc__ReedProject__c prj=new rdcc__ReedProject__c();
        prj.name='TestProject11';
        prj.rdcc__Project_ID__c='002';
        prj.rdcc__Stage__c='Planning';
        insert prj;
        System.assertEquals('002', prj.rdcc__Project_ID__c);  // added on 7/10/2018
        
        rdcc__Insight_Company__c insightCompany=new rdcc__Insight_Company__c();
        insightCompany.name='InsightCompanySample';
        insightCompany.rdcc__Company_ID__c='001';
        insert insightCompany;
        System.assertEquals('001', insightCompany.rdcc__Company_ID__c);  // added on 7/10/2018
        
        rdcc__ProjectCompanyRole__c compRole=new rdcc__ProjectCompanyRole__c();
        compRole.rdcc__CMD_Comapany_Id__c= insightCompany.rdcc__Company_ID__c;
        compRole.rdcc__Project__c=prj.Id;
        insert compRole;
        rdcc__ProjectCompanyRole__c compRolecheck = [Select Id, IsDeleted from rdcc__ProjectCompanyRole__c WHERE Id = :compRole.Id ALL ROWS];
        System.assertEquals(false, compRolecheck.IsDeleted);  // added on 7/10/2018
        
        rdcc__Bid__c bid = new rdcc__Bid__c();
        
        bid.rdcc__Project__c = prj.Id;
        bid.rdcc__Insight_Company__c = insightCompany.Id;
        insert bid;
        rdcc__Bid__c bidCheck = [Select Id, IsDeleted from rdcc__Bid__c WHERE Id = :bid.Id ALL ROWS];
        System.assertEquals(false, bidCheck.IsDeleted);  // added on 7/10/2018
        
        rdcc__AccountClassification__c classIf= new rdcc__AccountClassification__c();
       
        classIf.rdcc__Insight_Company__c =  insightCompany.Id;
        insert classIf;
        rdcc__AccountClassification__c classIfCheck = [Select Id, IsDeleted from rdcc__AccountClassification__c WHERE Id = :classIf.Id ALL ROWS];
        System.assertEquals(false, classIfCheck.IsDeleted);  // added on 7/10/2018
        
        rdcc__Insight_Contact__c contact = new rdcc__Insight_Contact__c();
        contact.rdcc__Contact_ID__c = '23132131';
        contact.Name = 'test';
        contact.rdcc__Last_Name__c = 'test';
        contact.rdcc__CMD_Comapany_Id__c = '001';
        contact.rdcc__Insight_Company__c  = insightCompany.id;
        insert contact;
        System.assertEquals('001', contact.rdcc__CMD_Comapany_Id__c);  // added on 7/10/2018
        
        Account acc = new Account();
        acc.Name = 'testac';
        insert acc;
        
        //Contact c = DataConvertHelper.mapContactFields(contact,acc.ID, 'Yes');
        //insert c;
        //Contact conCheck = [Select Id, IsDeleted from Contact WHERE Id = :c.Id ALL ROWS];  // added on 7/10/2018
        //System.assertEquals(false, conCheck.IsDeleted);  // added on 7/10/2018
        
        /*Contact c = DataConvertHelper.mapContactFields(contact, a.Id, 'Yes');
        insert c;
        Contact conCheck = [Select Id, IsDeleted, AccountId from Contact WHERE Id = :c.Id ALL ROWS];  // added on 7/10/2018
        System.assertEquals(false, conCheck.IsDeleted);  // added on 7/10/2018
        System.assertEquals(a.Id, conCheck.AccountId);  // added on 7/10/2018*/
        
        //Getting Mapped fields for Lead and Opportunity
        Lead l = new Lead();
        //DataConvertHelper.getMappedLeadWithMandatoryFields(l);
        
        Opportunity op = new Opportunity();
        //DataConvertHelper.getMappedOpportunityWithMandatoryFields(op);
        DataConvertHelper.checkForPrimaryContact = true; // added on 7/18/2018
        
        Map<String,String> compIdAccountIdMap = new  Map<String,String>();        
        compIdAccountIdMap.put('001', acc.Id);
        DataConvertHelper.afterCompanyConversion(new List<rdcc__Insight_Company__c>([select id,rdcc__Is_Converted__c,rdcc__Last_Updated__c,name,rdcc__Company_ID__c,rdcc__Fax__c,rdcc__Parent_Role__c,rdcc__Phone__c,rdcc__Role__c,rdcc__Shipping_City__c,rdcc__Shipping_Country__c,rdcc__Shipping_County__c,rdcc__Shipping_Postal_Code__c,rdcc__Shipping_State__c,rdcc__Shipping_Street__c,rdcc__Type__c,rdcc__URL__c ,(select id,rdcc__CMD_Comapany_Id__c,rdcc__Account__c,rdcc__Insight_Company__c from rdcc__Company_Roles__r),(select id,rdcc__CMD_Comapany_Id__c,rdcc__Insight_Company__c,rdcc__Account__c from rdcc__Insight_Classifications__r),(select id,rdcc__From__c,rdcc__CMD_Comapany_Id__c,rdcc__Insight_Company__c from rdcc__Insight_Bids__r),(select id,rdcc__CMD_Comapany_Id__c,rdcc__Contact_ID__c,rdcc__Email__c,rdcc__Fax__c,rdcc__First_Name__c,rdcc__Insight_Company__c,rdcc__Insight_Company__r.name,rdcc__Is_Company_Converted__c,rdcc__Last_Name__c,rdcc__Lead_Source__c,rdcc__Linked_In_Profile__c,rdcc__Mailing_City__c,rdcc__Mailing_Country__c,rdcc__Mailing_Postal_Code__c,rdcc__Mailing_State__c,rdcc__Mailing_Street__c,rdcc__Phone__c,rdcc__Type__c,rdcc__URL__c from rdcc__Insight_Contacts__r) from rdcc__Insight_Company__c where Id =:insightCompany.id]), compIdAccountIdMap);
        
        // new 
        Map<String,String> fieldApiNmTypeMap = new Map<String,String>();
        Map<string,List<rdcc__ProjectContactRole__c>> conRoleMap = new Map<string,List<rdcc__ProjectContactRole__c>>();
        List<rdcc__ProjectContactRole__c> pcr = new List<rdcc__ProjectContactRole__c>();
        Map<String, Contact> insConIdConMap = new Map<String, Contact>();
        
        rdcc__Insight_Company__c insightCompany1=new rdcc__Insight_Company__c();
        insightCompany1.name='InsightCompanySample454';
        insightCompany1.rdcc__Company_ID__c='0017';
        insert insightCompany1;
        
        rdcc__Insight_Contact__c contact1 = new rdcc__Insight_Contact__c();
        contact1.rdcc__Contact_ID__c = '27832131';
        contact1.Name = 'test';
        contact1.rdcc__Last_Name__c = 'test45454';
        contact1.rdcc__CMD_Comapany_Id__c = '0017';
        contact1.rdcc__Insight_Company__c  = insightCompany1.id;
        insert contact1;
        
        rdcc__ProjectContactRole__c prjconrl = new rdcc__ProjectContactRole__c();
        prjconrl.rdcc__Insight_Contact__c = contact1.ID;
        insert prjconrl;
        pcr.add(prjconrl);
        conRoleMap.put(contact1.rdcc__Contact_ID__c,pcr);
        
        Contact con = new Contact();
        con.LastName = 'testConLName';
        con.Email = 'tst@test.com';
        con.rdcc__Contact_ID__c = '27832131';
        insert con;
        System.assertEquals('testConLName',con.LastName);
        
        insConIdConMap.put(con.rdcc__Contact_ID__c, con);
        DataConvertHelper.updateConRole(conRoleMap, insConIdConMap);
        
    }
    
    
    
    
    public static testMethod void test3(){
        UtilityClass.getObjectPrefix('Account');
        UtilityClass.accessConfigurableSettings();
        UtilityClass.queryAllFields('Account');
        UtilityClass.addMessageToPage('Test Message','Error');
        UtilityClass.addMessageToPage('Test Message','Info');
        UtilityClass.addMessageToPage('Test Message','Warning');
        UtilityClass.addMessageToPage('Test Message','Success');
        UtilityClass.getSObjectFields('Account');
        UtilityClass.getMandatoryFields('Account');
    }

}