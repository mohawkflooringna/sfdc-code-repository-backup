@isTest
private class CreateSamplePerformanceInv_BatchTest {
    
    static List<Account> resAccListForInvoicing;
    static List<Product2> productList;
    static List<Product_Customer_Group_Relationship__c> pcgrList;
    
    @isTest
    static void init(){
        resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
        for(Integer i=0;i<resAccListForInvoicing.size();i++){
            resAccListForInvoicing[i].Chain_Name__c = 'R.752994';
            resAccListForInvoicing[i].Business_Type__c = 'Residential';
            resAccListForInvoicing[i].Customer_Group_Number__c = '900074';            
        }
        insert resAccListForInvoicing;
        
        Product_SFDC_Mapping__c  psm = new Product_SFDC_Mapping__c(
            Brand__c = 'ACC',
            Product_Type__c = 'A',
            Product_Type_Description__c = 'Carpet',
            Sample_Inventory_Category__c = 'Aladdin'
        );
        insert psm;
        
        Buying_Group__c bg = new Buying_Group__c(
            name = 'Test BG',
            Buying_Group_Number__c = '900074',
            Independent_Aligned_Group__c = true
        );
        insert bg;
        
        productList = Utility_Test.createProduct2(true,1);
        String RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get(UtilityCls.LABELREDIDENTALDISPLAY).getRecordTypeId();   
        for(Integer i=0;i<productList.size();i++){
            productList[i].Buying_Group__c = 'Abbey (IA)';
            productList[i].RecordTypeId = RecordTypeId;
            productList[i].IsActive = true;
            productList[i].External_Id__c = 'ext id';
            productList[i].Trackable_Display_asset__c = true;   
        }
        update productList;
        
        pcgrList = Utility_Test.createProdCusGrpRelationship(false,1);
        for(Integer i=0;i<pcgrList.size();i++){
            pcgrList[i].Product__c = productList[0].id;
        }
        insert pcgrList;
        
        Sample_Display_Inventory__c sdi = new Sample_Display_Inventory__c();
        String brandType = '';
        sdi.Account__c = resAccListForInvoicing[0].Id;
        sdi.Inventory__c = productList[0].Id;
        sdi.RecordTypeId = Schema.SObjectType.Sample_Display_Inventory__c.getRecordTypeInfosByName().get(UtilityCls.SAMPLEREDIDENTALDISPLAY).getRecordTypeId();
        sdi.Customer_Group__c = 'Abbey (IA)';
        sdi.Customer_Group_Number__c = '900074';
        sdi.Placement_Status__c = UtilityCls.Not_In_Store;
        sdi.External_Id__c = '0000013456' +'_'+ 'R.BC134.7899.90';
        sdi.Name = 'Test Prod'; 
        
        insert sdi;
    }
    
    static testMethod void testMethod0() {
        init();
        createSamplePerformanceInventory_Batch  cls = new createSamplePerformanceInventory_Batch();
        string jobId = database.executeBatch(cls,10);   
    }
    
    @isTest
    static void methodForDeletion(){
        init();
        resAccListForInvoicing[0].Customer_Group_Number__c = '000000'; 
        update resAccListForInvoicing;
        createSamplePerformanceInventory_Batch  cls = new createSamplePerformanceInventory_Batch();
        string jobId = database.executeBatch(cls,10); 
    }
    
    @isTest
    static void methodForNegativeTest(){        
        createSamplePerformanceInventory_Batch  cls = new createSamplePerformanceInventory_Batch();
        createSamplePerformanceInventory_Batch.error = true;
        string jobId = database.executeBatch(cls,10); 
    }
}