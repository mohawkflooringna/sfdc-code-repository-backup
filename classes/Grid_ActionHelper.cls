public class Grid_ActionHelper {

    public static final String FILTERTYPE_PRICEZONE = 'PriceZone';
    public static final String FILTERTYPE_DISTRICT = 'District'; // Added by MB - Bug 68971 - 1/10/19
    public static final String FILTERTYPE_BUYINGGROUP = 'BuyingGroup';
    public static final String FILTERTYPE_WAREHOUSE = 'Warehouse';

    public static List<FilterData> getFilterOptions(String filterType, String productCategory, String gridType){
        return Grid_ActionHelper.getFilterOptions(filterType, productCategory, gridType, null);
    }    
    
    public static List<FilterData> getFilterOptions(String filterType, String productCategory, String gridType, String accountId){
        System.debug('### Grid_ActionHelper - getFilterOptions - productCategory: ' + productCategory);
        System.debug('### Grid_ActionHelper - getFilterOptions - gridType: ' + gridType);
        System.debug('### Grid_ActionHelper - getFilterOptions - filterType: ' + filterType);
        List<FilterData> returnList = new List<FilterData>();
        Map<String, String> dataMap;
        if(filterType == FILTERTYPE_PRICEZONE || filterType == FILTERTYPE_DISTRICT){
            ResidentialPriceGridProductCategory[] categories;
            if (Grid_BaseHelper.usersRole == Grid_BaseHelper.ROLE_SALESOPS){
                ResidentialPricingGridUtility.prodCategory = productCategory; //Added by MB - Bug 68971 - 1/16/19
                categories = ResidentialPricingGridUtility.getProductCategoriesForSalesOps(gridType,productCategory);
            }
            else{
                if (gridType == ResidentialPricingGridUtility.GRID_TYPE_MBP){
                    ResidentialPricingGridUtility.prodCategory = productCategory; //Added by MB - Bug 68971 - 1/10/19
                    categories = ResidentialPricingGridUtility.getProductCategoriesForPriceGridByLoggedInUser();
                }
                else
                    categories = ResidentialPricingGridUtility.getProductCategoriesForBuyingGroupGridByLoggedInUser();
            }
            for (ResidentialPriceGridProductCategory cat :categories){
                if(cat.productCategoryName == productCategory)
                    dataMap = cat.AvailableZones;
            }
        }else if(filterType == FILTERTYPE_BUYINGGROUP){
            dataMap = ResidentialPricingGridUtility.getAllBuyingGroups(productCategory, gridType);
            System.debug('### Grid_ActionHelper - getFilterOptions - dataMap: ' + dataMap);
            
        }else if(filterType == FILTERTYPE_WAREHOUSE){
            if (gridType == ResidentialPricingGridUtility.GRID_TYPE_MBP || gridType == ResidentialPricingGridUtility.GRID_TYPE_BGP) {
                //Price Grid and Buying Grid will query for logged user warehouses
                system.debug('### getFilterOptions for ' + gridType + ' without accountId');
                
                if (Grid_BaseHelper.usersRole == Grid_BaseHelper.ROLE_SALESOPS)
                    dataMap = ResidentialPricingGridUtility.getAllWarehouse();
                else
                    dataMap = ResidentialPricingGridUtility.getWarehousesByLoggedInUser();
            } 
            else {
                //CPL Grid will query for Account warehouses
                if (String.isNotEmpty(accountId)) {
                    system.debug('### getFilterOptions for CPL with accountID: ' + accountId);
                    dataMap = ResidentialPricingGridUtility.getWarehousesByAccount(accountId);
                }
            }
        }
        if(dataMap != null){
            for(String key : dataMap.keySet()){
                returnList.add(new FilterData(dataMap.get(key), key));
            }
        }
        returnList.sort();
        return returnList;
    }

    public class FilterData implements Comparable{
        public FilterData(String label, String value){
            this.label = label;
            this.value = value;
        }
        @AuraEnabled public String label;
        @AuraEnabled public String value;

        public Integer compareTo(Object objToCompare){
            return label.compareTo(((FilterData)objToCompare).label);
        }
    }
}