/**************************************************************************

Name : StrategicAccountSubmitApproval_CC_Test 

===========================================================================
Purpose :   This class is used for StrategicAccountSubmitApproval_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        10/March/2017     Created 
1.1        Susmitha      11/Nov/2017       Modified
***************************************************************************/
@isTest
private class StrategicAccountSubmitApproval_CC_Test { 
    static List<Account> accList;
    static Account acc;
    
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
        accList =  Utility_Test.createAccountsForNonInvoicing(true, 1);
        acc = (Account)accList.get(0);
        
    }
    
    static testMethod void testMethod1() {
        init();
        
        test.startTest();
        
        StrategicAccountSubmitApproval_CC.IsCurrentUseAdmin();
        StrategicAccountSubmitApproval_CC.getStrategicAccountFieldInfo();
        StrategicAccountSubmitApproval_CC.IsStrategicAccount(acc.Id); 
        StrategicAccountSubmitApproval_CC.runApproval(acc);
        system.assert(acc.Id!=null);
        
        
        StrategicAccountSubmitApproval_CC.getRecords(acc.Id, 'Id');
        StrategicAccountSubmitApproval_CC.getAccount(acc.Id);
        StrategicAccountSubmitApproval_CC.sessionId();
        //StrategicAccountSubmitApproval_CC.checkAccountFLS(acc.Id);
        StrategicAccountSubmitApproval_CC.isAccountLocked(acc.Id);
        StrategicAccountSubmitApproval_CC.checkAccountFLS(acc.id);//Added by susmitha on nov 11
        
        test.stopTest();
    }
    
    static testMethod void testMethod2() {
        init();
        MVP_Coordinator__c MVP = new MVP_Coordinator__c( name = 'Test' );
        insert MVP;
        system.assert(MVP.id!=null);
        test.startTest();
        //StrategicAccountSubmitApproval_CC.checkAccountFLS( acc.Id );
        StrategicAccountSubmitApproval_CC.getMVPInfo( 'Test' );
        StrategicAccountSubmitApproval_CC.getMVPList();
        test.stopTest();
    }
    
}