@isTest
private class distributionInAPController_Test {
    static List<Account_Profile_Settings__c> accProfileSettings;
    
    @TestSetup
    private static void init(){
        
         accProfileSettings = Utility_Test.createAccountProfileSettings(true, 3, 'Commercial');
    }
    
    
     static testmethod  void testGetDistribution() {
        System.runAs(Utility_Test.ADMIN_USER) {//Added By Mudit - 26/09/17
            
            Test.startTest();
            
             List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
        apSetting[0].Yellow_Threshold_Carpet__c = 10;
        apSetting[0].Red_Threshold_Carpet__c = 10;
        apSetting[0].Yellow_Threshold_Cushion__c = 10;
        apSetting[0].Red_Threshold_Cushion__c = 10;
        apSetting[0].Yellow_Threshold_Hardwood__c = 10;
        apSetting[0].Red_Threshold_Hardwood__c = 10; 
        apSetting[0].Yellow_Threshold_Laminate__c = 10; 
        apSetting[0].Red_Threshold_Laminate__c = 10;
        apSetting[0].Yellow_Threshold_Tile__c = 10;
        apSetting[0].Red_Threshold_Tile__c = 10;
        apSetting[0].Yellow_Threshold_Resilient__c = 10;
        apSetting[0].Red_Threshold_Resilient__c = 10;
        apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
        apSetting[0].Red_Threshold_Mainstreet__c = 10;
        apSetting[0].Multi_Family_Business_Split_2Team__c =10;
        apSetting[0].Retail_Business_Split_2Team__c = 10;
        apSetting[0].Retail_Business_Split_1Team__c = 10;
        apSetting[0].Builder_Business_Split_2_Team__c = 10;
        apSetting[0].Builder_Business_Split_1Team__c = 10;
        apSetting[0].Multi_Family_Business_Split_1team__c = 10;
        update apSetting[0];
            
            List<Account_Profile__c> apList = Utility_Test.createAccountProfile(true, 1, '', false);
            distributionInAccountProfileController.getAllDistributions(apList[0].Id);
            apList[0].Annual_Retail_Sales_Non_Flooring__c = 11;
            distributionInAccountProfileController.updateDistributions(apList[0]);
            
           
            distributionInAccountProfileController.APSMap(apSetting[0].id);
            
            Test.stopTest();
                
        }
    }
    
}