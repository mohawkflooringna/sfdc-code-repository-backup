/**************************************************************************

Name : genricLookupController_Test

===========================================================================
Purpose : Test Class for genricLookupController
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha       12/June/2018     Created 
***************************************************************************/
@isTest
private class genricLookupController_Test { 
  
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            List<Account> accList=Utility_Test.createCommercialAccountsForInvoicing(true,1);
            String whereClause = 'Id =: ' + accList[0].Id;
            List<Contact> contactList=Utility_Test.createContacts(true,1,accList);
            genricLookupController.searchDB('Contact','Name','Email',2,'email','test','AccountId',string.valueOf(accList[0].id));
            String result = genricLookupController.searchDBMultipleFields('Account', 'Id, Name ', whereClause, 1);
            test.stopTest();  
        }
    }
    
}