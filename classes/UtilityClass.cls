/*
* Copyright(c) CMD
* Author:      Nagarro
* Date:        April 26, 2017
* Description: A Utility class, defines all genric methods.
*/

public class UtilityClass {    
    
    private static Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    private static List<String> options;
    private static Map<String,String> countryMap;
    public static boolean isStateCountryPicklistEnabled;
    private static Map<String,String> stateMap;
    private static final String NOTFOUND = 'NOT FOUND';
    
    //takes object name and return 3 digit keyPrefix for that object.
    public static string getObjectPrefix(string objName){
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objName) ;    
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        String keyPrefix = r.getKeyPrefix();
        return keyPrefix;
    }
    
    
    //query and return configurable setting record.
    public static rdcc__Configurable_Settings__c  accessConfigurableSettings(){        
        String SobjectApiName = 'rdcc__Configurable_Settings__c';
        List<rdcc__Configurable_Settings__c> cs =  (List<rdcc__Configurable_Settings__c>)queryAllFields(SobjectApiName);
        if(cs!= null && !cs.isEmpty()){
            return cs[0];
        }
        return new rdcc__Configurable_Settings__c();
    }
    
    
    //query all fields of accepted sobject.
    public static List<SObject> queryAllFields(String SobjectApiName){
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        try{
            String commaSepratedFields = '';
            for(String fieldName : fieldMap.keyset()){
                if(fieldName.endsWith('__c')){
                    if(commaSepratedFields == null || commaSepratedFields == ''){
                        commaSepratedFields = fieldName;
                    }else{
                        commaSepratedFields = commaSepratedFields + ', ' + fieldName;
                    }
                }
            }        
            String query = 'select ' + commaSepratedFields + ' from ' + SobjectApiName + ' Limit 1';        
            List<SObject> config = Database.query(query); 
            
            return config;  
        }Catch(Exception e){
            System.debug('***Error Message:'+e.getMessage());
            UtilityClass.addMessageToPage(e.getMessage(),'ERROR');
            return null;
        }
    } 
    
    
    //add Message to page
    public static void addMessageToPage(String message, String msgType){
        if(msgType != null){
            if(msgType.equalsIgnoreCase('Error')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,message));
            }
            if(msgType.equalsIgnoreCase('Info')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO,message));
            } 
            if(msgType.equalsIgnoreCase('Success')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM,message));
            } 
            if(msgType.equalsIgnoreCase('Warning')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING,message));
            }
        }       
    }
    
    //return field api name and field label map.
    public static Map<String,String> getSObjectFields(String sObj){
        Map<String,String> fieldsApiNameLabelMap = new Map<String,String>();
        Map <String, Schema.SObjectField> fieldMap = getFieldMap(sObj);
        for(Schema.SObjectField sfield : fieldMap.Values()){
            
            schema.describefieldresult dfield = sfield.getDescribe();
            if(!(dfield.getName()=='Id'|| dfield.getName()=='SystemModstamp')){
                
                //else{
                //system.debug('Enter ELSE');
                fieldsApiNameLabelMap.put(dfield.getName(), dfield.getLabel());
                // }
            }
            
        }
        return fieldsApiNameLabelMap;
    }
    
    //accept Object name and picklist field api name and return all the picklist values of that field.
    public static List<String> getPicklistValues(String sObj, String picklistFieldApiName){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = getFieldMap(sObj).get(picklistFieldApiName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry cr: ple){            
            if(cr.getLabel() != NULL){
                options.add(cr.getLabel());
            }            
        }   
        return options;
    }
    
   
    
    public static Map<String,Schema.SObjectField> getFieldMap(String sObj){       
        return schemaMap.get(sObj).getDescribe().fields.getMap();
    }

    public static Map<String,String> getMandatoryFields(String sObjectName){
        
        Map<String,String> fieldApiNmLabelMap = new Map<String,String>();       
        for(Schema.SObjectField objField:getFieldMap(sObjectName).values()){
            Schema.DescribeFieldResult dfr = objField.getDescribe();
            if(!dfr.isNillable() && dfr.isCustom() && dfr.getType().name() != 'Boolean' && !dfr.isAutoNumber() && !dfr.isCalculated()){
                fieldApiNmLabelMap.put(dfr.getName(), dfr.getLabel());
            }
        }
        return fieldApiNmLabelMap;
    }
    
    public static Map<String,String> getMandatoryFieldsTypeMap(String sObjectName){
        Map<String,String> fieldApiNmTypeMap = new Map<String,String>();       
        for(Schema.SObjectField objField:getFieldMap(sObjectName).values()){
            Schema.DescribeFieldResult dfr = objField.getDescribe();
            if(!dfr.isNillable() && dfr.isCustom() && dfr.getType().name() != 'Boolean' && !dfr.isAutoNumber() && !dfr.isCalculated()){
                fieldApiNmTypeMap.put(dfr.getName(), dfr.getType().name());
            }
        }
        return fieldApiNmTypeMap;
    }
    
    public static boolean isStateCountryPicklistEnabled(){
        if(isStateCountryPicklistEnabled == null){
            isStateCountryPicklistEnabled = getFieldMap('Account').containsKey('billingstatecode');
        }
        return isStateCountryPicklistEnabled;
    }
    
    
    public static String getCountryName(String country){
        if(string.isNotBlank(country)){
        if(countryMap == null ){            
            countryMap =  new Map<String,String>();
            Map<String,Schema.SObjectField> objMap = getFieldMap('Account');            
            Schema.DescribeFieldResult fieldResult;
            if(objMap.get('billingcountrycode')!=null)
            {
                fieldResult = objMap.get('billingcountrycode').getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();            
                for( Schema.PicklistEntry f : ple){                
                    countryMap.put(f.getLabel().toUpperCase(),f.getValue().toUpperCase());
                }      
            }            
        }
        
        if(countryMap != null && countryMap.containsKey(country.toUpperCase() )){
            return countryMap.get(country.toUpperCase());
        }else if(countryMap != null && new Set<String>(countryMap.values()).contains(country.toUpperCase())){            
            return country;            
        }
        }
        return NOTFOUND;        
    }
    
    
    public static String getStateName(String state){        
        if(stateMap == null ){
            stateMap =  new Map<String,String>();
            Schema.DescribeFieldResult fieldResult;
            Map<String,Schema.SObjectField> objMap1 = getFieldMap('Account');
            if(getFieldMap('Account').get('billingstatecode')!=null)
            {
                fieldResult = objMap1.get('billingstatecode').getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();            
                for( Schema.PicklistEntry f : ple){                
                    stateMap.put(f.getLabel().toUpperCase() ,f.getValue().toUpperCase());
                    System.debug(f.getLabel()+'-->'+f.getValue());
                }    
            }
            
        }
        System.debug('***state'+state);
        if(stateMap.containsKey(state.toUpperCase())){
            System.debug('stateMap.get(state.toUpperCase())'+stateMap.get(state.toUpperCase()));
            return stateMap.get(state.toUpperCase());
        }else if(new Set<String>(stateMap.values()).contains(state.toUpperCase())){            
            return state;            
        }
        return NOTFOUND;
    }
    
    public static List<SelectOption> getRecordTypeMap(String obj){        
        List<SelectOption> recordTypeList = new List<SelectOption>();
        recordTypeList.add(new SelectOption('','--NONE--'));
        List<Schema.RecordTypeInfo> recordTypes = schemaMap.get(obj).getDescribe().getRecordTypeInfos();
        if(recordTypes != null && !recordTypes.isEmpty()){
            for(Schema.RecordTypeInfo eachRT:recordTypes){ 
                if(!eachRT.isMaster()){
                    recordTypeList.add(new SelectOption(eachRT.getRecordTypeId(), eachRT.getName()));
                }
            }                      
        }
        return recordTypeList; 
    }
    
    public static Set<Id> getAllowedRecordTypeIds(string obj)
    {
        Set<Id> recIds = new Set<Id>();
        for(RecordTypeInfo info: schemaMap.get(obj).getDescribe().getRecordTypeInfos()) {
            if(info.isAvailable()) {
                recIds.add(info.getRecordTypeId());
            }
        }
        return recIds;
    }
    
}