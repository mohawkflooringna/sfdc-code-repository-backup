/**************************************************************************

Name : FreightRateCalloutMock_Test 

===========================================================================
Purpose :  Create JSON for Freight Rate
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand        30/March/2017     Created 

***************************************************************************/

public with sharing class RequestToQuote_CC {
 
  @AuraEnabled
  public static String generateProject(String reqQuoteId, String oppStage) {

    Request_A_Quote__c reqQ = requestToQuoteData(reqQuoteId); 

    System.debug('test***'+reqQ);
    if(reqQ.Opportunity__c == null && reqQ.Is_Converted_To_Quote__c == false){

      String oppRTId = UtilityCls.getRecordTypeInfo('Opportunity', 'Transactional');
      //Create Opprotunity
      Opportunity opp  = new Opportunity();
      opp.Name         = reqQ.Name;
      opp.AccountId    = reqQ.Account__c;
      opp.RecordTypeId = oppRTId;
      opp.CloseDate    = reqQ.Expiration_Date__c;
      opp.StageName    = oppStage;
      opp.Primary_Dealer__c = reqQ.Dealer__c;

      Try{
          Database.SaveResult results = Database.insert(opp);
  	    if(results.isSuccess()){
  	    	Id oppId = results.getId();
  	    	System.debug('test' + oppId);

          // insert relatedAccount record in new project with Dealer__c
          /*Related_Account__c accountObj = new Related_Account__c(Opportunity__c  = oppId,
                                                                  Account__c      = reqQ.Dealer__c,
                                                                  Project_Role__c = 'Dealer',
                                                                  Primary__c      = true);
          database.INSERT(accountObj);*/

  	       //Update the current Request_A_Quote__c with inserted opportunity Id
  	       // if(reqQ.Opportunity__c == null){
  	       	   reqQ.Opportunity__c = oppId;
  	       	   // reqQ.Opportunity__c = opp.Id;
  	       	   Database.SaveResult reqQResults = Database.update(reqQ);
  	       	   if(reqQResults.isSuccess()){
  	       	   		System.debug('quote updated' + reqQResults.getId());
  	       	   	   //Create Standard Quote
                     //initStandardQuote(reqQ.Id, opp.Id);
  	       	   	return 'success';
  	       	   }
  	       // }
  	    }
      }catch(Exception e){
      	System.debug('test***'+e.getMessage());
      	return e.getMessage();
      } 
      return null;
    } else
      return 'Record already converted to Quote in Project. Unable to process';

  }

  //Method to create standard quote
 /* @AuraEnabled
  public static Quote initStandardQuote(Id reqQuoteId, Id oppId){
   
    Quote quote= new Quote();
    Request_A_Quote__c reqQ = requestToQuoteData(reqQuoteId); 
    quote.Name            = reqQ.Name;
    quote.OpportunityId   = reqQ.Opportunity__c;
    quote.Dealer__c       = reqQ.Dealer__c;
    quote.ExpirationDate  = reqQ.Expiration_Date__c;  
    quote.Influencing_Relationship__c = reqQ.Influencing_Relationship__c;
    //quote.Job_Location__c = rewQ.Job_Location__c;

    insert quote;
    return quote;

  } */

   //Method to return My appointment list records based on Appoinment(Event) Include_SAL_In_Mail__c flag 
   @AuraEnabled
   public static Request_A_Quote__c requestToQuoteData(String recId){
   
          return [SELECT Id, 
                         Name,
                         Account__c,
                         Opportunity__c,
                         Job_Location__c, 
                         Expiration_Date__c,
                         Dealer__c,
                         Influencing_City__c,
                         Influencing_Relationship__c,
                         Is_Converted_To_Quote__c,
                         Order_Status__c
                         FROM Request_A_Quote__c 
                         WHERE Id =: recId];
  }
}