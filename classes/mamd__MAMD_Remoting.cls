/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MAMD_Remoting {
    global MAMD_Remoting() {

    }
    @ReadOnly
    @RemoteAction
    global static Map<String,Object> remotingActionReadOnly(Map<String,String> remotingUrlParams) {
        return null;
    }
    @RemoteAction
    global static Map<String,Object> remotingAction(Map<String,String> remotingUrlParams) {
        return null;
    }
}
