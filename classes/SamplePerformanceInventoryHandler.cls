/**************************************************************************

Name : SamplePerformanceInventoryHandler

===========================================================================
Purpose : This class is used for SamplePerformanceInventory Trigger.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Sasi Naik     28/Apr/2017     Created 
***************************************************************************/
public class SamplePerformanceInventoryHandler {

	//AccountTriggerHandler
     public static void UpdateAccountCustomerGroupValues(Map<id, Sample_Display_Inventory__c> spiMap) {
         Set<Id> spiIds = new Set<Id>();
         List<Account> updatedAccountlist = new List<Account>(); 
         List<String> spiCusGroup = new List<String>();
         Set<String> accountIds = new Set<String>();
         String storeOldCustomerGroupval = '';
	        for (Sample_Display_Inventory__c spi : spiMap.values()) {
	            spiIds.add(spi.Id);
	            spiCusGroup.add(spi.Customer_Group__c);
	            accountIds.add(spi.Account__c);
	        }

	        if(spiIds.size()>0 && spiCusGroup != null && accountIds.size()>0){
	        	List<Account> spitoaccountlist = [Select id,Architech_Folder__c From Account where id IN:accountIds Limit 50000];
	        	for(Account a:spitoaccountlist){
	        		storeOldCustomerGroupval = a.Architech_Folder__c;
	        		for(Sample_Display_Inventory__c sp:spiMap.values()){
	        			if(sp.RecordTypeId != Schema.SObjectType.Sample_Display_Inventory__c.getRecordTypeInfosByName().get(UtilityCls.SAMPLECOMMERCIALDISPLAY).getRecordTypeId()){
		        			Account ac = new Account();
		        			ac.Id = a.Id;
		        			if(storeOldCustomerGroupval == null ){
		        				ac.Architech_Folder__c = sp.Architech_Folder__c;
		        			} else {
		        				ac.Architech_Folder__c = storeOldCustomerGroupval+';'+sp.Architech_Folder__c;
		        			}
		        			
		        			updatedAccountlist.add(ac);
	        			}
	        		}	
	        	}

	        	if(updatedAccountlist.size()>0){
	        		Database.update(updatedAccountlist,false);
	        	}

	        }
        
      }//End of Method

}