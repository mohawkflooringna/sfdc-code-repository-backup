public without sharing class CustomLookup_CC {
    @AuraEnabled
    public static List<sObject> fetchLookupValues(String keyWord, String objectName) {
        keyWord = keyWord + '%';
        System.debug(LoggingLevel.INFO, '*** objectName: ' + objectName);
        List<sObject> returnList = new List<sObject>();
        List<sObject> lstOfRecords = new List<sObject>();
        String sQuery;
        String sQuery1 = 'SELECT Id, Name ';
        String sQuery2 =  ', Competitor__c ,Competitor__r.Name';
        String sQuery3 =   ' FROM ' + objectName + ' WHERE Name LIKE: keyWord';
        String sQueryAP = ',Builder_Business_Split_1Team__c, Builder_Business_Split_2_Team__c, Multi_Family_Business_Split_1team__c, Multi_Family_Business_Split_2Team__c,Retail_Business_Split_1Team__c, Retail_Business_SPlit_2Team__c,Distribution_Carpet__c,Distribution_Cushion__c,Distribution_Hardwood__c,Distribution_Laminate__c,Distribution_Tile__c,Distribution_Resilient__c';

        if(objectName == 'Competitor_Products__c'){
            sQuery = sQuery1 + sQuery2 + sQuery3;
        }else if(objectName == 'Account_Profile_Settings__c'){
            sQuery = sQuery1 + sQueryAP + sQuery3;
            System.debug(LoggingLevel.INFO, '*** sQuery: ' + sQuery);
        }
        else{
            sQuery = sQuery1 + sQuery3;
        }
        System.debug('squer' + sQuery);
        Try{
          lstOfRecords = Database.query(sQuery);
        }Catch(Exception exp){
            system.debug('Error!!!'+exp.getMessage());
        }
        for(sObject  obj : lstOfRecords){
            returnList.add(obj);
        }
        return returnList;
        
    }
}