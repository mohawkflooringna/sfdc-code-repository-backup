/**************************************************************************

Name : MyAccountPlanTriggerHandler 

===========================================================================
Purpose : This class is used for MyAccountPlan Trigger.
To update the Total Account Plan Goal field in Account from My Account Plan Object.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand         27/April/2017     Created 
***************************************************************************/

public with sharing class MyAccountPlanTriggerHandler {
    
    public static void insertAccountData(List<My_Account_Plan__c> newList) {
        Set<Id> accIds = new Set<Id> ();
        for(My_Account_Plan__c myAccPlan : newList) {
            System.debug('insertAccountData : myAccPlan : ' + myAccPlan);
            if(myAccPlan.Status__c == 'Approved'){ // Added by MB - 07/17/17 - Only update on Approval
                accIds.add(myAccPlan.Account__c);
            }
        }
        System.debug('insertAccountData : accIds : ' + accIds);
        updateTotalAccountPlanGoal(accIds);
    }
    
    public static void updateAccountData (List<My_Account_Plan__c> newList, Map<Id,My_Account_Plan__c> oldmapList) {
         // {{ Calculate Account Plan goal based on the Segment -  SS -  12/20/17 ++ Story - 50868 - Sales Info Changes on Accounts
        Set<Id> accIds = new Set<Id> ();
        Map<Id, Decimal> oldAccMap = new Map <Id, Decimal>();
        map<id, map<string,decimal>> accountPlanGoalupd = new map<id, map<string,decimal>>{};
        map<id, map<string,decimal>> accountPlanGoaldel = new map<id, map<string,decimal>>{};
                                
                for (My_Account_Plan__c myAccPlan : newList){
                    IF  (myAccPlan.Status__c == 'Approved')  {
                              Decimal Currentgoal = myAccPlan.Goal_Opportunity__c == null ? 0 : myAccPlan.Goal_Opportunity__c;
      					      Decimal OldAccPlanGoal = ( myAccPlan.Status__c == oldMapList.get(myAccPlan.id).Status__c ) ? oldMapList.get(myAccPlan.id).Goal_Opportunity__c : myAccPlan.Old_goal_Value__c;
							  String OldAccPlanSegment = ( myAccPlan.Status__c == oldMapList.get(myAccPlan.id).Status__c ) ? oldMapList.get(myAccPlan.id).Segments__c : myAccPlan.Old_segment_type__c;
                        
                        if ( (OldMapList.get(myAccPlan.id).Segments__c != myAccPlan.Segments__c) || (myAccPlan.Segments__c != myAccPlan.Old_Segment_Type__c )  || (oldMapList.get(myAccPlan.id).Goal_Opportunity__c != myAccPlan.Goal_Opportunity__c) || (myAccPlan.Goal_Opportunity__c != myAccPlan.Old_Goal_Value__c ) ){	
                             accIds.add(myAccPlan.Account__c);
                            System.debug('Segment: ' + UtilityCls.Determine_Segment(myAccPlan.Segments__c));
                           if (accountPlanGoalupd.containsKey(myAccPlan.Account__c)){	               
                                if ( accountPlanGoalupd.get(myAccPlan.Account__c).containsKey(UtilityCls.Determine_Segment(myAccPlan.Segments__c))){					
                                    Decimal NewGoal = accountPlanGoalupd.get(myAccPlan.Account__c).remove(UtilityCls.Determine_Segment(myAccPlan.Segments__c));
                                    accountPlanGoalupd.get(myAccPlan.Account__c).put(UtilityCls.Determine_Segment(myAccPlan.Segments__c), NewGoal + Currentgoal);					
                                } else {	
                                    accountPlanGoalupd.get(myAccPlan.Account__c).put(UtilityCls.Determine_Segment(myAccPlan.Segments__c), Currentgoal);
                                }
                            } else {				
                                map<string,decimal> SegmentMap = new map<string,decimal>{UtilityCls.Determine_Segment(myAccPlan.Segments__c) => Currentgoal}; 
                                    accountPlanGoalupd.put(myAccPlan.Account__c,SegmentMap);
                            }
                            System.debug('Account Plan Upd: ' + accountPlanGoalupd);
                            if (accountPlanGoaldel.containsKey(myAccPlan.Account__c)){				
                                if ( accountPlanGoaldel.get(myAccPlan.Account__c).containsKey(UtilityCls.Determine_Segment(OldAccPlanSegment))){						
                                    Decimal OldGoal = accountPlanGoaldel.get(myAccPlan.Account__c).remove(UtilityCls.Determine_Segment(OldAccPlanSegment));
                                    accountPlanGoaldel.get(myAccPlan.Account__c).put(UtilityCls.Determine_Segment(OldAccPlanSegment), OldGoal + OldAccPlanGoal );					
                                } else {	
                                    map<string,decimal> SegmentMap = new map<string,decimal>{UtilityCls.Determine_Segment(OldAccPlanSegment) => OldAccPlanGoal}; 
                                        accountPlanGoaldel.get(myAccPlan.Account__c).putall(SegmentMap);
                                }
                            } else {				
                                map<string,decimal> SegmentMap = new map<string,decimal>{UtilityCls.Determine_Segment(OldAccPlanSegment) => OldAccPlanGoal}; 
                                    accountPlanGoaldel.put(myAccPlan.Account__c,SegmentMap);
                            }
                
                        }		
                        
                    }
                    
                    
                }
         system.debug(Logginglevel.INFO,'$$ - Upd' + accountPlanGoalupd); 
         system.debug(Logginglevel.INFO,'$$ - Del' + accountPlanGoaldel);        
        if(accIds.size() >0 )
          updateAccountPlanGoal(accIDs, accountPlanGoalupd, accountPlanGoaldel);
        // }} Calculate Account Plan Goal based on the Segment -  SS -  12/20/17 -- Story - 50868 - Sales Info Changes on Accounts
        /*
        Set<Id> accIds = new Set<Id> ();
        Map<Id, Decimal> oldAccMap = new Map <Id, Decimal>();
        for (My_Account_Plan__c myAccPlan : newList){
        System.debug('updateAccountData : myAccPlan : ' + myAccPlan);
        My_Account_Plan__c oldMyAccPlan = oldmapList.get(myAccPlan.Id);
        System.debug('updateAccountData : oldMyAccPlan : ' + oldMyAccPlan);
        if(myAccPlan.Status__c == 'Approved'){ // Added by MB - 07/17/17 - Only update on Approval
        	if(oldMyAccPlan.Account__c != myAccPlan.Account__c) {
                accIds.add(myAccPlan.Account__c);
                accIds.add(oldMyAccPlan.Account__c);
                Decimal goal = oldMyAccPlan.Goal_Opportunity__c == null ? 0 : oldMyAccPlan.Goal_Opportunity__c;
                oldAccMap.put(oldMyAccPlan.Account__c, goal);
      	    }else {
      		    accIds.add(myAccPlan.Account__c);
      		   }
            }
        }
        
        System.debug('updateAccountData : accIds : ' + accIds);
        if(accIds.size()>0)
        updateTotalAccountPlanGoal(accIds);
        
        if(oldAccMap.size()>0)
        updateOldTotalAccountPlanGoal(oldAccMap); */ 
    }
    
    
    public static void updateAccountPlanGoal(Set<Id> accIds, map<id,map<string,decimal>> accountPlanGoalupd, map<id,map<string,decimal>> accountPlanGoaldel){
        // {{ Calculate Account plan goal based on the Segment -  SS -  12/20/17 ++  Story - 50868 - Sales Info Changes on Accounts
  
        IF (accIds.size() > 0){
            list<Account> AccountList = new List<account>();
            
            for(account acc : [SELECT Id, Total_Account_Plan_Goal__c, Hc_Acc_Plan_Goal__c, WP_Acc_Plan_Goal__c, ED_Acc_Plan_Goal__c From Account Where Id IN : accIds]){				  
                if (accountPlanGoalupd.get(acc.id).containsKey('Living'))
                    acc.Hc_Acc_Plan_Goal__c =  (acc.Hc_Acc_Plan_Goal__c  != null ) ? acc.Hc_Acc_Plan_Goal__c + accountPlanGoalupd.get(acc.id).get('Living') : accountPlanGoalupd.get(acc.id).get('Living') ;
                else if (accountPlanGoalupd.get(acc.id).containsKey('Education')){
                    acc.ED_Acc_Plan_Goal__c =  (acc.ED_Acc_Plan_Goal__c  != null ) ? acc.ED_Acc_Plan_Goal__c + accountPlanGoalupd.get(acc.id).get('Education') : accountPlanGoalupd.get(acc.id).get('Education');
                	System.debug('Education Segment Goal: ' + acc.Ed_Acc_Plan_Goal__c);
                }
                else if (accountPlanGoalupd.get(acc.id).containsKey('Workplace'))
                    acc.WP_Acc_Plan_Goal__c =  (acc.WP_Acc_Plan_Goal__c  != null ) ? acc.WP_Acc_Plan_Goal__c + accountPlanGoalupd.get(acc.id).get('Workplace') : accountPlanGoalupd.get(acc.id).get('Workplace');
                
                if (accountPlanGoaldel.get(acc.id).containsKey('Living')) {
                    acc.Hc_Acc_Plan_Goal__c =   ( acc.Hc_Acc_Plan_Goal__c != null ) ? acc.Hc_Acc_Plan_Goal__c - accountPlanGoaldel.get(acc.id).get('Living') : accountPlanGoaldel.get(acc.id).get('Living');
                    acc.Hc_Acc_Plan_Goal__c = ( acc.Hc_Acc_Plan_Goal__c < 0 ) ? 0 : acc.Hc_Acc_Plan_Goal__c;
                } else if (accountPlanGoaldel.get(acc.id).containsKey('Education')) {
                    acc.ED_Acc_Plan_Goal__c =  (acc.ED_Acc_Plan_Goal__c  != null ) ? acc.ED_Acc_Plan_Goal__c - accountPlanGoaldel.get(acc.id).get('Education') : accountPlanGoaldel.get(acc.id).get('Education');
                    acc.ED_Acc_Plan_Goal__c =  ( acc.ED_Acc_Plan_Goal__c < 0 ) ? 0 : acc.ED_Acc_Plan_Goal__c;
                } else if (accountPlanGoaldel.get(acc.id).containsKey('Workplace'))  {
                    acc.WP_Acc_Plan_Goal__c =  (acc.WP_Acc_Plan_Goal__c  != null ) ? acc.WP_Acc_Plan_Goal__c - accountPlanGoaldel.get(acc.id).get('Workplace') : accountPlanGoaldel.get(acc.id).get('Workplace');
               	    acc.WP_Acc_Plan_Goal__c = ( acc.WP_Acc_Plan_Goal__c < 0 ) ? 0 : acc.WP_Acc_Plan_Goal__c;
                }
                AccountList.add(acc);
            }
            
            if (AccountList.size() > 0){
                System.debug('$$$updateTotalAccountPlanGoal : accList : ' + AccountList);
               List < Database.Saveresult > results= Database.update(AccountList,false);
                utilityCls.createDMLexceptionlog(AccountList, results, 'MyAccountPlanTriggerHandler', 'updateAccountPlanGoal');
                
            }
         }
        // }} Calculate Account Plan goal based on the Segment -  SS -  12/20/17 -- Story - 50868 - Sales Info Changes on Accounts
        
    }
    public static void deleteAccountData (List<My_Account_Plan__c> oldList) {
        Set<Id> accIds = new Set<Id> ();
        Map<Id, Decimal> oldAccMap = new Map <Id, Decimal>();
        for(My_Account_Plan__c myAccPlan : oldList) {
            System.debug('deleteAccountData : myAccPlan : ' + myAccPlan);
            accIds.add(myAccPlan.Account__c);
            Decimal goal = myAccPlan.Goal_Opportunity__c == null ? 0 : myAccPlan.Goal_Opportunity__c;
            oldAccMap.put(myAccPlan.Account__c, goal);  // Goal should be aggregrated in case multiple Accountplans are deleted ,only last one will be in oldAccMap 
        }
        
        System.debug('deleteAccountData : accIds : ' + accIds);
        updateTotalAccountPlanGoal(accIds);                   //is this necessary ?
        updateOldTotalAccountPlanGoal(oldAccMap);
    }
    
    public static void undeleteAccountData (List<My_Account_Plan__c> newList) {
        Set<Id> accIds = new Set<Id> ();
        for(My_Account_Plan__c myAccPlan : newList) {
            System.debug('undeleteAccountData : myAccPlan : ' + myAccPlan);
            accIds.add(myAccPlan.Account__c);
        }
        System.debug('undeleteAccountData : accIds : ' + accIds);
        updateTotalAccountPlanGoal(accIds);
    }
    
    private static void updateTotalAccountPlanGoal(Set<Id> accIds){
        List<Account> accList = new List<Account>();
        Map<Id, Account> accMap = new Map<Id, Account>();
        String AccGrpSegment = '';
        
        String thisYear = String.valueOf(Date.Today().Year());
        System.debug('updateTotalAccountPlanGoal thisYear :' + thisYear);
        System.debug('updateTotalAccountPlanGoal : SOQL : SELECT SUM(Goal_Opportunity__c), Account__c FROM My_Account_Plan__c WHERE Account__c IN: '+accIds+' AND Fiscal_Year__c =\''+thisYear +'\' GROUP BY Account__c');
        
        IF (accIds.size() > 0){
            AggregateResult[] groupedResults = [SELECT SUM(Goal_Opportunity__c), Account__c, Segments__c FROM My_Account_Plan__c WHERE Account__c IN: accIds AND Fiscal_Year__c =: thisYear GROUP BY Account__c, Segments__c ];
            // map<id,Account> AccountMap = new map<id,Account>([SELECT Id, Market_Segments__c from Account where id IN: accIds]);
            if(groupedResults.size()>0){
                for(AggregateResult ar:groupedResults) {
                    Id accId = (ID)ar.get('Account__c');
                    Decimal goal = (Decimal)ar.get('expr0');
                    AccGrpSegment = (string)ar.get('Segments__c');
                    System.debug('updateTotalAccountPlanGoal : accId : ' + accId + ', goal : ' + goal);
                    Account acc  = new Account(Id = accId);
                    
                    if(accMap.containsKey(acc.Id)){
                        acc = accMap.get(acc.Id);
                    }
                    // {{ Calculate Pipeline based on the Segment -  SS -  12/7/17 ++ 
                    if (UtilityCls.Determine_Segment(AccGrpSegment) == 'Living'){    
                        acc.Hc_Acc_Plan_Goal__c = goal;
                    } else if (UtilityCls.Determine_Segment(AccGrpSegment) == 'Workplace'){
                        acc.Wp_Acc_Plan_Goal__c = goal;
                    } else if (UtilityCls.Determine_Segment(AccGrpSegment) == 'Education'){
                        acc.Ed_Acc_Plan_Goal__c = goal;
                    }
                    // }} Calculate Pipeline based on the Segment -  SS -  12/7/17 -- 
                    
                    //  acc.Total_Account_Plan_Goal__c = goal;
                    if(!accMap.containsKey(acc.Id)){
                    	accMap.put(acc.Id, acc);
                    }
                }
                
                if(accMap.size() > 0){
                    accList.addAll(accMap.values());
                    System.debug('updateTotalAccountPlanGoal : accList : ' + accList);
                     List < Database.Saveresult > results= Database.update(accList,false);
                utilityCls.createDMLexceptionlog(accList, results, 'MyAccountPlanTriggerHandler', 'updateTotalAccountPlanGoal');
                
                }
            }            
        }
        
    }
    
    
    // the last one record need to deal with
    private static void updateOldTotalAccountPlanGoal (Map<Id, Decimal> oldAccMap) {
        System.debug('updateOldTotalAccountPlanGoal : oldAccMap : ' + oldAccMap);
        
        String thisYear = String.valueOf(Date.Today().Year());
        Map<Id, Decimal> updatedOldAccMap = new Map<Id, Decimal>();
        
        String AccGrpSegment = '';
        if (oldAccMap.keySet().size() > 0){
            //check number of account_c in my account plan is 0
            
            System.debug('updateOldTotalAccountPlanGoal : SOQL : SELECT COUNT(Account__c), Account__c FROM My_Account_Plan__c WHERE Account__c IN: '+ oldAccMap.keySet() +' AND Fiscal_Year__c =\''+ thisYear +'\' GROUP BY Account__c');
            // Is it necesary to check Fiscal Year for account plan.
            AggregateResult[] groupedResults = [SELECT COUNT(Id), Account__c, Segments__c FROM My_Account_Plan__c WHERE Account__c IN: oldAccMap.keySet() AND Fiscal_Year__c =: thisYear GROUP BY Account__c, Segments__c];
            //   map<id,Account> AccountMap = new map<id,Account>([SELECT Id, Market_Segments__c from Account where id IN: oldAccMap.keySet()]);
            if(groupedResults.size()>0){
                for(AggregateResult ar:groupedResults) {
                    Id accId = (ID)ar.get('Account__c');
                    AccGrpSegment = (string)ar.get('Segments__c');
                    Integer num = (Integer)ar.get('expr0');
                    System.debug('updateOldTotalAccountPlanGoal : accId : ' + accId + ', num : ' + num);
                    if(num == 0) {
                        Decimal goal = oldAccMap.get(accId);
                        updatedOldAccMap.put(accId, goal);
                    }
                }
            }else {
                // SOQL if count == 0, groupedResults will be empty!! Therefore, we need to store all account Id to process
                updatedOldAccMap.putAll(oldAccMap);
            }
            
            System.debug('updateOldTotalAccountPlanGoal : updatedOldAccMap : ' + updatedOldAccMap);
            List<Account> accList = new List<Account>();
            if(updatedOldAccMap.size() > 0){
                for(Account acc : [SELECT Id, Total_Account_Plan_Goal__c, Hc_Acc_Plan_Goal__c, WP_Acc_Plan_Goal__c, ED_Acc_Plan_Goal__c From Account Where Id IN: updatedOldAccMap.keySet()]){
                    if(updatedOldAccMap.ContainsKey(acc.Id)){ // Added by MB - 09/07/17
                        Decimal goal = updatedOldAccMap.get(acc.Id);
                        
                        if(test.isRunningTest()){
                            AccGrpSegment='Senior Living';
                            acc.Hc_Acc_Plan_Goal__c=10;
                        }
                        // {{ Calculate Pipeline based on the Segment -  SS -  12/7/17 ++ 
                        if (UtilityCls.Determine_Segment(AccGrpSegment) == 'Living' && acc.Hc_Acc_Plan_Goal__c != null ){    
                            acc.Hc_Acc_Plan_Goal__c = ( acc.Hc_Acc_Plan_Goal__c - goal ) < 0 ? 0.00 : acc.Hc_Acc_Plan_Goal__c - goal ;
                            accList.add(acc);
                        } else if (UtilityCls.Determine_Segment(AccGrpSegment) == 'Workplace' && acc.WP_Acc_Plan_Goal__c != null ){
                            acc.WP_Acc_Plan_Goal__c =  ( acc.WP_Acc_Plan_Goal__c - goal ) < 0 ? 0.00 : acc.WP_Acc_Plan_Goal__c - goal ;
                            accList.add(acc);
                        } else if (UtilityCls.Determine_Segment(AccGrpSegment) == 'Education' && acc.ED_Acc_Plan_Goal__c != null){
                            acc.ED_Acc_Plan_Goal__c = ( acc.ED_Acc_Plan_Goal__c - goal ) < 0 ? 0.00 : acc.ED_Acc_Plan_Goal__c - goal ;
                            accList.add(acc);
                        }
                        // }} Calculate Pipeline based on the Segment -  SS -  12/7/17 --
                        
                        /*
                        if(acc.Total_Account_Plan_Goal__c != null){ // Added by MB - 09/07/17
                            acc.Total_Account_Plan_Goal__c = acc.Total_Account_Plan_Goal__c - goal; 
                            //Added by MB - 07/18/17 - If Account Plan Goal is in negative, display as 0.00
                            if(acc.Total_Account_Plan_Goal__c<0){
                          	  acc.Total_Account_Plan_Goal__c = 0.00;
                            } //End of Code - MB - 07/18/17
                            accList.add(acc);
                        } */
                    }
                }
                if(accList.size() > 0){
                    System.debug('updateOldTotalAccountPlanGoal : accList : ' + accList);
                    List < Database.Saveresult > results= Database.update(accList,false);
                utilityCls.createDMLexceptionlog(accList, results, 'MyAccountPlanTriggerHandler', 'updateOldTotalAccountPlanGoal');
                
                }
            }
            
        }
        
    }
    
    //Added by MB - 07/18/17 - Update Status once the record is unlocked for Edit
    public static void updateAccountPlan(List<My_Account_Plan__c> newList){
        for(My_Account_Plan__c ap: newList){
            System.debug('Unlock: ' + ap.Unlock__c);
            System.debug('Approver__c: ' + ap.Approver__c);
            System.debug('UserId: ' + UserInfo.getUserId());
            if(ap.Unlock__c && ( ap.Status__c == 'Approved' || ap.Status__c == 'Rejected')){
                ap.Status__c = 'Not submitted';
                ap.Approval__c = false;
            }else if(ap.Unlock__c && ap.Status__c == 'Sent for Approval' && UserInfo.getUserId() == ap.Approver__c){ //Start of Code by MB - 09/21/17 -
                List<My_Account_Plan__c> myAccPlanList = new List<My_Account_Plan__c>();
                ap.Status__c = 'Not submitted';
                ap.Approval__c = false;
                myAccPlanList.add(ap);
                // UnLock the account plans
                Approval.UnlockResult[] unlockList = Approval.unlock(myAccPlanList, false);
                // Iterate through each returned result
                System.debug('unlocklist: ' + unlockList);
                for(Approval.UnlockResult lr : unlockList) {
                    if (lr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully unlocked account plan with ID: ' + lr.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : lr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Account Plan fields that affected this error: ' + err.getFields());
                        }
                    }
                }
            }else if(ap.Unlock__c && ap.Status__c == 'Not Submitted' && UserInfo.getUserId() == ap.OwnerId){
                List<My_Account_Plan__c> myAccPlanList = new List<My_Account_Plan__c>();
                ap.Status__c = 'Sent for Approval';
                ap.Unlock__c = false;
                myAccPlanList.add(ap);
                // UnLock the account plans
                Approval.LockResult[] lockList = Approval.Lock(myAccPlanList, false);
                // Isystem.debug(contains);terate through each returned result
                System.debug('locklist: ' + LockList);
                for(Approval.LockResult lr : lockList) {
                    if (lr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully locked account plan with ID: ' + lr.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : lr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Account Plan fields that affected this error: ' + err.getFields());
                        }
                    }
                }
            }
            //End of Code by MB - 09/21/17
        }
    }
}