public without sharing class ContactTriggerHandler {
    
    public static String commRTId = '';
    public static String resRTId = '';
    public static Boolean updatedBatch = false;
    public static String loginUserName = '';
    public static List<Id> contactIds = new List<Id>(); //Added by MB - Bug 67128 - 11/26/18
    
    static{
        commRTId = UtilityCls.getRecordTypeInfo('Contact', 'Commercial');
        resRTId  = UtilityCls.getRecordTypeInfo('Contact', 'Residential');
        loginUserName = UserInfo.getName();
    }
    public static void updateContact(List<Contact> newContactList, List<Contact> oldContactList, boolean insert_flag, boolean update_flag){
        Set<Id> accIds = new Set<Id>();
        Map<Id, Account> accMap = new Map<Id, Account>();
        Default_Configuration__c defConfig = Default_Configuration__c.getOrgDefaults();
        if(insert_flag){
            for(Contact newContact: newContactList){
                accIds.add(newContact.AccountId);
            }
            if(accIds.size()>0){
                for(Account acc: [SELECT Id, Global_Account_Number__c, ShippingStreet, ShippingCity, ShippingState, ShippingStateCode, ShippingCountry, ShippingCountryCode, ShippingPostalCode
                                 FROM Account
                                  WHERE Id IN :accIds]){
                                      accMap.put(acc.Id, acc);
                                  }
            }
            for(Contact newContact: newContactList){
                //newContact.Send_to_Hybris__c = true;
            if( accMap.ContainsKey( newContact.AccountId ) ){  //Added by krishnapriya on Nov 14 2017      
                Account acc = accMap.get(newContact.AccountId);
                //String globalId = acc.Global_Account_Number__c;
                //newContact.External_ID__c = globalId + '_' + newContact.Email;
                if(acc != null && newContact.MailingStreet == null && newContact.MailingCity == null &&
                   newContact.MailingStateCode == null && newContact.MailingPostalCode == null){
                       if(acc.ShippingStreet != null && acc.ShippingStreet.trim().length() >= 0){   //Added condition acc.ShippingStreet.trim().length() == 0  by krishnapriya on Nov 14 2017 
                           newContact.MailingStreet = acc.ShippingStreet;
                       }
                       
                       if( acc.ShippingCity != null){
                           newContact.MailingCity = acc.ShippingCity;
                       }
                       if( acc.ShippingStateCode != null){
                           newContact.MailingStateCode = acc.ShippingStateCode;
                       }
                       /*if(newContact.MailingState == null && acc.ShippingState != null){
                        newContact.MailingState = acc.ShippingState;
                        }*/
                       if(acc.ShippingCountryCode != null){
                           newContact.MailingCountryCode = acc.ShippingCountryCode;
                       }
                       if(acc.ShippingPostalCode != null){
                           newContact.MailingPostalCode = acc.ShippingPostalCode;
                       }
                   }
                 }
                /*if(newContact.RecordTypeId == CommRTId && defConfig.Commercial_Sales_Ops__c != null){
                    newContact.OwnerId = defConfig.Commercial_Sales_Ops__c;
                }else if(newContact.RecordTypeId == ResRTId && defConfig.Residential_Sales_Ops__c != null){
                    newContact.OwnerId = defConfig.Residential_Sales_Ops__c;
                }- Commented by MB - 10/12/17*/
            }
        }/*else if(update_flag){
            if(!loginUserName.contains('Integration') && !updatedBatch){
                for(Contact cont: newContactList){
                    cont.Send_to_Hybris__c = true;
                }
            }
        }*/
    }
    // Start of of Code - SSB- 9-28-17 - 
    // this code is needed for SFDC to LDS interface 
    // and should be deactivated once rollout is complete
    /*public static void ldsFlag(List<Contact> newList,Boolean update_flag,Boolean insert_flag ){
        User usr = [Select CAMS_Employee_Number__c from User where Id  =: UserInfo.getUserId()];
        for(Contact  cnt: newList){
            cnt.CAMS_Employee_ID_of_Logged_in_User__c = usr.CAMS_Employee_Number__c;   
            if(update_flag && cnt.RecordTypeId == commRTId){
                System.Debug('Contact Update LDS Flags' + 'update flag' + update_flag +'insert flag' + insert_flag);  
            //     if(cnt.LDS_Create__c){               // All updates will be handlled in batch mode. 10-16-17
             //       cnt.LDS_Create__c = false; 
             //   }   
              //  if(!cnt.LDS_Update__c){
               //     cnt.LDS_Update__c = true; 
                //} /                                    // All updates will be handlled in batch mode. 10-16-17
            } else if(insert_flag && cnt.RecordTypeId == CommRTId) {
                System.Debug('Contact Update LDS Flags' + 'update flag' + update_flag +'insert flag' + insert_flag);
                cnt.LDS_Create__c = true;
                cnt.LDS_Update__c = false;
             }
        }
     }*/
    // End of Code - SSB- 9-28-17
    
    /* Start of Code - MB - 09/11/18 
    public static void validateAndUpdateContact(Map<Id, Contact> oldMap, Map<Id, Contact> newMap){
        Set<String> emailSet = new Set<String>();
        Set<String> emailSetR = new Set<String>();
        Map<String, Contact> contEmailMap = new Map<String, Contact>();
        Map<String, Contact> contR = new Map<String, Contact>();
        String loginUserName = UserInfo.getName();
        List<Contact> updContactList = new List<Contact>();
        
        for(Contact cont: newMap.values()){
            if(!updatedBatch && !loginUserName.contains('Integration')){
            	emailSet.add(cont.Email);
        	}
        }
        
        if(emailSet.size()>0){
            for(List<Contact> contList: [SELECT Id, AccountId, Email, Business_Type__c, RecordTypeId, Send_to_Hybris__c,
                                         SAP_Contact_GUID__c, SAP_Contact_Number__c
                                         FROM Contact WHERE Email IN: emailSet]){
                                             for(Contact cont: contList){
                                                 if(!newMap.containsKey(cont.Id)){
                                                     contEmailMap.put(cont.Email, cont);
                                                 }
                                             }
                                         }
            
            for(Contact cont: newMap.values()){
                Contact exCont = contEmailMap.containsKey(cont.Email) ? contEmailMap.get(cont.Email) : null;
                if(exCont != null){
                    Contact tempCont = new Contact();
                    tempCont = cont.clone();
                    tempCont.Id = exCont.Id;
                    tempCont.RecordTypeId = exCont.RecordTypeId;
                    tempCont.SAP_Contact_GUID__c = exCont.SAP_Contact_GUID__c;
                    tempCont.SAP_Contact_Number__c = exCont.SAP_Contact_Number__c;
                    tempCont.AccountId = exCont.AccountId;
                    tempCont.Send_to_Hybris__c = false;
                    updContactList.add(tempCont);
                }
            }
        }
        if(updContactList.size()>0){
            updatedBatch = true;
            update updContactList;
        }
    }*/
    
    //Start of Code by Nagen - Bug 69429 - 3/15/19
    public static void checkOpenProject(List<Contact> newContactList){
        Set<Id> contactIds = new Set<Id>();
        map<Id,Boolean> contactIdHasOpenProjectMap = new  map<Id,Boolean>();
        String profName = UtilityCls.getProfileName(UserInfo.getProfileId());
        if(commRTId == ''){
            commRTId = UtilityCls.getRecordTypeInfo('Contact', 'Commercial');
        }
        System.debug('@@'+profName);
        if(profName == 'Commercial Sales User' || profName == 'Commercial Sales Manager'){
            for(Contact c: newContactList){
                if(commRTId == c.RecordTypeId){
                    contactIds.add(c.Id);
                }  
            }
            if(contactIds.size()>0){
                for(List<Opportunity> opportunityList : [SELECT Id,Contact__c,Status__c FROM Opportunity 
                                                         WHERE Contact__c in : contactIds AND Status__c = 'In Process']){
                                                             for(Opportunity opp: opportunityList){
                                                                 contactIdHasOpenProjectMap.put(Opp.Contact__c,true);
                                                             }
                                                         }
                for(List<Related_Account__c> relatedAccount : [SELECT Id,Opportunity__r.Status__c,Contact__c FROM Related_Account__c 
                                                               WHERE Contact__c in :contactIds 
                                                               AND Opportunity__r.Status__c = 'In Process']){
                                                                   for(Related_Account__c ra:relatedAccount){
                                                                       contactIdHasOpenProjectMap.put(ra.Contact__c,true);
                                                                   }
                                                               }
            }
            if(contactIdHasOpenProjectMap.size()>0){
                for(Contact c: newContactList){
                    if(contactIdHasOpenProjectMap.containskey(c.id)){
                        if(contactIdHasOpenProjectMap.get(c.id)){
                            c.addError(System.Label.Open_Project_Error_on_Contact);
                        }
                    }  
                }
            }
        }
    }
    //End of Code by Nagen - Bug 69429 - 3/15/19
    /* End of Code - MB - 09/11/18 */
    
    /* Start of Code - MB - Bug 67128 - 11/26/18 /
public static void checkDeleteFlag(Map<Id, Contact> newMap){
for(Contact cont: newMap.values()){
if(cont.Delete_Contact__c){
contactIds.add(cont.Id);
}
}
if(contactIds.size() > 0){
delete [select id from contact where id in :contactIds];
}
}
/* End of Code - MB - 11/26/18 */
}