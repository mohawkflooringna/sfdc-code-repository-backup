/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Batch_UpdateCompaniesNContacts implements Database.Batchable<rdcc__Material_Demand__c>, System.Schedulable {
    global Batch_UpdateCompaniesNContacts() {

    }
    global Batch_UpdateCompaniesNContacts(Set<String> dNameList) {

    }
    global void execute(System.SchedulableContext context) {

    }
    global void execute(Database.BatchableContext info, List<rdcc__Material_Demand__c> scope) {

    }
    global void finish(Database.BatchableContext info) {

    }
    global System.Iterable start(Database.BatchableContext BC) {
        return null;
    }
}
