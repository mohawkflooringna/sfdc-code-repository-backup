/**************************************************************************

Name : AccNonInvoicingNogenerator_Test

===========================================================================
Purpose : Test class for AccNonInvoicingNogenerator
===========================================================================
History:
--------
VERSION    AUTHOR         DATE             DETAIL          DESCRIPTION
1.0        Susmitha      07/nov/2017       Create  
***************************************************************************/
@isTest
private class AccNonInvoicingNogenerator_Test {
    static List<Account> resAccListForInvoicing;
    static List<Territory__c> terrList;
    static User userList;
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static List<Mohawk_Account_Team__c> mohAccTeam1;
    static List<Action_List__c> actListForResidentialFL;
    static List<Action_List__c> actListForResidentialSAL;
    static List<Action_List__c> actListForResidential = new List<Action_List__c>();
    static List<SAL_Territory__c>  salTerr;
    static List<Territory_User__c>  terrUser;
    static List<Territory_User__c>  terrUser1;
    static Id resiNonInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Non-Invoicing');
    static Id resiInvRTId    = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
    
    @TestSetup
    public static void init(){   
       Utility_Test.getTeamCreationCusSetting(); 
    }
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            
            
            resAccListForInvoicing = Utility_Test.createAccountsForNonInvoicing(true, 2);
            for(Integer i=0;i<resAccListForInvoicing.size();i++){
                resAccListForInvoicing[i].Business_Type__c = 'Residential';
            }
            update resAccListForInvoicing;
            test.startTest();  
            NonInvoiceGLoablAccNumber_Batch  cls = new NonInvoiceGLoablAccNumber_Batch ();
            string jobId=database.executeBatch(cls,10);
            system.assert(jobId!=null);
            test.stopTest();
        }
    }
      static testMethod void testMethod2() {
        System.runAs(Utility_Test.ADMIN_USER) {
            
           
            resAccListForInvoicing = Utility_Test.createAccountsForNonInvoicing(true, 2);
            for(Integer i=0;i<resAccListForInvoicing.size();i++){
                resAccListForInvoicing[i].Business_Type__c = UtilityCls.COMMERCIAL;
            }
            update resAccListForInvoicing;
            test.startTest();  
            
            Set<Id>  idList= new Set<Id>();
            for(Account acc:resAccListForInvoicing)
            {
             idList.add(acc.id);  
                
            }
           AccNonInvoicingNogenerator.SendHttpsreq(idList);
            system.assert(idList.size()>0);
          test.stopTest();
        }
    }
}