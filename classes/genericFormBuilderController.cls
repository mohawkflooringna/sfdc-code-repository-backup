public class genericFormBuilderController {
    
    @AuraEnabled
    public static formWrapper getFields( string objectName,string identifierStr ){
        List<fieldsWrapper>returnList = new List<fieldsWrapper>();
        for( Generic_Form__mdt GF : [ SELECT MasterLabel,( SELECT MasterLabel,Field_API_Name__c,Field_Type__c,isRequired__c,Sequence__c FROM Generic_Form_Fields__r WHERE Identifier__c =: identifierStr AND isShow__c =: true) FROM Generic_Form__mdt WHERE Object_API_Name__c =: objectName ] ){
            for( Generic_Form_Field__mdt GFF : GF.Generic_Form_Fields__r ){
                if( GFF.Field_Type__c == 'Picklist' ){
                	List<Map<String, Object>> options = new List<Map<String, Object>>();
                    for ( Schema.PickListEntry option : getPicklistValues( GFF.Field_API_Name__c,objectName ) ) {
                        options.add(
                        	new Map<String, Object>{
                            	'value' => option.getValue(),
                                'label' => option.getLabel()
                            }
                        );
                    }
                    returnList.add( new fieldsWrapper( GFF.Field_Type__c,GFF.MasterLabel,GFF.isRequired__c,GFF.Field_API_Name__c,GFF.Sequence__c,options ) );                
                }else{
                    returnList.add( new fieldsWrapper( GFF.Field_Type__c,GFF.MasterLabel,GFF.isRequired__c,GFF.Field_API_Name__c,GFF.Sequence__c,null ) );            
                }
                
            }            
        }
        formWrapper FW = new formWrapper();
        returnList.sort();
        FW.fields = returnList;
        FW.obj = getRecord( objectName );
        return FW;
    }
    
    private static SObject getRecord( String objectName ) {
        Schema.SObjectType objectType = Schema.getGlobalDescribe().get( objectName );
        return objectType.newSObject();
    }
    
    /*************************************************************************************
	This method is use to get the picklist value in list of string.
	*************************************************************************************/
	public static List<Schema.PickListEntry> getPicklistValues( string fieldName,string objectName ){
		//Map<string,string>returnMap = new Map<string,string>();
		Map<String,Schema.SObjectType> sObjectTypeMap = Schema.getGlobalDescribe();
		Schema.DescribeSObjectResult objectMetadata = sObjectTypeMap.get( objectName ).getDescribe();
		Map<String, Schema.SObjectField> fieldsMap = objectMetadata.fields.getMap();
		/*for ( Schema.PickListEntry entry : fieldsMap.get(fieldName).getDescribe().getPickListValues() ) {
			returnMap.put(entry.getValue(),entry.getLabel());
		}
		return returnMap;*/
        return fieldsMap.get(fieldName).getDescribe().getPickListValues();
	}
    
    public class formWrapper{
        @AuraEnabled
        public List<fieldsWrapper>fields;
        
        @AuraEnabled
        public SObject obj;
    }
    
    public class fieldsWrapper implements Comparable{
        @AuraEnabled
        public string type;
        @AuraEnabled
        public string label;
        @AuraEnabled
        public boolean required;
        @AuraEnabled
        public string APIName;
        @AuraEnabled
        public decimal sequence;
        @AuraEnabled
        public List<Map<String, Object>>options;
            
        public fieldsWrapper( string Type,string Label,boolean Required,string APIName,decimal Sequence,List<Map<String, Object>> o){
            this.type = Type;
            this.label = Label;
            this.required = Required;
            this.APIName = APIName;
            this.sequence = Sequence;
            if( o != null )
            	this.options = o;
        }
        
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(sequence - ((fieldsWrapper)objToCompare).sequence);               
        }
    }
    
    
    
}