@isTest
private class TerritoryCheckMohawAccTeam_Batch_Test {
   /*
      static List<Account> resAccListForInvoicing;
    static List<Territory__c> terrList;
    static User userList;
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static List<Mohawk_Account_Team__c> mohAccTeam1;
    static List<Action_List__c> actListForResidentialFL;
    static List<Action_List__c> actListForResidentialSAL;
    static List<Action_List__c> actListForResidential = new List<Action_List__c>();
    static List<SAL_Territory__c>  salTerr;
    static List<Territory_User__c>  terrUser;
    static List<Territory_User__c>  terrUser1;

    public static void init(){   
     Utility_Test.getTeamCreationCusSetting();
     Utility_Test.getDefaultConfCusSetting();
    }

     static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
          test.startTest(); 
                init();
                resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 2);

                terrList = Utility_Test.createTerritories(false, 2);

                 for(Integer i=0;i<terrList.size();i++){
                 	terrList[i].Name = 'Testterr'+i;
                    terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
                }

                insert terrList;

                for(Integer i=0;i<resAccListForInvoicing.size();i++){
                    resAccListForInvoicing[i].Chain_Name__c = 'ABCD123456';
                    resAccListForInvoicing[i].Business_Type__c = 'Residential';
                    resAccListForInvoicing[i].Territory__c = terrList[0].Id;
                }
                insert resAccListForInvoicing;

                terrUser = Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,2);  
               
 mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 2, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER);
                for(Integer i=0;i<mohAccTeam.size();i++){
                    mohAccTeam[i].Account_Access__c = 'Edit';
                    mohAccTeam[i].Source_CAMS__c = true;
                    
                    mohAccTeam[i].Account__c=resAccListForInvoicing[0].id;
                    
                    //mohAccTeam[i].User__c = Utility_Test.ADMIN_USER.Id;
                   mohAccTeam[i].RecordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL+' '+UtilityCls.INVOICING_ACCOUNT).getRecordTypeId();
                }
                insert mohAccTeam;

               system.debug('mohAccTeam !!!!'+mohAccTeam);
             TerritoryCheckMohawAccTeam_Batch  cls = new TerritoryCheckMohawAccTeam_Batch ();

            database.executeBatch(cls,10);

            TerritoryCheckPublicGroup_Batch clss = new TerritoryCheckPublicGroup_Batch();
              database.executeBatch(clss,10);
            test.stopTest();
        }
    }
static testMethod void testMethod3() {
        System.runAs(Utility_Test.ADMIN_USER) {
          test.startTest(); 
                init();
                resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 2);

                terrList = Utility_Test.createTerritories(false, 2);

                 for(Integer i=0;i<terrList.size();i++){
                 	terrList[i].Name = 'Testterr'+i;
                    terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
                }

                insert terrList;

                for(Integer i=0;i<resAccListForInvoicing.size();i++){
                    resAccListForInvoicing[i].Chain_Name__c = 'ABCD123456';
                    resAccListForInvoicing[i].Business_Type__c = 'Commercial';
                    resAccListForInvoicing[i].Territory__c = terrList[0].Id;
                    
                }
                insert resAccListForInvoicing;

                terrUser = Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,2);  
               
 mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 2, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER);
                for(Integer i=0;i<mohAccTeam.size();i++){
                    mohAccTeam[i].Account_Access__c = 'Edit';
                    mohAccTeam[i].Source_CAMS__c = true;
                    
                    mohAccTeam[i].Account__c=resAccListForInvoicing[0].id;
                    
                    //mohAccTeam[i].User__c = Utility_Test.ADMIN_USER.Id;
                   mohAccTeam[i].RecordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL+' '+UtilityCls.INVOICING_ACCOUNT).getRecordTypeId();
                }
                insert mohAccTeam;

               system.debug('mohAccTeam !!!!'+mohAccTeam);
             TerritoryCheckMohawAccTeam_Batch  cls = new TerritoryCheckMohawAccTeam_Batch ();

            database.executeBatch(cls,10);

            TerritoryCheckPublicGroup_Batch clss = new TerritoryCheckPublicGroup_Batch();
              database.executeBatch(clss,10);
            test.stopTest();
        }
    }

    static testMethod void testMethod2() {
        System.runAs(Utility_Test.ADMIN_USER) {
          test.startTest(); 
                init();
                resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);

                 terrList = Utility_Test.createTerritories(false, 1);

				      for(Integer i=0;i<terrList.size();i++){
                 	terrList[i].Name = 'Testterr'+i;
                    terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
                }

                insert terrList;

                for(Integer i=0;i<resAccListForInvoicing.size();i++){
                    resAccListForInvoicing[i].Chain_Name__c = '123456ABCD';
                    resAccListForInvoicing[i].Business_Type__c = 'Residential';
                    resAccListForInvoicing[i].Territory__c = terrList[i].Id;
                }
                insert resAccListForInvoicing;
              
                mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER);
                for(Integer i=0;i<mohAccTeam.size();i++){
                    mohAccTeam[i].Account_Access__c = 'Edit';
                    mohAccTeam[i].Source_CAMS__c = true;
                    //mohAccTeam[i].User__c = Utility_Test.ADMIN_USER.Id;
                   mohAccTeam[i].RecordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL+' '+UtilityCls.INVOICING_ACCOUNT).getRecordTypeId();
                }
                insert mohAccTeam;

               system.debug('mohAccTeam !!!!'+mohAccTeam);

             TerritoryCheckMohawAccTeam_Batch  cls = new TerritoryCheckMohawAccTeam_Batch ();
            database.executeBatch(cls,10);

             TerritoryCheckPublicGroup_Batch clss = new TerritoryCheckPublicGroup_Batch();
              database.executeBatch(clss,10);
              
            test.stopTest();
        }
    } */
}