/**************************************************************************

Name : OpportunityTriggerHelper

===========================================================================
Purpose : This class is used for Opportunity Trigger actions specific to Construct Connect processes
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mohsin         21/Aug/2018    Created  

***************************************************************************/
public without sharing class OpportunityTriggerHelper {
    
    public static Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe(); 
    public static void createProjectRelatedAccounts(Map<Id, Opportunity> newListMap)
    {
        Map<Id,Id> mapOfProjectIdAndOppId = new Map<Id,Id>();
        Set<Id> accountAlreadyConverted = new Set<Id>();
        for(Opportunity opp : newListMap.values())
        {
            if(opp.rdcc__Project__c != NULL)
            {
                mapOfProjectIdAndOppId.put(opp.rdcc__Project__c,opp.Id);                
            }
            System.debug('Account ID>>>'+opp.AccountId);
            accountAlreadyConverted.add(opp.AccountId);
        }
        List<Related_Account__c> newRelatedAccounts = new List<Related_Account__c>();
        List<String> allCompaniesIds = new List<String>(); //Company External Ids
        for(rdcc__ReedProject__c project : [select id,(select id,rdcc__Insight_Company__c,rdcc__Account__c,rdcc__CMD_Comapany_Id__c from R00N40000001LLaZEAW__r WHERE rdcc__Primary_Contact__c != NULL) from rdcc__ReedProject__c WHERE id IN :mapOfProjectIdAndOppId.keySet()])
        {
            if(!project.R00N40000001LLaZEAW__r.isEmpty() && project.R00N40000001LLaZEAW__r.size() > 0)
            {
                for(rdcc__ProjectCompanyRole__c companyRole : project.R00N40000001LLaZEAW__r)
                {
                    if(companyRole.rdcc__Account__c != NULL && !accountAlreadyConverted.contains(companyRole.rdcc__Account__c))
                    {
                        System.debug('Inside of it');
                        Related_Account__c relatedAccount = new Related_Account__c();
                        relatedAccount.Account__c = companyRole.rdcc__Account__c;
                        relatedAccount.Opportunity__c = mapOfProjectIdAndOppId.get(project.Id);
                        newRelatedAccounts.add(relatedAccount);
                    }
                    else if(companyRole.rdcc__Insight_Company__c != NULL)
                    {
                        allCompaniesIds.add(companyRole.rdcc__CMD_Comapany_Id__c);
                    }   
                }
            }
        }
        System.debug('Companies to be converted>>>'+allCompaniesIds);
        List<rdcc__Insight_Company__c> insComps = new List<rdcc__Insight_Company__c>([select id from rdcc__Insight_Company__c where rdcc__Company_ID__c =: allCompaniesIds]);
        Map<String,String> convertedAccountId = convertAccount(allCompaniesIds);
        DataConvertHelper.afterCompanyConversion(insComps,convertedAccountId);
        for(rdcc__ProjectCompanyRole__c companyRole : [select id,rdcc__Account__c,rdcc__CMD_Comapany_Id__c,rdcc__Project__c from rdcc__ProjectCompanyRole__c WHERE rdcc__Account__c IN :convertedAccountId.values()])
        {
            if(mapOfProjectIdAndOppId.containsKey(companyRole.rdcc__Project__c))
            {
                Related_Account__c relatedAccount = new Related_Account__c();
                relatedAccount.Account__c = companyRole.rdcc__Account__c;
                System.debug('Project Id:'+companyRole.rdcc__Project__c);
                relatedAccount.Opportunity__c = mapOfProjectIdAndOppId.get(companyRole.rdcc__Project__c);
                newRelatedAccounts.add(relatedAccount);
            }
        }
        System.debug('newRelatedAccounts>>>>'+newRelatedAccounts);
        insert newRelatedAccounts;
    }
    
    public static Map<String,String> convertAccount(List<String> allInsComIds){
        List<Account> accList=new List<Account>();
        List<Account> accListFoundInSystem = new List<Account>([select id,rdcc__Company_ID__c,OwnerId, name from account where rdcc__Company_ID__c =: allInsComIds]);
        List<rdcc__Insight_Company__c> companies = new List<rdcc__Insight_Company__c>([select id,OwnerId, rdcc__Is_Converted__c,rdcc__Last_Updated__c,name,rdcc__Company_ID__c,rdcc__Fax__c,rdcc__Parent_Role__c,rdcc__Phone__c,rdcc__Role__c,rdcc__Shipping_City__c,rdcc__Shipping_Country__c,rdcc__Shipping_County__c,rdcc__Shipping_Postal_Code__c,rdcc__Shipping_State__c,rdcc__Shipping_Street__c,rdcc__Type__c,rdcc__URL__c from rdcc__Insight_Company__c where rdcc__Company_ID__c =: allInsComIds]);
        Map<String,Id> accIdCompId = new Map<String,Id>();
        String insightUpdatesValue = 'Yes';
        Map<String,String> accountCustomMandatoryFieldMap = getMandatoryFields('Account');
        //rdcc.UtilityClass.getMandatoryFields('Account');
        for(Account acc : accListFoundInSystem)
        {
            accIdCompId.put(acc.rdcc__Company_ID__c,String.valueOf(acc.Id));
        }
        if(accIdCompId.size() == allInsComIds.size())
        {
            return accIdCompId;
        }
        else
        {
            for(rdcc__Insight_Company__c insCom : companies)
            {
                Account a = mapAccountFields(insCom, insightUpdatesValue);
                accList.add(a);
            }
        }
        
        if(!accList.isEmpty())
        {          
            Database.DMLOptions dml=new Database.DMLOptions();
            dml.DuplicateRuleHeader.AllowSave=true;            
            Database.UpsertResult[] srList = Database.upsert(accList,Account.rdcc__Company_ID__c,false);
            for(Account a : accList)
            {
                accIdCompId.put(a.rdcc__Company_ID__c,String.valueOf(a.Id));
            }            
        }
        //System.debug('*****Inside Convert Account Method******');
        return accIdCompId;
    }
    
    public static Account mapAccountFields(rdcc__Insight_Company__c insComp, String insightUpdatesValue)
    {
        rdcc__Configurable_Settings__c config = accessConfigurableSettings();
        Account a=new Account();
        if(insComp == null){
            return a;
        }
        
        a.Name=insComp.Name; 
        a.OwnerId = UserInfo.getUserId();
        a.rdcc__Company_ID__c=insComp.rdcc__Company_ID__c;
        a.rdcc__URL__c=insComp.rdcc__URL__c;
        a.Fax=insComp.rdcc__Fax__c;
        a.Phone=insComp.rdcc__Phone__c;
        a.BillingCity=insComp.rdcc__Shipping_City__c;
        a.ShippingCity=insComp.rdcc__Shipping_City__c;
        
        //Changed for CRM-163
        //a.Type=insComp.rdcc__Type__c;
        a.rdcc__Construct_Connect_Type__c = insComp.rdcc__Type__c;
        
        
        if(isStateCountryPicklistEnabled()){            
            if(getCountryName(insComp.rdcc__Shipping_Country__c) != 'NOT FOUND'){                
                a.put('BillingCountryCode',getCountryName(insComp.rdcc__Shipping_Country__c));                
                a.put('ShippingCountryCode',getCountryName(insComp.rdcc__Shipping_Country__c));
                if(getStateName(insComp.rdcc__Shipping_State__c) != 'NOT FOUND'){
                    a.put('BillingStateCode',getStateName(insComp.rdcc__Shipping_State__c));
                    a.put('ShippingStateCode',getStateName(insComp.rdcc__Shipping_State__c));
                }else{
                    //rdcc.UtilityClass.addMessageToPage('ERROR', 'State is Invalid');
                }
            }else{
                //rdcc.UtilityClass.addMessageToPage('ERROR', 'Country is Invalid');
            }
        }else{
            a.BillingCountry=insComp.rdcc__Shipping_Country__c;
            a.BillingState=insComp.rdcc__Shipping_State__c;
            a.shippingCountry=insComp.rdcc__Shipping_Country__c;
            a.shippingState=insComp.rdcc__Shipping_State__c;
        }
        
        a.BillingStreet=insComp.rdcc__Shipping_Street__c;
        a.shippingStreet=insComp.rdcc__Shipping_Street__c;
        a.BillingPostalCode=insComp.rdcc__Shipping_Postal_Code__c;
        a.ShippingPostalCode=insComp.rdcc__Shipping_Postal_Code__c;
        a.rdcc__Billing_County__c=insComp.rdcc__Shipping_County__c;
        
        if(insightUpdatesValue!= null && insightUpdatesValue =='Yes'){
            a.rdcc__Allow_Future_ConstructConnect_Updates__c= true;
        }
        else if(config.rdcc__Allow_Future_Updates_to_Account_Contact__c != null && config.rdcc__Allow_Future_Updates_to_Account_Contact__c == 'Yes')
        {
            a.rdcc__Allow_Future_ConstructConnect_Updates__c = TRUE;
        }
        else{
            a.rdcc__Allow_Future_ConstructConnect_Updates__c = false;
        }
        
        if(config.rdcc__Account_Record_Type__c != null && config.rdcc__Account_Record_Type__c != '' && config.rdcc__Account_Record_Type__c != '--NONE--'){
            List<SelectOption> recTypes = getRecordTypeMap('Account');
            Set<id> recordTypeIds = getAllowedRecordTypeIds('Account');
            if(recTypes.size()>1){
                if(recordTypeIds.contains(config.rdcc__Account_Record_Type__c))
                    a.put('recordTypeId',config.rdcc__Account_Record_Type__c);
            }
        }
        return a;
    }
    
    
    //Utility class 
    public static boolean isStateCountryPicklistEnabled(){
        boolean isStateCountryPicklistEnabled;
        if(isStateCountryPicklistEnabled == null){
            isStateCountryPicklistEnabled = getFieldMap('Account').containsKey('billingstatecode');
        }
        return isStateCountryPicklistEnabled;
    }
    
    
    public static String getCountryName(String country){
        String NOTFOUND = 'NOT FOUND';
        if(String.isNotBlank(country))
        {
            Map<String,String> countryMap;
            if(countryMap == null ){            
                countryMap =  new Map<String,String>();
                Map<String,Schema.SObjectField> objMap = getFieldMap('Account');            
                Schema.DescribeFieldResult fieldResult;
                if(objMap.get('billingcountrycode')!=null)
                {
                    fieldResult = objMap.get('billingcountrycode').getDescribe();
                    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();            
                    for( Schema.PicklistEntry f : ple){                
                        countryMap.put(f.getLabel().toUpperCase(),f.getValue().toUpperCase());
                    }      
                }            
            }
            
            if(countryMap != null && countryMap.containsKey(country.toUpperCase() )){
                return countryMap.get(country.toUpperCase());
            }else if(countryMap != null && new Set<String>(countryMap.values()).contains(country.toUpperCase())){            
                return country;            
            }
        }
        return NOTFOUND;        
    }
    
    public static Set<Id> getAllowedRecordTypeIds(string obj)
    {
        Set<Id> recIds = new Set<Id>();
        for(RecordTypeInfo info: schemaMap.get(obj).getDescribe().getRecordTypeInfos()) {
            if(info.isAvailable()) {
                recIds.add(info.getRecordTypeId());
            }
        }
        return recIds;
    }
    
    public static List<SelectOption> getRecordTypeMap(String obj){        
        List<SelectOption> recordTypeList = new List<SelectOption>();
        recordTypeList.add(new SelectOption('','--NONE--'));
        List<Schema.RecordTypeInfo> recordTypes = schemaMap.get(obj).getDescribe().getRecordTypeInfos();
        if(recordTypes != null && !recordTypes.isEmpty()){
            for(Schema.RecordTypeInfo eachRT:recordTypes){ 
                if(!eachRT.isMaster()){
                    recordTypeList.add(new SelectOption(eachRT.getRecordTypeId(), eachRT.getName()));
                }
            }                      
        }
        return recordTypeList; 
    }
    public static Map<String,String> getMandatoryFields(String sObjectName){
        
        Map<String,String> fieldApiNmLabelMap = new Map<String,String>();       
        for(Schema.SObjectField objField:getFieldMap(sObjectName).values()){
            Schema.DescribeFieldResult dfr = objField.getDescribe();
            if(!dfr.isNillable() && dfr.isCustom() && dfr.getType().name() != 'Boolean' && !dfr.isAutoNumber() && !dfr.isCalculated()){
                fieldApiNmLabelMap.put(dfr.getName(), dfr.getLabel());
            }
        }
        return fieldApiNmLabelMap;
    }
    
    public static String getStateName(String state){
        String NOTFOUND = 'NOT FOUND';
        if(String.isNotBlank(state))
        {
            Map<String,String> stateMap;        
            if(stateMap == null ){
                stateMap =  new Map<String,String>();
                Schema.DescribeFieldResult fieldResult;
                Map<String,Schema.SObjectField> objMap1 = getFieldMap('Account');
                if(getFieldMap('Account').get('billingstatecode')!=null)
                {
                    fieldResult = objMap1.get('billingstatecode').getDescribe();
                    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();            
                    for( Schema.PicklistEntry f : ple){                
                        stateMap.put(f.getLabel().toUpperCase() ,f.getValue().toUpperCase());
                        //System.debug(f.getLabel()+'-->'+f.getValue());
                    }    
                }
                
            }
            //System.debug('***state'+state);
            if(stateMap.containsKey(state.toUpperCase())){
                //System.debug('stateMap.get(state.toUpperCase())'+stateMap.get(state.toUpperCase()));
                return stateMap.get(state.toUpperCase());
            }else if(new Set<String>(stateMap.values()).contains(state.toUpperCase())){            
                return state;            
            }
        }
        return NOTFOUND;
    }
    public static Map<String,Schema.SObjectField> getFieldMap(String sObj){       
        return schemaMap.get(sObj).getDescribe().fields.getMap();
    }
    
    public static rdcc__Configurable_Settings__c  accessConfigurableSettings(){        
        String SobjectApiName = 'rdcc__Configurable_Settings__c';
        List<rdcc__Configurable_Settings__c> cs =  (List<rdcc__Configurable_Settings__c>)queryAllFields(SobjectApiName);
        if(cs!= null && !cs.isEmpty()){
            return cs[0];
        }
        return new rdcc__Configurable_Settings__c();
    }
    
    
    //query all fields of accepted sobject.
    public static List<SObject> queryAllFields(String SobjectApiName){
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        try{
            String commaSepratedFields = '';
            for(String fieldName : fieldMap.keyset()){
                if(fieldName.endsWith('__c')){
                    if(commaSepratedFields == null || commaSepratedFields == ''){
                        commaSepratedFields = fieldName;
                    }else{
                        commaSepratedFields = commaSepratedFields + ', ' + fieldName;
                    }
                }
            }        
            String query = 'select ' + commaSepratedFields + ' from ' + SobjectApiName + ' Limit 1';        
            List<SObject> config = Database.query(query); 
            
            return config;  
        }Catch(Exception e){
            System.debug('***Error Message:'+e.getMessage());
            //UtilityClass.addMessageToPage(e.getMessage(),'ERROR');
            return null;
        }
    } 
    //Utility class ends
    
    
}