@isTest
private class Rest_MATDeleteFromCAMS_TEST{
    
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static List<Territory__c> terrList;
    static Id resiNonInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Non-Invoicing');
    static Id commRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Commercial');
    static Id territoryResidentialId = UtilityCls.getRecordTypeInfo('Territory_User__c', 'Residential');
    static List<Account> resAccListForInvoicing;
    
    private static testmethod void testMethodFirst() {
        Test.startTest();
        
        resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        terrList = Utility_Test.createTerritories(true, 2);
        //List<Territory_User__c> terrUserList=Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,1);
        system.debug('territory'+terrList);
        List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.75299', false); 
        mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiNonInvRTId, accProfile, 'Horizon');
        for(Integer i=0;i<mohAccTeam.size();i++){
            mohAccTeam[i].External_Id__c = 'Test Cams Delete';
            mohAccTeam[i].Territory_User_Number__c = '05-787';
        }
        insert mohAccTeam;
        Mohawk_Account_Team__c testRecordAccTeam =  mohAccTeam[0];
        
        Rest_MATDeleteFromCAMS.response res1 = new Rest_MATDeleteFromCAMS.response();        
        res1 = Rest_MATDeleteFromCAMS.MATDelete(testRecordAccTeam);
        
        system.assertnotequals(res1.success, null);
        
        Test.stopTest();        
    }
    
    private static testmethod void testMethodSecond() {
        Test.startTest();
        
        resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        terrList = Utility_Test.createTerritories(true, 2);
        //List<Territory_User__c> terrUserList=Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,1);
        system.debug('territory'+terrList);
        List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.75299', false); 
        mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiNonInvRTId, accProfile, 'Horizon');
        for(Integer i=0;i<mohAccTeam.size();i++){
            mohAccTeam[i].External_Id__c = 'Test Cams Delete';
            mohAccTeam[i].Territory_User_Number__c = '05-787';
        }
        insert mohAccTeam;
        Mohawk_Account_Team__c testRecordAccTeam =  mohAccTeam[0];
        
        List<Territory_User__c> terrUserList = Utility_Test.createTerritoryWithUsers( false,terrList,Utility_Test.COMMERCIAL_USER,1 );
        for(Integer i = 0; i < terrUserList.size(); i++){
            terrUserList[i].Name = '05-787';
            terrUserList[i].RecordtypeID = UtilityCls.getRecordTypeInfo('Territory_User__c', 'Residential');
            terrUserList[i].User__c = Utility_Test.COMMERCIAL_USER.id;
        }
        insert terrUserList;
        
        Rest_MATDeleteFromCAMS.response res1 = new Rest_MATDeleteFromCAMS.response();        
        res1 = Rest_MATDeleteFromCAMS.MATDelete(testRecordAccTeam);
        
        system.assertnotequals(res1.success, null);
        
        Test.stopTest();        
    }
    
    private static testmethod void testMethodThird() {
        Test.startTest();
        
        resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        terrList = Utility_Test.createTerritories(true, 2);
        //List<Territory_User__c> terrUserList=Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,1);
        system.debug('territory'+terrList);
        List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.75299', false); 
        mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiNonInvRTId, accProfile, 'Horizon');
        for(Integer i=0;i<mohAccTeam.size();i++){
            mohAccTeam[i].External_Id__c = 'Test Cams Delete';
            mohAccTeam[i].Territory_User_Number__c = '05-787';
        }
        insert mohAccTeam;
        Mohawk_Account_Team__c testRecordAccTeam =  mohAccTeam[0];
        
        List<Territory_User__c> terrUserList = Utility_Test.createTerritoryWithUsers( false,terrList,Utility_Test.COMMERCIAL_USER,1 );
        for(Integer i = 0; i < terrUserList.size(); i++){
            terrUserList[i].Name = '05-787';
            terrUserList[i].RecordtypeID = UtilityCls.getRecordTypeInfo('Territory_User__c', 'Residential');
            terrUserList[i].User__c = null;
        }
        insert terrUserList;
        
        Rest_MATDeleteFromCAMS.response res1 = new Rest_MATDeleteFromCAMS.response();        
        res1 = Rest_MATDeleteFromCAMS.MATDelete(testRecordAccTeam);
        
        system.assertnotequals(res1.success, null);
        
        Test.stopTest();        
    }
    
    private static testmethod void testMethodFourth() {
        Test.startTest();
        
        resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        terrList = Utility_Test.createTerritories(true, 2);
        //List<Territory_User__c> terrUserList=Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,1);
        system.debug('territory'+terrList);
        List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.75299', false); 
        mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiNonInvRTId, accProfile, 'Horizon');
        for(Integer i=0;i<mohAccTeam.size();i++){
            mohAccTeam[i].External_Id__c = 'Test Cams Delete';
            mohAccTeam[i].Territory_User_Number__c = '05-787;06-898;06-090';
        }
        insert mohAccTeam;
        Mohawk_Account_Team__c testRecordAccTeam =  mohAccTeam[0];
        testRecordAccTeam .Territory_User_Number__c = '06-898';
        
        List<Territory_User__c> terrUserList = Utility_Test.createTerritoryWithUsers( false,terrList,Utility_Test.COMMERCIAL_USER,1 );
        for(Integer i = 0; i < terrUserList.size(); i++){
            terrUserList[i].Name = '05-787';
            terrUserList[i].RecordtypeID = UtilityCls.getRecordTypeInfo('Territory_User__c', 'Residential');
            terrUserList[i].User__c = null;
        }
        insert terrUserList;
        
        Rest_MATDeleteFromCAMS.response res1 = new Rest_MATDeleteFromCAMS.response();        
        res1 = Rest_MATDeleteFromCAMS.MATDelete(testRecordAccTeam);
        
        system.assertnotequals(res1.success, null);
        
        Test.stopTest();        
    }
    
    private static testmethod void testMethodFiveth() {
        Test.startTest();
        
        resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        terrList = Utility_Test.createTerritories(true, 2);
        //List<Territory_User__c> terrUserList=Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,1);
        system.debug('territory'+terrList);
        List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.75299', false); 
        mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiNonInvRTId, accProfile, 'Horizon');
        for(Integer i=0;i<mohAccTeam.size();i++){
            mohAccTeam[i].External_Id__c = 'Test Cams Delete';
            mohAccTeam[i].Territory_User_Number__c = null;
        }
        insert mohAccTeam;
        Mohawk_Account_Team__c testRecordAccTeam =  mohAccTeam[0];
        
        Rest_MATDeleteFromCAMS.response res1 = new Rest_MATDeleteFromCAMS.response();        
        res1 = Rest_MATDeleteFromCAMS.MATDelete(testRecordAccTeam);
        
        system.assertnotequals(res1.success, null);
        
        Test.stopTest();        
    }
    
    private static testmethod void testMethodSixth() {
        Test.startTest();
        
        resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        terrList = Utility_Test.createTerritories(true, 2);
        //List<Territory_User__c> terrUserList=Utility_Test.createTerritoryWithUsers(true,terrList,Utility_Test.RESIDENTIAL_USER,1);
        system.debug('territory'+terrList);
        List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.75299', false); 
        mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiNonInvRTId, accProfile, 'Horizon');
        for(Integer i=0;i<mohAccTeam.size();i++){
            mohAccTeam[i].External_Id__c = 'Test Cams Delete';
            mohAccTeam[i].Territory_User_Number__c = null;
            mohAccTeam[i].User__c = null;
        }
        insert mohAccTeam;
        Mohawk_Account_Team__c testRecordAccTeam =  mohAccTeam[0];
        
        Rest_MATDeleteFromCAMS.response res1 = new Rest_MATDeleteFromCAMS.response();        
        res1 = Rest_MATDeleteFromCAMS.MATDelete(testRecordAccTeam);
        
        system.assertnotequals(res1.success, null);
        
        Test.stopTest();        
    }
    
    private static testmethod void testMethodSeventh() {
        Test.startTest();
        
        resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        terrList = Utility_Test.createTerritories(true, 1);
        List<Territory_User__c> terrUserList=Utility_Test.createTerritoryWithUsers(false,terrList,Utility_Test.RESIDENTIAL_USER,1);
        User testuser = Utility_Test.getTestUser('ResSalesOpps','Residential Sales Manager');
        insert testuser;
        for(Integer i = 0; i < terrUserList.size(); i++){
            terrUserList[i].Name = '05-787';
            terrUserList[i].RecordtypeID = UtilityCls.getRecordTypeInfo('Territory_User__c', 'Residential');
            terrUserList[i].User__c = testuser.id;
        }
        insert terrUserList;
        
        system.debug('territory'+terrUserList);
        List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.75299', false); 
  
        mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiNonInvRTId, accProfile, 'Horizon');
        for(Integer i=0;i<mohAccTeam.size();i++){
            mohAccTeam[i].External_Id__c = 'R.134233.0012_34324';
            mohAccTeam[i].Territory_User_Number__c = '05-787';
            mohAccTeam[i].User__c = Utility_Test.COMMERCIAL_USER.id;
        }
    
        insert mohAccTeam;
        Mohawk_Account_Team__c testRecordAccTeam =  mohAccTeam[0];
     
        Rest_MATDeleteFromCAMS.response res = new Rest_MATDeleteFromCAMS.response(); 
        res = Rest_MATDeleteFromCAMS.MATDelete(testRecordAccTeam);
        system.assertnotequals(res.success, null);
        
        testRecordAccTeam.External_Id__c = null;
        Rest_MATDeleteFromCAMS.response res1 = new Rest_MATDeleteFromCAMS.response();        
        res1 = Rest_MATDeleteFromCAMS.MATDelete(testRecordAccTeam);
        
        system.assertnotequals(res1.success, null);
        
        Test.stopTest();        
    }
    
    
    private static testmethod void testMethodCommericial() {
        Test.startTest();
           
        List<Account> commAccListForInvoicing = Utility_Test.createCommercialAccountsForInvoicing(true, 1);
        terrList = Utility_Test.createTerritories(true, 1);
        mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, commAccListForInvoicing , terrList, Utility_Test.COMMERCIAL_USER, CommRTId );
        for(Integer i=0;i<mohAccTeam.size();i++){
            mohAccTeam[i].External_Id__c = 'C.32345.0000_3232';
            mohAccTeam[i].User__c = Utility_Test.COMMERCIAL_USER.id;
            mohAccTeam[i].Account__c = commAccListForInvoicing[0].id;
        }
        system.debug('mohAccTeam - ' + mohAccTeam);
        insert mohAccTeam[0];
        Mohawk_Account_Team__c testRecordAccTeam =  mohAccTeam[0];
        
        Rest_MATDeleteFromCAMS.response res1 = new Rest_MATDeleteFromCAMS.response();        
        res1 = Rest_MATDeleteFromCAMS.MATDelete(testRecordAccTeam);
        
        system.assertnotequals(res1.success, null);
        
        Test.stopTest();        
    }
}