/**************************************************************************

   Name : OutBoundDeletion

   ===========================================================================
   Purpose : This class is used for Processing Deletes out of SFDC to External
             Systems. Will be called from Triggers for Deleted Objects
   ===========================================================================
   History:
   --------
   VERSION    AUTHOR         DATE             DETAIL          DESCRIPTION
   1.0        Nagendra Singh 20/DEc/2018                        Created         
   
 ***************************************************************************/
public without sharing class OutBoundDeletion {

    public static boolean deletefromAccount = false;

    public static void deletenonDisplayMATs(set < id > accIds) {
        // SFDC Display flag from CAMS - MAT delete from Account ++ 02/28/18
        list < id > matList = new list < id >();
        for(Mohawk_Account_Team__c mat : [select id from mohawk_account_team__c where account__c in: accIds and recordtype.name = 'Residential Invoicing']){
          matList.add(mat.id);
        }
       
        if (!matList.isempty()) {
            List < ApplicationLog__c > appLogList = new List < ApplicationLog__c > ();
            OutBoundDeletion.deletefromAccount = true;
            List < Database.DeleteResult > results = Database.delete(matList , false);
            UtilityCls.createDeleteDMLexceptionlog(null, matList , results, 'OutBoundDeletion', 'deletenonDisplayMATs');
        }
        // SFDC Display flag from CAMS - Sample Display delete from Account
        OutBoundDeletion.deleteNonDisplaySampleInventories(accIds); //Added by - Nagendra 3-3-2019

    }
     public static void deleteNonDisplaySampleInventories( set < id > accIds) {
        
        list < id > inventoryList = new list < id >();
        for(Sample_Display_Inventory__c inventory : [select id from Sample_Display_Inventory__c where account__c in: accIds ]){
          inventoryList.add(inventory.id);
        }
       
        if (!inventoryList.isempty()) {
            List < ApplicationLog__c > appLogList = new List < ApplicationLog__c > ();
            //OutBoundDeletion.deletefromAccount = true;
            List < Database.DeleteResult > results = Database.delete(inventoryList , false);
            UtilityCls.createDeleteDMLexceptionlog(null, inventoryList , results, 'OutBoundDeletion', 'deleteNonDisplaySampleInventries');
        }

    }
    public static void onDeleteMhkAccountTeam(List < Mohawk_Account_Team__c > oldList) {
        List < ApplicationLog__c > appLogList = new List < ApplicationLog__c > ();
        Id resiInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');

        set < Id > accProfileSet = new set < Id > ();
        set < ID > accids = new set < Id > ();
        set < string > twoTeamUsers = new set < string > ();
        set < String > deletedMatChainNumberUserIdSet = new set < String > ();

        list < Deleted_Objects__c > deletedRecordList = new list < Deleted_Objects__c > ();

        if (!OutBoundDeletion.deletefromAccount) {
            for (Mohawk_Account_Team__c mohAccTeam: oldList) {
                if (mohAccTeam.Account_Profile__c != null) {
                    accProfileSet.add(mohAccTeam.Account_Profile__c);
                    deletedMatChainNumberUserIdSet.add(mohAccTeam.Chain_Number_UserId__c);
                    accids.add(mohAccTeam.account__c);
                }

            }

            for (AggregateResult ar: [SELECT count(Id), Chain_Number_UserId__c FROM Mohawk_Account_Team__c
                    WHERE recordtype.name = 'Residential Invoicing'
                    And
                    Account_Profile__r.Two_Sales_Teams__c = true And
                    account__r.SFDC_Display__c = true
                    And Chain_Number_UserId__c IN: deletedMatChainNumberUserIdSet
                    group by Chain_Number_UserId__c having count(id) > = 1
                ]) {

                twoTeamUsers.add(string.valueof(ar.get('Chain_Number_UserId__c')));
            }

            // SFDC Display Logic changes ++ 02/27/18
            map < id, Account > notValidAccs = new map < id, account > ([select id, SFDC_Display__c from account where id in: accids and SFDC_Display__c = false]);

            for (Mohawk_Account_Team__c mohAccTeam: oldList) {
                if ((mohAccTeam.Account_Profile__c == null && mohAccTeam.recordtypeId == resiInvRTId) ||
                    (mohAccTeam.Account_Profile__c != null && !twoTeamUsers.contains(mohAccTeam.Chain_Number_UserId__c))) {
                    if (!notValidAccs.containsKey(mohAccTeam.account__c)) { // SFDC Display Logic changes ++ 02/27/18
                        Deleted_Objects__c obj = new Deleted_Objects__c();
                        obj.ID__c = mohAccTeam.id;
                        obj.External_Id__c = mohAccTeam.External_ID__c;
                        obj.Object_Name__c = 'Mohawk_Account_Team__c';
                        obj.BI_External_Id__c = mohAccTeam.Chain_Number_UserId__c;
                        deletedRecordList.add(obj);
                    }
                }
            }

            OutBoundDeletion.doSave_DeletedObject(deletedRecordList, 'onDeleteMhkAccountTeam');
        }

    }
    public static void onDeleteAccount(List < Account > oldList) {
        Set < Id > accountIds = new Set < Id > ();
        list < Deleted_Objects__c > deletedRecordList = new list < Deleted_Objects__c > ();
        for (Account acc: oldList) {
            if (acc.sfdc_display__c == true) { // SFDC Display Logic changes ++ 02/27/18
                accountIds.add(acc.id); //Code added by - Nagendra 22 -01-2019
            }
            Deleted_Objects__c obj = new Deleted_Objects__c();
            obj.ID__c = acc.id;
            obj.BI_External_Id__c = acc.id;
            obj.External_Id__c = acc.Global_Account_Number__c;
            obj.Object_Name__c = 'Account';
            obj.CAMS_External_Id__c = acc.CAMS_Account_number__c;
            deletedRecordList.add(obj);
        }
        OutBoundDeletion.doSave_DeletedObject(deletedRecordList, 'onDeleteAccount');
        System.debug('AccountIds@@@' + accountIds);
        //start of Code  Nagendra 22 -01-2019 
        DateTime startTime = System.now();
        DateTime endTime = System.now().addMinutes(-1);
        List < Mohawk_Account_Team__c > toBeDeletedMhkTeam = new List < Mohawk_Account_Team__c > ();
        toBeDeletedMhkTeam = [select id, Account__c, name, Account_Profile__c, Chain_Number_UserId__c, recordtypeId, External_ID__c
            from Mohawk_Account_Team__c where Account__c in: accountIds And recordtype.name = 'Residential Invoicing'
            And(LastModifiedDate <=: startTime AND LastModifiedDate >=: endTime) All Rows
        ];
        if (toBeDeletedMhkTeam.size() > 0) {
            OutBoundDeletion.onDeleteMhkAccountTeam(toBeDeletedMhkTeam);
        }

        System.debug('@@@@' + toBeDeletedMhkTeam);
        //End of Code  Nagendra 22 -01-2019 
        //
        //Start of Code - MB - Bug 71191 - 3/4/19
        if(accountIds.size()>0){
            List<Sample_Display_Inventory__c> sampleList = new List<Sample_Display_Inventory__c>();
            try{
                sampleList = [SELECT Id, External_Id__c FROM Sample_Display_Inventory__c WHERE Account__c IN: accountIds 
                              AND (LastModifiedDate <=: startTime AND LastModifiedDate >=: endTime) ALL ROWS];
                System.debug('sampleList: ' + sampleList);
            }
            catch(Exception ex){
                System.debug('Error on fetching Sample Display on Account Delete: ' + ex.getMessage());
            }
            if(sampleList.size()>0){
                onDeleteSampleDisplayInventory(sampleList);
            }
        }
        //End of Code - MB - Bug 71191 - 3/4/19
    }
    public static void onDeleteSampleDisplayInventory(List < Sample_Display_Inventory__c > oldList) {
        list < Deleted_Objects__c > deletedRecordList = new list < Deleted_Objects__c > ();
        for (Sample_Display_Inventory__c sample: oldList) {
            Deleted_Objects__c obj = new Deleted_Objects__c();
            obj.ID__c = sample.id;
            obj.External_Id__c = sample.External_Id__c;
            obj.Object_Name__c = 'Sample_Display_Inventory__c';
            obj.BI_External_Id__c = sample.External_Id__c;
            deletedRecordList.add(obj);
        }
        OutBoundDeletion.doSave_DeletedObject(deletedRecordList, 'onDeleteSampleDisplayInventory');
    }
    public static void doSave_DeletedObject(list < Deleted_Objects__c > deletedRecordList, String SourceFunction) {
        List < ApplicationLog__c > appLogList = new List < ApplicationLog__c > ();
        if (deletedRecordList.size() > 0) {
            Integer count = 0;
            if (Test.isRunningTest()) {
                Deleted_Objects__c TestdeletedRecord = new Deleted_Objects__c(ID__c = '0012323221');
                deletedRecordList.add(TestdeletedRecord);
            }
            try {
                Database.SaveResult[] results = Database.insert(deletedRecordList, false);
                for (Database.SaveResult result: results) {
                    if (result.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully saved record: ' + result.getId());
                    } else {
                        Deleted_Objects__c obj = deletedRecordList.get(count);
                        // Operation failed, so get all errors                
                        for (Database.Error err: result.getErrors()) {
                            System.debug('Error while saving record: ' + err.getMessage());
                            ApplicationLog__c appLog = new ApplicationLog__c();
                            appLog.Message__c = err.getMessage();
                            appLog.Source__c = 'OutBoundDeletion';
                            appLog.SourceFunction__c = SourceFunction;
                            appLog.DebugLevel__c = 'Error';
                            appLog.RecordId__c = obj.ID__c;
                            appLogList.add(appLog);
                        }
                    }
                    count += 1;
                }
            } catch (System.DMLException dmle) {
                for (Integer i = 0; i < dmle.getNumDml(); i++) {
                    system.debug(dmle.getDmlMessage(i));
                    ApplicationLog__c appLog = new ApplicationLog__c();
                    appLog.Message__c = dmle.getMessage();
                    appLog.Source__c = 'OutBoundDeletion';
                    appLog.SourceFunction__c = SourceFunction;
                    appLog.DebugLevel__c = 'Error';
                    appLogList.add(appLog);
                }
            }
        }

        if (appLogList.size() > 0) {
            insert appLogList;
        }
    }
}