/**************************************************************************

Name : createExpandableSectionControllerTest

===========================================================================
Purpose : This tess class is used for createExpandableSectionController
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Radhika       7/June/2018     Created         
***************************************************************************/
@isTest
public class createExpandableSContentControllerTest
{
    public static Account_Profile__c apRec;
    public static Account accRec;
    public static Mohawk_Account_Team__c matRec;
    public static Account_Profile_Settings__c apsRec;
    
    static void dataSetUp()
    {
        
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
        
        //apsRec = new Account_Profile_Settings__c(name='Retail');
        //insert apsRec;
        
        List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
        apSetting[0].Yellow_Threshold_Carpet__c = 10;
        apSetting[0].Red_Threshold_Carpet__c = 10;
        apSetting[0].Yellow_Threshold_Cushion__c = 10;
        apSetting[0].Red_Threshold_Cushion__c = 10;
        apSetting[0].Yellow_Threshold_Hardwood__c = 10;
        apSetting[0].Red_Threshold_Hardwood__c = 10; 
        apSetting[0].Yellow_Threshold_Laminate__c = 10; 
        apSetting[0].Red_Threshold_Laminate__c = 10;
        apSetting[0].Yellow_Threshold_Tile__c = 10;
        apSetting[0].Red_Threshold_Tile__c = 10;
        apSetting[0].Yellow_Threshold_Resilient__c = 10;
        apSetting[0].Red_Threshold_Resilient__c = 10;
        apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
        apSetting[0].Red_Threshold_Mainstreet__c = 10;
        apSetting[0].Multi_Family_Business_Split_2Team__c =10;
        apSetting[0].Retail_Business_Split_2Team__c = 10;
        apSetting[0].Retail_Business_Split_1Team__c = 10;
        apSetting[0].Builder_Business_Split_2_Team__c = 10;
        apSetting[0].Builder_Business_Split_1Team__c = 10;
        apSetting[0].Multi_Family_Business_Split_1team__c = 10;
        apSetting[0].Retail_Multi_channel_split__c =100;
        apSetting[0].Builder_Multi_Family_Multi_Channel_Split__c  =100;

        update apSetting[0];
        
        apRec = new Account_Profile__c();
        apRec.Name = 'test record';
        apRec.Multi_Channel__c = true;
        apRec.Primary_Business__c = apSetting[0].id;
        apRec.Product_Category_Allocation_Carpet__c = 10;
        apRec.Product_Category_Allocation_Cushion__c = 10;
        apRec.Product_Category_Allocation_Wood__c = 10;
        apRec.Product_Category_Allocation_Laminate__c = 10;
        apRec.Product_Category_Allocation_Tile__c = 10;
        apRec.Product_Category_Allocation_Resilient__c = 10;
        apRec.retail__c = 15;
        apRec.builder__c = 15;
        apRec.Multi_Family__c = 15;
        
        insert apRec;
        
        accRec = new Account();
        accRec.name = 'test Account';
        accRec.Account_Profile__c = apRec.id;
        insert accRec;
        
        matRec = new Mohawk_Account_Team__c();
        matRec.account__c=accRec.id;
        matRec.Account_Profile__c = apRec.id;
        insert matRec;
        
    }
    static testMethod void testmethod1()
    {
        dataSetUp();
        List<AP_Section_Fields__mdt> apSectionFieldsmdt = returnAPSectionFieldRecords('Primary_Inputs');
        
        List<createExpandableSectionContentController.blockSectionContentWrapper> testWrapListNew = 
              createExpandableSectionContentController.createBlockSectionsContent(apSectionFieldsmdt,apRec.id);
        
        
        
        
        List<createExpandableSectionController.tableSectionContentWrapper>TSCWList = new List<createExpandableSectionController.tableSectionContentWrapper>();
        createExpandableSectionController.tableSectionContentWrapper TSCW = new createExpandableSectionController.tableSectionContentWrapper();
        TSCW.listOfTotal = [ SELECT DeveloperName,Alignment__c,AP_Section__c,Field_Name__c,isShow__c, Label_L__c,Label_M__c,Label_S__c,Length__c,Sequence__c,Type__c FROM AP_Section_Fields__mdt where AP_Section__r.Section_Object__c = 'Account Profile' LIMIT 1 ];
        TSCW.APSectionMasterLabel = 'TEST';
        TSCW.APSectionMasterSequence = 1;
        TSCW.listOfFields = [ SELECT DeveloperName,Alignment__c,AP_Section__c,Field_Name__c,isShow__c, Label_L__c,Label_M__c,Label_S__c,Length__c,Sequence__c,Type__c FROM AP_Section_Fields__mdt where AP_Section__r.Section_Object__c = 'Account Profile' LIMIT 1 ];
        TSCWList.add( TSCW );
        
        List<createExpandableSectionController.expendableSectionWrapper> testWrapList = new List<createExpandableSectionController.expendableSectionWrapper>();
        createExpandableSectionController.expendableSectionWrapper ESW = new createExpandableSectionController.expendableSectionWrapper();
        ESW.sectionSequence = string.valueof( 1 );
        ESW.sectionHeader = 'Test';
        ESW.sectionDisplayType = 'Table';
        ESW.isSectionDefaultOn = true;
        ESW.isSectionDefaultOn_PHONE = true;
        ESW.isSectionDefaultOn_TABLET = true;
        ESW.isSectionAlwaysShow = true;
        ESW.tableSectionContent = TSCWList;
        testWrapList.add( ESW );
                      
              
              
        List<createExpandableSectionController.tableSectionContentWrapper> tableWrapList = new List<createExpandableSectionController.tableSectionContentWrapper>();
        for(createExpandableSectionController.expendableSectionWrapper ep : testWrapList)
        {
            tableWrapList.addAll(ep.tableSectionContent);
        }
        createExpandableSectionContentController.tableWrapper twp = 
            createExpandableSectionContentController.createTableSectionsContent(JSON.serialize(tableWrapList),apRec.id,'Total Overview');
        
        createExpandableSectionContentController.calculateGreenOpportunity( 10,12 );
        createExpandableSectionContentController.calculateGreenOpportunity( 12,10 );
        
        createExpandableSectionContentController.calculateYellowOpportunity( 12,6,6 );
        createExpandableSectionContentController.calculateYellowOpportunity( 12,11,1 );
        createExpandableSectionContentController.calculateYellowOpportunity( 12,12,1 );
        
        createExpandableSectionContentController.calculateRedOpportunity( 12,12,12,12,12 );
        createExpandableSectionContentController.calculateRedOpportunity( 11,12,5,6,1 );
        createExpandableSectionContentController.calculateRedOpportunity( 13,12,6,7,1 );
        
        createExpandableSectionContentController.isNullChk( null );
        createExpandableSectionContentController.isNullChk( 'TEST' );
        
        createExpandableSectionContentController.rollupOnLoad( apRec.id );
        createExpandableSectionContentController.getAccountProfileId( apRec.id );
        
    }
    static List<AP_Section_Fields__mdt> returnAPSectionFieldRecords(String parentSectionName)
    {
        List<AP_Section_Fields__mdt> apSectionFieldsmdt = [ SELECT DeveloperName,Alignment__c,AP_Section__c,Field_Name__c,isShow__c,
                                      Label_L__c,Label_M__c,Label_S__c,Length__c,Sequence__c,Type__c FROM AP_Section_Fields__mdt where AP_Section__r.DeveloperName = :parentSectionName];
        return apSectionFieldsmdt;
    }
}