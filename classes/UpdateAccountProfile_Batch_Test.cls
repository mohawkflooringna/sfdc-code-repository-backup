/**************************************************************************

Name : UpdateAccountProfile_Batch_Test
===========================================================================
Purpose : Test class for UpdateAccountProfile_Batch
===========================================================================
History:
--------
VERSION    AUTHOR         DATE             DETAIL          DESCRIPTION
1.0        Anand         21/August/2017    Create  
2.0		Susmitha         26/March/2018      Modified
***************************************************************************/
@isTest
private class UpdateAccountProfile_Batch_Test {
    
    static testMethod void testMethod1() {
        List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
        apSetting[0].Yellow_Threshold_Carpet__c = 10;
    	apSetting[0].Red_Threshold_Carpet__c = 10;
    	apSetting[0].Yellow_Threshold_Cushion__c = 10;
    	apSetting[0].Red_Threshold_Cushion__c = 10;
    	apSetting[0].Yellow_Threshold_Hardwood__c = 10;
    	apSetting[0].Red_Threshold_Hardwood__c = 10; 
    	apSetting[0].Yellow_Threshold_Laminate__c = 10; 
    	apSetting[0].Red_Threshold_Laminate__c = 10;
    	apSetting[0].Yellow_Threshold_Tile__c = 10;
    	apSetting[0].Red_Threshold_Tile__c = 10;
    	apSetting[0].Yellow_Threshold_Resilient__c = 10;
    	apSetting[0].Red_Threshold_Resilient__c = 10;
    	apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
    	apSetting[0].Red_Threshold_Mainstreet__c = 10;
    	update apSetting[0];
    	List<Account_Profile__c> apList = new List<Account_Profile__c>();
    	Account_Profile__c ap = new Account_Profile__c();
    	ap.Primary_Business__c = apSetting[0].Id;
    	ap.Annual_Retail_Sales__c = 300000;
    	ap.Annual_Retail_Sales_Non_Flooring__c = 10;
    	ap.Chain_Number__c = '3213311';
    	apList.add(ap);

    	insert apList;

    	List<Territory__c> terrList = Utility_Test.createTerritories(false, 1);
         for(Integer i=0;i<terrList.size();i++){
            terrList[i].Name = 'Testterr'+i;
            terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
         }


    	//List<Account> acList = Utility_Test.createAccounts(true, 1);
    	List<Account> resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
    	 for(Integer i=0;i<resAccListForInvoicing.size();i++){
                    resAccListForInvoicing[i].Business_Type__c = 'Residential';
                     resAccListForInvoicing[i].Chain_Number__c = '3213311';
                     resAccListForInvoicing[i].Account_Profile__c = aplist[i].Id;
        }
        insert resAccListForInvoicing;

         List<Mohawk_Account_Team__c> mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER);
         List<Mohawk_Account_Team__c> mohAccTeam2 = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER);
         for(Integer i=0;i<mohAccTeam2.size();i++){
            mohAccTeam2[i].BMF_Team_Member__c = true;
            //mohAccTeam[i].RecordTypeId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL+' '+UtilityCls.INVOICING_ACCOUNT).getRecordTypeId();
            mohAccTeam.add(mohAccTeam2[i]);
         }

	      insert mohAccTeam;
        system.assert(mohAccTeam.size()>0);
 		List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false);
	     /* if(mohAccTeam[0] != null){
             mohAccTeam[0].BMF_Team_Member__c = true;
	      }
          
          update mohAccTeam[0];*/

           test.startTest();
         
	          UpdateAccountProfile_Batch  cls = new UpdateAccountProfile_Batch ();
              String jobId = database.executeBatch(cls);
              System.assert(jobId != null);
           test.stopTest();

    }
    
    static testMethod void testMethod2() {
        List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
        apSetting[0].Yellow_Threshold_Carpet__c = 10;
    	apSetting[0].Red_Threshold_Carpet__c = 10;
    	apSetting[0].Yellow_Threshold_Cushion__c = 10;
    	apSetting[0].Red_Threshold_Cushion__c = 10;
    	apSetting[0].Yellow_Threshold_Hardwood__c = 10;
    	apSetting[0].Red_Threshold_Hardwood__c = 10; 
    	apSetting[0].Yellow_Threshold_Laminate__c = 10; 
    	apSetting[0].Red_Threshold_Laminate__c = 10;
    	apSetting[0].Yellow_Threshold_Tile__c = 10;
    	apSetting[0].Red_Threshold_Tile__c = 10;
    	apSetting[0].Yellow_Threshold_Resilient__c = 10;
    	apSetting[0].Red_Threshold_Resilient__c = 10;
    	apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
    	apSetting[0].Red_Threshold_Mainstreet__c = 10;
    	update apSetting[0];
    	List<Account_Profile__c> apList = new List<Account_Profile__c>();
    	Account_Profile__c ap = new Account_Profile__c();
    	ap.Primary_Business__c = apSetting[0].Id;
    	ap.Annual_Retail_Sales__c = 300000;
    	ap.Annual_Retail_Sales_Non_Flooring__c = 10;
    	ap.Chain_Number__c = '3213311';
    	apList.add(ap);

    	insert apList;

    	List<Territory__c> terrList = Utility_Test.createTerritories(false, 1);
         for(Integer i=0;i<terrList.size();i++){
            terrList[i].Name = 'Testterr'+i;
            terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
         }


    	//List<Account> acList = Utility_Test.createAccounts(true, 1);
    	List<Account> resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
    	 for(Integer i=0;i<resAccListForInvoicing.size();i++){
                    resAccListForInvoicing[i].Business_Type__c = 'Residential';
                     resAccListForInvoicing[i].Chain_Number__c = '3213311';
                     resAccListForInvoicing[i].Account_Profile__c = aplist[i].Id;
        }
        insert resAccListForInvoicing;

         List<Mohawk_Account_Team__c> mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER);
         List<Mohawk_Account_Team__c> mohAccTeam2 = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER);
        
 		List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false);
	      for(Integer i=0;i<mohAccTeam2.size();i++){
            mohAccTeam2[i].BMF_Team_Member__c = true;
            mohAccTeam2[i].account_profile__c = accProfile[0].Id;
                mohAccTeam2[i].Brands__c = 'Karastan';

            mohAccTeam.add(mohAccTeam2[i]);
         }

	      insert mohAccTeam;

           test.startTest();
         
	          UpdateAccountProfile_Batch  cls = new UpdateAccountProfile_Batch ();
              String jobId = database.executeBatch(cls);
              System.assert(jobId != null);
           test.stopTest();

    }
     static testMethod void testMethod3() {
        List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
        apSetting[0].Yellow_Threshold_Carpet__c = 10;
    	apSetting[0].Red_Threshold_Carpet__c = 10;
    	apSetting[0].Yellow_Threshold_Cushion__c = 10;
    	apSetting[0].Red_Threshold_Cushion__c = 10;
    	apSetting[0].Yellow_Threshold_Hardwood__c = 10;
    	apSetting[0].Red_Threshold_Hardwood__c = 10; 
    	apSetting[0].Yellow_Threshold_Laminate__c = 10; 
    	apSetting[0].Red_Threshold_Laminate__c = 10;
    	apSetting[0].Yellow_Threshold_Tile__c = 10;
    	apSetting[0].Red_Threshold_Tile__c = 10;
    	apSetting[0].Yellow_Threshold_Resilient__c = 10;
    	apSetting[0].Red_Threshold_Resilient__c = 10;
    	apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
    	apSetting[0].Red_Threshold_Mainstreet__c = 10;
    	update apSetting[0];
    	List<Account_Profile__c> apList = new List<Account_Profile__c>();
    	Account_Profile__c ap = new Account_Profile__c();
    	ap.Primary_Business__c = apSetting[0].Id;
    	ap.Annual_Retail_Sales__c = 300000;
    	ap.Annual_Retail_Sales_Non_Flooring__c = 10;
    	ap.Chain_Number__c = '3213311';
    	apList.add(ap);

    	insert apList;

    	List<Territory__c> terrList = Utility_Test.createTerritories(false, 1);
         for(Integer i=0;i<terrList.size();i++){
            terrList[i].Name = 'Testterr'+i;
            terrList[i].RecordTypeId = Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIAL).getRecordTypeId();
         }


    	//List<Account> acList = Utility_Test.createAccounts(true, 1);
    	List<Account> resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
    	 for(Integer i=0;i<resAccListForInvoicing.size();i++){
                    resAccListForInvoicing[i].Business_Type__c = 'Residential';
                     resAccListForInvoicing[i].Chain_Number__c = '3213311';
                     resAccListForInvoicing[i].Account_Profile__c = aplist[i].Id;
        }
        insert resAccListForInvoicing;

         List<Mohawk_Account_Team__c> mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER);
         List<Mohawk_Account_Team__c> mohAccTeam2 = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER);
        
 		List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false);
	      for(Integer i=0;i<mohAccTeam2.size();i++){
            mohAccTeam2[i].BMF_Team_Member__c = true;
            mohAccTeam2[i].account_profile__c = accProfile[0].Id;
                mohAccTeam2[i].Brands__c = null;

            mohAccTeam.add(mohAccTeam2[i]);
         }

	      insert mohAccTeam;

           test.startTest();
         
	          UpdateAccountProfile_Batch  cls = new UpdateAccountProfile_Batch ();
              String jobId = database.executeBatch(cls);
              System.assert(jobId != null);
           test.stopTest();

    }
}