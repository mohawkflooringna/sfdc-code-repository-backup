/**************************************************************************

Name : NonInvoiceGLoablAccNumberMock_Test 

===========================================================================
Purpose :  the right JSON format 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Sasi Naik     18/APR/2017     Created 
***************************************************************************/
@isTest
global class NonInvoiceGLoablAccNumberMock_Test implements HttpCalloutMock{
    
    global HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json;charset=UTF-8');
        res.setBody('{"UniqueId" : "000000123"}' );
        res.setStatus('OK');
        res.setStatusCode(200);
        //res.setBody('{'+'"UniqueId" : "000000123"'+'}' );
        return res;
    }
}