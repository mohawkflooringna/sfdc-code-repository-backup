/*
 * Copyright(c) CMD
 * Author:      Nagarro
 * Date:        May 2, 2017
 * Description: SLDSLookupController component Controller, query and return records for dynamically created lookup fields.
 */
 
public class SLDSLookupController {
    
    public SLDSLookupController (){}
    
    /*
     * public variables
     *
     */
    public String uniqueComponentId{get;set;}
    public String objectLabelPlural{get;set;}
    public String objectName{get;
         set{
             if(objectName != value){
                 objectName = value;
                 System.debug('objectName:'+objectName);
                 objectLabelPlural = Schema.getGlobalDescribe().get(objectName).getDescribe().getLabelPlural();
             }
         }
                             
    }
    
    public String label{
        get;
        set{
            label = value;
            uniqueComponentId = label.replace(' ', '').toLowerCase();
        }
    }
    
    
    
    /**
      * Remote action method to send list of records
      * @param  searchText 
      * @return  List<Wrapper>
      */
      
    @RemoteAction
    public static List<Wrapper> search(String objectName, String displayFieldNames, String fieldsPattern, String photoValue,String searchText) {
        System.debug('objectName:'+objectName);
        String query;
        List<String> displayFieldNamesLst;
        String photoFieldName;
        List<Wrapper> results = new List<Wrapper>();
        String finalQuery;
        String photoToDisplay;
        String queryLimit = ' LIMIT 1000';   
        try{
            if(String.isBlank(fieldsPattern)){
                fieldsPattern = displayFieldNames;
            }
            
            //prepare a where clause
            displayFieldNamesLst = displayFieldNames.split(',');
            String whereClause = '';
            if(searchText!='')
            {
                for(String fieldName : displayFieldNamesLst){
                    // System.debug('fieldName:'+fieldName);
                    whereClause += String.isBlank(whereClause) ? ' WHERE ' + fieldName + ' LIKE \'%{text}%\'' : ' OR ' + fieldName + ' LIKE \'%{text}%\'';
                }
            }
            
            
            //add Id field to field names if necessary
            if(!displayFieldNames.toLowerCase().contains('id')){
                displayFieldNames += ', Id';
            }
            
            //add photo field if not added
            if(photoValue.toLowerCase().contains('field')){
                List<String> photoValueLst = photoValue.split('->');
                if(photoValueLst.size() > 1 && !displayFieldNames.toLowerCase().contains(photoValueLst[1].toLowerCase())){
                    photoFieldName = photoValueLst[1];
                    displayFieldNames += ', '+photoValueLst[1];
                }
            }else if(photoValue.toLowerCase().contains('url')){
                List<String> photoValueLst = photoValue.split('->');
                if(photoValueLst.size() > 1){
                    photoToDisplay = photoValueLst[1];
                }
            }
            
            query = 'SELECT ' + displayFieldNames + ' FROM ' + objectName + whereClause;
             System.debug('query:'+query);
            if(!searchText.equals('')){
                finalQuery = query.replace('{text}', searchText);
                //  System.debug('finalQuery:'+finalQuery);
            }
            else{
                finalQuery = 'SELECT Id,Name FROM RecentlyViewed WHERE TYPE = \''+objectName+'\'';
            }
            finalQuery += queryLimit;
            System.debug('finalQuery:'+finalQuery);
            for(Sobject sobj : database.query(finalQuery)){
                String displayValue = fieldsPattern;
                for(String fieldName : displayFieldNamesLst){
                    String fieldValue = sobj.get(fieldName) == null ? '' : String.valueOf(sobj.get(fieldName));
                    displayValue = displayValue.replace(fieldName, fieldValue);
                    if(String.isNotBlank(photoFieldName) && sobj.get(photoFieldName) != null){
                        photoToDisplay = String.valueOf(sobj.get(photoFieldName));
                    }
                }
                results.add(new Wrapper(String.valueOf(sobj.get('Id')), displayValue, photoToDisplay));
            }
        }Catch(Exception ex){
           
        }
        return results;
    }
    
    
    /************************** WRAPPER *****************************/
    public class Wrapper{
        public Id recordId{get;set;}
        public String displayValue{get;set;}
        public String photoUrl{get;set;}
        public Wrapper(Id recordId, String displayValue, String photoUrl){
            this.recordId = recordId;
            this.displayValue = displayValue;
            this.photoUrl = photoUrl;
        }
    }
}