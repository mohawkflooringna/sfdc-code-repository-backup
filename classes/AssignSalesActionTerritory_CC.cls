/**************************************************************************

Name : AssignSalesActionTerritory_CC

===========================================================================
Purpose : This class is used to assign sales actions to multiple terrirories based on territory type and region. 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand         23/Feb/2017    Modified                       
***************************************************************************/
public with sharing class AssignSalesActionTerritory_CC {
     
    public static String resTerrRTId = UtilityCls.getRecordTypeInfo(UtilityCls.Territory, UtilityCls.RESIDENTIAL);
    
    public class TerritoryModel{
        @AuraEnabled
        public Territory__c territory;
        @AuraEnabled
        public Boolean selected;
        @AuraEnabled 
        public String Role;
        @AuraEnabled
        public String salId;
        
        public TerritoryModel( Territory__c territory){
            this.territory =  new Territory__c();
            this.territory = territory;
            selected = false; 
        }
    }

    @AuraEnabled
    public static Territory__c tempTerritory(String saId){ 

        String rtName = [SELECT RecordType.Name from Action_List__c WHERE id =: saId].RecordType.Name;
        Territory__c ter = new Territory__c();
        if(rtName.Contains(UtilityCls.COMMERCIAL)){
            ter.RecordTypeId = UtilityCls.getRecordTypeInfo('Territory__c', UtilityCls.COMMERCIAL);
            ter.Type__c = 'Hard Surface';
        }else if(rtName.Contains(UtilityCls.RESIDENTIAL)){
            ter.RecordTypeId = UtilityCls.getRecordTypeInfo('Territory__c', UtilityCls.RESIDENTIAL);
            ter.Type__c = 'Hard Surface';
        }
        return ter;
    }

    public static Map<String,String> existingTerritoies(String saId){
        Map<String, String> terIdAndSALMap = new Map<String, String>();
        //List<SAL_Territory__c> terIds = new List<SAL_Territory__c>();
        List<SAL_Territory__c> salTerList = [SELECT Id,Territory__c FROM SAL_Territory__c WHERE Action_List__c =: saId];
        for(SAL_Territory__c sal : salTerList){
          terIdAndSALMap.put(sal.Territory__c, sal.Id);
        }
       /* for(SAL_Territory__c each : salTerList){
             terIds.add(each.Territory__c);
        } */
        
        return terIdAndSALMap; 
    } 
   
        /*
     * Method  : retrieveTerritories
     * Purpose : Retrieve territory records based on selected territory Type and Region.
     */
    @AuraEnabled
    public static List<TerritoryModel> retrieveTerritories(String saId , List<String> type, List<String> region){
        String typeQry = '';
        String regionQry = '';
        System.debug(LoggingLevel.INFO, '*** region: ' + region);
        List<TerritoryModel> terModelList = new List<TerritoryModel>();
        String typeStr = UtilityCls.toString4ListMembers(type);
        String regionStr = UtilityCls.toString4ListMembers(region);
        System.debug(LoggingLevel.INFO, '*** region.size(): ' + region.size());
        System.debug(LoggingLevel.INFO, '*** regionStr: ' + regionStr);
        System.debug(LoggingLevel.INFO, '*** typeStr: ' + typeStr);

        String qry = 'SELECT Id,Name, Type__c, Territory_Code__c,Region__c,Sector__c FROM Territory__c WHERE' ;

        //String regionQry1 = ' WHERE Region__c IN ' + regionStr ;
        if(type!= null && type.size() >=1 && !typeStr.contains('All')){
            typeQry = ' Type__c in '+ typeStr;
          	qry += typeQry;
        }
        if(region!= null && region.size()>=1 ){
            regionQry = (typeStr.contains('All') ? (' Region__c in ' + regionStr) : ('AND Region__c in ' + regionStr));
            qry += regionQry;
        }
      
        List<Territory__c> terList =  database.query(qry);
        System.debug(LoggingLevel.INFO, '*** terList: ' + terList);
        //Map<Id,Territory__c> terMap = new  Map<Id,Territory__c>(terList);

        Map<String, String> terIdAndSALMap = existingterritoies(saId);
          
        for(Territory__c ter : terList){

              TerritoryModel tModel = new TerritoryModel(ter);
              //Select checkbox if territory is already assoiciated with current action list
              If(terIdAndSALMap.keySet().contains(ter.id)){
                 tModel.selected = true;
                 tModel.salId = terIdAndSALMap.get(ter.Id);
              }
              terModelList.add(tModel);
        }
        system.debug('&&&&terModelList&&'+terModelList);
         
        return terModelList;
    }//End Method


     /*
     * Method  : createSalTerritories
     * Purpose : Insert Sal Territory Records for selected Territories in order to link selected territories with Action List.
     */

    @AuraEnabled
    public static void createSalTerritories(String saId ,List<Id> selectedTerIds, List<Id> uncheckedSalIds){
        System.debug(LoggingLevel.INFO, '*** saId: ' + saId);
        System.debug(LoggingLevel.INFO, '*** selectedTerIds: ' + selectedTerIds);
        System.debug(LoggingLevel.INFO, '*** uncheckedSalIds: ' + uncheckedSalIds);
        if(saId !=null && selectedTerIds!=null && selectedTerIds.size() >0){
            List<SAL_Territory__c> salTerList =  new  List<SAL_Territory__c>();
            Map<String,String> existingTerIds = existingterritoies(saId);
            for(Id terId : selectedTerIds){
              
                if(!existingTerIds.keySet().contains(terId)){

                  SAL_Territory__c salTer = new SAL_Territory__c(Action_List__c = saId, Territory__c = terId);
                  salTerList.add(salTer);
                }
            }

             
             List<Database.SaveResult> result=  database.Insert(salTerList, false);
            UtilityCls.createDMLexceptionlog(salTerList,result,'AssignSalesActionTerritory_CC','createSalTerritories');
              
        }
        if(uncheckedSalIds!=null && uncheckedSalIds.size() >0){
           List<SAL_Territory__c> curSALList = [SELECT Id FROM SAL_Territory__c WHERE Id IN:uncheckedSalIds ];  
          // delete curSALList; 
            List<Database.DeleteResult> result1=  database.delete(curSALList, false);
            UtilityCls.createDeleteDMLexceptionlog(curSALList,null,result1,'AssignSalesActionTerritory_CC','createSalTerritories');
           
        }
        
    }//End of Method

  
    /*Lillian Chen 7/7/2017*/
    @AuraEnabled
    public static  Map<String, List<String>> getPicklistFieldValues(String strObjName, String strFieldName){ 
        Map<String, List<String>> valMap = new Map<String, List<String>>();
        List<String> typeList = new List<String>();
        //List<String> regionList = new List<String>();
        typeList.add('All');
        //typeList.add('None');
        //regionList.add('None');
        typeList.addAll(UtilityCls.getPicklistValues(strObjName, strFieldName));
        //List<Residential_Regions__c> rrList = Residential_Regions__c.getall().values();
        Map<String, List<String>> rrMap = new Map<String, List<String>>();
        
        /*for(Residential_Regions__c rr : rrList){
          String rrStr = rr.Regions__c;
          List<String> rrStrList = rrStr.split(',');
          rrMap.put(rr.Name, rrStrList);
          
        }*/
        
        //Start of Code - MB - To get Regions from Territory instead of Custom Settings - Bug 58596 - 4/3/18
        for(List<Territory__c> terrList: [SELECT Region__c, Type__c from Territory__c
                                          WHERE Type__c != '' 
                                          AND RecordTypeId =: resTerrRTId]){
                                              for(Territory__c terr: terrList){
                                                  if(rrMap.containsKey(terr.Type__c)){
                                                      rrMap.get(terr.Type__c).add(terr.Region__c);
                                                  }else{
                                                      List<String> regionList = new List<String>();
                                                      regionList.add(terr.Region__c);
                                                      rrMap.put(terr.Type__c, regionList);
                                                  }
                                              }
                                          }
        //End of Code - MB - Bug 58596 - 4/3/18
        valMap.put('Type', typeList);
        //valMap.putAll(getDependentPicklistOptions('Territory__c', 'Type__c', 'Region__c','Soft Surface'));
        valMap.putAll(rrMap);
        //valMap.put('Region', regionList);
        return valMap;

    }

  /* @AuraEnabled
    public static Map<String,List<String>> getDependentPicklistOptions(String pObjName, String pControllingFieldName, String pDependentFieldName,String controllingFieldValue){
        Map<String, List<String>> pickMap = new Map<String, List<String>>();
        //pickMap.put('All', new List<String>{'All'});
        pickMap.putAll(UtilityCls.getDependentOptions(pObjName,pControllingFieldName,pDependentFieldName,controllingFieldValue));
        return pickMap;
    }
    /*Lillian Chen 7/7/2017*/

 
}