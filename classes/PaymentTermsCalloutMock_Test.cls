/**************************************************************************

Name : PaymentTermsCalloutMock_Test 

===========================================================================
Purpose :  the right JSON format 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        14/Feb/2017     Created 
***************************************************************************/
@isTest
global class PaymentTermsCalloutMock_Test implements HttpCalloutMock{
    
    global HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setStatus('OK');
        res.setStatusCode(200);
        res.setBody('{'

                         

                            +'"ERRORCODE": "TEST ERRORCODE",'

                            +'"MESSAGE": "TEST MESSAGE",'
							
                            +'"AccountName": "J A Lotourneau & Fils Inc",'

                            +'"DateTimeSFDC": "*February 10 - 2017, 01:47pm EST*",'


                            +'"InfoMessage": "If you have a question regarding Terms, please contact Mohawk Financial Services at (800) 427-4900.",'

                            +'"TermsDataArray": ['

                              +'{'

                                +'"TACTICALCAT": "Soft Surface",'

                                +'"PRODTYPEDESC": "Residential",'

                                +'"TERMSDESC": "5/30 N40",'

                                +'"NONSTDIND": "Y"'

                              +'},'


                              +'{'

                                +'"TACTICALCAT": "Soft Surface",'

                                +'"PRODTYPEDESC": "Helios",'

                                +'"TERMSDESC": "Net 30 Days",'

                                +'"NONSTDIND": "N"'

                              +'}'
                            +']'

                          

                        +'}');
          
          return res;
    }
}