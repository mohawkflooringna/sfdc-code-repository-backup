/**************************************************************************

Name : AssignPubGrpToAccountSharing_Batch_Test
===========================================================================
Purpose : Uitlity class to AssignPubGrpToAccountSharing_Batch test data 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mudit          03/OCT/2017    Create  
2.0        Susmitha       15/June/2018   Modified     

***************************************************************************/

@isTest
private class AssignPubGrpToAccountSharing_Batch_Test {
    
    static testMethod void testMethod1() {
        Utility_Test.getDefaultConfCusSetting();
        
        string com = 'Commercial';
        list<Account> parentAccs = Utility_Test.createAccounts(false, 2);
        
        List<Territory__c>  terrList = Utility_Test.createTerritories(false, 2);
        terrList[0].Name='Test_Test';
        insert terrList;
        
        for(Account a : parentAccs){
            a.Workplace_Retail_Territory__c=terrList[0].id;
            a.Business_Type__c=com;
            a.strategic_account__c=false;
            a.Type='Non-Invoicing';
        }
        insert parentAccs;
        List<Group> createGroupList=utility_Test.createGroup(false,1,terrList[0].Name);
        createGroupList[0].developername=terrList[0].Name;
        createGroupList[0].name=terrList[0].Name;

        insert createGroupList;
        system.assert(terrList!=null);
        system.assert(parentAccs!=null);
        
        
        
        test.startTEST();
        
        AssignPubGrpToAccountSharing_Batch rollupbatch = new AssignPubGrpToAccountSharing_Batch('Download');
        
        String jobId = Database.executeBatch(rollupbatch,200); 
        
        
        system.assert(jobId!=null);
        test.stoptest();
        
        
        
    }
    static testMethod void testMethod2() {
        Utility_Test.getDefaultConfCusSetting();
        
        string com = 'Commercial';
        list<Account> parentAccs = Utility_Test.createAccounts(false, 2);
        
        List<Territory__c>  terrList = Utility_Test.createTerritories(false, 2);
        terrList[0].Name='Test_Test';
        insert terrList;
        for(Account a : parentAccs){
            a.Business_Type__c=com;
            a.strategic_account__c=false;
            a.Type='Non-Invoicing';
            a.Workplace_Retail_Territory__c=terrList[0].id;
        }
        insert parentAccs;
        List<Group> createGroupList=utility_Test.createGroup(false,1,terrList[0].Name);
        createGroupList[0].developername=terrList[0].Name;
        createGroupList[0].name=terrList[0].Name;

        insert createGroupList;
        system.assert(terrList!=null);
        system.assert(parentAccs!=null);
        
        
        
        test.startTEST();
        
        AssignPubGrpToAccountSharing_Batch rollupbatch = new AssignPubGrpToAccountSharing_Batch('Update');
        
        String jobId = Database.executeBatch(rollupbatch,200); 
        
        
        system.assert(jobId!=null);
        test.stoptest();
        
        
        
        
        
    }
    static testMethod void testMethod3() {
        Utility_Test.getDefaultConfCusSetting();
        
        string com = 'Commercial';
        list<Account> parentAccs = Utility_Test.createAccounts(false, 2);
        
        List<Territory__c>  terrList = Utility_Test.createTerritories(false, 2);
        terrList[0].Name='Test_Test';
        insert terrList;
        for(Account a : parentAccs){
            a.Business_Type__c=com;
            a.strategic_account__c=false;
            a.Type='Non-Invoicing';
            a.Education_Govt_Territory__c=terrList[0].id;
        }
        insert parentAccs;
        List<Group> createGroupList=utility_Test.createGroup(false,1,terrList[0].Name);
        createGroupList[0].developername=terrList[0].Name;
        createGroupList[0].name=terrList[0].Name;

        insert createGroupList;
        system.assert(terrList!=null);
        system.assert(parentAccs!=null);
        
        
        
        test.startTEST();
        
        AssignPubGrpToAccountSharing_Batch rollupbatch = new AssignPubGrpToAccountSharing_Batch('Update');
        
        String jobId = Database.executeBatch(rollupbatch,200); 
        
        
        system.assert(jobId!=null);
        test.stoptest();
        
        
        
        
        
    }
    static testMethod void testMethod5() {
        Utility_Test.getDefaultConfCusSetting();
        
        string com = 'Commercial';
        list<Account> parentAccs = Utility_Test.createAccounts(false, 2);
        
        List<Territory__c>  terrList = Utility_Test.createTerritories(false, 2);
        terrList[0].Name='Test_Test';
        insert terrList;
        for(Account a : parentAccs){
            a.Business_Type__c=com;
            a.strategic_account__c=false;
            a.Type='Non-Invoicing';
            a.Territory__c=terrList[0].id;
        }
        insert parentAccs;
        List<Group> createGroupList=utility_Test.createGroup(false,1,terrList[0].Name);
        createGroupList[0].developername=terrList[0].Name;
        createGroupList[0].name=terrList[0].Name;

        insert createGroupList;
        system.assert(terrList!=null);
        system.assert(parentAccs!=null);
        
        
        
        test.startTEST();
        
        AssignPubGrpToAccountSharing_Batch rollupbatch = new AssignPubGrpToAccountSharing_Batch('Update');
        
        String jobId = Database.executeBatch(rollupbatch,200); 
        
        
        system.assert(jobId!=null);
        test.stoptest();
        
        
        
        
        
    }
    static testMethod void testMethod4() {
        Utility_Test.getDefaultConfCusSetting();
        
        string com = 'Commercial';
        list<Account> parentAccs = Utility_Test.createAccounts(false, 2);
        
        List<Territory__c>  terrList = Utility_Test.createTerritories(false, 2);
        terrList[0].Name='Test_Test';
        insert terrList;
        for(Account a : parentAccs){
            a.Business_Type__c=com;
            a.strategic_account__c=false;
            a.Type='Non-Invoicing';
            
            a.Healthcare_Sr_Living_Territory__c=terrList[0].id;
        }
        insert parentAccs;
        List<Group> createGroupList=utility_Test.createGroup(false,1,terrList[0].Name);
        createGroupList[0].developername=terrList[0].Name;
        createGroupList[0].name=terrList[0].Name;

        insert createGroupList;
        system.assert(terrList!=null);
        system.assert(parentAccs!=null);
        
        AccountShare accShare = new AccountShare();
        accShare.AccountId = parentAccs[0].Id;
        accShare.AccountAccessLevel = 'Edit';
        accShare.ContactAccessLevel = 'Edit';
        accShare.OpportunityAccessLevel = 'Edit';
        accShare.UserOrGroupId = createGroupList[0].id;
        insert accShare;        
        
        test.startTEST();
        
        AssignPubGrpToAccountSharing_Batch rollupbatch = new AssignPubGrpToAccountSharing_Batch('Update');
        
        String jobId = Database.executeBatch(rollupbatch,200); 
        
        
        system.assert(jobId!=null);
        test.stoptest();
        
        
        
        
        
    }
    /*
static testMethod void testMethod1() {

test.startTest(); 

List<Territory__c>terrList = Utility_Test.createTerritories(true, 1);
List<Account>accList = Utility_Test.createAccounts(false, 1);
accList[0].Business_Type__c = 'Commercial';
accList[0].Territory__c = terrList[0].id;
insert accList;

Utility_Test.createGroup(true, 1, 'Test Territory');

AssignPubGrpToAccountSharing_Batch APG = new AssignPubGrpToAccountSharing_Batch();
String jobid=Database.executeBatch( APG);
// Schedule the test job
System.assert(jobid != null);
test.stopTest();


}
static testMethod void testMethod2() {

test.startTest(); 

List<Territory__c>terrList = Utility_Test.createTerritories(true, 1);
List<Account>accList = Utility_Test.createAccounts(false, 1);
accList[0].Business_Type__c = 'Commercial';
accList[0].Territory__c =null;
insert accList;

Utility_Test.createGroup(true, 1, 'Test Territory');

AssignPubGrpToAccountSharing_Batch APG = new AssignPubGrpToAccountSharing_Batch();
String jobid=Database.executeBatch( APG);
// Schedule the test job
System.assert(jobid != null);
test.stopTest();


}
*/
}