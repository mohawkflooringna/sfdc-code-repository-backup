/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/territory/query')
global class TerritoryQueryProcessor {
    global TerritoryQueryProcessor() {

    }
    @HttpPost
    global static sma.TerritoryQueryProcessor.MARestResponse post(sma.TerritoryQueryProcessor.MARestRequest request) {
        return null;
    }
global class MARestRequest {
    global MARestRequest() {

    }
}
global class MARestResponse {
    global MARestResponse() {

    }
}
}
