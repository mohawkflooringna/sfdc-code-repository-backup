@isTest
private class Multiday_Visit_WindowsHandler_Test{
    static List<Account> resAccListForInvoicing;
    static List<Territory__c> terrList;
    static Id resiInvRTId    = Schema.Sobjecttype.Mohawk_Account_Team__c.getRecordTypeInfosByName().get('Residential Invoicing').getRecordTypeId();
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static List<mamd__Multiday_Visit_Windows__c>multiDayaList = new List<mamd__Multiday_Visit_Windows__c>();
    
    @TestSetup
    public static void init(){
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
        
        /*resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
        for(Account a :resAccListForInvoicing){
            a.Total_Account_Chain_Sales__c=50;
            //a.Account_Profile__c=aplist[0].id;
            a.Chain_Number__c = '3213311';
        }
        insert resAccListForInvoicing;*/
        
        resAccListForInvoicing = Utility_Test.createAccounts(true,1);
        terrList = Utility_Test.createTerritories(true, 2);
        mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
        for(Mohawk_Account_Team__c mat : mohAccTeam){
            mat.Account__c = resAccListForInvoicing[0].id;
        }
        insert mohAccTeam;
        mamd__Multiday_Visit_Windows__c MVW = new mamd__Multiday_Visit_Windows__c(
            name = 'Test Data',
            mamd__Default__c = true,
            mamd__VW_Account__c = resAccListForInvoicing[0].Id,
            mamd__Days_of_Week__c = 'Monday',
            mamd__Time_Window_Start_1__c = '1:00 AM',
            mamd__Time_Window_End_1__c = '1:30 AM'
        );
        multiDayaList.add( MVW );
    }
    
    
    static testMethod void testMethod1() {
        test.startTest();
            init();
            insert multiDayaList;
            for( mamd__Multiday_Visit_Windows__c updateMVW : multiDayaList ){
                updateMVW.mamd__Time_Window_End_1__c = '2:30 AM';
            }
            update multiDayaList;
            delete multiDayaList;
        test.stopTest();
    }
    
}