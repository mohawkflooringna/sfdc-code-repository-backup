/**************************************************************************

Name : UserSearch_CC_Test

===========================================================================
Purpose : This tess class is used for UserSearch_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Sasi Naik      2/May/2017     Created         CSR:  
2.0        Susmitha       3/March/2018   Modified
***************************************************************************/
@isTest
private class UserSearch_CC_Test {
    
    static List<Account> accList;
    static Account acc;
    static List<lead> leadList;
    static Lead lead;
    static List<Account> resAccListForInvoicing;
    static List<Territory__c> terrList;
    static User userList;
    static List<Territory_User__c>  terrUser;
    static List<Opportunity> oppList;
    
    static List<OpportunityTeamMember>OpportunityTeamMemberList;//Added By Mudit - 26/09/17
    
    
    public static void init(){
        List<Account> accList = Utility_Test.createAccounts(true, 3);
        oppList = Utility_Test.createOpportunities(false, 3, accList);
        oppList[0].Recordtypeid = UtilityCls.getRecordTypeInfo('Opportunity', 'Commercial Traditional'); 
        oppList[1].Recordtypeid = UtilityCls.getRecordTypeInfo('Opportunity', 'Commercial Traditional'); 
        oppList[2].Recordtypeid = UtilityCls.getRecordTypeInfo('Opportunity', 'Commercial Traditional'); 
        insert oppList;
        
        OpportunityTeamMemberList = Utility_Test.createOppTeamMember( false,3,oppList,Utility_Test.ADMIN_USER );//Added By Mudit - 26/09/17
        OpportunityTeamMemberList[0].Team_Role_Commercial__c = 'Account Executive';
        OpportunityTeamMemberList[1].Team_Role_Commercial__c = 'Account Executive';
        OpportunityTeamMemberList[2].Team_Role_Commercial__c = 'Account Executive';
        OpportunityTeamMemberList[0].TeamMemberRole = 'Account Executive';
        OpportunityTeamMemberList[1].TeamMemberRole = 'Account Executive';
        OpportunityTeamMemberList[2].TeamMemberRole = 'Account Executive';
        insert OpportunityTeamMemberList;
        
        
        resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 2);
        acc = resAccListForInvoicing.get(0); 
        leadList = Utility_Test.createLeads(false, 1);
        for(Integer i=0;i<leadList.size();i++){
            leadList[i].RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Commercial').getRecordTypeId();
        }
        insert leadList;
        system.assert(leadList!=null);
        lead = leadList.get(0);    
        
        List<Territory__c>terrList = Utility_Test.createTerritories(true, 1);
        List<Territory_User__c> terUserList = Utility_Test.createTerritoryUsers(false,1);
        terUserList[0].User__c = UserInfo.getUserId();
        terUserList[0].Territory__c = terrList[0].Id;
        insert terUserList;
    }
    
    
    static testMethod void testMethod4() {
        System.runAs(Utility_Test.COMMERCAIL_USER) {
            Test.startTest();
            init();
            UserSearch_CC.getUserInfo('test','NY','NU','MHJ',true);
            
            UserSearch_CC.editProjectTeammember(OpportunityTeamMemberList[0].id,'Regional Vice President') ;  
            UserSearch_CC.AddProjectProductTeammembers(oppList[0].Id,Utility_Test.ADMIN_USER.Id,'Strategic Account Manager', '');
            UserSearch_CC.ChangeOwner(acc.Id);
            UserSearch_CC.GetCurrentOwnerName(acc.Id);
            UserSearch_CC.ChangeAccountOwner(acc.Id, Utility_Test.ADMIN_USER.Id);
            UserSearch_CC.ChangeLeadOwner(lead.Id, Utility_Test.ADMIN_USER.Id);
            
            UserSearch_CC.getMajorMetorsPickListvalues();
            System.assert( UserSearch_CC.getMajorMetorsPickListvalues().size() > 0 );
            
            UserSearch_CC.getStatePickListvalues(); // Added by MB - 07/10/17
            System.assert( UserSearch_CC.getStatePickListvalues().size() > 0 );
            
            UserSearch_CC.getProjectTeammemberRolevalues();
            //UserSearch_CC.getUserInitialize();
            
            List<User> userList = UserSearch_CC.getTerrUserOwnerlist('Mohan', 'All Users within my Territory','NJ', 'NY', 'SF', '');
            
            List<User> userList_1 = UserSearch_CC.getTerrUserOwnerlist('Mohan', 'All Users','NJ', 'NY', 'SF', '');
            system.assert(userList_1!=null);
            
            
            UserSearch_CC.getProjectNameInfo(oppList[0].Id, OpportunityTeamMemberList[0].id );
            UserSearch_CC.getProjectTeamMember( OpportunityTeamMemberList[0].id );//Added By Mudit - 26/09/17            
            Test.stopTest();
        }
    }
    
}