@isTest
public class generalDropDownController_Test {
    public static testmethod void  testmethod1(){
        List<Account_Profile__c> accProfile = new list<Account_Profile__c>();   
        List<Account_Profile_Settings__c> apSetting = new List<Account_Profile_Settings__c>();
        accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false);
        apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
        
        generalDropDownController.getsObjectList('Account_Profile__c', accProfile[0].id);
        System.assert( generalDropDownController.getsObjectList('Account_Profile__c', accProfile[0].id) != null );
        
        generalDropDownController.getAllDistributions(accProfile[0].id);
        System.assert( generalDropDownController.getAllDistributions(accProfile[0].id) != null );
        
        generalDropDownController.getObject(accProfile[0].Primary_Business__c,'Account_Profile_Settings__c');
        System.assert( generalDropDownController.getObject(accProfile[0].Primary_Business__c,'Account_Profile_Settings__c') != null );
    }
}