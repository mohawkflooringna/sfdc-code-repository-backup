@isTest
private class updateMATForBI_BatchTest {
    
    static List<Account> resAccListForInvoicing;
    static List<Territory__c> terrList;
    static User userList;
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static List<Mohawk_Account_Team__c> mohAccTeam1;
    static List<Action_List__c> actListForResidentialFL;
    static List<Action_List__c> actListForResidentialSAL;
    static List<Action_List__c> actListForResidential = new List<Action_List__c>();
    static List<SAL_Territory__c>  salTerr;
    static List<Territory_User__c>  terrUser;
    static List<Territory_User__c>  terrUser1;
    static Id resiNonInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Non-Invoicing');
    static Id resiInvRTId    = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
    
    public static void init(){   
        MAT_Batch_Setting__c MBS = new MAT_Batch_Setting__c();
        MBS.Name = 'updateMATForBI_Batch';
        MBS.Batch_Size__c = 1;
        MBS.Batch_Query__c = 'SELECT Id,Chain_Number_UserId__c, Main_Profile__c, LastModifiedDate FROM Mohawk_Account_Team__c WHERE  RecordTypeId =\'' + resiInvRTId + '\'';
        MBS.Last_Run_At__c = system.now().addDays(-1);
        insert MBS;
        
        Utility_Test.getTeamCreationCusSetting(); 
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
    }
    
    static testMethod void testMethod0() {
        System.runAs(Utility_Test.ADMIN_USER) {        
        test.startTest(); 
        init();
        resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        terrList = Utility_Test.createTerritories(false, 2);
        for(Territory__c ter :terrList){
            ter.Type__c = UtilityCls.BUILDERMULTIFAMILY;
        }
        insert terrList;
        mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
        for(Mohawk_Account_Team__c mat:mohAccTeam){
            mat.Account__c=resAccListForInvoicing[0].id;
        }
        insert mohAccTeam;
        system.assert(mohAccTeam.size()>0);
        
        updateMATForBI_Batch  cls = new updateMATForBI_Batch();
        string jobId=database.executeBatch(cls,10);
        
        //String jobIdSch = System.schedule('Test my class', '0 0 0 1 JAN,APR,JUL,OCT ? *', new updateMATForBI_Batch());
        
        test.stopTest();
        }
    }
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {        
        test.startTest(); 
        //init();
        
        
        Utility_Test.getTeamCreationCusSetting(); 
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
        
        
        resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
        terrList = Utility_Test.createTerritories(false, 2);
        for(Territory__c ter :terrList){
            ter.Type__c = UtilityCls.BUILDERMULTIFAMILY;
        }
        insert terrList;
        mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
        for(Mohawk_Account_Team__c mat:mohAccTeam){
            mat.Account__c=resAccListForInvoicing[0].id;
            mat.Main_Profile__c = true;
        }
        insert mohAccTeam;
        system.assert(mohAccTeam.size()>0);
        
        updateMATForBI_Batch  cls = new updateMATForBI_Batch();
        string jobId=database.executeBatch(cls,10);
        
        //String jobIdSch = System.schedule('Test my class', '0 0 0 1 JAN,APR,JUL,OCT ? *', new updateMATForBI_Batch());
        
        test.stopTest();
        }
    }
    
}