public class MATtoTerritoryAccountService {
    
    public static void createTerritoryAccounts(list <Mohawk_account_team__c> matTab) {
        map<string, set<id>> terrAccounts = new map<string, set<id>>();
        map<string,string> terrRoles = new map<string,string>();
        
        for ( Mohawk_account_team__c mat : matTab ){
            if (mat.Territory_User_Number__c != null){
                for(String tmNum : mat.Territory_User_Number__c.split(';')){
                    if(terrAccounts.containskey(TmNum)){
                        terrAccounts.get(tmNum).add(mat.Account__c);
                    }else {
                        terrAccounts.put(tmNum, new set<id>{mat.Account__c});  		
                    }	
                }                
            }  
        } 
        
        list<Territory_User__c> terrUsers = [Select Id, Name, Role__c, Territory_Code__c,Region_Code__c,Sector_Code__c  from Territory_User__c where name in :terrAccounts.keyset()];	
        for (Territory_User__c terrUser : terrUsers){	   
            String tmRolecode = 'D' + terrUser.Territory_Code__c;
            terrRoles.put(tmRolecode, terrUser.name);	     
        }
        
        map<string,id> terrRoleIds = new map<string,string>();
        list<ObjectTerritory2Association> territoryObjectList = new list<ObjectTerritory2Association>();
        list<Territory2> territoryList = [Select Id, developername  from Territory2 where developername in : terrRoles.keyset()];
        for(Territory2 territory : territoryList){
            if( terrRoles.containskey(territory.developername)){
                if(terrAccounts.containskey(terrRoles.get(territory.developername))){
                    for(id accId : terrAccounts.get(terrRoles.get(territory.developername))){
                        ObjectTerritory2Association territoryObject = new ObjectTerritory2Association();
                        territoryObject.Territory2Id = territory.id;
                        territoryObject.objectId =  accId ;			
                        territoryObject.AssociationCause = 'Territory2Manual';                         
                        territoryObjectList.add(territoryObject);
                    }
                } 
            }	  
        }
        
        if (!territoryObjectList.isempty()){
            List < ApplicationLog__c > appLogList = new List < ApplicationLog__c > ();
            MohawkAccountTeamService.updateFromEvent = true;  
            Database.SaveResult[] results = Database.insert(territoryObjectList, false);  
            UtilityCls.createDMLexceptionlog(territoryObjectList,results,'MATtoTerritoryAccountService','createTerritoryAccounts'); 	   
        }
        
    }
    
    public static void deleteTerritoryAccounts(list <Mohawk_account_team__c> matTab) {
        map<string, set<id>> terrAccounts = new map<string, set<id>>();
        map<string, set<id>> terrAccsByrole = new map<string,set<id>>();
        set<id> accIds = new set<id>();
        map<string,string> terrRoles = new map<string,string>();
        
        for ( Mohawk_account_team__c mat : matTab ){
            if (mat.Territory_User_Number__c != null){
                accIds.add(mat.Account__c);                
                for(String tmNum : mat.Territory_User_Number__c.split(';')){                
                    if(terrAccounts.containskey(TmNum)){
                        terrAccounts.get(tmNum).add(mat.Account__c);
                    }else {
                        terrAccounts.put(tmNum, new set<id>{mat.Account__c});  		
                    }	
                }
            }
        } 
        
        list<Territory_User__c> terrUsers = [Select Id, Name, Role__c, Territory_Code__c  from Territory_User__c where name in :terrAccounts.keyset()];	
        for (Territory_User__c terrUser : terrUsers){	   
            String tmRolecode = 'D' + terrUser.Territory_Code__c;
            terrRoles.put(tmRolecode, terrUser.name);
            if (terrAccsByrole.containsKey(tmRolecode)){
                terrAccsByrole.get(tmRolecode).addall(terrAccounts.get(terrUser.name));
            } else {
                terrAccsByrole.put(tmRolecode,terrAccounts.get(terrUser.name)); 
            }
            
        }
        
        list<Mohawk_Account_Team__c> activeMats = [select  Territory_User_Number__c, account__c from Mohawk_account_team__c where account__c in :accIds];
        for(Mohawk_Account_Team__c mat : activeMats){
            for(String tmNum : mat.Territory_User_Number__c.split(';')){
                String tmRolecode = terrRoles.get(tmNum);
                if(terrAccsByrole.containsKey(tmRolecode) && terrAccsByrole.get(tmRolecode).contains(mat.account__c)){
                    terrAccsByrole.get(tmRolecode).remove(mat.account__c);
                }                  
            }             
        }
        
        
        
        List<id> deleteObjAssignment = new  List<id>();     
        list<ObjectTerritory2Association> terrObjAssignments = [Select Id, objectid, Territory2.DeveloperName  from ObjectTerritory2Association where Territory2.DeveloperName in :terrRoles.keyset() and objectid in:accIds];	
        for (ObjectTerritory2Association terrAssign : terrObjAssignments){	   
            if( terrRoles.containskey(terrAssign.Territory2.DeveloperName )){
                if(terrAccsByrole.containskey(terrAssign.Territory2.DeveloperName)){
                    if(terrAccsByrole.get(terrAssign.Territory2.DeveloperName).contains(terrAssign.objectid)){
                        deleteObjAssignment.add(terrAssign.id);
                    }
                }
            }
        }
        
        if (!deleteObjAssignment.isempty()){
            List < ApplicationLog__c > appLogList = new List < ApplicationLog__c > ();
            MohawkAccountTeamService.updateFromEvent = true;  
            List<Database.DeleteResult> results = Database.delete(deleteObjAssignment, false);
            UtilityCls.createDeleteDMLexceptionlog(null,deleteObjAssignment, results,'MATtoTerritoryAccountService','deleteTerritoryAccounts');
        }     
        
        
    }
    
    public static void syncTerritoryAccounts(list <Mohawk_account_team__c> matTab) {
        
        map<string, set<id>> terrAccounts = new map<string, set<id>>();
        map<string, set<id>> matAccsByrole = new map<string,set<id>>();
        set<id> accIds = new set<id>();
        list<id> delTerrObjectList = new list<id>();
        list<ObjectTerritory2Association> newTerrObjectList = new list<ObjectTerritory2Association>();
        map<string,Set<Id>> objTerr2AccountsMap = new map<string,Set<Id>>();
        
        for(Mohawk_Account_Team__c mat:matTab){
            accIds.add(mat.Account__c); 
        }
        
        // Fetch all accounts assigned to MAT
        for ( Mohawk_account_team__c mat : [select Id,Territory_User_Number__c,User__c,Account__c from Mohawk_Account_Team__c where Account__c in :accIds]){
            if (mat.Territory_User_Number__c != null){
                for(String tmNum : mat.Territory_User_Number__c.split(';')){
                    if(terrAccounts.containskey(TmNum)){
                        terrAccounts.get(tmNum).add(mat.Account__c);
                    }else {
                        terrAccounts.put(tmNum, new set<id>{mat.Account__c});  		
                    }	
                }                
            }         
        } 
        
        list<Territory_User__c> terrUsers = [Select Id, Name, Territory_Code__c  from Territory_User__c where name in :terrAccounts.keyset()];	
        for (Territory_User__c terrUser : terrUsers){	   
            String distCode = 'D' + terrUser.Territory_Code__c;      
            if (matAccsByrole.containsKey(distCode)){
                matAccsByrole.get(distCode).addall(terrAccounts.get(terrUser.name));
            } else {
                matAccsByrole.put(distCode,terrAccounts.get(terrUser.name)); 
            }			
        }
        
 	  // Compare the Territory Accounts with the MAT Accounts
        list<ObjectTerritory2Association> terrObjAssignments = [Select Id, objectid, Territory2.DeveloperName  from ObjectTerritory2Association where  objectid in:accIds];	
        
        for (ObjectTerritory2Association terrAssign : terrObjAssignments){		 
            if(!objTerr2AccountsMap.containsKey(terrAssign.Territory2.DeveloperName)){
                objTerr2AccountsMap.put(terrAssign.Territory2.DeveloperName,new set<id>{terrAssign.objectid});
            }else{
                objTerr2AccountsMap.get(terrAssign.Territory2.DeveloperName).add(terrAssign.objectid);
            }
            
            if(matAccsByrole.containskey(terrAssign.Territory2.DeveloperName)) {
                if(!matAccsByrole.get(terrAssign.Territory2.DeveloperName).contains(terrAssign.objectid)){
                    delTerrObjectList.add(terrAssign.id);
                }
            } else {
                delTerrObjectList.add(terrAssign.id);
            }
        }
        
        
        // Compare the MAT Accounts with the Territory Accounts
        list<Territory2> territoryList = [Select Id, developername  from Territory2 where developername in : matAccsByrole.keyset()];
        for(Territory2 territory : territoryList){
            for(id accID : matAccsByrole.get(territory.developername)){
                if (!objTerr2AccountsMap.containsKey(territory.developername) || (objTerr2AccountsMap.containsKey(territory.developername) && !objTerr2AccountsMap.get(territory.developername).contains(accID))){
                    ObjectTerritory2Association territoryObject = new ObjectTerritory2Association();
                    territoryObject.Territory2Id = territory.id;
                    territoryObject.objectId =  accId ;			
                    territoryObject.AssociationCause = 'Territory2Manual';                         
                    newTerrObjectList.add(territoryObject);
                }	
            }  
        }
                
        if (!newTerrObjectList.isempty()){
            List < ApplicationLog__c > appLogList = new List < ApplicationLog__c > ();
            MohawkAccountTeamService.updateFromEvent = true;  
            Database.SaveResult[] results = Database.insert(newTerrObjectList);  
            UtilityCls.createDMLexceptionlog(newTerrObjectList,results,'MATtoTerritoryAccountService','syncTerritoryAccounts-Insert'); 	   
        }
        
        if (!delTerrObjectList.isempty()){
            List < ApplicationLog__c > appLogList = new List < ApplicationLog__c > ();
            MohawkAccountTeamService.updateFromEvent = true;  
            List<Database.DeleteResult> results = Database.delete(delTerrObjectList, false);
            UtilityCls.createDeleteDMLexceptionlog(null,delTerrObjectList, results,'MATtoTerritoryAccountService','syncTerritoryAccounts-Delete');
        }     
    }
    
}