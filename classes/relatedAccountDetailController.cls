public without sharing class relatedAccountDetailController {
    
    @AuraEnabled
    public static List<string>getPickListValuesIntoList(){
        List<string>pickListValuesList = new List<string>();
        Schema.DescribeFieldResult fieldResult = Related_Account__c.Account_Role__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        pickListValuesList.add('--None--');
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList;
    }
    @AuraEnabled
    public static String createRARecord(Related_Account__c relAccRec, Id parentAccId,Id parentProjId, List<Contact> contactList/*,String AccountRole,Boolean specifier, String projRole,String Notes*/){
        String ErrorMsg='Success';
        List<Related_Account__c> insertRecdList = new List<Related_Account__c>();
        List<Related_Account__c> existingRecdList = new List<Related_Account__c>();
        Set<Id> contactIds=new set<id>();
        String TempStr='';
        Integer i=0;
        
        if(contactList!=null){
            for(Contact c : contactList){
                contactIds.add(c.id);
                system.debug(':::::::::::contact id ::::::::'+c.id);
            }
        }
        Account acc=new Account();
        if(relAccRec!=null)   
        {
            relAccRec.Account__c = parentAccId;
            relAccRec.Opportunity__c = parentProjId;
            acc=[SELECT ID,Strategic_Account__c, Role_C__c FROM Account Where ID=:parentAccId Limit 1];
            if(acc!=null && acc.Strategic_Account__c){
                relAccRec.Project_Role__c = 'Strategic Account';
            }
            if(contactList!=null && contactList.size()==1)
            {
                relAccRec.Contact__c = contactList[0].id;
                insertRecdList.add(relAccRec);
            }
            else if(contactList!=null && contactList.size()>1)
            {
                for(Contact conRec : contactList )
                {
                    Related_Account__c tempRec = relAccRec.clone(false,true,false,false);
                    tempRec.Contact__c = conRec.id;
                    insertRecdList.add(tempRec);
                }
            }else if((contactList == null || contactList.size() == 0) && (acc.Strategic_Account__c || acc.Role_C__c != 'Dealer')){
                insertRecdList.add(relAccRec);
            }
        }
        if(contactIds.size()>0){
            existingRecdList=[SELECT ID,Contact__r.Name FROM Related_Account__c WHERE Opportunity__c=:parentProjId and Contact__c in: contactIds];
        }
        system.debug('existingRecdList :::::::::::'+existingRecdList.size());
        system.debug('insertRecdList :::::::::::'+insertRecdList);
        if(insertRecdList!=null && insertRecdList.size()>0){
            // insert insertRecdList;
            if(existingRecdList.size()<1){
                List<Database.SaveResult> results = Database.INSERT(insertRecdList,false);
                System.debug('Results: ' + results);
            }
            else{
                for( Related_Account__c rel : existingRecdList){
                    if(i==0){
                        TempStr+=rel.Contact__r.Name;
                    }
                    else{  
                        TempStr+=', '+rel.Contact__r.Name;
                    }
                    i++;
                }
                return TempStr;
            }
        }
        
        system.debug('ErrorMsg ::::::::'+ErrorMsg);
        return 'Success';
        
    }
    @AuraEnabled
    public static String getOpptyFromIdId(String optyId){
        List<Opportunity> opptyList=new List<Opportunity>();
        if(optyId!=null){
            opptyList=[SELECT ID,Name FROM Opportunity WHERE Id=:optyId limit 1];
        }
        if(opptyList.size()>0){
            system.debug('Entry>>>');
            return opptyList[0].Name;
        }
        else{
            return null;
        }
    }
    
    @AuraEnabled
    public static Boolean checkAccountisStrategic(Id recordId){
        Boolean strAcc = false;
        if(recordId != null){
            try{
                strAcc = [SELECT Strategic_Account__c FROM Account WHERE Id =: recordId].Strategic_Account__c;
                return strAcc;
            }catch (Exception ex){
                System.debug('Error: ' + ex.getMessage());
            }
        }
        return strAcc;
    }
    
}