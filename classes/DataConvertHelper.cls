public without sharing class DataConvertHelper 
{
    public static String importContact = 'All';
    public static boolean checkForPrimaryContact = false;
    public static Set<String> insContactsToBeProcessed; 
    public static string objectType;
    public static rdcc__Configurable_Settings__c config = OpportunityTriggerHelper.accessConfigurableSettings();
    public static Id ownerId = UserInfo.getUserId();
    
    public static void updateConRole(Map<string,List<rdcc__ProjectContactRole__c>> conRoleMap, Map<String, Contact> insConIdConMap){
        List<rdcc__ProjectContactRole__c> conRoleListUpdate = new List<rdcc__ProjectContactRole__c>();
        if(insConIdConMap != null){
            for(String conId:insConIdConMap.keySet()){
                if(conRoleMap.containsKey(conId)){
                    for(rdcc__ProjectContactRole__c pConRole: conRoleMap.get(conId)){
                        Contact myCon = insConIdConMap.get(conId);
                        pConRole.rdcc__Contact__c = myCon.Id;
                        pConRole.rdcc__Account__c = myCon.accountId;
                        pConRole.rdcc__Insight_Contact__c = NULL;
                        conRoleListUpdate.add(pConRole);
                    }
                }
            }
        }       
        
        // updating contact roles with new contact
        if(conRoleListUpdate.size()>0){
            Database.update(conRoleListUpdate,false);
        } 
    }
    
    public static contact mapContactFields(rdcc__Insight_Contact__c insCon, string accountId, String insightUpdatesValue){
        // map contact fields
        Contact contact=new Contact();
        
        if(insCon == null){
            return contact;
        }
        contact.OwnerId = ownerId;
        contact.FirstName=insCon.rdcc__First_Name__c;
        contact.LastName=insCon.rdcc__Last_Name__c;
        contact.rdcc__Contact_ID__c=insCon.rdcc__Contact_ID__c;
        contact.LeadSource=insCon.rdcc__Lead_Source__c;
        contact.Phone=insCon.rdcc__Phone__c;
        contact.Fax=insCon.rdcc__Fax__c;
        contact.Email=insCon.rdcc__Email__c;
        contact.rdcc__URL__c=insCon.rdcc__URL__c;
        contact.MailingStreet=insCon.rdcc__Mailing_Street__c;
        
        contact.MailingPostalCode=insCon.rdcc__Mailing_Postal_Code__c;
        contact.MailingCity=insCon.rdcc__Mailing_City__c;
        //contact.MailingCountry=insCon.rdcc__Mailing_Country__c;
        contact.rdcc__Linked_In_Profile__c = insCon.rdcc__Linked_In_Profile__c;
        contact.AccountId=accountId;
        if(insightUpdatesValue!= null && insightUpdatesValue =='Yes') {                
            contact.rdcc__Allow_Future_ConstructConnect_Updates__c= true;
        }else{
             contact.rdcc__Allow_Future_ConstructConnect_Updates__c = false;
        }
        
        if(UtilityClass.isStateCountryPicklistEnabled()){
            if(UtilityClass.getCountryName(insCon.rdcc__Mailing_Country__c) != 'NOT FOUND'){
                contact.put('MailingCountryCode',UtilityClass.getCountryName(insCon.rdcc__Mailing_Country__c));
                if(UtilityClass.getStateName(insCon.rdcc__Mailing_State__c) != 'NOT FOUND'){                    
                    contact.put('MailingStateCode',UtilityClass.getStateName(insCon.rdcc__Mailing_State__c));
                }else{
                    UtilityClass.addMessageToPage('ERROR', 'State is Invalid');
                }
            }else{
                UtilityClass.addMessageToPage('ERROR', 'Country is Invalid');
            }
        }else{
            contact.MailingCountry = insCon.rdcc__Mailing_Country__c;
            contact.MailingState = insCon.rdcc__Mailing_State__c;
        }
        
        if(config.rdcc__Contact_Record_Type__c != null && config.rdcc__Contact_Record_Type__c != ''){
             List<SelectOption> recTypes = UtilityClass.getRecordTypeMap('Contact');
             Set<id> recordTypeIds = UtilityClass.getAllowedRecordTypeIds('Contact');
              if(recTypes.size()>1){
                  if(recordTypeIds.contains(config.rdcc__Contact_Record_Type__c))
                    contact.put('recordTypeId',config.rdcc__Contact_Record_Type__c);
              }  
        }
        return contact;
    }
    
    public static SObject filledMandatoryFields(SObject record,  Map<String,String> fieldApiNmTypeMap){
        Set<String> textMandatoryFields = new Set<String>{'TEXT','TEXTAREA','URL','PICKLIST','EMAIL','PHONE','STRING'};
        Set<String> integerMandatoryFields = new Set<String>{'CURRENCY','NUMBER','PERCENT','DOUBLE','INTEGER','DECIMAL'};
        System.debug('fieldApiNmTypeMap:'+fieldApiNmTypeMap);
        for(String eachField : fieldApiNmTypeMap.keySet()){               
            if(textMandatoryFields.contains(fieldApiNmTypeMap.get(eachField))){
                if(fieldApiNmTypeMap.get(eachField).equalsIgnoreCase('PICKLIST')){
                    List<String> values = UtilityClass.getPicklistValues(objectType ,eachField );
                    if(values != null && !values.isEmpty()){
                        record.put(eachField,values[0]);
                    }
                }else if(fieldApiNmTypeMap.get(eachField).equalsIgnoreCase('EMAIL')){
                    record.put(eachField,'dummy@dummy.com');
                }else{
                    record.put(eachField,'NA');
                }                    
            }else if(integerMandatoryFields.contains(fieldApiNmTypeMap.get(eachField))){
                record.put(eachField,0);
            }else if(fieldApiNmTypeMap.get(eachField).equalsIgnoreCase('DATE')){
                record.put(eachField, Date.today());
            }else if(fieldApiNmTypeMap.get(eachField).equalsIgnoreCase('DATETIME')){
                record.put(eachField, System.now());
            }                
        }
        return record;
    }
    
    
    public static Contact getMappedContactWithMandatoryFields(rdcc__Insight_Contact__c insCon, string accountId){       
        objectType = 'Contact';
        Contact contact = new Contact();
        String insightUpdatesValue = 'Yes';
        if(config != null && config.rdcc__Allow_Future_Updates_to_Account_Contact__c != null && config.rdcc__Allow_Future_Updates_to_Account_Contact__c.equalsIgnoreCase('No')){
            insightUpdatesValue = 'No';
        }
        contact = mapContactFields(insCon,accountId,insightUpdatesValue); 
        Map<String,String> fieldApiNmTypeMap = UtilityClass.getMandatoryFieldsTypeMap('Contact');
        System.debug('fieldApiNmTypeMap:'+fieldApiNmTypeMap);
        if(fieldApiNmTypeMap != null && !fieldApiNmTypeMap.isEmpty()){
           contact = (Contact)filledMandatoryFields(contact,fieldApiNmTypeMap);
        }
        return contact;        
    }
    
    public static void afterCompanyConversion(List<rdcc__Insight_Company__c> companyList, Map<String, String> compIdAccountIdMap){ 
        
        companyList = [select id,OwnerId, rdcc__Is_Converted__c,rdcc__Last_Updated__c,name,rdcc__Company_ID__c,rdcc__Fax__c,rdcc__Parent_Role__c,rdcc__Phone__c,rdcc__Role__c,rdcc__Shipping_City__c,rdcc__Shipping_Country__c,rdcc__Shipping_County__c,rdcc__Shipping_Postal_Code__c,rdcc__Shipping_State__c,rdcc__Shipping_Street__c,rdcc__Type__c,rdcc__URL__c ,(select id, rdcc__Primary_Contact__c,rdcc__CMD_Comapany_Id__c,rdcc__Account__c,rdcc__Insight_Company__c from rdcc__Company_Roles__r),(select id,rdcc__CMD_Comapany_Id__c,rdcc__Insight_Company__c,rdcc__Account__c from rdcc__Insight_Classifications__r),(select id,rdcc__From__c,rdcc__CMD_Comapany_Id__c,rdcc__Insight_Company__c from rdcc__Insight_Bids__r),(select id,rdcc__CMD_Comapany_Id__c,rdcc__Contact_ID__c,rdcc__Email__c,rdcc__Fax__c,rdcc__First_Name__c,rdcc__Insight_Company__c,rdcc__Insight_Company__r.name,rdcc__Is_Company_Converted__c,rdcc__Last_Name__c,rdcc__Lead_Source__c,rdcc__Linked_In_Profile__c,rdcc__Mailing_City__c,rdcc__Mailing_Country__c,rdcc__Mailing_Postal_Code__c,rdcc__Mailing_State__c,rdcc__Mailing_Street__c,rdcc__Phone__c,rdcc__Type__c,rdcc__URL__c ,Name from rdcc__Insight_Contacts__r) from rdcc__Insight_Company__c where id  IN:companyList];
        
        Map<string,List<rdcc__ProjectCompanyRole__c>> comRoleMap = new Map<string,List<rdcc__ProjectCompanyRole__c>>();
        Map<string,List<rdcc__AccountClassification__c>> classMap = new Map<string,List<rdcc__AccountClassification__c>>();
        Map<string,List<rdcc__Bid__c>> bidMap = new Map<string,List<rdcc__Bid__c>>();
        Map<string,List<rdcc__Insight_Contact__c>> insConMap = new Map<string,List<rdcc__Insight_Contact__c>>();        
        Map<string,List<rdcc__ProjectContactRole__c>> conRoleMap = new Map<string,List<rdcc__ProjectContactRole__c>>();
        Map<String, Contact> insConIdConMap = new Map<String, Contact>();
        Map<string,rdcc__Insight_Contact__c> conIdInsConMap = new Map<string,rdcc__Insight_Contact__c>();
        List<rdcc__ProjectCompanyRole__c> comRoleListUpdate = new List<rdcc__ProjectCompanyRole__c>();
        List<rdcc__Bid__c> bidListUpdate = new List<rdcc__Bid__c>();
        List<rdcc__AccountClassification__c> classListUpdate = new List<rdcc__AccountClassification__c>();
        List<Contact> conList = new List<Contact>();
        Set<String> insConIds = new Set<String>();
        Set<String> primaryContacts = new Set<String>();
        //try{
            for(rdcc__Insight_Company__c record : companyList){            
                if(!record.rdcc__Company_Roles__r.isEmpty())
                    comRoleMap.put(record.rdcc__Company_ID__c,record.rdcc__Company_Roles__r);
                if(!record.rdcc__Insight_Bids__r.isEmpty())
                    bidMap.put(record.rdcc__Company_ID__c,record.rdcc__Insight_Bids__r);
                if(!record.rdcc__Insight_Classifications__r.isEmpty())
                    classMap.put(record.rdcc__Company_ID__c,record.rdcc__Insight_Classifications__r);
                if(!record.rdcc__Insight_Contacts__r.isEmpty())
                    insConMap.put(record.rdcc__Company_ID__c, record.rdcc__Insight_Contacts__r);
                
            }
            
            for(String companyId:compIdAccountIdMap.keySet()){
                if(comRoleMap.containsKey(companyId)){
                    for(rdcc__ProjectCompanyRole__c pComRole: comRoleMap.get(companyId)){
                        if(pComRole.rdcc__Primary_Contact__c != null){
                            primaryContacts.add(pComRole.rdcc__Primary_Contact__c.trim());
                        }
                        pComRole.rdcc__Account__c = compIdAccountIdMap.get(companyId);
                        pComRole.rdcc__Insight_Company__c = NULL;
                        comRoleListUpdate.add(pComRole);
                    }
                }
                
                if(bidMap.containsKey(companyId)){
                    for(rdcc__Bid__c bid: bidMap.get(companyId)){
                        bid.rdcc__From__c = compIdAccountIdMap.get(companyId);
                        bid.rdcc__Insight_Company__c = NULL;
                        bidListUpdate.add(bid);
                    }
                }
                
                if(classMap.containsKey(companyId)){
                    for(rdcc__AccountClassification__c classif: classMap.get(companyId)){
                        classif.rdcc__Account__c = compIdAccountIdMap.get(companyId);
                        classif.rdcc__Insight_Company__c = NULL;
                        classListUpdate.add(classif);
                    }
                }
                
                if(insConMap.containsKey(companyId)){
                    for(rdcc__Insight_Contact__c insCon:insConMap.get(companyId)){
                       
                        conList.add(getMappedContactWithMandatoryFields(insCon, compIdAccountIdMap.get(companyId)));
                        System.debug('@@Contact'+conList[0]);
                        insConIds.add(insCon.Id);
                        conIdInsConMap.put(insCon.rdcc__Contact_ID__c, insCon);
                    }
                }
            }
            
            for(rdcc__Insight_Contact__c conl: [select id,rdcc__CMD_Comapany_Id__c,rdcc__Contact_ID__c, (select id, rdcc__CMD_Contact_Id__c,rdcc__Contact__c,rdcc__Account__c,rdcc__Insight_Contact__c from rdcc__Contact_Roles__r) from rdcc__Insight_Contact__c where id IN: insConIds]){            
                if(!conl.rdcc__Contact_Roles__r.isEmpty())
                    conRoleMap.put(conl.rdcc__Contact_ID__c,conl.rdcc__Contact_Roles__r);
            }
            
            // updating company roles with new account
            if(!comRoleListUpdate.isEmpty()){
                system.debug('comRoleListUpdate $$$ '+comRoleListUpdate);
                database.update(comRoleListUpdate,false);
            }
            
            // updating bids with new account
            if(!bidListUpdate.isEmpty()){
                database.update(bidListUpdate,false);
            }
            
            // updating classification with new account
            if(!classListUpdate.isEmpty()){
                database.update(classListUpdate,false);
            }
            
            List<rdcc__Insight_Contact__c> insConToDelete = new List<rdcc__Insight_Contact__c>();
            
            
            if(checkForPrimaryContact){
                Set<String> insConExternalIds = new Set<String>();
                System.debug('##importContact : '+importContact);
                System.debug('##conList : '+conList);
                if(importContact.equalsIgnoreCase('None')){
                   
                    for(Contact eachCon :  conList){
                        insConExternalIds.add(eachCon.rdcc__Contact_ID__c);
                    }
                    conList = new List<Contact>();
                     System.debug('##insConExternalIds : '+insConExternalIds);
                }else if(importContact.equalsIgnoreCase('Few')){
                    List<Contact> conListToProcess = new List<Contact>();
                    System.debug('insContactsToBeProcessed>>>'+insContactsToBeProcessed);
                    for(Contact eachCon :  conList){
                        String conExternalId = eachCon.rdcc__Contact_ID__c;
                        if(insContactsToBeProcessed != null && insContactsToBeProcessed.contains(conExternalId))
                            conListToProcess.add(eachCon);
                        else
                            insConExternalIds.add(eachCon.rdcc__Contact_ID__c);
                    }
                    conList = conListToProcess;                    
                }else if(importContact.equalsIgnoreCase('Primary')){
                    List<Contact> conListToProcess = new List<Contact>();
                    for(Contact eachCon :  conList){
                        String contactName = eachCon.FirstName + ' ' + eachCon.LastName;
                        if(primaryContacts != null && primaryContacts.contains(contactName.trim()))
                            conListToProcess.add(eachCon);
                        else
                            insConExternalIds.add(eachCon.rdcc__Contact_ID__c);
                    }
                    conList = conListToProcess;                    
                }
                insConToDelete.addAll([Select id from rdcc__Insight_Contact__c WHERE rdcc__Contact_ID__c IN : insConExternalIds]);
                
            }
             
            System.debug('##insConToDelete : '+insConToDelete); 
            if(!conList.isEmpty()){            
                Schema.SObjectField f = Contact.Fields.rdcc__Contact_ID__c;
                List<Contact> conListInsert = new List<Contact>();   
                List<Contact> conListUpdate = new List<Contact>();   
                Map<String, Contact> conMap = new Map<String, Contact>();
                for(Contact con:[Select Id, rdcc__Contact_ID__c From Contact WHERE rdcc__Contact_ID__c IN: conIdInsConMap.keySet()]){
                    conMap.put(con.rdcc__Contact_ID__c,con);                   
                }
                List<Database.SaveResult> saveResults = new List<Database.SaveResult>();
                
                for(Contact eachCon:conList){
                    if(conMap.containsKey(eachCon.rdcc__Contact_ID__c)){
                        eachCon.Id = conMap.get(eachCon.rdcc__Contact_ID__c).id;
                        conListUpdate.add(eachCon);
                    }else{
                        conListInsert.add(eachCon);
                    }
                }
                
                Database.DMLOptions dml=new Database.DMLOptions();
                dml.DuplicateRuleHeader.AllowSave=true;
                if(!conListUpdate.isEmpty()){
                    saveResults.addAll(Database.update(conListUpdate,dml));
                }
                if(!conListInsert.isEmpty()){
                    saveResults.addAll(Database.insert(conListInsert,dml));
                }
                
                for(Integer i=0;i<saveResults.size();i++){
                    Database.SaveResult result = saveResults[i];
                    if(result.isSuccess()){                    
                        insConIdConMap.put(conList[i].rdcc__Contact_ID__c, conList[i]);                    
                        insConToDelete.add(conIdInsConMap.get(conList[i].rdcc__Contact_ID__c));                     
                    }
                }    
            }
            
            if(!insConIdConMap.isEmpty()){
                system.debug('conRoleMap $$$ '+conRoleMap);
                updateConRole(conRoleMap,insConIdConMap);
            }
            
            if(!insConToDelete.isEmpty()){
                //InsightContactTriggerHelperClass.isSelfDelete = TRUE;
                delete insConToDelete;
                //InsightContactTriggerHelperClass.isSelfDelete = FALSE;
            }
            
            if(companyList.size() > 0){ 
                //InsightCompanyTriggerHelperClass.isInsightCompanyTriggerExecuting = TRUE;          
                delete companyList;
                //InsightCompanyTriggerHelperClass.isInsightCompanyTriggerExecuting = FALSE;
            }
        //}Catch(Exception e){
        //    System.debug(e.getStackTraceString());
        //}
    }
}