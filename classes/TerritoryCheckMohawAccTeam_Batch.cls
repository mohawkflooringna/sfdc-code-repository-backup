/**************************************************************************

Name : TerritoryCheckMohawAccTeam_Batch

===========================================================================
Purpose : Batch class to Check the fact that all Terrirory name should be match the same with Mohawk Account Teams.
          If Terrirory has a new  territory user reocrd, you have to create this recod in Mohawk Account Teams.
          If Terrirory removes a territory user reocrd, you have to remove this recod in Mohawk Account Teams.
          If Terrirory changes existing territory user record , firstly you have to remove this recod in Mohawk Account Teams. 
          Then, have to create this recod in Mohawk Account Teams.
===========================================================================
History:
--------
VERSION    AUTHOR            DATE           DETAIL          DESCRIPTION
1.0        Sasi Naik      13/MAR/2017        Created          CSR:

***************************************************************************/
global class TerritoryCheckMohawAccTeam_Batch {
    
}
/*
global class TerritoryCheckMohawAccTeam_Batch implements Database.Batchable<sObject>,Schedulable {
    
    Static String resRole;
    Static String commRole;
    Static {       
       resRole = UtilityCls.CS_RESROLE.Role__c;
       commRole = UtilityCls.CS_COMMROLE.Role__c; 
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //Need to add condition to retrieve recent modified users last 1 DAY
        //String query = 'SELECT Id, User__c, Territory__c, RecordType.Name,IsDeleted, Role__c, Territory__r.Name from Territory_User__c WHERE RecordTypeId !=null AND (Role__c =:resRole OR Role__c =:commRole)';
        String query = 'SELECT Id, User__c, Territory__c, RecordType.Name,IsDeleted, Role__c, Territory__r.Name from Territory_User__c WHERE LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY';  
        System.debug(LoggingLevel.INFO, '*** query 12: ' + Database.getQueryLocator(Query));
        return Database.getQueryLocator(Query);
        //LastModifiedDate >= Today AND
    }
    

    global void execute(Database.BatchableContext BC, List<Territory_User__c> scope) {

        Set<Id> territoryuserRecordIds = new Set<Id>();
        Map<String,Set<Id>> matMap  = new Map<String,Set<Id>>();
        Map<String, Set<Id>> matterUserIdsMap = new Map<String, Set<Id>>();
        Map<Id, Map<Id,String>> terUsersMap = new  Map<Id,  Map<Id,String>>(); //for Mohawk Account team update
        Map<String,String> terUsersredtypeMap = new  Map<String,String>(); //for Mohawk Account team 
        Map<String,String> terUsersrecordtypeMap = new  Map<String,String>(); //for Mohawk Account team 
        Set<Id> AccountIdset = new Set<Id>();
        for(Territory_User__c tu:Scope){
            territoryuserRecordIds.add(tu.Territory__c);
             if(matterUserIdsMap.containsKey(tu.Territory__c) ){
                    if(tu.User__c != null ||tu.User__c != '' ){
                        matterUserIdsMap.get(tu.Territory__c).add(tu.User__c);
                    }
               }else{ 
                    Set<Id> userids = new Set<Id>(); 
                    userids.add(tu.User__c);  
                   if(userids.size()>0){
                     matterUserIdsMap.put(tu.Territory__c, userids);                    
                   }  
                    
               }
            
             //for Mohawk Account team update (Key: Territory Id, Value: Territory User Ids)
              if(terUsersMap.containsKey(tu.Territory__c) && (tu.Role__c == resRole ||  tu.Role__c == commRole)){
                  
                  terUsersMap.get(tu.Territory__c).put(tu.user__c, tu.Role__c);

             }else if(tu.Role__c == resRole ||  tu.Role__c == commRole){

                  Map<Id,String> userMap = new Map<Id,String>(); 
                  userMap.put(tu.user__c, tu.Role__c);
                    terUsersMap.put(tu.Territory__c, userMap);
             }
        }

        Map<Id,List<Mohawk_Account_Team__c>> accMohawkAccTeamMap = new Map<Id,List<Mohawk_Account_Team__c>>();
        Map<String, Set<Id>> MatIdsMap= new Map<String, Set<Id>>();

       for(account a: [select Id,Territory__c,Business_Type__c,RecordType.Name,Territory__r.Name, (Select Id,Name,Role__c,User__c,Account__c,Account__r.Territory__c from Mohawk_Account_Teams__r WHERE Source_CAMS__c = true) from Account
             WHERE Territory__c IN: matterUserIdsMap.keySet() AND Business_Type__c != null]){ 
            AccountIdset.add(a.Id);
           
              if(MatIdsMap.containsKey(a.Territory__c)){
                 //Considering only user as Mohawk Accoount Team  for that using filter on user id starts with '005' 
                  for(Mohawk_Account_Team__c m : a.Mohawk_Account_Teams__r){
                        MatIdsMap.get(a.Territory__c).add(m.User__c);
                        //terUsersRoleMap.get(a.Territory__c).add(m.Role__c);                       
                  }
                  matMap.get(a.Territory__c).add(a.Id);                      
             }else{
                  Set<Id> userIds = new Set<Id>(); 
                  Set<Id> accids = new Set<Id>();
                    for(Mohawk_Account_Team__c m : a.Mohawk_Account_Teams__r){
                        userIds.add(m.User__c);
                    }
                   // if(userIds.size()>0){
                        MatIdsMap.put(a.Territory__c, userIds);
                     // }
                        accids.add(a.Id);
                        matMap.put(a.Territory__c, accids);
                        
                }
                 
                 terUsersredtypeMap.put(a.Id, a.Business_Type__c);
                 terUsersrecordtypeMap.put(a.Id, a.RecordType.Name);

        }
        System.debug(LoggingLevel.INFO, '*** : matterUserIdsMap 123' + matterUserIdsMap);
        System.debug(LoggingLevel.INFO, '*** : matIdsMap 123' + MatIdsMap);
        System.debug(LoggingLevel.INFO, '*** : matMap 123' + matMap);
        System.debug(LoggingLevel.INFO, '*** : terUsersredtypeMap 123' + terUsersredtypeMap);
        updateMHKAccountTeam(matterUserIdsMap,matIdsMap,matMap,terUsersredtypeMap,terUsersMap,terUsersrecordtypeMap);
      }


       /*
    * Method  : updateMHKAccountTeam
    * Purpose : Update Account's Mohawk account team based on changes in Territory users
                Change only users with role
                Commercial : Salesperson
                Residential: Business Development Manager

      matterUserIdsMap Map<String, Set<Id>> - TerrId - Modified Terr UserIds
      MatIdsMap        Map<String, Set<Id>> - TerritoryId - Mohawk Ac team User__c

    */
  /*
   global void updateMHKAccountTeam(Map<String, Set<Id>> matterUserIdsMap,  Map<String, Set<Id>> matIdsMap,  Map<String, Set<Id>> matMap,  Map<String, String> terUsersredtypeMap, Map<Id, Map<Id,String>> terUsersMap,Map<String, String> terUsersrecordtypeMap){
        Default_Configuration__c  defaultConfig =  Default_Configuration__c.getOrgDefaults(); 
        List<Mohawk_Account_Team__c> insertMohawkAccountTeamList = new List<Mohawk_Account_Team__c>();
        List<AccountTeamMember> acTeamMemList = new List<AccountTeamMember>();
        List<Id> deleteMATList = new List<Id>();
        List<Id> deleteMATUserIdList = new List<Id>();
        String Recordtypename = '';
        
    try{   
           
          for(String terName : matterUserIdsMap.keySet()){ 
            
            if(matMap.containsKey(terName)){

              for(Id accid : matMap.get(terName)){ // account Loop
                System.debug(LoggingLevel.INFO, '*** accid 144: ' + accid);

                if(String.valueOf(terUsersredtypeMap.get(accid)) == UtilityCls.RESIDENTIAL){
                    Recordtypename = String.valueOf(terUsersredtypeMap.get(accid))+' '+String.valueOf(terUsersrecordtypeMap.get(accid));
                 } else {
                    Recordtypename = String.valueOf(terUsersredtypeMap.get(accid));
                 } 

                if(matterUserIdsMap.get(terName) != null && MatIdsMap.get(terName) == null){
                    for(id tUserId : matterUserIdsMap.get(terName)){
                    //Residential 
                     if(String.valueOf(terUsersredtypeMap.get(accid)) == UtilityCls.RESIDENTIAL){
                          Mohawk_Account_Team__c matMember = new Mohawk_Account_Team__c(Account__c = accid,
                                                                          User__c = tUserId
                                                                          ,Role__c =String.valueOf(terUsersMap.get(terName).get(tUserId))
                                                                          ,Source_CAMS__c = true
                                                                          ,Account_Access__c = defaultConfig.Account_Team_Access__c
                                                                          ,RecordTypeId =UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', Recordtypename));

                          system.debug('##Insidet first Insert ::'+matMember);
                          insertMohawkAccountTeamList.add(matMember); 
                      //Commercial - Create standard Account Team
                      }else{
                        AccountTeamMember atm = new AccountTeamMember();
                         atm.AccountId        = accid;
                         atm.TeamMemberRole   = String.valueOf(terUsersMap.get(terName).get(tUserId));
                         atm.UserId           = tUserId;
                         atm.AccountAccessLevel = defaultConfig.Account_Team_Access__c;
                         atm.ContactAccessLevel = defaultConfig.Contact_Access__c;
                         atm.OpportunityAccessLevel = defaultConfig.Opportunity_Team_Access__c;
                         acTeamMemList.add(atm);
                      }

                          System.debug(LoggingLevel.INFO, '*** insertMohawkAccountTeamList 118 : ' + insertMohawkAccountTeamList );
                      }
                    }



                     if(matterUserIdsMap.get(terName) != null && MatIdsMap.get(terName) != null){

                        System.debug(LoggingLevel.INFO, '*** 147: ');
                         // add new team members 
                         if(matterUserIdsMap.get(terName).size() >= MatIdsMap.get(terName).size()){
                            System.debug(LoggingLevel.INFO, '*** 150: ');

                                System.debug(LoggingLevel.INFO, '*** 153: ');
                                for(id tUserId : matterUserIdsMap.get(terName)){
                                System.debug(LoggingLevel.INFO, '*** 155: ');

                                if(!MatIdsMap.get(terName).contains(tUserId) && matMap.get(terName).contains(accid)){
                                    System.debug(LoggingLevel.INFO, '*** 158: ');
                                    //Residential
                                    if(String.valueOf(terUsersredtypeMap.get(accid)) == UtilityCls.RESIDENTIAL){
                                         Mohawk_Account_Team__c matMember = new Mohawk_Account_Team__c(Account__c = accid,
                                                                                  User__c = tUserId
                                                                                  ,Role__c = String.valueOf(terUsersMap.get(terName).get(tUserId))
                                                                                  ,Source_CAMS__c = true
                                                                                  ,Account_Access__c = 'Read'
                                                                                  ,RecordTypeId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c',Recordtypename));


                                          system.debug('##Insidet Second Insert ::'+matMember);
                                        insertMohawkAccountTeamList.add(matMember); 
                                    //Commercial
                                    }else{
                                        AccountTeamMember atm = new AccountTeamMember();
                                         atm.AccountId        = accid;
                                         atm.TeamMemberRole   = String.valueOf(terUsersMap.get(terName).get(tUserId));
                                         atm.UserId           = tUserId;
                                         atm.AccountAccessLevel = defaultConfig.Account_Team_Access__c;
                                         atm.ContactAccessLevel = defaultConfig.Contact_Access__c;
                                         atm.OpportunityAccessLevel = defaultConfig.Opportunity_Team_Access__c;
                                         acTeamMemList.add(atm);

                                    }//End of else
                                  }//End of If
                               }
                               //}
                         }//End of main if


                          //Remove existing team members 
                        if(matterUserIdsMap.get(terName).size() <= MatIdsMap.get(terName).size()){ 

                            for(id tUserId : MatIdsMap.get(terName)){

                                if(!matterUserIdsMap.get(terName).contains(tUserId)){
                                    
                                      deleteMATList.add(accid); //Add group id to list to query group member records to delete
                                      deleteMATUserIdList.add(tUserId); //Add userid to list to query group member records to delete
                                       system.debug('##deleteMATLIST ::'+deleteMATList);
                                        system.debug('##deleteMATUserIdList ::'+deleteMATUserIdList);
                                  }
                            }//End of If 
                        }//end of For


                     }
                   
                    }
                }      
            }
        
         List<Mohawk_Account_Team__c> delMATList = [SELECT Id FROM Mohawk_Account_Team__c WHERE Account__c in: deleteMATList AND User__c in: deleteMATUserIdList AND Source_CAMS__c = true LIMIT 50000];
         List<AccountTeamMember> delAcTeamList = [SELECT Id FROM AccountTeamMember WHERE AccountId in: deleteMATList AND UserId in: deleteMATUserIdList LIMIT 50000];
          if(delMATList.size()>0){
                System.debug(LoggingLevel.INFO, '*** Delete 12 : ' + delMATList ); 
                Database.delete(delMATList,false);   
          }
           if(delAcTeamList.size()>0){
                System.debug(LoggingLevel.INFO, '*** Delete 12 : ' + delAcTeamList ); 
                Database.delete(delAcTeamList,false);   
          }
        
        if(insertMohawkAccountTeamList.size()>0){
            System.debug(LoggingLevel.INFO, '*** insertMohawkAccountTeamList 12 : ' + insertMohawkAccountTeamList ); 
            Database.SaveResult[] sr = Database.Insert(insertMohawkAccountTeamList, false);
            System.debug(LoggingLevel.INFO, '*** sr: ' + sr);
            Set<Id> savedIds = new Set<Id>();
            FOR(Integer i =0; i < sr.size() ; i ++ ){
                Database.SaveResult srs = sr[i];
                savedIds.add(srs.getId());
        
              }
          }

          if(acTeamMemList.size() > 0){
             Database.Insert(acTeamMemList,false); 
          }
        }Catch(Exception dmle){
                  System.debug('**Error in DML operation:'+ dmle.getMessage());
                  System.debug('**line number'+dmle.getLineNumber());
                  System.debug('**line number'+dmle);
             }       
   }



  global void finish(Database.BatchableContext BC) {
                        
  }
     // Schedulable bath
     global void execute(SchedulableContext sc){
            Database.executeBatch(new TerritoryCheckMohawAccTeam_Batch());
      }

 
} */