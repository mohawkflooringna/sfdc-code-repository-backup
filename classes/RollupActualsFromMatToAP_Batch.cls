/**************************************************************************

Name : RollupActualsFromMatToAP_Batch

===========================================================================
Purpose : Batch class to rollup Actuals FROM Mohawk Account Team to Account Profile .
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand         28/Feb/2017    Created          CSR:

***************************************************************************/
global class RollupActualsFromMatToAP_Batch implements Database.Batchable < sObject > , Database.Stateful, Schedulable {
    global boolean DefaultQueryRun;
    global boolean Last_BatchRunDate;

    global decimal AMPC = 0;
    global decimal AMPA = 0;
    global decimal AMPAC = 0;
    global decimal AMPHO = 0;
    global decimal AMPK = 0;
    global decimal AMPCAPP = 0;

    global decimal AMPCU = 0;
    global decimal AMPCUA = 0;
    global decimal AMPCUAC = 0;
    global decimal AMPCUHO = 0;
    global decimal AMPCUK = 0;
    global decimal AMPCUPP = 0;

    global decimal AMPH = 0;
    global decimal AMPHR = 0;
    global decimal AMPHPP = 0;

    global decimal AMPL = 0;
    global decimal AMPLR = 0;
    global decimal AMPLPP = 0;

    global decimal AMPR = 0;
    global decimal AMPRPP = 0;

    global decimal AMPT = 0;
    global decimal AMPTR = 0;
    global decimal AMPTPP = 0;

    global decimal AMPACT = 0;
    global decimal AMPRC = 0;
    global decimal AMPRR = 0;

    global decimal LR12MP_A = 0;
    global decimal LR12MP_Cus_A = 0;
    global decimal LR12MP_Car = 0;
    global decimal LR12MP_Cus = 0;
    global decimal LR12MP_Har = 0;
    global decimal LR12MP_Hor = 0;
    global decimal LR12MP_Cus_Hor = 0;
    global decimal LR12MP_Kar = 0;
    global decimal LR12MP_Cus_Kar = 0;
    global decimal LR12MP_Lam = 0;
    global decimal LR12MP_Cus_PP = 0;
    global decimal LR12MP_Car_PP = 0;
    global decimal LR12MP_Tile_PP = 0;
    global decimal LR12MP_Har_PP = 0;
    global decimal LR12MP_Lam_PP = 0;
    global decimal LR12MP_Res_PP = 0;
    global decimal LR12MP_Lam_RL = 0;
    global decimal LR12MP_R = 0;
    global decimal LR12MP_Tile_RT = 0;
    global decimal LR12MP_Har_RW = 0;
    global decimal LR12MP_T = 0;
    global decimal LR12MP_AC = 0;
    global decimal LR12MP_Cus_AC = 0;
    global decimal LR12MP_RC = 0;
    global decimal LR12MP_RR = 0;

    //Start -logic to run batch in DefaultQuery mode or LastBatchRun mode - Nagendra Changes 20-11-18
    global RollupActualsFromMatToAP_Batch(Boolean runDefaultQuery, Boolean LastBatchRunDate) {
        DefaultQueryRun = runDefaultQuery;
        Last_BatchRunDate = LastBatchRunDate;
   
    }
    //End - Nagendra Changes
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //Start-logic to run batch in DefaultQuery mode or LastBatchRun mode - Nagendra Changes -20-11-18
        String Query;
        DateTime batchRunDate;

        MAT_Batch_Setting__c custoSettingVal =  MAT_Batch_Setting__c.getInstance('RollupActualsFromMatToAP_Batch');
        if (custoSettingVal != null) {
            batchRunDate = custoSettingVal.Last_Run_At__c != null ? custoSettingVal.Last_Run_At__c : system.now();
            batchRunDate = (custoSettingVal.MinutesDelay__c !=null && batchRunDate !=null ) ? batchRunDate.addMinutes(Integer.valueof(custoSettingVal.MinutesDelay__c )) : batchRunDate ;
        }
     
        if (DefaultQueryRun) {  
            Batch_Query__mdt[] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'RollupActualsFromMatToAP_Batch']; //default query 
            If(QureyList.size() > 0) {
                query = QureyList[0].Query_String__c;
                
            } else {
                Query = 'SELECT Id,Account_Profile__c FROM Mohawk_Account_Team__c WHERE (LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY) AND Actuals_Sent_from_BI__c = true AND Account_Profile__r.Multi_Channel__c = true Order By Account_Profile__c ASC ALL ROWS'; //Added Mohawk_Account_Team__c.User__c by Susmitha on Feb 23,2018                 
               
            }
        } else if (Last_BatchRunDate) {
            Query = 'SELECT Id,Account_Profile__c FROM Mohawk_Account_Team__c WHERE (LastModifiedDate >= :batchRunDate) AND Actuals_Sent_from_BI__c = true AND Account_Profile__r.Multi_Channel__c = true Order By Account_Profile__c ASC ALL ROWS';

        }

       //End - logic to run batch in DefaultQuery mode or LastBatchRun mode - Nagendra Changes 20-11-18


        return Database.getQueryLocator(Query);
    }

    global void execute(Database.BatchableContext BC, List < Mohawk_Account_Team__c > scope) {
        /*************** PREVIOUS CODE ***************
        Map < id, List < Mohawk_Account_Team__c >> calculationMap = new Map < id, List < Mohawk_Account_Team__c >> ();
        for (Mohawk_Account_Team__c MAT: scope) {
            if (calculationMap.ContainsKey(MAT.Account_Profile__c)) {
                calculationMap.get(MAT.Account_Profile__c).add(MAT);
            } else {
                calculationMap.put(MAT.Account_Profile__c, new List < Mohawk_Account_Team__c > {
                    MAT
                });
            }
        }
        if (calculationMap.size() > 0) {
            retailCalculation(calculationMap);
            reSetAmps();
            BMFCalculation(calculationMap);
        }
        *************** PREVIOUS CODE ***************/
        /*************** NEW CODE - MUDIT - 17-01-2019 ***************/
        Map < id, List < Mohawk_Account_Team__c >> calculationMap = new Map < id, List < Mohawk_Account_Team__c >> ();
        set<string>apIds = new set<string>();
        for (Mohawk_Account_Team__c MAT: scope) {
            if( MAT.Account_Profile__c != null ){
                apIds.add( MAT.Account_Profile__c );
            }
        }
        
        if( apIds != null && apIds.size() > 0 ){
            for( Mohawk_Account_Team__c MAT : [SELECT Id,Account_Profile__c,Account_Profile__r.Multi_Channel__c,Actuals_Sent_from_BI__c,Brands__c,Product_Types__c, BMF_Team_Member__c, Actual_Mohawk_Purchases_Carpet__c , Actual_Mohawk_Purchases_Aladdin__c , Actual_Mohawk_Purchases_Aladdin_Comm__c , Actual_Mohawk_Purchases_Horizon__c , Actual_Mohawk_Purchases_Karastan__c , Act_MHK_Purch_Car_PP__c , Actual_Mohawk_Purchases_Cushion__c , Act_MHK_Purch_CU_ALA__c , Act_MHK_Purch_CU_HOR__c , Act_MHK_Purch_CU_KAR__c , Act_MHK_Purch_CU_ALA_Comm__c , Act_MHK_Purch_CU_PP__c , Actual_Mohawk_Purchases_Hardwood__c , Act_MHK_Purch_HW_RT__c , Act_MHK_Purch_HW_PP__c , Actual_Mohawk_Purchases_Laminate__c , Act_MHK_Purch_Lam_RT__c , Act_MHK_Purch_Lam_PP__c , Actual_Mohawk_Purchases_Tile__c , Act_MHK_Purch_Tile_RT__c , Act_MHK_Purch_Tile_PP__c , Actual_Mohawk_Purchases_Resilient__c , Act_MHK_Purch_Resi_PP__c , Actual_Mohawk_Purch_Resilient_Comm__c , Actual_Mohawk_Purch_Resilient_Retail__c , Actual_Mohawk_Purch_Aladdin_Comm_Total__c,Actual_Mohawk_Purchases_Total__c,Chain_Number_UserId__c,LR12_AMP_Al_REPO__c,LR12_AMP_Al_Comm_REPO__c,LR12_AMP_Ca_REPO__c,LR12_AMP_Cu_REPO__c,LR12_AMP_Ha_REPO__c, LR12_AMP_Ho_REPO__c, LR12_AMP_Ka_REPO__c,LR12_AMP_La_REPO__c,LR12_AMP_Mainstreet_Com_REPO__c,LR12_AMP_Prod_Cat_Other_REPO__c,LR12_AMP_Re_REPO__c,LR12_AMP_Re_Comm_REPO__c,LR12_AMP_Re_Retail_REPO__c,LR12_AMP_Ti_REPO__c,LR12_AMP_Car_PP_BI__c,LR12_AMP_CU_ALA_BI__c,LR12_AMP_CU_ALA_Comm_BI__c,LR12_AMP_CU_HOR_BI__c,LR12_AMP_CU_KAR_BI__c,LR12_AMP_CU_PP_BI__c,LR12_AMP_Lam_PP_BI__c,LR12_AMP_Lam_RT_BI__c,LR12_AMP_Resi_PP_BI__c,LR12_AMP_Tile_PP_BI__c,LR12_AMP_Tile_RT_BI__c,LR12_AMP_HW_PP_BI__c,LR12_AMP_HW_RT_BI__c FROM Mohawk_Account_Team__c WHERE Account_Profile__c IN : apIds AND Actuals_Sent_from_BI__c = true AND Account_Profile__r.Multi_Channel__c = true Order By Account_Profile__c ASC, name ASC  ] ){
                if ( calculationMap.ContainsKey( MAT.Account_Profile__c ) ) {
                    calculationMap.get( MAT.Account_Profile__c ).add( MAT );
                } else {
                    calculationMap.put( MAT.Account_Profile__c, new List < Mohawk_Account_Team__c > {
                        MAT
                    });
                }
            }
            
            if (calculationMap.size() > 0) {
                retailCalculation(calculationMap);
                reSetAmps();
                BMFCalculation(calculationMap);
            }
        }
        
        /*************** NEW CODE - MUDIT - 17-01-2019 ***************/
    }

    global void retailCalculation(Map < id, List < Mohawk_Account_Team__c >> calculationMap) {
        List < Account_Profile__c > updateAccountProfileList = new List < Account_Profile__c > ();
        if (calculationMap.size() > 0) {
            List < Account_Profile__c > apList = accountProfileList(calculationMap.keySet());            
            for (Account_Profile__c AP: apList) {
                reSetAmps();
                List < String > teamMeberIds = new List < String > ();
                for (Mohawk_Account_Team__c MAT_Retail: calculationMap.get(ap.id)) {
                    if (!MAT_Retail.BMF_Team_Member__c) {
                        //  brandList = MAT_Retail.Brands__c.split(';');
                        //  ProductTypesList = MAT_Retail.Product_Types__c.split(';');
                        if (!teamMeberIds.contains(MAT_Retail.Chain_Number_UserId__c)) {

                            AMPC += MAT_Retail.Actual_Mohawk_Purchases_Carpet__c != null ? MAT_Retail.Actual_Mohawk_Purchases_Carpet__c : 0;
                            AMPA += MAT_Retail.Actual_Mohawk_Purchases_Aladdin__c != null ? MAT_Retail.Actual_Mohawk_Purchases_Aladdin__c : 0;
                            AMPAC += MAT_Retail.Actual_Mohawk_Purchases_Aladdin_Comm__c != null ? MAT_Retail.Actual_Mohawk_Purchases_Aladdin_Comm__c : 0;
                            AMPHO += MAT_Retail.Actual_Mohawk_Purchases_Horizon__c != null ? MAT_Retail.Actual_Mohawk_Purchases_Horizon__c : 0;
                            AMPK += MAT_Retail.Actual_Mohawk_Purchases_Karastan__c != null ? MAT_Retail.Actual_Mohawk_Purchases_Karastan__c : 0;
                            AMPCAPP += MAT_Retail.Act_MHK_Purch_Car_PP__c != null ? MAT_Retail.Act_MHK_Purch_Car_PP__c : 0;

                            AMPCU += MAT_Retail.Actual_Mohawk_Purchases_Cushion__c != null ? MAT_Retail.Actual_Mohawk_Purchases_Cushion__c : 0;
                            AMPCUA += MAT_Retail.Act_MHK_Purch_CU_ALA__c != null ? MAT_Retail.Act_MHK_Purch_CU_ALA__c : 0;
                            AMPCUHO += MAT_Retail.Act_MHK_Purch_CU_HOR__c != null ? MAT_Retail.Act_MHK_Purch_CU_HOR__c : 0;
                            AMPCUK += MAT_Retail.Act_MHK_Purch_CU_KAR__c != null ? MAT_Retail.Act_MHK_Purch_CU_KAR__c : 0;
                            AMPCUAC += MAT_Retail.Act_MHK_Purch_CU_ALA_Comm__c != null ? MAT_Retail.Act_MHK_Purch_CU_ALA_Comm__c : 0;
                            AMPCUPP += MAT_Retail.Act_MHK_Purch_CU_PP__c != null ? MAT_Retail.Act_MHK_Purch_CU_PP__c : 0;

                            AMPH += MAT_Retail.Actual_Mohawk_Purchases_Hardwood__c != null ? MAT_Retail.Actual_Mohawk_Purchases_Hardwood__c : 0;
                            AMPHR += MAT_Retail.Act_MHK_Purch_HW_RT__c != null ? MAT_Retail.Act_MHK_Purch_HW_RT__c : 0;
                            AMPHPP += MAT_Retail.Act_MHK_Purch_HW_PP__c != null ? MAT_Retail.Act_MHK_Purch_HW_PP__c : 0;

                            AMPL += MAT_Retail.Actual_Mohawk_Purchases_Laminate__c != null ? MAT_Retail.Actual_Mohawk_Purchases_Laminate__c : 0;
                            AMPLR += MAT_Retail.Act_MHK_Purch_Lam_RT__c != null ? MAT_Retail.Act_MHK_Purch_Lam_RT__c : 0;
                            AMPLPP += MAT_Retail.Act_MHK_Purch_Lam_PP__c != null ? MAT_Retail.Act_MHK_Purch_Lam_PP__c : 0;

                            AMPT += MAT_Retail.Actual_Mohawk_Purchases_Tile__c != null ? MAT_Retail.Actual_Mohawk_Purchases_Tile__c : 0;
                            AMPTR += MAT_Retail.Act_MHK_Purch_Tile_RT__c != null ? MAT_Retail.Act_MHK_Purch_Tile_RT__c : 0;
                            AMPTPP += MAT_Retail.Act_MHK_Purch_Tile_PP__c != null ? MAT_Retail.Act_MHK_Purch_Tile_PP__c : 0;

                            AMPR += MAT_Retail.Actual_Mohawk_Purchases_Resilient__c != null ? MAT_Retail.Actual_Mohawk_Purchases_Resilient__c : 0;
                            AMPRPP += MAT_Retail.Act_MHK_Purch_Resi_PP__c != null ? MAT_Retail.Act_MHK_Purch_Resi_PP__c : 0;
                            AMPRC += MAT_Retail.Actual_Mohawk_Purch_Resilient_Comm__c != null ? MAT_Retail.Actual_Mohawk_Purch_Resilient_Comm__c : 0;
                            AMPRR += MAT_Retail.Actual_Mohawk_Purch_Resilient_Retail__c != null ? MAT_Retail.Actual_Mohawk_Purch_Resilient_Retail__c : 0;

                            AMPACT += MAT_Retail.Actual_Mohawk_Purch_Aladdin_Comm_Total__c != null ? MAT_Retail.Actual_Mohawk_Purch_Aladdin_Comm_Total__c : 0;

                            LR12MP_A += MAT_Retail.LR12_AMP_Al_REPO__c != null ? MAT_Retail.LR12_AMP_Al_REPO__c : 0;
                            LR12MP_Cus_A += MAT_Retail.LR12_AMP_CU_ALA_BI__c != null ? MAT_Retail.LR12_AMP_CU_ALA_BI__c : 0;
                            LR12MP_Car += MAT_Retail.LR12_AMP_Ca_REPO__c != null ? MAT_Retail.LR12_AMP_Ca_REPO__c : 0;
                            LR12MP_Cus += MAT_Retail.LR12_AMP_Cu_REPO__c != null ? MAT_Retail.LR12_AMP_Cu_REPO__c : 0;
                            LR12MP_Har += MAT_Retail.LR12_AMP_Ha_REPO__c != null ? MAT_Retail.LR12_AMP_Ha_REPO__c : 0;
                            LR12MP_Hor += MAT_Retail.LR12_AMP_Ho_REPO__c  != null ? MAT_Retail.LR12_AMP_Ho_REPO__c : 0; // 69744 - Account Profile - PROD Integration issues
                            LR12MP_Cus_Hor += MAT_Retail.LR12_AMP_CU_HOR_BI__c != null ? MAT_Retail.LR12_AMP_CU_HOR_BI__c : 0;
                            LR12MP_Kar += MAT_Retail.LR12_AMP_Ka_REPO__c != null ? MAT_Retail.LR12_AMP_Ka_REPO__c : 0;
                            LR12MP_Cus_Kar += MAT_Retail.LR12_AMP_CU_KAR_BI__c != null ? MAT_Retail.LR12_AMP_CU_KAR_BI__c : 0;
                            LR12MP_Lam += MAT_Retail.LR12_AMP_La_REPO__c != null ? MAT_Retail.LR12_AMP_La_REPO__c : 0;
                            LR12MP_Cus_PP += MAT_Retail.LR12_AMP_CU_PP_BI__c != null ? MAT_Retail.LR12_AMP_CU_PP_BI__c : 0;
                            LR12MP_Car_PP += MAT_Retail.LR12_AMP_Car_PP_BI__c != null ? MAT_Retail.LR12_AMP_Car_PP_BI__c : 0;
                            LR12MP_Tile_PP += MAT_Retail.LR12_AMP_Tile_PP_BI__c != null ? MAT_Retail.LR12_AMP_Tile_PP_BI__c : 0;
                            LR12MP_Har_PP += MAT_Retail.LR12_AMP_HW_PP_BI__c != null ? MAT_Retail.LR12_AMP_HW_PP_BI__c : 0;
                            LR12MP_Lam_PP += MAT_Retail.LR12_AMP_Lam_PP_BI__c != null ? MAT_Retail.LR12_AMP_Lam_PP_BI__c : 0;
                            LR12MP_Res_PP += MAT_Retail.LR12_AMP_Resi_PP_BI__c != null ? MAT_Retail.LR12_AMP_Resi_PP_BI__c : 0;
                            LR12MP_Lam_RL += MAT_Retail.LR12_AMP_Lam_RT_BI__c != null ? MAT_Retail.LR12_AMP_Lam_RT_BI__c : 0;
                            LR12MP_R += MAT_Retail.LR12_AMP_Re_REPO__c != null ? MAT_Retail.LR12_AMP_Re_REPO__c : 0;
                            LR12MP_Tile_RT += MAT_Retail.LR12_AMP_Tile_RT_BI__c != null ? MAT_Retail.LR12_AMP_Tile_RT_BI__c : 0;
                            LR12MP_Har_RW += MAT_Retail.LR12_AMP_HW_RT_BI__c != null ? MAT_Retail.LR12_AMP_HW_RT_BI__c : 0;
                            LR12MP_T += MAT_Retail.LR12_AMP_Ti_REPO__c != null ? MAT_Retail.LR12_AMP_Ti_REPO__c : 0;
                            LR12MP_AC += MAT_Retail.LR12_AMP_Al_Comm_REPO__c != null ? MAT_Retail.LR12_AMP_Al_Comm_REPO__c : 0;
                            LR12MP_Cus_AC += MAT_Retail.LR12_AMP_CU_ALA_Comm_BI__c != null ? MAT_Retail.LR12_AMP_CU_ALA_Comm_BI__c : 0;
                            LR12MP_RC += MAT_Retail.LR12_AMP_Re_Comm_REPO__c != null ? MAT_Retail.LR12_AMP_Re_Comm_REPO__c : 0;
                            LR12MP_RR += MAT_Retail.LR12_AMP_Re_Retail_REPO__c != null ? MAT_Retail.LR12_AMP_Re_Retail_REPO__c : 0;

                            teamMeberIds.add(MAT_Retail.Chain_Number_UserId__c);
                        }
                    }
                }
                ap.R_AMP_Car__c = AMPC;
                ap.R_AMP_A__c = AMPA;
                ap.R_AMP_Hor__c = AMPHO;
                ap.R_AMP_Kar__c = AMPK;
                ap.R_AMP_AC__c = AMPAC;
                ap.R_AMP_Car_PP__c = AMPCAPP;

                ap.R_AMP_Cus__c = AMPCU;
                ap.R_AMP_Cus_A__c = AMPCUA;
                ap.R_AMP_Cus_Hor__c = AMPCUHO;
                ap.R_AMP_Cus_Kar__c = AMPCUK;
                ap.R_AMP_Cus_AC__c = AMPCUAC;
                ap.R_AMP_Cus_PP__c = AMPCUPP;

                ap.R_AMP_Har__c = AMPH;
                ap.R_AMP_Har_RW__c = AMPHR;
                ap.R_AMP_Har_PP__c = AMPHPP;

                ap.R_AMP_Lam__c = AMPL;
                ap.R_AMP_Lam_RL__c = AMPLR;
                ap.R_AMP_Lam_PP__c = AMPLPP;

                ap.R_AMP_T__c = AMPT;
                ap.R_AMP_Tile_RT__c = AMPTR;
                ap.R_AMP_Tile_PP__c = AMPTPP;

                ap.R_AMP_R__c = AMPR;
                ap.R_AMP_Res_PP__c = AMPRPP;
                ap.R_AMP_RC__c = AMPRC;
                ap.R_AMP_RR__c = AMPRR;

                ap.R_AMP_ACT__c = AMPACT;


                ap.R_LR12MP_A__c = LR12MP_A;
                ap.R_LR12MP_Cus_A__c = LR12MP_Cus_A;
                ap.R_LR12MP_Car__c = LR12MP_Car;
                ap.R_LR12MP_Cus__c = LR12MP_Cus;
                ap.R_LR12MP_Har__c = LR12MP_Har;
                ap.R_LR12MP_Hor__c = LR12MP_Hor; // 69744 - Account Profile - PROD Integration issues
                ap.R_LR12MP_Cus_Hor__c = LR12MP_Cus_Hor;
                ap.R_LR12MP_Kar__c = LR12MP_Kar;
                ap.R_LR12MP_Cus_Kar__c = LR12MP_Cus_Kar;
                ap.R_LR12MP_Lam__c = LR12MP_Lam;
                ap.R_LR12MP_Cus_PP__c = LR12MP_Cus_PP;
                ap.R_LR12MP_Car_PP__c = LR12MP_Car_PP;
                ap.R_LR12MP_Tile_PP__c = LR12MP_Tile_PP;
                ap.R_LR12MP_Har_PP__c = LR12MP_Har_PP;
                ap.R_LR12MP_Lam_PP__c = LR12MP_Lam_PP;
                ap.R_LR12MP_Res_PP__c = LR12MP_Res_PP;
                ap.R_LR12MP_Lam_RL__c = LR12MP_Lam_RL;
                ap.R_LR12MP_R__c = LR12MP_R;
                ap.R_LR12MP_Tile_RT__c = LR12MP_Tile_RT;
                ap.R_LR12MP_Har_RW__c = LR12MP_Har_RW;
                ap.R_LR12MP_T__c = LR12MP_T;
                ap.R_LR12MP_AC__c = LR12MP_AC;
                ap.R_LR12MP_Cus_AC__c = LR12MP_Cus_AC;
                ap.R_LR12MP_RC__c = LR12MP_RC;
                ap.R_LR12MP_RR__c = LR12MP_RR;

                updateAccountProfileList.add(ap);
            }
        }
        system.debug(':::RETAIL BEFORE UPDATE::::' + updateAccountProfileList);
        if (updateAccountProfileList.size() > 0) {
            mohawkaccountteamservice.updateFromEvent = true;
            update updateAccountProfileList;
        }
    }

    global void BMFCalculation(Map < id, List < Mohawk_Account_Team__c >> calculationMap) {
        List < Account_Profile__c > updateAccountProfileList = new List < Account_Profile__c > ();
        if (calculationMap.size() > 0) {
            List < Account_Profile__c > apList = accountProfileList(calculationMap.keySet());
            /* List<string>brandList = new List<string>();
               List<string>ProductTypesList = new List<string>(); */
            for (Account_Profile__c AP: apList) {
                reSetAmps();
                List < String > teamMeberIds = new List < String > ();
                for (Mohawk_Account_Team__c MAT_BMF: calculationMap.get(ap.id)) {
                    if (MAT_BMF.BMF_Team_Member__c) {
                        /* brandList = MAT_BMF.Brands__c.split(';');
                           ProductTypesList = MAT_BMF.Product_Types__c.split(';'); */
                        if (!teamMeberIds.contains(MAT_BMF.Chain_Number_UserId__c)) {
                            AMPC += MAT_BMF.Actual_Mohawk_Purchases_Carpet__c != null ? MAT_BMF.Actual_Mohawk_Purchases_Carpet__c : 0;
                            AMPA += MAT_BMF.Actual_Mohawk_Purchases_Aladdin__c != null ? MAT_BMF.Actual_Mohawk_Purchases_Aladdin__c : 0;
                            AMPAC += MAT_BMF.Actual_Mohawk_Purchases_Aladdin_Comm__c != null ? MAT_BMF.Actual_Mohawk_Purchases_Aladdin_Comm__c : 0;
                            AMPHO += MAT_BMF.Actual_Mohawk_Purchases_Horizon__c != null ? MAT_BMF.Actual_Mohawk_Purchases_Horizon__c : 0;
                            AMPK += MAT_BMF.Actual_Mohawk_Purchases_Karastan__c != null ? MAT_BMF.Actual_Mohawk_Purchases_Karastan__c : 0;
                            AMPCAPP += MAT_BMF.Act_MHK_Purch_Car_PP__c != null ? MAT_BMF.Act_MHK_Purch_Car_PP__c : 0;

                            AMPCU += MAT_BMF.Actual_Mohawk_Purchases_Cushion__c != null ? MAT_BMF.Actual_Mohawk_Purchases_Cushion__c : 0;
                            AMPCUA += MAT_BMF.Act_MHK_Purch_CU_ALA__c != null ? MAT_BMF.Act_MHK_Purch_CU_ALA__c : 0;
                            AMPCUHO += MAT_BMF.Act_MHK_Purch_CU_HOR__c != null ? MAT_BMF.Act_MHK_Purch_CU_HOR__c : 0;
                            AMPCUK += MAT_BMF.Act_MHK_Purch_CU_KAR__c != null ? MAT_BMF.Act_MHK_Purch_CU_KAR__c : 0;
                            AMPCUAC += MAT_BMF.Act_MHK_Purch_CU_ALA_Comm__c != null ? MAT_BMF.Act_MHK_Purch_CU_ALA_Comm__c : 0;
                            AMPCUPP += MAT_BMF.Act_MHK_Purch_CU_PP__c != null ? MAT_BMF.Act_MHK_Purch_CU_PP__c : 0;

                            AMPH += MAT_BMF.Actual_Mohawk_Purchases_Hardwood__c != null ? MAT_BMF.Actual_Mohawk_Purchases_Hardwood__c : 0;
                            AMPHR += MAT_BMF.Act_MHK_Purch_HW_RT__c != null ? MAT_BMF.Act_MHK_Purch_HW_RT__c : 0;
                            AMPHPP += MAT_BMF.Act_MHK_Purch_HW_PP__c != null ? MAT_BMF.Act_MHK_Purch_HW_PP__c : 0;

                            AMPL += MAT_BMF.Actual_Mohawk_Purchases_Laminate__c != null ? MAT_BMF.Actual_Mohawk_Purchases_Laminate__c : 0;
                            AMPLR += MAT_BMF.Act_MHK_Purch_Lam_RT__c != null ? MAT_BMF.Act_MHK_Purch_Lam_RT__c : 0;
                            AMPLPP += MAT_BMF.Act_MHK_Purch_Lam_PP__c != null ? MAT_BMF.Act_MHK_Purch_Lam_PP__c : 0;

                            AMPT += MAT_BMF.Actual_Mohawk_Purchases_Tile__c != null ? MAT_BMF.Actual_Mohawk_Purchases_Tile__c : 0;
                            AMPTR += MAT_BMF.Act_MHK_Purch_Tile_RT__c != null ? MAT_BMF.Act_MHK_Purch_Tile_RT__c : 0;
                            AMPTPP += MAT_BMF.Act_MHK_Purch_Tile_PP__c != null ? MAT_BMF.Act_MHK_Purch_Tile_PP__c : 0;

                            AMPR += MAT_BMF.Actual_Mohawk_Purchases_Resilient__c != null ? MAT_BMF.Actual_Mohawk_Purchases_Resilient__c : 0;
                            AMPRPP += MAT_BMF.Act_MHK_Purch_Resi_PP__c != null ? MAT_BMF.Act_MHK_Purch_Resi_PP__c : 0;
                            AMPRC += MAT_BMF.Actual_Mohawk_Purch_Resilient_Comm__c != null ? MAT_BMF.Actual_Mohawk_Purch_Resilient_Comm__c : 0;
                            AMPRR += MAT_BMF.Actual_Mohawk_Purch_Resilient_Retail__c != null ? MAT_BMF.Actual_Mohawk_Purch_Resilient_Retail__c : 0;

                            AMPACT += MAT_BMF.Actual_Mohawk_Purch_Aladdin_Comm_Total__c != null ? MAT_BMF.Actual_Mohawk_Purch_Aladdin_Comm_Total__c : 0;

                            LR12MP_A += MAT_BMF.LR12_AMP_Al_REPO__c != null ? MAT_BMF.LR12_AMP_Al_REPO__c : 0;
                            LR12MP_Cus_A += MAT_BMF.LR12_AMP_CU_ALA_BI__c != null ? MAT_BMF.LR12_AMP_CU_ALA_BI__c : 0;
                            LR12MP_Car += MAT_BMF.LR12_AMP_Ca_REPO__c != null ? MAT_BMF.LR12_AMP_Ca_REPO__c : 0;
                            LR12MP_Cus += MAT_BMF.LR12_AMP_Cu_REPO__c != null ? MAT_BMF.LR12_AMP_Cu_REPO__c : 0;
                            LR12MP_Har += MAT_BMF.LR12_AMP_Ha_REPO__c != null ? MAT_BMF.LR12_AMP_Ha_REPO__c : 0;
                            LR12MP_Hor += MAT_BMF.LR12_AMP_Ho_REPO__c  != null ? MAT_BMF.LR12_AMP_Ho_REPO__c : 0; // 69744 - Account Profile - PROD Integration issues
                            LR12MP_Cus_Hor += MAT_BMF.LR12_AMP_CU_HOR_BI__c != null ? MAT_BMF.LR12_AMP_CU_HOR_BI__c : 0;
                            LR12MP_Kar += MAT_BMF.LR12_AMP_Ka_REPO__c != null ? MAT_BMF.LR12_AMP_Ka_REPO__c : 0;
                            LR12MP_Cus_Kar += MAT_BMF.LR12_AMP_CU_KAR_BI__c != null ? MAT_BMF.LR12_AMP_CU_KAR_BI__c : 0;
                            LR12MP_Lam += MAT_BMF.LR12_AMP_La_REPO__c != null ? MAT_BMF.LR12_AMP_La_REPO__c : 0;
                            LR12MP_Cus_PP += MAT_BMF.LR12_AMP_CU_PP_BI__c != null ? MAT_BMF.LR12_AMP_CU_PP_BI__c : 0;
                            LR12MP_Car_PP += MAT_BMF.LR12_AMP_Car_PP_BI__c != null ? MAT_BMF.LR12_AMP_Car_PP_BI__c : 0;
                            LR12MP_Tile_PP += MAT_BMF.LR12_AMP_Tile_PP_BI__c != null ? MAT_BMF.LR12_AMP_Tile_PP_BI__c : 0;
                            LR12MP_Har_PP += MAT_BMF.LR12_AMP_HW_PP_BI__c != null ? MAT_BMF.LR12_AMP_HW_PP_BI__c : 0;
                            LR12MP_Lam_PP += MAT_BMF.LR12_AMP_Lam_PP_BI__c != null ? MAT_BMF.LR12_AMP_Lam_PP_BI__c : 0;
                            LR12MP_Res_PP += MAT_BMF.LR12_AMP_Resi_PP_BI__c != null ? MAT_BMF.LR12_AMP_Resi_PP_BI__c : 0;
                            LR12MP_Lam_RL += MAT_BMF.LR12_AMP_Lam_RT_BI__c != null ? MAT_BMF.LR12_AMP_Lam_RT_BI__c : 0;
                            LR12MP_R += MAT_BMF.LR12_AMP_Re_REPO__c != null ? MAT_BMF.LR12_AMP_Re_REPO__c : 0;
                            LR12MP_Tile_RT += MAT_BMF.LR12_AMP_Tile_RT_BI__c != null ? MAT_BMF.LR12_AMP_Tile_RT_BI__c : 0;
                            LR12MP_Har_RW += MAT_BMF.LR12_AMP_HW_RT_BI__c != null ? MAT_BMF.LR12_AMP_HW_RT_BI__c : 0;
                            LR12MP_T += MAT_BMF.LR12_AMP_Ti_REPO__c != null ? MAT_BMF.LR12_AMP_Ti_REPO__c : 0;
                            LR12MP_AC += MAT_BMF.LR12_AMP_Al_Comm_REPO__c != null ? MAT_BMF.LR12_AMP_Al_Comm_REPO__c : 0;
                            LR12MP_Cus_AC += MAT_BMF.LR12_AMP_CU_ALA_Comm_BI__c != null ? MAT_BMF.LR12_AMP_CU_ALA_Comm_BI__c : 0;
                            LR12MP_RC += MAT_BMF.LR12_AMP_Re_Comm_REPO__c != null ? MAT_BMF.LR12_AMP_Re_Comm_REPO__c : 0;
                            LR12MP_RR += MAT_BMF.LR12_AMP_Re_Retail_REPO__c != null ? MAT_BMF.LR12_AMP_Re_Retail_REPO__c : 0;

                            teamMeberIds.add(MAT_BMF.Chain_Number_UserId__c);
                        }
                    }
                }



                ap.B_AMP_Car__c = AMPC;
                ap.B_AMP_A__c = AMPA;
                ap.B_AMP_Hor__c = AMPHO;
                ap.B_AMP_Kar__c = AMPK;
                ap.B_AMP_AC__c = AMPAC;
                ap.B_AMP_Car_PP__c = AMPCAPP;
                ap.B_AMP_Cus__c = AMPCU;
                ap.B_AMP_Cus_A__c = AMPCUA;
                ap.B_AMP_Cus_Hor__c = AMPCUHO;
                ap.B_AMP_Cus_Kar__c = AMPCUK;
                ap.B_AMP_Cus_AC__c = AMPCUAC;
                ap.B_AMP_Cus_PP__c = AMPCUPP;

                ap.B_AMP_Har__c = AMPH;
                ap.B_AMP_Har_RW__c = AMPHR;
                ap.B_AMP_Har_PP__c = AMPHPP;

                ap.B_AMP_Lam__c = AMPL;
                ap.B_AMP_Lam_RL__c = AMPLR;
                ap.B_AMP_Lam_PP__c = AMPLPP;

                ap.B_AMP_T__c = AMPT;
                ap.B_AMP_Tile_RT__c = AMPTR;
                ap.B_AMP_Tile_PP__c = AMPTPP;

                ap.B_AMP_R__c = AMPR;
                ap.B_AMP_Res_PP__c = AMPRPP;
                ap.B_AMP_RC__c = AMPRC;
                ap.B_AMP_RR__c = AMPRR;

                ap.B_AMP_ACT__c = AMPACT;

                ap.B_LR12MP_A__c = LR12MP_A;
                ap.B_LR12MP_Cus_A__c = LR12MP_Cus_A;
                ap.B_LR12MP_Car__c = LR12MP_Car;
                ap.B_LR12MP_Cus__c = LR12MP_Cus;
                ap.B_LR12MP_Har__c = LR12MP_Har;
                ap.B_LR12MP_Hor__c = LR12MP_Hor;       // 69744 - Account Profile - PROD Integration issues
                ap.B_LR12MP_Cus_Hor__c = LR12MP_Cus_Hor;
                ap.B_LR12MP_Kar__c = LR12MP_Kar;
                ap.B_LR12MP_Cus_Kar__c = LR12MP_Cus_Kar;
                ap.B_LR12MP_Lam__c = LR12MP_Lam;
                ap.B_LR12MP_Cus_PP__c = LR12MP_Cus_PP;
                ap.B_LR12MP_Car_PP__c = LR12MP_Car_PP;
                ap.B_LR12MP_Tile_PP__c = LR12MP_Tile_PP;
                ap.B_LR12MP_Har_PP__c = LR12MP_Har_PP;
                ap.B_LR12MP_Lam_PP__c = LR12MP_Lam_PP;
                ap.B_LR12MP_Res_PP__c = LR12MP_Res_PP;
                ap.B_LR12MP_Lam_RL__c = LR12MP_Lam_RL;
                ap.B_LR12MP_R__c = LR12MP_R;
                ap.B_LR12MP_Tile_RT__c = LR12MP_Tile_RT;
                ap.B_LR12MP_Har_RW__c = LR12MP_Har_RW;
                ap.B_LR12MP_T__c = LR12MP_T;
                ap.B_LR12MP_AC__c = LR12MP_AC;
                ap.B_LR12MP_Cus_AC__c = LR12MP_Cus_AC;
                ap.B_LR12MP_RC__c = LR12MP_RC;
                ap.B_LR12MP_RR__c = LR12MP_RR;

                updateAccountProfileList.add(ap);
            }
        }
        system.debug(':::BMF BEFORE UPDATE::::' + updateAccountProfileList);
        if (updateAccountProfileList.size() > 0) {
            mohawkaccountteamservice.updateFromEvent = true;
            update updateAccountProfileList;
        }
    }

    global void reSetAmps() {

        AMPC = 0;
        AMPA = 0;
        AMPAC = 0;
        AMPHO = 0;
        AMPK = 0;
        AMPCAPP = 0;

        AMPCU = 0;
        AMPCUA = 0;
        AMPCUAC = 0;
        AMPCUHO = 0;
        AMPCUK = 0;
        AMPCUPP = 0;

        AMPH = 0;
        AMPHR = 0;
        AMPHPP = 0;

        AMPL = 0;
        AMPLR = 0;
        AMPLPP = 0;

        AMPR = 0;
        AMPRPP = 0;

        AMPT = 0;
        AMPTR = 0;
        AMPTPP = 0;

        AMPACT = 0;
        AMPRC = 0;
        AMPRR = 0;

        LR12MP_A = 0;
        LR12MP_Cus_A = 0;
        LR12MP_Car = 0;
        LR12MP_Cus = 0;
        LR12MP_Har = 0;
        LR12MP_Hor = 0; // 69744 - Account Profile - PROD Integration issues
        LR12MP_Cus_Hor = 0;
        LR12MP_Kar = 0;
        LR12MP_Cus_Kar = 0;
        LR12MP_Lam = 0;
        LR12MP_Cus_PP = 0;
        LR12MP_Car_PP = 0;
        LR12MP_Tile_PP = 0;
        LR12MP_Har_PP = 0;
        LR12MP_Lam_PP = 0;
        LR12MP_Res_PP = 0;
        LR12MP_Lam_RL = 0;
        LR12MP_R = 0;
        LR12MP_Tile_RT = 0;
        LR12MP_Har_RW = 0;
        LR12MP_T = 0;
        LR12MP_AC = 0;
        LR12MP_Cus_AC = 0;
        LR12MP_RC = 0;
        LR12MP_RR = 0;
    }

    global List < Account_Profile__c > accountProfileList(set < id > accountProfileIds) {
        return [SELECT id, R_AMP_ACT__c, R_AMP_AC__c, R_AMP_RC__c, R_AMP_RR__c, R_AMP_A__c, R_AMP_Car__c, R_AMP_Cus__c, R_AMP_Har__c, R_AMP_Hor__c, R_AMP_Kar__c, R_AMP_Lam__c, R_AMP_R__c, R_AMP_T__c, B_AMP_ACT__c, B_AMP_AC__c, B_AMP_RC__c, B_AMP_RR__c, B_AMP_A__c, B_AMP_Car__c, B_AMP_Cus__c, B_AMP_Har__c, B_AMP_Hor__c, B_AMP_Kar__c, B_AMP_Lam__c, B_AMP_R__c, B_AMP_T__c, R_LR12MP_A__c, R_LR12MP_Cus_A__c, R_LR12MP_Car__c, R_LR12MP_Cus__c, R_LR12MP_Har__c, R_LR12MP_Cus_Hor__c, R_LR12MP_Kar__c, R_LR12MP_Cus_Kar__c, R_LR12MP_Lam__c, R_LR12MP_Cus_PP__c, R_LR12MP_Car_PP__c, R_LR12MP_Tile_PP__c, R_LR12MP_Har_PP__c, R_LR12MP_Lam_PP__c, R_LR12MP_Res_PP__c, R_LR12MP_Lam_RL__c, R_LR12MP_R__c, R_LR12MP_Tile_RT__c, R_LR12MP_Har_RW__c, R_LR12MP_T__c, R_LR12MP_AC__c, R_LR12MP_Cus_AC__c, R_LR12MP_RC__c, R_LR12MP_RR__c, B_LR12MP_A__c, B_LR12MP_Cus_A__c, B_LR12MP_Car__c, B_LR12MP_Cus__c, B_LR12MP_Har__c, B_LR12MP_Cus_Hor__c, B_LR12MP_Kar__c, B_LR12MP_Cus_Kar__c, B_LR12MP_Lam__c, B_LR12MP_Cus_PP__c, B_LR12MP_Car_PP__c, B_LR12MP_Tile_PP__c, B_LR12MP_Har_PP__c, B_LR12MP_Lam_PP__c, B_LR12MP_Res_PP__c, B_LR12MP_Lam_RL__c, B_LR12MP_R__c, B_LR12MP_Tile_RT__c, B_LR12MP_Har_RW__c, B_LR12MP_T__c, B_LR12MP_AC__c, B_LR12MP_Cus_AC__c, B_LR12MP_RC__c, B_LR12MP_RR__c FROM Account_Profile__c WHERE ID IN: accountProfileIds];
    }

    global void finish(Database.BatchableContext BC) {
        //Start-logic to run batch in DefaultQuery mode or LastBatchRun mode NAgendra changes 20-11-18
        AsyncApexJob a = [SELECT Id, Status, CompletedDate FROM AsyncApexJob WHERE Id =: BC.getJobId()];
        MAT_Batch_Setting__c custoSettingVal =  MAT_Batch_Setting__c.getInstance('RollupActualsFromMatToAP_Batch');
        if (custoSettingVal != null) {
            custoSettingVal.Last_Run_At__c = a.CompletedDate;
            update custoSettingVal;
        }else{
            custoSettingVal = new MAT_Batch_Setting__c(name = 'RollupActualsFromMatToAP_Batch', Last_Run_At__c = a.CompletedDate);
            insert custoSettingVal;
        }
        //End- NAgendra changes
    }

    //Schedulable 
    global void execute(SchedulableContext SC) {
        //Database.executeBatch(new RollupActualsFromMatToAP_Batch(false, true), 200);
        MAT_Batch_Setting__c custoSettingVal =  MAT_Batch_Setting__c.getInstance('RollupActualsFromMatToAP_Batch');
        integer batchSize =  integer.valueof( custoSettingVal.Batch_Size__c );
        Database.executeBatch(new RollupActualsFromMatToAP_Batch(false, true), batchSize);
    }

}