/**************************************************************************

Name : MACustomLaunchAppointment_cc_Test
DESC : This tess class is used for MACustomLaunchAppointment_cc
CreatedBy: Susmitha 
Date:Aug 22,2018

***************************************************************************/
@isTest
private class MACustomLaunchAppointment_cc_Test {
    
    private static testmethod void testMethod1() {
        Test.startTest();
        List<Account> accList = Utility_Test.createAccountsForNonInvoicing(true, 1);
            List<Contact> contList = Utility_Test.createContacts(true, 1, accList);
            List<Contact> contList1 = Utility_Test.createContacts(true, 1);
            
            // Contact from Account
            List<Event>  eveList = Utility_Test.createEvents(false, 1, accList);
            for(Integer i=0;i<eveList.size();i++){
                eveList[i].WhoId = contList[i].Id;
                eveList[i].whatId = accList[i].Id;

            }
            insert eveList;
        MACustomLaunchAppointment_cc.getEventId(eveList[0].Id);
        MACustomLaunchAppointment_cc.getEventId(accList[0].Id);

        MACustomLaunchAppointment_cc mc=new MACustomLaunchAppointment_cc();
        Test.stopTest();
    }
}