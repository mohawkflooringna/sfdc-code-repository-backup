/**************************************************************************

Name : MashupComponent_CC_Test 

===========================================================================
Purpose : This tess class is used for MashupComponent_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         26/April/2017     Created 
2.0        Susmitha       22/Dec/2017       Modified
***************************************************************************/
@isTest
private class MashupComponent_CC_Test {
    static List<Mashup_Management__c> mashupManList; 
    static Mashup_Management__c dataProduct;
    static User comUser = Utility_Test.getTestUser('Commercial', 'Commercial Sales User');
    static User resUser = Utility_Test.getTestUser('Residential', 'Residential Sales User');
    static Mashup_Management__c dataCom;
    // static Mashup_Management__c dataGeneral;
    // static Mashup_Management__c dataAccount;
    // static Mashup_Management__c dataContact;
    // static Mashup_Management__c dataProject;
    // static Mashup_Management__c dataQuote;
    // static Mashup_Management__c dataSampleOrder;
    
    public static void init(){
        mashupManList  = Utility_Test.getMashupManagementCusSetting();
        dataProduct = mashupManList.get(4);
        dataCom = mashupManList.get(1);
        System.debug('dataProduct : ' + dataProduct);
    }
    
    
    static testMethod void testMethod1() {
        System.runAs(comUser) {
            init();
            Test.startTest();
            
            MashupComponent_CC mashupCom = new MashupComponent_CC();
            mashupCom.objectName = 'Account';
            mashupCom.recordType = 'Reports';
            List<Account> resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
            mashupCom.objectId = resAccListForInvoicing[0].Id;
            mashupCom.getRenderObjectData(); // base on objectId        
            Map<String, Map<String,list<Mashup_Management__c>>> recordList = mashupCom.getRecordsByGroup();
            system.assert(mashupCom.recordType =='Reports');
            Test.stopTest();
        }
    }
    
    static testMethod void testMethod2() {
        System.runAs(resUser) {
            init();
            Test.startTest();
            MashupComponent_CC mashupCom = new MashupComponent_CC();
            mashupCom.mashupName = 'Create Product Order - Product';
            mashupCom.getMashupRecord(); // base on mashupName
            mashupCom.getRecordsByGroup();
            system.assert(mashupCom.mashupName == 'Create Product Order - Product');
            Test.stopTest();        
        }
    }
    
    static testMethod void testMethod3() {
        init();
        Test.startTest();
        System.runAs(Utility_Test.ADMIN_USER) {
            
            MashupComponent_CC mashupCom = new MashupComponent_CC();
            mashupCom.objectName = 'Account';
            mashupCom.recordType = 'Hybris';
            List<Account> resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
            mashupCom.objectId = resAccListForInvoicing[0].Id;
            mashupCom.getRenderObjectData(); // base on objectId        
            Map<String, Map<String,list<Mashup_Management__c>>> recordList = mashupCom.getRecordsByGroup();
            system.assert(mashupCom.objectName == 'Account');
        }
        
        Test.stopTest();
    }
    
}