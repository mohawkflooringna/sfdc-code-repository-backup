@isTest
public class MATMainProfile_BatchTest {
  static testmethod void testBatch() {
       List<Account> resAccListForInvoicing;
        List<Territory__c> terrList;
        
        List<Mohawk_Account_Team__c> mohAccTeam;
        Id resiNonInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
         MAT_Batch_Setting__c setting = new MAT_Batch_Setting__c();
            setting.Last_Run_At__c = SYstem.today().adddays(1);
            setting.MinutesDelay__c = 5;
            setting.Batch_Size__c = 200;
            setting.Name='MATMainProfile_Batch';
            setting.Batch_Query__c = 'Select Id,Chain_Number_UserId__c,Main_Profile__c,CreatedDate from Mohawk_Account_Team__c where Main_Profile__c =false';
            insert setting;
          
          Utility_Test.getTeamCreationCusSetting(); 
            AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
            APSC.Name = 'Two Team Account';
            APSC.Exclude_Chain_Numbers_1__c = '11';
            APSC.Exclude_Chain_Numbers_2__c = '12';
            insert APSC;
        System.runAs(Utility_Test.ADMIN_USER) {
            
           
            List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false); 
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1); 
            
            terrList = Utility_Test.createTerritories(true, 1);
            system.assert(terrList.size()>0);
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiNonInvRTId, accProfile, 'Horizon');
            mohAccTeam[0].Main_Profile__c =false;
            insert mohAccTeam;
            test.setCreatedDate(mohAccTeam[0].id, DateTime.newInstance(2019,1,29));
            test.startTest(); 
            mohAccTeam[0].Main_Profile__c =false;
            update mohAccTeam;
             system.assert(mohAccTeam!=null);
            String jobId= DataBase.executeBatch(new MATMainProfile_Batch(),200);
            System.assert(jobId != null);
            List<Mohawk_Account_Team__c> lstMHK = [SELECT createdDate,Main_Profile__c From Mohawk_Account_Team__c WHERE Id=:mohAccTeam[0].id];
            
           
            test.stopTest();
        }
      
  }
  static testmethod void testBatch1() {
       List<Account> resAccListForInvoicing;
        List<Territory__c> terrList;
        
        List<Mohawk_Account_Team__c> mohAccTeam;
        Id resiNonInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Non-Invoicing');
         
        System.runAs(Utility_Test.ADMIN_USER) {
            
            
            List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false); 
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1); 
            
            terrList = Utility_Test.createTerritories(true, 1);
            system.assert(terrList.size()>0);
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiNonInvRTId, accProfile, 'Horizon');
            mohAccTeam[0].Main_Profile__c =false;
            insert mohAccTeam;
            test.setCreatedDate(mohAccTeam[0].id, DateTime.newInstance(2019,1,29));
            test.startTest(); 
            mohAccTeam[0].Main_Profile__c =false;
            update mohAccTeam;

            Id jobId = DataBase.executeBatch(new MATMainProfile_Batch(),200);
            List<Mohawk_Account_Team__c> lstMHK = [SELECT createdDate,Main_Profile__c From Mohawk_Account_Team__c];
            String jobIdSch = System.schedule('Test my class', '0 0 0 ? * * *', new MATMainProfile_Batch());
            Mohawk_Account_Team__c MHK=[select id,Main_Profile__c from Mohawk_Account_Team__c where id=:mohAccTeam[0].id];
            System.assert(MHK.Main_Profile__c ==false);
            SYstem.assert(jobId !=null);
            SYstem.assert(jobIdSch !=null);
            test.stopTest();
        }
  }
  
   static testMethod void test_CheckMainProfileCheckBox_Insert() {
       MAT_Batch_Setting__c setting = new MAT_Batch_Setting__c();
            setting.Last_Run_At__c = SYstem.today().adddays(1);
            setting.MinutesDelay__c = 5;
            setting.Batch_Size__c = 200;
            setting.Name='MATMainProfile_Batch';
            //setting.Batch_Query__c = 'Select Id,Chain_Number_UserId__c,Main_Profile__c,CreatedDate from Mohawk_Account_Team__c where Main_Profile__c =false';
            insert setting;
        List<Account> resAccListForInvoicing;
        List<Territory__c> terrList;
        List<Mohawk_Account_Team__c> mohAccTeam;
        System.runAs(Utility_Test.ADMIN_USER) {        
            test.startTest(); 
            //init();
                      
            Utility_Test.getTeamCreationCusSetting(); 
            AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
            APSC.Name = 'Two Team Account';
            APSC.Exclude_Chain_Numbers_1__c = '11';
            APSC.Exclude_Chain_Numbers_2__c = '12';
            insert APSC;
            Id resiInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
            List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false); // Added by MB - 07/10/17
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1); 
            //resAccListForInvoicing = Utility_Test.createAccountsForInvoicing(true, 1); // Added by MB - 07/07/17
            terrList = Utility_Test.createTerritories(true, 2);
            system.assert(terrList.size()>0);
            mohAccTeam = Utility_Test.createMohawkAccountTeam(true, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId, accProfile, 'Horizon');
           
            List<Account> acList = new List<Account>();
            for(Integer i = 0; i < 1; i++){                
                acList.add(new Account(Name = 'Residential Account for Invoicing'+i, 
                                       Business_Type__c = 'Residential', Customer_Group__c = 'Mohawk Aligned;Independent Aligned;Open Line',
                                       Role_C__c = 'End User', BillingPostalCode = '100'+i, ShippingPostalCode = '100'+i, Facilities__c= '0-10',
                                       ShippingCountry = 'United States', ShippingCity= 'New Year', ShippingStreet = 'Street',
                                       Global_Account_Number__c = 'R20000'+i, CAMS_Account_Number__c = 'R.900124.1'+i, Chain_Number__c = 'R.900124',
                                       RecordTypeId = UtilityCls.getRecordTypeInfo('Account', UtilityCls.INVOICING_ACCOUNT) ));                 
            }
            
            insert acList;
            
            //  List<Account> resAccListForInvoicing1 = Utility_Test.createResidentialAccountsForInvoicing(true, 1); 
            List<Mohawk_Account_Team__c> mohAccTeam3  = Utility_Test.createMohawkAccountTeam(true, 1, acList, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId, accProfile, 'Aladdin');
            mohAccTeam3[0].Main_Profile__c = false; 
            update acList;
            
           Id jobId= DataBase.executeBatch(new MATMainProfile_Batch(),200);
            String jobIdSch = System.schedule('Test my class', '0 0 0 ? * * *', new MATMainProfile_Batch());
            SYstem.assert(jobId !=null);
            SYstem.assert(jobIdSch !=null);
            Mohawk_Account_Team__c MHK=[select id,Main_Profile__c from Mohawk_Account_Team__c where id=:mohAccTeam[0].id];
            System.assert(MHK.Main_Profile__c ==true);
            
            //String jobIdSch = System.schedule('Test my class', '0 0 0 1 JAN,APR,JUL,OCT ? *', new updateMATForBI_Batch());
            
            test.stopTest();
        }
    }
    
}