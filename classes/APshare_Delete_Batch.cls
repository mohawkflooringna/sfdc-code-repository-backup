global class APshare_Delete_Batch implements Database.Batchable<sObject>, Database.Stateful, Schedulable{
    
    global static set<id> processedAPs = new set<id>();
    global Database.QueryLocator start(Database.BatchableContext bc) {
      
        String Query;
        id invMATRTId = Schema.SObjectType.Mohawk_Account_Team__c.getRecordTypeInfosByName().get('Residential Invoicing').getRecordTypeId();
        Batch_Query__mdt [] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'APshare_Delete_Batch'];
        If(QureyList.size() > 0) {
            query = QureyList[0].Query_String__c ;
        } else {
            String fields = 'Id, IsDeleted, Name, RecordTypeId, Account_profile__c, User__c, Territory__c, Territory_User_Number__c ';
            String whereCond = 'RecordTypeId = :invMATRTId AND Account_profile__c != null AND isdeleted = true AND ( LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY ) '; 
            query = 'SELECT ' + fields + ' FROM Mohawk_Account_Team__c WHERE ' + whereCond + ' ORDER BY Chain_Number__c ASC ALL Rows';
            System.debug('Query: ' + query);
        }
        return Database.getQueryLocator(Query);
    }
    
    
    
    global static void execute(Database.BatchableContext BC, List<Mohawk_Account_Team__c> scope){
        List<Mohawk_Account_Team__c> delMATList = new List<Mohawk_Account_Team__c>();

        map<id, Set<id>> delAPuser = new map<id, set<id>>();
        map<id, Set<String>> delAPTMs = new map<id, Set<String>>();
        
        for(Mohawk_Account_Team__c mat: scope){
            if(processedAPs.isEmpty() || (!processedAPs.isEmpty() && !processedAPs.contains(mat.Account_profile__c))){
                // Collecting all deleted users and TMnums
                if (mat.User__c != null){
                    if(delAPuser.containsKey(mat.Account_profile__c)){
                        delAPuser.get(mat.Account_profile__c).add(mat.User__c);
                    } else {
                        delAPuser.put(mat.Account_profile__c, new Set<Id>{mat.User__c});
                    }
                }
                
                
                if(mat.Territory_User_Number__c != null && mat.Territory_User_Number__c != ''){
                    List<String> tmNumbers = new List<String>();
                    String tm = '';
                    tm = mat.Territory_User_Number__c;
                    if(tm.contains(';')){
                        tmNumbers = tm.split(';');
                    } else {
                        tmNumbers.add(tm);
                    }
                    
                    for(String tmNumber: tmNumbers){
                        String tmNum = '';
                        tmNum = tmNumber.replaceAll('\\s+', '');
                        if(delAPTMs.containsKey(mat.Account_profile__c)){
                            delAPTMs.get(mat.Account_profile__c).add(tmNum);
                        }else{
                            delAPTMs.put(mat.Account_profile__c,  new Set<String>{tmNum});
                        }
                    }
                }
                processedAPs.add(mat.Account_Profile__c); 
            }            
        }
        
        
        
        if(delAPTMs != null && delAPTMs.size()>0){
            String fieldsql = 'Id, IsDeleted, Name, RecordTypeId, Account_profile__c, User__c, Territory__c, Territory_User_Number__c ';
            Set<id> delMATAPIds = new Set<Id>();
            delMATAPIds.addAll(delAPuser.keyset());
            delMATList = Database.query('SELECT ' + fieldsql + ' FROM Mohawk_Account_Team__c WHERE Account_Profile__c IN : delMATAPIds AND Territory_User_Number__c != null');
        }
        
        
        if(delAPuser.size() > 0 || delAPTMs.size() > 0){
            ProcessBatchUpdate(delMATList,delAPuser, delAPTMs);
        }
    }
    
    global static void ProcessBatchUpdate(List<Mohawk_Account_Team__c> mhkATList,  map<id, Set<id>> delAPuser, map<id, Set<String>> delAPTMs ){
        List<Account_Profile__share> delAccShareIdsList = new List<Account_Profile__share>();
        
        Map<Id, Set<String>> apTMMap = new Map<Id, Set<String>>();
        Map<String, String> tmTerrMap = new Map<String, String>();
        Map<String, Id> grpNameMap = new Map<String, Id>();
        Map<String, String> extmTerrMap = new Map<String, String>();
        
        Map<String, ID> existingApGroupMap = new  Map<String, ID>();
        Set<String> existingGrpset = new Set<string>();
      //  Map<Id, Set<String>> apGroupMap = new Map<Id, Set<String>>();
        
        Set<Id> apIds = new Set<Id>();
        Set<ID> UserOrGroupIdset = new Set<ID>();
        
        Set<String> TMNumSet = new Set<String>();
        Set<String> grpNameSet = new Set<String>();
        
        
        for(set<String> tmNums :delAPTMs.values()){
            TMNumSet.addAll(tmNums);
        }         
        
        // Get all the Districts for the Deleted TM Numbers
        if(TMNumSet.size()>0){
            for(List<Territory_User__c> terrUserList: [SELECT Id, Name, Territory__r.Name, User__c
                                                       FROM Territory_User__c
                                                       WHERE Name IN :TMNumSet
                                                       AND Role__c = 'Territory Manager']){
                                                           System.debug('Territory List: ' + terrUserList);
                                                           for(Territory_User__c terrUser: terrUserList){
                                                               tmTerrMap.put(terrUser.Name, terrUser.Territory__r.Name);
                                                               grpNameSet.add(terrUser.Territory__r.Name);
                                                           }
                                                       }
        }    
        
        
        TMNumSet = new Set<String>();
        // Process all non deleted MATs for the AP.
        for(Mohawk_Account_Team__c mat: mhkATList){
            String tm = '';
            apIds.add(mat.Account_profile__c);           
            // Check the user is already in other MATs
            if(delAPuser.get(mat.Account_profile__c) != null && delAPuser.get(mat.Account_profile__c).contains(mat.user__c)){
                delAPuser.get(mat.Account_profile__c).remove(mat.user__c);
            } 
            
            if(mat.Territory_User_Number__c != null && mat.Territory_User_Number__c != ''){
                List<String> tmNumbers = new List<String>();
                tm = mat.Territory_User_Number__c;
                if(tm.contains(';')){
                    tmNumbers = tm.split(';');
                }else{
                    tmNumbers.add(tm);
                }
                for(String tmNumber: tmNumbers){
                    String tmNum = '';
                    tmNum = tmNumber.replaceAll('\\s+', '');
                    TMNumSet.add(tmNum);
                    if (delAPTMs.containsKey(mat.Account_profile__c) != null && delAPTMs.get(mat.Account_profile__c).contains(tmNum) ){
                        delAPTMs.get(mat.Account_profile__c).remove(tmNum);
                    }
                    if (apTMmap.containsKey(mat.Account_profile__c)){
                        apTMmap.get(mat.Account_profile__c).add(tmNum);
                    } else {
                        apTMmap.put(mat.Account_profile__c, new Set<String>{tmNum});
                    }
                }
            }
        }
        
        // Get all the Districts for the existing TMs
        if(TMNumSet.size()>0){
            for(List<Territory_User__c> terrUserList: [SELECT Id, Name, Territory__r.Name, User__c
                                                       FROM Territory_User__c
                                                       WHERE Name IN :TMNumSet
                                                       AND Role__c = 'Territory Manager']){
                                                           System.debug('Territory List: ' + terrUserList);
                                                           for(Territory_User__c terrUser: terrUserList){
                                                               extmTerrMap.put(terrUser.Name, terrUser.Territory__r.Name);                                                           
                                                           }
                                                       }
        }
        
      /*  for(id apId: apIds){            
            if(apTMmap.containsKey(apId)){
                for(string tmNum : apTMmap.get(apId)){
                    tmNum = tmNum.replaceAll('\\s+', '');
                    if(apGroupMap.containsKey(apid)){
                        apGroupMap.get(apId).add(extmTerrMap.get(tmNum));
                    } else {
                        apGroupMap.put(apId, new Set<String>{extmTerrMap.get(tmNum)});
                    }
                }                
            }
        } */
        
        if(grpNameSet.size()>0 ){
            System.debug('Group Name Set: ' + grpNameSet);
            List<Group> grpList = [SELECT Id, Name FROM Group 
                                   WHERE Name IN :grpNameSet 
                                   AND Type = 'Regular'];
            
            for(Group grp: grpList){
                grpNameMap.put(grp.Name, grp.Id);
                UserOrGroupIdset.add(grp.Id);
            }
        }
        
        
        
        for(set<id> userIds :delAPuser.values()){
            UserOrGroupIdset.addAll(userIds);
        }
        
        system.debug(':::: UserOrGroupIdset::::' +  UserOrGroupIdset);
        system.debug(':::: delAPuser.keyset() :::' + delAPuser.keyset() );
        if(apIds.size()>0){
            // Get APshare records for the deleted records
            for(List<Account_Profile__Share> apShareList : [SELECT Id, ParentID , UserOrGroupId 
                                                            FROM Account_Profile__Share
                                                            WHERE parentid IN :delAPuser.keyset()
                                                            AND UserOrGroupId IN : UserOrGroupIdset ]){
                                                                for(Account_Profile__Share apShare: apShareList){
                                                                    existingApGroupMap.put(String.valueOf(apShare.ParentID) + String.valueOf(apShare.UserOrGroupId), apShare.Id) ;
                                                                }
                                                            }
        }
        
        system.debug( '::::existingApGroupMap::::' + existingApGroupMap );
        for(Id apID: delAPuser.keyset()){
            Set<String> tmNumbers = new Set<String>();
          //  tmNumbers = delAPTMs.get(apID);
             tmNumbers = apTMmap.get(apID);
            if(tmNumbers != null){
                for(String tmNum: tmNumbers){
                    tmNum = tmNum.replaceAll('\\s+', '');
                    String terrName = tmTerrMap.get(tmNum);
                 /*  System.debug('tmNum: ' + tmNum + ', Terr. Name: ' + terrName);
                    if (apGroupMap.containsKey(apID) && apGroupMap.get(apID).contains(terrName)){
                        continue;
                    }  */                                      
                 
                    String apGrpid = String.valueOf(apID) + String.valueOf(grpNameMap.get(terrName)) ;
                    system.debug('::::terrName::::::::' + String.valueOf(terrName));
                    system.debug('::::apID::::::::' + String.valueOf(apID));
                    system.debug(':::: apGrpid::::' +  apGrpid);
                    if(existingApGroupMap.containsKey(apGrpid)){
                        existingApGroupMap.remove(apGrpid);
                      // delAccShareIdsList.add(new account_profile__share(id = existingApGroupMap.get(apGrpid)));                   
                    }
                }
            }
            
            // Delete AP users validated against the existing MATs
            set<ID> userIDs =  delAPuser.get(apID);
            for(id uid : userIDs){
                String apGrpid = String.valueOf(apID) + String.valueOf(uid) ;
                if(existingApGroupMap.containsKey(apGrpid)){ 
                    delAccShareIdsList.add(new account_profile__share (id = existingApGroupMap.get(apGrpid)));
                    existingApGroupMap.remove(apGrpid);
                }                  
            }         
            
        }
        
        // Delete the Group ids that doesn't exist for the Account Profile
      
        if (existingApGroupMap != null && existingApGroupMap.size() > 0){
            for(id shareId : existingApGroupMap.values()){
               delAccShareIdsList.add(new account_profile__share (id = shareId)); 
            }
        }
        
        if(delAccShareIdsList != null && delAccShareIdsList.size()>0){
              try{
                Database.DeleteResult[] results = Database.delete(delAccShareIdsList, false);
                for (Database.DeleteResult result : results) {
                    if (result.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully deleted AccountShare: ' + result.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : result.getErrors()) {
                            System.debug('Error while deleting AccountShare: ' + err.getMessage());
                        }
                    }
                } 
            }
            catch (System.DMLException dmle){
                for(Integer i=0; i<dmle.getNumDml(); i++){
                    system.debug(dmle.getDmlMessage(i));
                }
            }
        }
    }
    global void finish( Database.BatchableContext BC ) {
        
    }
    
    global void execute(SchedulableContext sc){
        Database.executeBatch(new APshare_Delete_Batch(),100);
    }
}