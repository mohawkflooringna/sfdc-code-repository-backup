/**************************************************************************

Name : CompetitiveAssessment_CC
DESC : This tess class is used for CompetitiveAssessment_CC
CreatedBy: Lillian Chen

***************************************************************************/
@isTest
private class CompetitiveAssessment_CC_Test {
    
    private static testmethod void testinitalAssessments() {
        Test.startTest();
        System.runAs(Utility_Test.ADMIN_USER){
            
            List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false);
            system.assert(accProfile!=null);
            List<Competitive_Assessment__c> assement=Utility_Test.getCompetitiveAssessment(accProfile);
            
            CompetitiveAssessment_CC.initalAssessments(accProfile.get(0).id);
            CompetitiveAssessment_CC.getFilterList(accProfile.get(0).id,'Test','Test11');
            CompetitiveAssessment_CC.getProductTypeId();
            CompetitiveAssessment_CC.getShareTypeId();
            CompetitiveAssessment_CC.getSegementValues();
            CompetitiveAssessment_CC.updateAssessments(assement.get(0));
            CompetitiveAssessment_CC.insertAssessment(assement.get(0),accProfile.get(0).id);
            CompetitiveAssessment_CC.deleteAssessment(assement.get(0));
            CompetitiveAssessment_CC.createNewCompetitorProduct( '{ "Name": "Test" }','Carpet' );
            CompetitiveAssessment_CC.getCompetitorProductsRecordTypeId('Carpet');
            CompetitiveAssessment_CC.getrecordtypeIdforAll('Account','Invoicing');
        }
        Test.stopTest();
    }
    
    private static testmethod void genricLookupController() {
        Test.startTest();
        System.runAs(Utility_Test.ADMIN_USER){
          list<account> accountList = Utility_Test.createAccounts(true,3);
          //string SearchResults =  genricLookupController.searchDB('Account', 'Name', 'Id', 4, 'Name', 'Test Account', 'RecordtypeID') ;
        }
        Test.stopTest();
    }
    
}