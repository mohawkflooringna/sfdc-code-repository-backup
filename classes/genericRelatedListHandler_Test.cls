/**************************************************************************

Name : genericRelatedListHandler_Test 

===========================================================================
Purpose : This tess class is used for genericRelatedListHandler
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
**************************************************************************/
@isTest
private class genericRelatedListHandler_Test {
    
    
    static testMethod void testMethod1() {
        List<Account> accList  =  Utility_Test.createAccounts(true, 1);

        Account_Profile__c apRec = new Account_Profile__c();
        apRec.Name = 'test record';
        apRec.Multi_Channel__c = true;
        insert apRec;
        genericRelatedListHandler.getRelatedListNames( apRec.id );
        //genericRelatedListHandler.getIconName('CPL_Cushion__c','Account_Profile__c');

        
    }
}