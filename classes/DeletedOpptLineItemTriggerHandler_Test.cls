/**************************************************************************

Name : DeletedOpptLineItemTriggerHandler_Test
===========================================================================
Purpose : Test class for DeletedOpptLineItemTriggerHandler
===========================================================================
History:
--------
VERSION    AUTHOR         DATE             DETAIL          DESCRIPTION
1.0        Susmitha       2/March/2018    Create  
***************************************************************************/
@isTest
private class DeletedOpptLineItemTriggerHandler_Test {
    static testMethod void testMethod1() {
        Map<Id, Opportunity_Line_Item__c> newListMap =new Map<Id, Opportunity_Line_Item__c>();
        List<Opportunity_Line_Item__c> oppLineItemList = new List<Opportunity_Line_Item__c>();
        List<Opportunity> oppList=Utility_test.createOpportunities(true,3);
        
        
        for(Integer i = 0; i < 3; i++){
            oppLineItemList.add( new Opportunity_Line_Item__c(Opportunity__c = oppList[i].Id));
        }
        insert oppLineItemList ;
        
        for(Opportunity_Line_Item__c op :oppLineItemList){
            newListMap.put(op.Id,op);
        }
        system.assert(newListMap!=null);
        // Negetive test.
        
        Map<Id, Opportunity_Line_Item__c> delOppItemMap = new Map<Id, Opportunity_Line_Item__c>([select id, name from Opportunity_Line_Item__c where isdeleted = true all rows]);
        DeletedOpptLineItemTriggerHandler.deleteAfterInsert(delOppItemMap); 
    }
    
    
}