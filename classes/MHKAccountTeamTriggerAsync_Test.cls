/**************************************************************************

Name : MHKAccountTeamTriggerAsync_Test

===========================================================================
Purpose :   This class is used for MHKAccountTeamTriggerAsync
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION

1.0        Susmitha     27/March/2018      Created
***************************************************************************/
@isTest
private class MHKAccountTeamTriggerAsync_Test{

     static Id resInVoicingRTId    = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
     static List<Territory2> terr2List = new List<Territory2>();
    public static void init(){   
      //  Utility_Test.getTeamCreationCusSetting(); 
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
        
       
    }
    
    static testMethod void testMethod2() {
        init();
        List<Mohawk_Account_Team__c> mohAccTeam = new List<Mohawk_Account_Team__c>();
        List<Territory_User__c>  terrUser;
        List<user>testUser = Utility_Test.createTestUsers( false , 2,  'Commercial Sales Manager' );
        testUser[0].MHKUserRole__c = 'Senior Vice President';
        testUser[1].MHKUserRole__c ='Senior Vice President';
        
        insert testUser;
        
        System.runAs( testUser[0] ) {
            Test.startTest();
            List<Account>accList = Utility_Test.createAccounts( false,1 );
            List<Territory__c>terrList = Utility_Test.createTerritories( true, 1 );
            List<Territory_User__c> territoryUserList = new List<Territory_User__c>();
            territoryUserList.add(new Territory_User__c(Territory__c = terrList[0].Id, Role__c = 'Salesperson', User__c = testUser[0].Id)); 
            
            territoryUserList[0].Territory__c = terrList[0].Id;
            territoryUserList[0].Role__c = UtilityCls.AE;
            territoryUserList[0].user__c=testUser[0].Id;
            territoryUserList[0].Segments__c = 'Corporate';
            territoryUserList[0].Last_Lead_Received_Time__c = System.Now();
            territoryUserList[0].Millisecond__c = 1;
            insert territoryUserList;
            system.assert(territoryUserList.size()>0);
            for( Integer i=0; i < accList.size(); i++ ){                
                accList[i].Territory__c = terrList[i].Id;
                accList[i].Workplace_Retail_Territory__c=terrList[i].id;
            }
            insert accList;
            
            List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
            apSetting[0].Yellow_Threshold_Carpet__c = 10;
            apSetting[0].Red_Threshold_Carpet__c = 10;
            apSetting[0].Yellow_Threshold_Cushion__c = 10;
            apSetting[0].Red_Threshold_Cushion__c = 10;
            apSetting[0].Yellow_Threshold_Hardwood__c = 10;
            apSetting[0].Red_Threshold_Hardwood__c = 10; 
            apSetting[0].Yellow_Threshold_Laminate__c = 10; 
            apSetting[0].Red_Threshold_Laminate__c = 10;
            apSetting[0].Yellow_Threshold_Tile__c = 10;
            apSetting[0].Red_Threshold_Tile__c = 10;
            apSetting[0].Yellow_Threshold_Resilient__c = 10;
            apSetting[0].Red_Threshold_Resilient__c = 10;
            apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
            apSetting[0].Red_Threshold_Mainstreet__c = 10;
            apSetting[0].Retail_Sales_Carpet__c = 10;
            update apSetting[0];
            List<Account_Profile__c> apList = new List<Account_Profile__c>();
            Account_Profile__c ap = new Account_Profile__c();
            ap.Primary_Business__c = apSetting[0].Id;
            ap.Annual_Retail_Sales__c = 300000;
            ap.Annual_Retail_Sales_Non_Flooring__c = 10;
            ap.Chain_Number__c = '3213111';
            apList.add(ap);
            Insert aplist;
            
            accList[0].Account_Profile__c = aplist[0].Id;
            update accList;
            
          
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, accList, terrList, Utility_Test.RESIDENTIAL_USER, resInVoicingRTId);
            for(Mohawk_Account_Team__c mat:mohAccTeam){
                mat.Account__c=accList[0].id;
                mat.User__c=testUser[1].id;
                mat.Territory_User_Number__c = '05-J8L';
            }
            insert mohAccTeam;
           Set<Id> accId = new Set<Id>();
           Set<Id> matIds = new Set<Id>();
           for(Account acc:accList){
               accId.add(acc.id);
           }
           for(Mohawk_Account_Team__c mat:mohAccTeam){
               matIds.add(mat.id);
           }
            //System.debug('resCommercialRTId'+resCommercialRTId);
           MHKAccountTeamTriggerAsync.updateMohawkAccountTeamMemberIdsAsync(accId,matIds,true,false);
           //MHKAccountTeamTriggerAsync.updateMohawkAccountTeamMemberIdsAsync(accId,matIds,false,false,true);
          Test.stopTest();  
            
        }
        
    }
     static testmethod void testdeleteTerritoryAccounts() {
           init();
           List<Territory__c> terrList = new List<Territory__c>();
          List<Account> resAccListForInvoicing = new List<Account>();
          List<Mohawk_Account_Team__c> mohAccTeam = new List<Mohawk_Account_Team__c>();
          List<Territory_User__c>  terrUserList = new  List<Territory_User__c>();
          List<User> userList = new List<User>();
          Id resTerrUserRecTypeId = UtilityCls.getRecordTypeInfo('Territory_User__c', 'Residential');
       System.runAs(Utility_Test.ADMIN_USER) {
           
            test.startTest();
            init();
            
           
            
            User u=Utility_Test.getTestUser('TestUser',null);
            insert u;
            Territory2Model terr2Model = [SELECT Id FROM Territory2Model LIMIT 1];
            Territory2Type terr2Type = [SELECT Id FROM Territory2Type LIMIT 1];
            
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'District 991 TM', 'D991_TM'));
            terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'District 991 DM', 'D991'));
            //terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'R991 - South Soft RVP ', 'R991'));
            //terr2List.add((Territory2)setTerritory2Fields(terr2Model.Id, terr2Type.Id, 'S991 - S/S East SVP ', 'S991'));
            insert terr2List;
           
            terrList =  new List<Territory__c>();
            Territory__c terr = new Territory__c();
            terr.RecordTypeId = UtilityCls.getRecordTypeInfo('Territory__c', 'Residential');
            terr.Territory_Code__c = '991';
            terr.Region_Code__c = '991';
            terr.Sector_Code__c = '991';
            terr.Name = 'District 991';
            terrList.add(terr);
            insert terrList;
           userList = Utility_Test.createTestUsers(false, 5, 'Residential Sales User');
           insert userList;
             terrUserList = new List<Territory_User__c>();
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-J8L', 'Territory Manager', userList[0].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-K8L', 'District Manager', userList[1].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-H8L', 'Business Development Manager', userList[2].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-M8L', 'Regional Vice President', userList[3].Id));
            terrUserList.add((Territory_User__c)setTerritoryUserFields(terrList[0].Id, resTerrUserRecTypeId, '05-N8L', 'Senior Vice President', userList[4].Id));
            insert terrUserList;
            
            /***Creating Account Profile Setting and Account Profile***8/22/2017****/
            List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
            apSetting[0].Yellow_Threshold_Carpet__c = 10;
            apSetting[0].Red_Threshold_Carpet__c = 10;
            apSetting[0].Yellow_Threshold_Cushion__c = 10;
            apSetting[0].Red_Threshold_Cushion__c = 10;
            apSetting[0].Yellow_Threshold_Hardwood__c = 10;
            apSetting[0].Red_Threshold_Hardwood__c = 10; 
            apSetting[0].Yellow_Threshold_Laminate__c = 10; 
            apSetting[0].Red_Threshold_Laminate__c = 10;
            apSetting[0].Yellow_Threshold_Tile__c = 10;
            apSetting[0].Red_Threshold_Tile__c = 10;
            apSetting[0].Yellow_Threshold_Resilient__c = 10;
            apSetting[0].Red_Threshold_Resilient__c = 10;
            apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
            apSetting[0].Red_Threshold_Mainstreet__c = 10;
            apSetting[0].Retail_Sales_Carpet__c = 10;
            update apSetting[0];
            List<Account_Profile__c> apList = new List<Account_Profile__c>();
            Account_Profile__c ap = new Account_Profile__c();
            ap.Primary_Business__c = apSetting[0].Id;
            ap.Annual_Retail_Sales__c = 300000;
            ap.Annual_Retail_Sales_Non_Flooring__c = 10;
            ap.Chain_Number__c = '3213111';
            apList.add(ap);
            Insert aplist;
            
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for(Account a :resAccListForInvoicing){
                a.Total_Account_Chain_Sales__c=50;
                a.Account_Profile__c=aplist[0].id;
            }
            insert resAccListForInvoicing;
            system.assert(resAccListForInvoicing.size()>0);
            
            //update Account with Account profile information
            resAccListForInvoicing[0].Chain_Number__c = '3213311';
            resAccListForInvoicing[0].Account_Profile__c = aplist[0].Id;
            update resAccListForInvoicing;
           
            /***END Creating Account Profile Setting and Account Profile****/
           
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resInVoicingRTId);

           for(Mohawk_Account_Team__c mat:mohAccTeam){
                mat.Account__c=resAccListForInvoicing[0].id;
                mat.User__c=u.id;
                mat.Territory_User_Number__c = '05-J8L';
            }
            insert mohAccTeam;
           
           Set<Id> accId = new Set<Id>();
           Set<Id> matIds = new Set<Id>();
           for(Account acc:resAccListForInvoicing){
               accId.add(acc.id);
           }
           for(Mohawk_Account_Team__c mat:mohAccTeam){
               matIds.add(mat.id);
           }
            //System.debug('resCommercialRTId'+resCommercialRTId);
           MHKAccountTeamTriggerAsync.updateMohawkAccountTeamMemberIdsAsync(accId,matIds,false,true);
           test.stopTest();
        }
            
         
        

    }
     public static Territory_User__c setTerritoryUserFields(Id terrId, Id recTypeId, String name, String role, String userId){
        Territory_User__c tUser = new Territory_User__c();
        tUser.RecordTypeId = recTypeId;
        tUser.Territory__c = terrId;
        tUser.Name = name;
        tUser.Role__c = role;
        tUser.User__c = userId;
        return tUser;
    }
    public static Territory2 setTerritory2Fields(Id terr2ModelId, Id terr2TypeId, String name, String devName){
        Territory2 terr2 = new Territory2();
        terr2.Territory2ModelId = terr2ModelId;
        terr2.Territory2TypeId = terr2TypeId;
        terr2.Name = name;
        terr2.DeveloperName = devName;
        return terr2;
    }
   
}