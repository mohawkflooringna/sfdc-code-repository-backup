//Deprecated
/**************************************************************************

Name : StrategicAccountSubmitApproval_CC 

===========================================================================
Purpose :  As a Sales User, I want the ability to submit a request to the Strategic Account Admin 
for the creation of a strategic account so that I can manage information associated with the strategic account.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester        8/March/2017    Created         CSR:

***************************************************************************/
public with sharing class StrategicAccountSubmitApproval_CC {
    private static List<String> fieldToCheckList = new List<String>(); // get the Strategics Account FieldSet
    
    static {
        fieldToCheckList = UtilityCls.getFieldSetAllFieldPaths('Account', UtilityCls.STRATEGIC_ACCOUNT_FIELD_SET);
    }
    
    @AuraEnabled
    public static void runApproval(Account approvalAccount){ 
        
        saveAccount(approvalAccount);
        String accountId =  approvalAccount.Id;
        
        // Create an approval request for the account
        Approval.ProcessSubmitRequest req1 =  new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(accountId);
        
        // Submit on behalf of a specific submitter
        req1.setSubmitterId(UserInfo.getUserId()); 
        
        // Submit the record to specific process and skip the criteria evaluation
        req1.setProcessDefinitionNameOrId(UtilityCls.UPDATE_STRATEGIC_ACCOUNT); // process name
        req1.setSkipEntryCriteria(false);    
        
        // Submit the approval request
        Approval.ProcessResult result = Approval.process(req1);
        
        System.debug('result : ' + result);
    }
    
    @AuraEnabled
    public static Boolean  IsCurrentUseAdmin(){
        Profile profile = [Select Name from Profile where Id =: UserInfo.getProfileid()];
        String profileName = profile.Name == null ? '' : profile.Name;
        System.debug('profileName : ' + profileName);
        System.debug('status : ' + profileName.equalsIgnoreCase(UtilityCls.SYSTEM_ADMINISTRATOR));
        //if(profileName.equalsIgnoreCase(UtilityCls.SYSTEM_ADMINISTRATOR) || UtilityCls.checkUserPermissionSet(UserInfo.getUserId(), UtilityCls.PERM_SA_ADMIN)){ - Commented by MB - 08/07/2017
        if(profileName.equalsIgnoreCase(UtilityCls.SYSTEM_ADMINISTRATOR) || profileName.equalsIgnoreCase(UtilityCls.ADMINISTRATOR) ||   profileName.equalsIgnoreCase(UtilityCls.BU_IS_USERS) || profileName.equalsIgnoreCase(UtilityCls.COMM_SALES_OPS) ||   UtilityCls.checkUserPermissionSet(UserInfo.getUserId(), UtilityCls.PERM_SA_ADMIN)){ // Added by MB - 08/07/2017
            //if(profileName.equalsIgnoreCase(UtilityCls.SYSTEM_ADMINISTRATOR) || profileName.equalsIgnoreCase(UtilityCls.COMM_STRATEGIC_MANAGER)){
            // allow Edit button visible only for admin or strategic mananger
            return true;
        }else {
            return false;
        }
    }
    
    @AuraEnabled
    public static Boolean  IsStrategicAccount(Id id) {
        Account account = [ SELECT Id, Strategic_Account__c From Account WHERE Id = :id ];
        Boolean isStrategicAccount = account.Strategic_Account__c == null ? false : account.Strategic_Account__c;
        return isStrategicAccount;
    }
    
    /*
return all Strategics Accoun Feild Set's level security is true for the current user
*/
    @AuraEnabled
    public static Sobject checkAccountFLS(Id recordId) { 
        // public static Map<String, Boolean>  checkFieldLevelSecurityforAccount() { 
        //Map<String, Boolean> checkFLSMap = new Map<String, Boolean>();
        Map<Integer, String> checkFLSMap = new Map<Integer, String>();
        
        // Obtain the field name map for the Account object
        Map<String,Schema.SObjectField> accountMap = Schema.SObjectType.Account.fields.getMap();
        
        Boolean isAccessible =  false;
        String fields='';
        
        for (String fieldToCheck : fieldToCheckList) {
            isAccessible = false;
            if (accountMap.get(fieldToCheck).getDescribe().isAccessible()) {
                
                isAccessible = true;
                fields += fieldToCheck+', ';
            }
            // checkFLSMap.put(fieldToCheck, isAccessible);
        }
        
        System.debug('checkFLSMap : ' + checkFLSMap);
        /*Added By Susmitha on NOV 11*/
        if(fields.endsWith(', ')){ 
            system.debug('entry test');
            fields=fields.removeEnd(', ');  
        }
        System.debug('fields : ' + fields);
        /*Added by Susmitha on Nov 11*/
        
        
        String soql = 'Select ' + fields + ' from Account where Id = :recordId';
        Sobject rec = Database.query(soql);   
        return rec;
    }
    
    @AuraEnabled
    public static List<AccountFieldWrapper> getStrategicAccountFieldInfo() {
        List<AccountFieldWrapper> accfiledWrapperList = new List<AccountFieldWrapper>();
        // Obtain the field name map for the Account object
        Map<String,Schema.SObjectField> accountMap = Schema.SObjectType.Account.fields.getMap();
        for (String fieldToCheck : fieldToCheckList) {
            if (accountMap.get(fieldToCheck).getDescribe().isAccessible()) {
                Schema.DescribeFieldResult dfr = accountMap.get(fieldToCheck).getDescribe();
                String fieldLabel = dfr.getLabel() == null ? '' :  dfr.getLabel();
                String apiName = dfr.getName() == null ? '' : dfr.getName();
                String type = dfr.getType() == null ? '' : String.valueOf(dfr.getType());
                
                // 03/15/2017
                List<String> fieldOptions = new List<String>();
                if(type =='PICKLIST' || type =='MULTIPICKLIST')
                    fieldOptions = UtilityCls.getPicklistValues('Account', apiName);
                
                AccountFieldWrapper accfiledWrapper = new AccountFieldWrapper(fieldLabel, apiName, type, fieldOptions);
                accfiledWrapperList.add(accfiledWrapper);
            } 
        }
        
        System.debug('getStrategicAccountFieldInfo : List<AccountFieldWrapper> : ' + accfiledWrapperList);
        
        return accfiledWrapperList;
    }
    
    public class AccountFieldWrapper{
        @AuraEnabled
        public String fieldLabel {get;set;} // field label name
        @AuraEnabled
        public String apiName {get;set;} // field api name
        @AuraEnabled
        public String type {get;set;} // field data type, such as String
        @AuraEnabled
        public List<String> options {get;set;} // field data type, such as String
        
        public AccountFieldWrapper(String fieldLabel, String apiName, String type, List<String> options){
            this.fieldLabel = fieldLabel;
            this.apiName = apiName;
            this.type = type;
            this.options = options;
        }
    }
    
    
    @AuraEnabled
    public static Sobject getRecords(Id recordId, String fieldsToShow) {
        String objectName = recordId.getSobjectType().getDescribe().getName();
        String soql = 'Select ' + fieldsToShow + ' from ' + objectName + ' where Id = :recordId';
        Sobject rec = Database.query(soql);
        return rec;
    }
    
    @AuraEnabled
    // public static String isAccountLocked(Id id) {
    //   return Approval.isLocked(id) == true ? 'yes' : 'no';
    // }
    public static Boolean isAccountLocked(Id id) {
        return Approval.isLocked(id);
    }
    
    @AuraEnabled
    public static Account  getAccount(Id id) { 
        String query = 'SELECT Id, name, Strategic_Account__c, Real_Estate_Service_Provider__c, Real_Estate_Service_Provider__r.Name';
        String fileds = '';
        String queryEnd = ' FROM Account WHERE Id = \'' + id + '\'';
        for(String filedApiName : fieldToCheckList) {
            fileds += ', ' + filedApiName;
        }
        String fullQuery = query + fileds + queryEnd;
        System.debug('getAccount : fullQuery : ' + fullQuery);
        Account account = (Account)Database.query(fullQuery);
        
        
        return account;
        
    }
    
    @AuraEnabled
    public static Account  saveAccount(Account existingAccount) {
        System.debug('saveAccount : existingAccount : ' + existingAccount);
        Id accId = existingAccount.Id;
        if(accId != null) {
            //Start of Code - MB - 07/13/17
            if(existingAccount.MVP_Coordinator__c != null && existingAccount.MVP_Coordinator__c != ''){
                MVP_Coordinator__c MVPInfo = getMVPInfo(existingAccount.MVP_Coordinator__c);
                existingAccount.MVP_Email__c = MVPInfo.MVP_Email__c;
                existingAccount.MVP_Phone__c = MVPInfo.MVP_Phone__c;
            } //Start of Code - MB - 07/13/17
            Database.update(existingAccount);
        }
        
        //return String.valueOf(existingAccount.Id);
        return existingAccount;
    }
    
    @AuraEnabled
    public static String sessionId() {
        return UtilityCls.sessionId();
    }
    
    // Start of Code - MB - 07/13/17
    @AuraEnabled
    public static MVP_Coordinator__c getMVPInfo(String MVPName){
        MVP_Coordinator__c MVPInfo = [SELECT MVP_Phone__c, MVP_Email__c from MVP_Coordinator__c
                                      WHERE Name =: MVPName];
        System.debug('MVP Info: ' + MVPInfo);
        return MVPInfo;
    }
    // End of Code - Mb - 07/13/17
    // 
    // // Start of Code - MB - 07/25/17
    @AuraEnabled
    public static List<String> getMVPList(){
        List<String> MVPList = new List<String>();
        for(MVP_Coordinator__c MVP : [SELECT Name FROM MVP_Coordinator__c]){
            MVPList.add(MVP.Name);
        }
        System.debug('MVP Info: ' + MVPList);
        return MVPList;
    }
    // End of Code - Mb - 07/25/17
    @AuraEnabled
    public static List<SelectItem> populateMultiSelect( string fieldName,string previousVal ){
        system.debug('::::fieldName::::' + fieldName );
        List<SelectItem> returnList = new List<SelectItem>();
        Schema.sObjectType sobject_type = Account.getSObjectType(); 
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        List<Schema.PicklistEntry> pick_list_values = field_map.get(fieldName).getDescribe().getPickListValues();
        List<string>previousValue = new List<string>();
        if( previousVal != null && previousVal != '' ){
            previousValue = previousVal.split(';');
        }
        for (Schema.PicklistEntry a : pick_list_values) {
            returnList.add( new SelectItem( a.getLabel(),a.getValue(),previousValue.contains( a.getValue() ) ? true:false ) );
        }
        system.debug('::::returnList::::' + returnList );
        return returnList;
    }
}