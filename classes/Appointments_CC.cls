/**************************************************************************

Name : Appointments_CC

===========================================================================
Purpose : Controller class to update the appointment status as completed.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand          19/Dec/2016    Created         CSR:
2.0        Susmitha       2/May/2018     Modified                       

***************************************************************************/
public class Appointments_CC{  
    
    /* Start of Code - MB - 08/07/18 - To Handle proper message */
    public class EventInfo{
        @AuraEnabled public Event event;
        @AuraEnabled public Boolean error = false;
        @AuraEnabled public String message = '';
        @AuraEnabled public List<Action_List__c> listOfSALs;
        public EventInfo(){
            event = new Event();
            listOfSALs = new List<Action_List__c>();
        }
    }
    /* End of Code - MB - 08/07/18 - To Handle proper message */
    
    private static List<String> fieldToCheckList = new List<String>(); // get the Strategics Account FieldSet
    public static String businessType = ''; //Added by MB - 3/1/18
    
    static {
        fieldToCheckList = UtilityCls.getFieldSetAllFieldPaths('Contact', 'Quick_Create_Contact');
        businessType = [SELECT Business_Type__c from User where Id =: UserInfo.getUserId() LIMIT 1].Business_Type__c;
    }
    
    @AuraEnabled
    public static List<ContactFieldWrapper> getContactFieldInfo() {
        
        List<ContactFieldWrapper> contactFieldList = new List<ContactFieldWrapper>();
        Map<String,Schema.SObjectField> fieldMap = Schema.SObjectType.Contact.fields.getMap();
        
        for (String fieldToCheck : fieldToCheckList) {
            if (fieldMap.get(fieldToCheck).getDescribe().isAccessible()) {
                Schema.DescribeFieldResult dfr = fieldMap.get(fieldToCheck).getDescribe();
                System.debug('field' + dfr);
                String fieldLabel = dfr.getLabel() == null ? '' :  dfr.getLabel();
                String apiName = dfr.getName() == null ? '' : dfr.getName();
                //String type = dfr.getType() == null ? '' : String.valueOf(dfr.getType());
                string type = dfr.getType() == null ? '' : ( String.valueOf( dfr.getType() ) == 'REFERENCE' ? 'STRING' : String.valueOf(dfr.getType()) );
                
                
                // 03/15/2017
                List<String> fieldOptions = new List<String>();
                if(type =='PICKLIST' || type =='MULTIPICKLIST')
                    fieldOptions = UtilityCls.getPicklistValues('Contact', apiName);
                
                ContactFieldWrapper fieldWrapper = new ContactFieldWrapper(fieldLabel, apiName, type, fieldOptions);
                contactFieldList.add(fieldWrapper);
            } 
        }
        
        System.debug('getContactFieldInfo : List<ContactFieldWrapper> : ' + contactFieldList);
        return contactFieldList;
    }
    
    public class ContactFieldWrapper{
        @AuraEnabled
        public String fieldLabel {get;set;} // field label name
        @AuraEnabled
        public String apiName {get;set;} // field api name
        @AuraEnabled
        public String type {get;set;} // field data type, such as String
        @AuraEnabled
        public List<String> options {get;set;} // field data type, such as String
        
        public ContactFieldWrapper(String fieldLabel, String apiName, String type, List<String> options){
            this.fieldLabel = fieldLabel;
            this.apiName = apiName;
            this.type = type;
            this.options = options;
        }
    }
    
    @AuraEnabled
    public static String saveContactRecord(Contact contactObj, Id eventId) {
        String accPhone;
        // , String eventId
        // Event eventObj = UtilityCls.getRecord(apiName,eventId, 'Id');
        String event_recordId = validateRecordId(eventId); //Added by MB - 3/28/18 - For MA Checkout
        Event e = new Event();
        if(event_recordId != null){
            e = [SELECT Id, WhatId, Accountid, WhoId, EventWhoIds FROM Event WHERE Id =: event_recordId];
        }
        Id recTypeId;
        // // Begin of Changes - SS - 3/20/19 - 71906 - Changed from Whatid to Accountid
        if(e.Accountid != null){
            Account ac = [SELECT Id, Business_Type__c,phone, RecordtypeId, RecordType.Name From Account Where id =: e.Accountid];
            if(ac.Business_Type__c != null){
                recTypeId = UtilityCls.getRecordTypeInfo('Contact', ac.Business_Type__c);
            }
            if(ac.Phone!=null){
                system.debug('Acc phone >>>>>>'+ac.Phone);
                accPhone=ac.Phone;
            }
        }
        //Create Contact 
        Contact cont = new Contact();
        cont = contactObj;
        cont.recordTypeId = recTypeId;
        cont.Phone=accPhone;
        
        // Database.SaveResult result = Database.insert(contactObj);
        list<Contact> conLis=new list<Contact>();
        system.debug('MobilePhone MobilePhone >>>>>>'+contactObj.MobilePhone);
        conLis.add(contactObj);
        list<Database.SaveResult> result = Database.insert(conLis,false);
        UtilityCls.createDMLexceptionlog(conLis,result,'Appointments_CC','saveContactRecord');
        system.debug('Result >>>>>>'+result[0]);
        
        if(result[0].isSuccess()){
            Id newContactId = result[0].getId();
            // return 'success' + eventObject.WhoId + ' -- ' + eventObject.EventWhoIds;
            //Create invitees 
            EventRelation  eRel = new EventRelation(EventId = e.Id, Relationid = newContactId, IsInvitee = false, IsParent = true); 
            Insert eRel;
            return 'success';
        } else
            return result[0].getErrors()[0].getMessage();//'fail';
        
        
    }
    // 03/17/2017 - vivek end
    
    // common method to retrieve a record from any Object
    @AuraEnabled
    public static List<sObject> getObjectRecord(String apiName, String recordId, String fields){
        String event_recordId = validateRecordId(recordId);
        System.debug(event_recordId);
        if(event_recordId == null || event_recordId == ''){
            event_recordId = recordId;
        }
        return  UtilityCls.getRecord(apiName,event_recordId,fields);
    }
    
    // validate record id. It can be appointment, account or Mohawk Account Team.
    // This logic is built for MapAnything to check the object based on ID and get the Event ID and pass back
    
    @AuraEnabled
    public static String validateRecordId(String recordId){
        String event_recordId = '';
        Event eventRecord = new Event();
        String objName = UtilityCls.getObjectNameById(recordId);
        system.debug(objName);
        if (objName == 'Event'){
            event_recordId = recordId;
        }else if(objName == 'Account' || objName == 'Mohawk_Account_Team__c'){
            system.debug(recordId);
            system.debug(recordId);
            system.debug(UserInfo.getUserId());
            try{
                eventRecord = [SELECT Id FROM Event WHERE WhatId = :recordId 
                               AND OwnerId = :UserInfo.getUserId() 
                               AND StartDateTime = TODAY LIMIT 1];
                event_recordId = eventRecord.Id;
            }
            catch (Exception e){
                System.debug('Error Message'+e.getMessage());
                if(recordId!=null)
                    UtilityCls.createExceptionLog(e,'Appointments_CC','validateRecordId','',recordId,'');
            }
            System.debug(eventRecord.Id);
            if(event_recordId == null || event_recordId == ''){
                event_recordId = recordId;
            }
        }
        system.debug(event_recordId);
        return event_recordId;
    }
    
    @AuraEnabled
    public static String getBusinessType(){
        return businessType;
    }
    
    @AuraEnabled
    public static eventInfo updateAppoinment(Event eventObject, String notes){
        EventInfo eventInfo = new EventInfo();
        //eventInfo.event = eventObject;
        try{
            Database.SaveResult result = Database.update(eventObject);
            if(UtilityCls.isStrNotNull(notes) && result.isSuccess()){
                Savepoint sp = Database.setSavepoint();
                String title  = notes.length() > 20 ? notes.substring(0,20)+'...' : notes; 
                
                ContentNote contNote = new ContentNote( Title = title,
                                                       Content = Blob.valueOf(notes)); 
                
                Database.SaveResult res = Database.insert(contNote);  // INSERT contNote;
                if(res.isSuccess()){
                    //Relating inserted note with Event 
                    List<ContentDocumentLink> conDocLinkList = new  List<ContentDocumentLink>();
                    conDocLinkList.add(new ContentDocumentLink(ContentDocumentId = contNote.Id,
                                                               LinkedEntityId = eventObject.Id,
                                                               ShareType = 'V'));
                    
                    conDocLinkList.add(new ContentDocumentLink(ContentDocumentId = contNote.Id,
                                                               LinkedEntityId = eventObject.WhatId,
                                                               ShareType = 'V'));
                    
                    list<Database.SaveResult> resultList= Database.INSERT(conDocLinkList,false); 
                    for(database.SaveResult resultli: resultList){
                        if(!resultli.isSuccess()){
                            for(Database.Error err : resultli.getErrors()){
                                eventInfo.error = true;
                                eventInfo.message = err.getMessage();
                                Database.rollback(sp);
                            }
                        }
                    }
                }else{
                    for(Database.Error err : res.getErrors()){
                        eventInfo.error = true;
                        eventInfo.message = err.getMessage();
                    }
                }
            }else if(!result.isSuccess()){
                for(Database.Error err : result.getErrors()){
                    eventInfo.error = true;
                    eventInfo.message = err.getMessage();
                }
            }
        }catch(Exception ex){
            eventInfo.error = true;
            eventInfo.message = ex.getMessage();
        }
        return eventInfo;
    }
    
    @AuraEnabled
    public static EventInfo getAppointmentSAL(String recordId){
        EventInfo eventInfo = new EventInfo();
        try{
            Event_Action__c eveAc = [SELECT Id, (SELECT Id, Event_Action__c, Action_List__r.Id, Action_List__r.Name, Action_List__r.Brand_Collection__c , Action_List__r.Customer_Group__c, Action_List__r.Customer_Group_Family__c FROM My_Appointment_List__r) 
                                     FROM Event_Action__c WHERE Event_Id__c =: recordId];
            if(eveAc != null){
                for(My_Appointment_List__c appSAL: eveAc.My_Appointment_List__r){
                    Action_List__c a = appSAL.Action_List__r;
                    eventInfo.listOfSALs.add(a);
                }
            }else{
                eventInfo.error = true;
                eventInfo.message = 'No SAL record found for the Appointment';
            }
        }catch(Exception ex){
            eventInfo.error = true;
            eventInfo.message = 'No SAL record found for the Appointment';
        }
        return eventInfo;
    } 
}