public class AssignOpportunityOwnerToInsight 
{
	@InvocableMethod(description='Returns the user')
    public static void opportunityOwnerToInsight(List<Id> projectIds) 
    {	
        List<rdcc__ReedProject__c> updatedProj = new List<rdcc__ReedProject__c>();
        for (rdcc__ReedProject__c proj: [select id,(select id,User__c FROM rdcc__Search_Project_Bridges__r),OwnerId from rdcc__ReedProject__c where id in :projectIds])
        {	
            List<rdcc__Search_Project_Bridge__c> crmExportDetails = proj.rdcc__Search_Project_Bridges__r;
            
            if(crmExportDetails!=Null)
            {	
                
                List<string> crmExDId = new List<string>();
                for (rdcc__Search_Project_Bridge__c crmExD : crmExportDetails)
                {	
                    String s = crmExD.User__c.substringBetween('href="','"');
                 if(String.isNotBlank(s))
                 	{crmExDId.add(s);}
                 }
                
                if(crmExDId.contains(String.valueOf((UserInfo.getUserId()).subString(0,15))))
                {	
                    proj.OwnerId = UserInfo.getUserId();
                    updatedProj.add(proj);
                    
                }
            }
            
        }
        update updatedProj;
    }
    
    

}