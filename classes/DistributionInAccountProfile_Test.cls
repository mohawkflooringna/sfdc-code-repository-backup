@isTest
public class DistributionInAccountProfile_Test {

    static List<Account_Profile__c> accProfile;
    
   
    
    static testmethod void testmethod1(){
        
        Test.startTest();
        accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false);

        distributionInAccountProfileController.getAllDistributions(accProfile.get(0).id);
        distributionInAccountProfileController.updateDistributions(accProfile.get(0));
        distributionInAccountProfileController.fieldLabelMap();
       List<Account_Profile_Settings__c>  accps= Utility_Test.createAccountProfileSettings(true,3,'Retail');
        distributionInAccountProfileController.APSMap(accps.get(0).id);
        
        distributionInAccountProfileController.createAnnualSalesPickList();
        accProfile[0].Annual_Retail_Sales__c=1000;
       // update accProfile; 
        system.assert(accProfile[0].Annual_Retail_Sales__c==1000);
        Account_Profile__History aph=new Account_Profile__History(Field='Annual_Retail_Sales__c',parentid=accProfile[0].id);
        insert aph;
        Account_Profile__History aps= [select Id,createdDate,CreatedBy.Name,OldValue,NewValue,Field from Account_Profile__History order by createddate desc limit 1 ];
          
        distributionInAccountProfileController.getFieldHistory(aps.id);
        distributionInAccountProfileController.dataWrapper ds = new  distributionInAccountProfileController.dataWrapper(aps);
          
        
        distributionInAccountProfileController.isChainNumberExist(accProfile.get(0));
        
        accProfile.get(0).retail__c = 10;
        accProfile.get(0).builder__c = 10;
        accProfile.get(0).Multi_Family__c = 10;
        update accProfile.get(0);
        distributionInAccountProfileController.rollupMat(accProfile.get(0).id);
        Test.stopTest();   
    } 
}