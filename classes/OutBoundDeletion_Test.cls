/**************************************************************************


Name : OutBoundDeletion_Test 
===========================================================================
Purpose :  Test class to OutBoundDeletion test data 
===========================================================================
History:
--------
VERSION    AUTHOR              DATE           DETAIL          DESCRIPTION
1.0        Nagendra Singh     21/Dec/2018       

***************************************************************************/
@isTest
public class OutBoundDeletion_Test {
    static Id resiInvRTId    = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
    static List<Account> accList = new List<Account>();
    static Sample_Display_Inventory__c sampleDisplay = new Sample_Display_Inventory__c();
    static Mohawk_Account_Team__c mhkTeam = new Mohawk_Account_Team__c();
    static User usr = new User();
    
    @isTest
    static void init(){
        List<User> userList = Utility_Test.createTestUsers(true, 1, 'Residential Sales User');
        usr = userList[0];
        
        accList = Utility_Test.createAccounts( false,1 );
        accList[0].sfdc_display__c = true;
        insert accList;
        
        sampleDisplay.Name = 'TEST';
        sampleDisplay.Account__c = accList[0].id; 
        sampleDisplay.Dealer__c = accList[0].id; 
        sampleDisplay.Customer_Group__c = 'TEST';
        sampleDisplay.RecordTypeID = Schema.SObjectType.Sample_Display_Inventory__c.getRecordTypeInfosByName().get( UtilityCls.SAMPLEREDIDENTALDISPLAY ).getRecordTypeId() ;
        sampleDisplay.Architech_Folder__c = 'ANALOGUE';        
        insert sampleDisplay;
        
        mhkTeam.Account__c = accList[0].Id;
        mhkTeam.User__c = usr.Id;
        mhkTeam.RecordTypeId = resiInvRTId;
        mhkTeam.Role__c = 'Territory Manager';
        insert mhkTeam;
        
        
    }
    static testMethod void testSampleDisplayInventory() {
        List<Account>accList = Utility_Test.createAccounts( true,1 );
        Sample_Display_Inventory__c SDI = new Sample_Display_Inventory__c();
        SDI.Name = 'TEST';
        SDI.Account__c = accList[0].id; 
        SDI.Dealer__c = accList[0].id; 
        SDI.Customer_Group__c = 'TEST';
        SDI.RecordTypeID = Schema.SObjectType.Sample_Display_Inventory__c.getRecordTypeInfosByName().get( UtilityCls.SAMPLEREDIDENTALDISPLAY ).getRecordTypeId() ;
        SDI.Architech_Folder__c = 'ANALOGUE';        
        insert SDI;
        test.startTest();
        Delete SDI;
        test.stopTest();
    }
    static testMethod void testDeleteAccount() {
        test.startTest();
        init();
        test.stopTest();
        Delete accList;
    }
    static testmethod void testDeleteMAT(){
        List<Account> resAccListForInvoicing;
        List<Territory__c> terrList;
        
        List<Mohawk_Account_Team__c> mohAccTeam;
        Id resiNonInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Non-Invoicing');
        
        System.runAs(Utility_Test.RESIDENTIAL_USER) {
            test.startTest(); 
            
            List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false); 
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1); 
            
            terrList = Utility_Test.createTerritories(true, 2);
            system.assert(terrList.size()>0);
            mohAccTeam = Utility_Test.createMohawkAccountTeam(true, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiNonInvRTId, accProfile, 'Horizon');
            delete mohAccTeam; 
            
            test.stopTest();
        }
    }
    
    @isTest
    static void testforNegativeScenario() {
        List<Account>accList = Utility_Test.createAccounts( true,2 );
        test.startTest();
        List<Account> accDelList = new List<Account>();
        Id accId = accList[0].Id;
        Account acc = [SELECT Id, Name from Account WHERE Id =: accId FOR UPDATE];
        Delete accList;
        test.stopTest();
    }
    @isTest
    static void testdeletenonDisplayMATs() {
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
        
        Account_Profile__c apRec;
        apRec = new Account_Profile__c();
        apRec.Name = 'test record';
        apRec.Multi_Channel__c = true;
        insert apRec;
        
       set<id> nonDisplayAccs = new set<ID>();
       Mohawk_Account_Team__c matRec; 
        
        
        List<Territory__c>terrList = Utility_Test.createTerritories(false, 3);
        insert terrList;
        List<Account> accList = new List<Account>();
        accList = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
        accList[0].Account_Profile__c = apRec.id;
        accList[0].Territory__c = terrList[0].id;
        insert  accList; 
        
       
         List<Mohawk_Account_Team__c> mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, accList, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
        for(Mohawk_Account_Team__c mat:mohAccTeam){
            mat.Account__c=accList[0].id;
            mat.Main_Profile__c = true;
        }
        insert mohAccTeam;
        test.startTest();
            accList[0].sfdc_display__c =false;
            accList[0].Territory__c =terrList[1].id;
            update accList;
            
        for(Account acc:accList){
            if(!acc.sfdc_display__c)
                nonDisplayAccs.add(acc.id);
                
        }
        OutBoundDeletion.deletenonDisplayMATs(nonDisplayAccs);
        test.stopTest();
    }
    static testMethod void test_deleteNonDisplaySampleInventories() {
         set<id> nonDisplayAccs = new set<ID>();
        List<Account>accList = Utility_Test.createAccounts( false,1 );
        accList[0].sfdc_display__c = true;
        insert accList;
        
        Sample_Display_Inventory__c SDI = new Sample_Display_Inventory__c();
        SDI.Name = 'TEST';
        SDI.Account__c = accList[0].id; 
        SDI.Dealer__c = accList[0].id; 
        SDI.Customer_Group__c = 'TEST';
        SDI.RecordTypeID = Schema.SObjectType.Sample_Display_Inventory__c.getRecordTypeInfosByName().get( UtilityCls.SAMPLEREDIDENTALDISPLAY ).getRecordTypeId() ;
        SDI.Architech_Folder__c = 'ANALOGUE';        
        insert SDI;
        test.startTest();
         accList[0].sfdc_display__c = false;
         update accList;
         
         for(Account acc:accList){
            if(!acc.sfdc_display__c)
                nonDisplayAccs.add(acc.id);
                
        }
        //OutBoundDeletion.deleteNonDisplaySampleInventories(nonDisplayAccs);
        System.assert([Select id,sfdc_display__c from Account][0].sfdc_display__c == false);
        test.stopTest();
    }
}