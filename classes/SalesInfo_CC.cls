public without sharing class SalesInfo_CC {
    
    @AuraEnabled
    // {{ Calculate Salesinfo based on the User's Territory -  SS -  12/20/17 -- Story - 50868 - Sales Info Changes on Accounts
    public static list<salesInfo> getSalesInfo(Id recordId){
        Decimal TotalForecastedRevenue;
        Decimal TotalAccountPlan;
        Decimal TotalPipeline;
        Decimal TotalYTD;
        Decimal TotalOpenOrders;
        
        list<salesInfo> SalesInfoList = new list<salesInfo>();
        list<Territory_User__c> terUserList = new list<Territory_User__c>();
        String profName = UtilityCls.getProfileName(UserInfo.getProfileId());
        
        Account acc = [SELECT Strategic_Account__c, Education_Govt_Territory__c, Ed_Acc_Plan_Goal__c, Ed_FR__c, Ed_Open_Orders__c, Ed_Pipeline__c, Ed_YTD_Sales__c,
                       Healthcare_Sr_Living_Territory__c, Hc_Acc_Plan_Goal__c, Hc_FR__c, Hc_Open_Orders__c, Hc_Pipeline__c, Hc_YTD_Sales__c, Workplace_Retail_Territory__c, Wp_Acc_Plan_Goal__c, Wc_FR__c, Wp_Open_Orders__c, Wp_Pipeline__c, Wp_YTD_Sales__c
                       FROM Account Where Id =: recordId LIMIT 1];
        
        User usr = [Select Id, Corporate_User__c, UserRole.DeveloperName from User where Id =: UserInfo.getUserId()];
        
        if (acc.Workplace_Retail_Territory__c != null || acc.Education_Govt_Territory__c != null || acc.Healthcare_Sr_Living_Territory__c != null ) {  
            terUserList = [SELECT Territory__c FROM Territory_User__c WHERE Territory__c IN ( :acc.Workplace_Retail_Territory__c , :acc.Education_Govt_Territory__c, :acc.Healthcare_Sr_Living_Territory__c) and User__c =:UserInfo.getUserId()];
        }
        if (terUserList.size() > 0  || (profName == UtilityCls.SYSTEM_ADMINISTRATOR || profName == UtilityCls.ADMINISTRATOR ||
                                        profName == UtilityCls.BU_IS_USERS || profName == UtilityCls.COMM_SALES_OPS || (profName == UtilityCls.COMM_SALES_MGR && acc.Strategic_Account__c) ||
                                        usr.Corporate_User__c || usr.UserRole.DeveloperName == UtilityCls.SAM)){
                                            TotalAccountPlan = TotalPipeline = TotalOpenOrders = TotalYTD  = 0;
                                            // for (Territory_User__c tr: terUserList){
                                            // if (acc.workplace_retail_territory__c != null && tr.Territory__c == acc.workplace_retail_territory__c){
                                            SalesinfoList.add(new salesInfo( 'Workplace/Retail',acc.Wp_Acc_Plan_Goal__c,acc.Wp_YTD_Sales__c,acc.Wp_Open_Orders__c,acc.Wp_Pipeline__c,acc.Wc_FR__c));
                                            TotalAccountPlan = ( acc.Wp_Acc_Plan_Goal__c != null ) ? acc.Wp_Acc_Plan_Goal__c : 0;
                                            TotalPipeline = ( acc.Wp_Pipeline__c != null ) ?  acc.Wp_Pipeline__c : 0;
                                            TotalOpenOrders = ( acc.Wp_Open_Orders__c != null ) ? acc.Wp_Open_Orders__c : 0;              
                                            TotalYTD = ( acc.Wp_YTD_Sales__c != null ) ? acc.Wp_YTD_Sales__c : 0; 
                                            TotalForecastedRevenue = ( acc.Wc_FR__c != null ) ? acc.Wc_FR__c  : 0;
                                            //  }
                                            
                                            // if (acc.Education_Govt_Territory__c != null && tr.Territory__c == acc.Education_Govt_Territory__c){
                                            SalesinfoList.add(new salesInfo('Ed/Gov',acc.Ed_Acc_Plan_Goal__c,acc.Ed_YTD_Sales__c,acc.Ed_Open_Orders__c, acc.Ed_Pipeline__c,acc.Ed_FR__c));
                                            TotalAccountPlan += ( acc.Ed_Acc_Plan_Goal__c != null ) ? acc.Ed_Acc_Plan_Goal__c : 0;
                                            TotalPipeline += ( acc.ED_Pipeline__c != null ) ?  acc.Ed_Pipeline__c : 0;
                                            TotalOpenOrders += ( acc.Ed_Open_Orders__c != null ) ? acc.Ed_Open_Orders__c : 0;              
                                            TotalYTD += ( acc.Ed_YTD_Sales__c != null ) ? acc.Ed_YTD_Sales__c : 0; 
                                            TotalForecastedRevenue += ( acc.Ed_FR__c != null ) ? acc.Ed_FR__c  : 0;
                                            // TotalAccountPlan += acc.Ed_Acc_Plan_Goal__c; TotalPipeline += acc.Ed_Pipeline__c; TotalOpenOrders += acc.Ed_Open_Orders__c; TotalYTD = +acc.Ed_YTD_Sales__c; TotalForecastedRevenue += acc.Ed_FR__c;
                                            // } 
                                            
                                            // if (acc.Healthcare_Sr_Living_Territory__c != null && tr.Territory__c == acc.Healthcare_Sr_Living_Territory__c){  
                                            SalesinfoList.add(new salesInfo('Healthcare/SL', acc.Hc_Acc_Plan_Goal__c, acc.Hc_YTD_Sales__c, acc.Hc_Open_Orders__c,acc.Hc_Pipeline__c,acc.Hc_FR__c));
                                            TotalAccountPlan += ( acc.HC_Acc_Plan_Goal__c != null ) ? acc.HC_Acc_Plan_Goal__c : 0;
                                            TotalPipeline += ( acc.HC_Pipeline__c != null ) ?  acc.HC_Pipeline__c : 0;
                                            TotalOpenOrders += ( acc.HC_Open_Orders__c != null ) ? acc.HC_Open_Orders__c : 0;              
                                            TotalYTD += ( acc.HC_YTD_Sales__c != null ) ? acc.HC_YTD_Sales__c : 0; 
                                            TotalForecastedRevenue += ( acc.HC_FR__c != null ) ? acc.HC_FR__c  : 0;
                                            //  TotalAccountPlan += acc.Hc_Acc_Plan_Goal__c;  TotalPipeline += acc.Hc_Pipeline__c; TotalOpenOrders += acc.Hc_Open_Orders__c; TotalYTD = +acc.Hc_YTD_Sales__c; TotalForecastedRevenue += acc.Hc_FR__c;
                                            // }
                                            
                                            //  if (TotalAccountPlan != null || TotalYTD != null || TotalOpenOrders != null || TotalPipeline != null || TotalForecastedRevenue != null )   
                                            SalesinfoList.add(new salesInfo('Total', TotalAccountPlan, TotalYTD, TotalOpenOrders,TotalPipeline, TotalForecastedRevenue)); 
                                            // }
                                            
                                            
                                            // }
                                            
                                            
                                        }
        return salesinfoList;          
    }
    // }} Calculate Salesinfo based on the User's Territory -  SS -  12/20/17 -- Story - 50868 - Sales Info Changes on Accounts
    
    /*Method for User */
    @AuraEnabled
    public static WrapSalesInfo getUserSalesInfo(Id recordId){
        User usr = new User();
        WrapSalesInfo usersalesInfo = new WrapSalesInfo(usr, false);
        Boolean isValid = false;
        System.debug('UserId: ' + recordId);
        usr = [SELECT Industry_Experience__c,Sales_Budget__c,Addition_or_Replacement__c,Separation_Date__c, Start_Date__c, MHKUserRole__c, Profile_Name__c
               FROM User Where Id =: recordId LIMIT 1];
        
        User loginUser = [SELECT Industry_Experience__c,Sales_Budget__c,Addition_or_Replacement__c,Separation_Date__c, Start_Date__c, MHKUserRole__c, Profile_Name__c
                          FROM User Where Id =: UserInfo.getUserId() LIMIT 1];
        
        String profName = UtilityCls.getProfileName(UserInfo.getProfileId());
        system.debug('Profile Name: ' + profName+'recordId'+recordId);
        usersalesInfo.loggedInUserProf = profName;
        if (profName != UtilityCls.SYSTEM_ADMINISTRATOR && profName != UtilityCls.ADMINISTRATOR &&
            profName != UtilityCls.BU_IS_USERS && profName != UtilityCls.COMM_SALES_OPS && recordId != UserInfo.getUserId()){
                if (loginUser.MHKUserRole__c == 'Account Executive'){
                    usersalesInfo.user = usr;   
                    usersalesInfo.isValid = false;
                } else if  (loginUser.MHKUserRole__c == 'Regional Vice President' && usr.MHKUserRole__c != 'Account Executive' ) {
                    usersalesInfo.user = usr;   
                    usersalesInfo.isValid = false;
                } else if (loginUser.MHKUserRole__c  == 'Senior Vice President' && usr.MHKUserRole__c == 'Senior Vice President'){
                    usersalesInfo.user = usr;   
                    usersalesInfo.isValid = false;
                } else if ((loginUser.MHKUserRole__c  != 'Account Executive' && usr.MHKUserRole__c == 'Account Executive') || 
                           (loginUser.MHKUserRole__c  == 'Senior Vice President' && usr.MHKUserRole__c == 'Regional Vice President')){
                                if(loginUser != null){
                                    Territory_User__c usrTerr = [SELECT Territory__c FROM Territory_User__c WHERE User__c =: usr.Id  LIMIT 1];
                                    for(Territory_User__c ut: [SELECT User__c FROM Territory_User__c WHERE Territory__c =: usrTerr.Territory__C]){
                                        if(ut.User__c == UserInfo.getUserId()){
                                            usersalesInfo.user = usr;
                                            usersalesInfo.isValid = true;
                                            break;
                                        }else {
                                            usersalesInfo.user = usr;
                                            usersalesInfo.isValid = false;    
                                        }
                                    }
                                } 
                            } 
            }else{                                              // User is accessing his Own profile or is a Admin or sales ops user.
                usersalesInfo.user = usr;
                usersalesInfo.isValid = true;
              
            }
        System.debug('Sales Info: ' + usersalesInfo);
        usersalesInfo.loggedInUserProf = UtilityCls.getProfileName(UserInfo.getProfileId());
        if (usersalesInfo.isValid == false){
            userSalesInfo.User.clear();
        }
        
        return usersalesInfo;
    }
    
    @AuraEnabled
    public static String setUserSalesInfo(User salesInfo){
        User usr = new User();
        if(salesInfo != null){
            usr = salesInfo;
            System.debug('Userinfo: ' + usr);
            try{
                Database.SaveResult result = Database.update(usr);
                if (result.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    return 'Success';
                }
                else {
                    // Operation failed, so get all errors   
                    String errMsg = 'Error Occurred while saving. ';             
                    for(Database.Error err : result.getErrors()){
                        errMsg = errMsg + err.getMessage();
                    }
                    return errMsg;
                }
            }
            Catch(Exception dmle){
        UtilityCls.createExceptionLog(dmle,  'SalesInfo_CC', 'setUserSalesInfo', 'logCode', '', 'referenceInfo');
                return 'Error Occurred while saving. Please contact Administrator'; 
            }
        }
        return 'Something went wrong. Please try again later. If issue continues, please contact Administrator';
    }
    
    @AuraEnabled
    public static WrapPickListValues getPicklistValues(){
        WrapPickListValues picklists = new WrapPickListValues();
        picklists.indExpList = UtilityCls.getPicklistValues('User', 'Industry_Experience__c');
        picklists.addReplList = UtilityCls.getPicklistValues('User','Addition_or_Replacement__c');
        return picklists;
    }
    
    public class WrapPickListValues{
        @AuraEnabled
        public List<String> indExpList;
        
        @AuraEnabled
        public List<String> addReplList;
    }
    
    /* mehod for User*/
    public class WrapSalesInfo{
        @AuraEnabled
        public Account acc;
        @AuraEnabled
        public Boolean isValid;
        @AuraEnabled
        public User user;
        @AuraEnabled
        public String loggedInUserProf;
        
        public WrapSalesInfo(Account ac,Boolean isValid){
            this.acc = ac;
            this.isValid = isValid;
        }
        public WrapSalesInfo(User usr,Boolean isValid){
            this.user = usr;
            this.isValid = isValid;
        }
        
    }
    
    Public class SalesInfo{
        @AuraEnabled
        public string Name;
        @AuraEnabled
        public decimal goal;
        @AuraEnabled
        public decimal YTDSales;
        @AuraEnabled
        public decimal OpenOrders;
        @AuraEnabled
        public decimal Pipeline;
        @AuraEnabled
        public decimal Revenue;
        
        public SalesInfo (String Iname, Decimal Igoal, Decimal IYtdsales, Decimal IOpenOrders,Decimal IPipeline, Decimal IRevenue){
            this.name = Iname;
            this.goal = ( IGoal != null ) ? IGoal : 0; 
            this.YTDsales = ( IYTDsales != null ) ? IYTDsales : 0; 
            this.OpenOrders = ( IOpenOrders != null ) ? IOpenOrders : 0; 
            this.Pipeline = ( IPipeline != null ) ? IPipeline : 0; 
            this.revenue = ( IRevenue != null ) ? IRevenue : 0;                  
        }
        
        
        
    }
    
}