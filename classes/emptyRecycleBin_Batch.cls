/**************************************************************************

Name : emptyRecycleBin_Batch

===========================================================================
Purpose : Batch class to delete Records FROM Recycle Bin .
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mudit          10/OCT/2018    Created          

***************************************************************************/
global class emptyRecycleBin_Batch implements Database.Batchable<sobject>, Database.Stateful,Schedulable  {
    global final String Query;
    global Database.QueryLocator start( Database.BatchableContext bc ) {
         if (Query != null)
            return Database.getQueryLocator(Query);
        else
            return null;
    }
    
    global emptyRecycleBin_Batch (String q){
      if(q != null){
          Query = q;
      } else {
        Batch_Query__mdt [] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'emptyRecycleBin_Batch'];
        If( QureyList != null && QureyList.size() > 0 )
               Query = QureyList[0].Query_String__c ;
      
      } 
    }
    
    global void execute( Database.BatchableContext BC, List<sObject> scope ) {
        if( scope != null && scope.size() > 0 )
            DataBase.emptyRecycleBin( scope );
    }
    
    global void finish( Database.BatchableContext BC ) {
        
    }
    
    global void execute(SchedulableContext sc){
        Database.executeBatch(new emptyRecycleBin_Batch(Query));
    }
}