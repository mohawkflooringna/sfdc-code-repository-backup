/**************************************************************************

Name    :   createExpandableSectionContentController 

===========================================================================
Purpose :   This class is used in lightning component which is used in 
            Account Profile Detail Page.
===========================================================================
History:
--------
VERSION     AUTHOR      DATE            DETAIL      DESCRIPTION
1.0         Mudit       05/June/2018                Created
***************************************************************************/
public with sharing class createExpandableSectionContentController{
    
    @AuraEnabled
    public static void rollupOnLoad( string recordId ){
        Account_Profile__c AP = [ SELECT lastModifiedDate FROM Account_Profile__c WHERE id =: recordId ];
        for( Mohawk_Account_Team__c MAT : [SELECT lastModifiedDate FROM Mohawk_Account_Team__c WHERE Account_Profile__c =: recordId] ){
            if( AP.lastModifiedDate < MAT.lastModifiedDate ){
                //CALL ROLLUP
                distributionInAccountProfileController.rollupMat( recordId );
                break;
            }
        }
        //List<Mohawk_Account_Team__c>MATList = new List<Mohawk_Account_Team__c>([ SELECT lastModifiedDate FROM Mohawk_Account_Team__c WHERE Account_Profile__c =: recordId ]);
    }
    
    @AuraEnabled
    public static List<blockSectionContentWrapper> createBlockSectionsContent( List<AP_Section_Fields__mdt>fields,string recordId ){
        List<blockSectionContentWrapper>contentList = new List<blockSectionContentWrapper>();
        if( fields != null && fields.size() > 0 ){
            List<string>fieldList = new List<string>();
            for( AP_Section_Fields__mdt APSF : fields ){
                /*if( APSF.Field_Name__c != null && APSF.Field_Name__c != '' )
                    fieldList.add( APSF.Field_Name__c );*/
                if( APSF.Field_Name__c != null && APSF.Field_Name__c != '' ){
                    fieldList.add( APSF.Field_Name__c );
                    if( APSF.Field_Name__c.Contains('__r') ){
                        if( !fieldList.Contains( APSF.Field_Name__c.substringBefore('.').replace('__r','__c') ) ){
                        	fieldList.add( APSF.Field_Name__c.substringBefore('.').replace('__r','__c') );    
                        }
                    }
                }
            }            
            system.debug('::::fieldList::::' + fieldList );
            
            Map<string,string>fetchData = sectionQuery( customDetailUtility.getObjectName( recordId ),fieldList,recordId );
            for( AP_Section_Fields__mdt APSF : fields ){
                system.debug( ':::FIELD CHECK:::' + APSF.Field_Name__c + ' ::::: ' + fetchData.get( APSF.Field_Name__c ) );
                contentList.add(                    
                    new blockSectionContentWrapper( APSF.Sequence__c,APSF.Label_L__c,APSF.Label_M__c,APSF.Label_S__c,fetchData.get( APSF.Field_Name__c ),APSF.Type__c )
                    //new blockSectionContentWrapper( APSF.Sequence__c,APSF.Label_L__c,APSF.Label_M__c,APSF.Label_S__c,(APSF.Field_Name__c.contains('__r') ? APSF.Field_Name__c.substringBefore('.').replace('__r','__c') : fetchData.get( APSF.Field_Name__c ) ),APSF.Type__c )
                );
            }
            
        }
        return contentList;
    }
    
    @AuraEnabled
    public static tableWrapper createTableSectionsContent( string fieldsData,string recordId,string headerName ){
        system.debug(':::::fieldsData:::::' + fieldsData);
        List<Header> hdrs = new List<Header>();
        List<Row> rws = new List<Row>();
        tableWrapper gridData = new tableWrapper();
        string objectName = customDetailUtility.getObjectName( recordId );
        
        List<string>totalFieldList = new List<string>();
        List<createExpandableSectionController.tableSectionContentWrapper> fields = (List<createExpandableSectionController.tableSectionContentWrapper>)JSON.deserialize(fieldsData,List<createExpandableSectionController.tableSectionContentWrapper>.class);
        
        if( fields != null && fields.size() > 0 ){
            if( headerName == 'Total Overview' || headerName == 'Overview' ){
                hdrs.add( new Header( 0,'Product Category','Product Category','Product Category' ) );    
            }else{
                hdrs.add( new Header( 0,'Brand Family','Brand Family','Brand Family' ) );
            }
            List<string>fieldList = new List<string>();
            List<string>duplicateHeaderCheck = new List<string>();
            
            for( createExpandableSectionController.tableSectionContentWrapper header : fields ){
                for( AP_Section_Fields__mdt fieldValue : header.listOfFields ){
                    if( !duplicateHeaderCheck.Contains( fieldValue.MasterLabel ) ){
                        hdrs.add( new Header( fieldValue.Sequence__c,fieldValue.Label_L__c,fieldValue.Label_M__c,fieldValue.Label_S__c ) );
                        duplicateHeaderCheck.add( fieldValue.MasterLabel );
                    }
                    
                    if( fieldValue.Field_Name__c != null && fieldValue.Field_Name__c != '' )
                        fieldList.add( fieldValue.Field_Name__c );
                }
                for( AP_Section_Fields__mdt totalFields : header.listOfTotal ){
                    if( !totalFieldList.Contains( totalFields.Field_Name__c ) )
                        totalFieldList.add( totalFields.Field_Name__c );
                }
            }
            
            Map<string,string>fetchData = sectionQuery( customDetailUtility.getObjectName( recordId ),fieldList,recordId );            
            //if( customDetailUtility.getObjectName( recordId ) == 'Mohawk_Account_Team__c' ){
                
                for( createExpandableSectionController.tableSectionContentWrapper header : fields ){
                    Row rw = New Row();                
                    rw.cols = new List<Col>();
                    rw.order = header.APSectionMasterSequence;
                    rw.cols.add( new Col( 0,header.APSectionMasterLabel,hdrs[0].title_L,hdrs[0].title_M,hdrs[0].title_S,'Left','Text' ) );
                    Map<string,string>GRYCal = getDefaultValues( header.listOfFields,fetchData );
                    system.debug( '::::GRYCal::::' + GRYCal );
                    for( AP_Section_Fields__mdt fieldValue : header.listOfFields ){
                      if ( objectName == 'Mohawk_Account_Team__c'){
                        if( fieldValue.MasterLabel == 'Green Opportunity' ){                        
                            string GOPP = calculateGreenOpportunity( decimal.valueOf( GRYCal.get('Mohawk Purchases') ),decimal.valueOf( GRYCal.get('Green Opportunity') ) );
                            rw.cols.add( new Col( fieldValue.Sequence__c,GOPP,fieldValue.Label_L__c,fieldValue.Label_M__c,fieldValue.Label_S__c,fieldValue.Alignment__c,fieldValue.Type__c ) );  
                        }else if( fieldValue.MasterLabel == 'Yellow Opportunity' ){
                            string YOPP = calculateYellowOpportunity( decimal.valueOf( GRYCal.get('Mohawk Purchases') ),decimal.valueOf( GRYCal.get('Green Opportunity') ),decimal.valueOf( GRYCal.get('Yellow Opportunity') ) );
                            rw.cols.add( new Col( fieldValue.Sequence__c,YOPP,fieldValue.Label_L__c,fieldValue.Label_M__c,fieldValue.Label_S__c,fieldValue.Alignment__c,fieldValue.Type__c ) );  
                        }else if( fieldValue.MasterLabel == 'Red Opportunity' ){
                            string ROPP = calculateRedOpportunity( decimal.valueOf( GRYCal.get('Mohawk Purchases') ),decimal.valueOf( GRYCal.get('Wholesale Purchases') ),decimal.valueOf( GRYCal.get('Green Opportunity') ),decimal.valueOf( GRYCal.get('Yellow Opportunity') ),decimal.valueOf( GRYCal.get('Red Opportunity') ));
                            rw.cols.add( new Col( fieldValue.Sequence__c,ROPP,fieldValue.Label_L__c,fieldValue.Label_M__c,fieldValue.Label_S__c,fieldValue.Alignment__c,fieldValue.Type__c ) );  
                        }else{
                            rw.cols.add( new Col( fieldValue.Sequence__c,fetchData.get( fieldValue.Field_Name__c ),fieldValue.Label_L__c,fieldValue.Label_M__c,fieldValue.Label_S__c,fieldValue.Alignment__c,fieldValue.Type__c ) );  
                        }
                      } else {
                            rw.cols.add( new Col( fieldValue.Sequence__c,fetchData.get( fieldValue.Field_Name__c ),fieldValue.Label_L__c,fieldValue.Label_M__c,fieldValue.Label_S__c,fieldValue.Alignment__c,fieldValue.Type__c ) );  
                      }
                    }
                    rw.cols.sort();
                    rws.add(rw);
                }
            //}
            //
            gridData.rows = rws;
            gridData.headers = hdrs;
            gridData.headers.sort();
            gridData.rows.sort();
            
            //MAKING TOTAL ROW
            Row rw = New Row();
            rw.cols = new List<Col>();
            //rw.order = fields.size() + 1;            
            rw.order = rws[rws.size()-1].order + 1;
            rw.cols.add( new Col( 0,'Total','','','','Right','Text' ) );            
            Map<string,string>fetchTotalData = sectionQuery( customDetailUtility.getObjectName( recordId ),totalFieldList,recordId );
            List<string>duplicateTotalCheck = new List<string>();
            //if( customDetailUtility.getObjectName( recordId ) == 'Mohawk_Account_Team__c' ){
                for( createExpandableSectionController.tableSectionContentWrapper totalRow : fields ){
                    Map<string,string>GRYCalTotal = getDefaultValues( totalRow.listOfTotal,fetchTotalData );
                    system.debug( '::::GRYCalTotal::::' + GRYCalTotal );
                    for( AP_Section_Fields__mdt totalCol : totalRow.listOfTotal ){
                   
                        if( !duplicateTotalCheck.Contains( totalCol.Field_Name__c ) ){
                            if ( objectName == 'Mohawk_Account_Team__c'){                         
                                    if( totalCol.MasterLabel == 'Green Opportunity' ){                        
                                        string GOPP = calculateGreenOpportunity( decimal.valueOf( GRYCalTotal.get('Mohawk Purchases') ),decimal.valueOf( GRYCalTotal.get('Green Opportunity') ));
                                        rw.cols.add( new Col( totalCol.Sequence__c,GOPP,totalCol.Label_L__c,totalCol.Label_M__c,totalCol.Label_S__c,totalCol.Alignment__c,totalCol.Type__c ) );
                                    }else if( totalCol.MasterLabel == 'Yellow Opportunity' ){
                                        string YOPP = calculateYellowOpportunity( decimal.valueOf( GRYCalTotal.get('Mohawk Purchases') ),decimal.valueOf( GRYCalTotal.get('Green Opportunity') ),decimal.valueOf( GRYCalTotal.get('Yellow Opportunity') ) );
                                        rw.cols.add( new Col( totalCol.Sequence__c,YOPP,totalCol.Label_L__c,totalCol.Label_M__c,totalCol.Label_S__c,totalCol.Alignment__c,totalCol.Type__c ) );
                                    }else if( totalCol.MasterLabel == 'Red Opportunity' ){
                                        string ROPP = calculateRedOpportunity( decimal.valueOf( GRYCalTotal.get('Mohawk Purchases') ),decimal.valueOf( GRYCalTotal.get('Wholesale Purchases') ),decimal.valueOf( GRYCalTotal.get('Green Opportunity') ),decimal.valueOf( GRYCalTotal.get('Yellow Opportunity') ),decimal.valueOf( GRYCalTotal.get('Red Opportunity') ));
                                        rw.cols.add( new Col( totalCol.Sequence__c,ROPP,totalCol.Label_L__c,totalCol.Label_M__c,totalCol.Label_S__c,totalCol.Alignment__c,totalCol.Type__c ) );
                                    }else{
                                        rw.cols.add( new Col( totalCol.Sequence__c,fetchTotalData.get( totalCol.Field_Name__c ),totalCol.Label_L__c,totalCol.Label_M__c,totalCol.Label_S__c,totalCol.Alignment__c,totalCol.Type__c ) );
                                    }
                            } else {
                                 rw.cols.add( new Col( totalCol.Sequence__c,fetchTotalData.get( totalCol.Field_Name__c ),totalCol.Label_L__c,totalCol.Label_M__c,totalCol.Label_S__c,totalCol.Alignment__c,totalCol.Type__c ) );
                            }
                            //rw.cols.add( new Col( totalCol.Sequence__c,fetchTotalData.get( totalCol.Field_Name__c ),totalCol.Label_L__c,totalCol.Label_M__c,totalCol.Label_S__c,totalCol.Alignment__c,totalCol.Type__c ) );
                            duplicateTotalCheck.add( totalCol.Field_Name__c );                           
                        }
                    }    
                }
            //}
            rw.cols.sort();
            rws.add(rw);
            //MAKING TOTAL ROW
            
            
            gridData.rows = rws;
            gridData.headers = hdrs;
            //gridData.headers.sort();
            //gridData.rows.sort();
        }
        return gridData;
    }
    
    public class blockSectionContentWrapper{
        @AuraEnabled
        public decimal fieldSequence;
        
        @AuraEnabled
        public string fieldLabel_L;
        
        @AuraEnabled
        public string fieldLabel_M;
        
        @AuraEnabled
        public string fieldLabel_S;
        
        @AuraEnabled
        public string fieldValue;
        
        @AuraEnabled
        public string fieldType;
        
        //FOR LOOKUP ONLY
        @AuraEnabled
        public string fieldID;
        
        public blockSectionContentWrapper( decimal fs,string fl_l,string fl_m,string fl_s,string fv,string ft ){
            this.fieldSequence = fs;
            this.fieldLabel_L = fl_l;
            this.fieldLabel_M = fl_m;
            this.fieldLabel_S = fl_s;
            this.fieldType = ft;
            if( ft == 'Lookup' ){
                if( fl_l == 'Primary Business' ){
                    this.fieldID = fv;
                    this.fieldValue = getPrimaryBussinessName( fv );
                }else{
                    this.fieldID = getAccountProfileId( fv );
                    this.fieldValue = fv.split(' - ')[1];
                }
                
                
            }else if( ft == 'Text' || ft == 'Other' ){
                if( fl_l == '# of Sales Teams' ){
                    this.fieldValue = ( fv == 'true' ? 'Multi' : 'Single' );
                }else if( fl_l == 'Channel' ){
                    this.fieldValue = ( fv == 'true' ? 'Multi' : 'Single' );
                }else if( fl_l == 'Karastan' ){
                    this.fieldValue = ( fv == 'true' ? 'No' : 'Yes' );
                }else{
                    this.fieldValue = (fv != null && fv != '' && fv.trim().length() > 0 ? fv : '0');    
                }
            }else{
                this.fieldValue = (fv != null && fv != '' && fv.trim().length() > 0 ? fv : '0');
            }
        }
    }    
    
    public class tableWrapper{
        @AuraEnabled
        public List<Header> headers;
        
        @AuraEnabled
        public List<Row> rows;
    }
    
    public class Header implements Comparable{
        @AuraEnabled
        public decimal sequence;
        @AuraEnabled
        public String title_L;
        @AuraEnabled
        public String title_M;
        @AuraEnabled
        public String title_S;
        
        public Header( decimal s, string tl,string tm,string ts ){
            this.sequence = s;
            this.title_L = tl;
            this.title_M = tm;
            this.title_S = ts;
        }
        
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(sequence - ((Header)objToCompare).sequence);               
        }
    }
    
    public class Row implements Comparable{
        @AuraEnabled
        public decimal order;
        @AuraEnabled
        public List<Col> cols;
        
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(order - ((Row)objToCompare).order);               
        }
    }
    
    public class Col implements Comparable{
        @AuraEnabled
        public decimal order;
        @AuraEnabled
        public string value;
        @AuraEnabled
        public String dataLabel_L;
        @AuraEnabled
        public String dataLabel_M;
        @AuraEnabled
        public String dataLabel_S;
        @AuraEnabled
        public String alignment;
        @AuraEnabled
        public String dataType;
        
        public Col( decimal o, string v,string dl_l,string dl_m,string dl_s,string al,string dt ){
            this.order = o;
            this.value = (v != null && v != '' && v.trim().length() > 0 ? v : '0');
            this.dataLabel_L = dl_l;
            this.dataLabel_M = dl_m;
            this.dataLabel_S = dl_s;
            this.alignment = al;
            this.dataType = dt;
        }
        
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(order - ((Col)objToCompare).order);               
        }
    }
    
    /*************************************************************************************
    This method is run soql and get result.
    *************************************************************************************/
    public static Map<string,string>sectionQuery( string objectName,List<string> fieldToQuery,string recordId ){
        Map<string,string>returnMap = new Map<string,string>();
        system.debug( '::::objectName:::::' + objectName );
        system.debug( '::::fieldToQuery:::::' + fieldToQuery );
        system.debug( '::::recordId:::::' + recordId );
        string soql = customDetailUtility.generateSOQLQuery( objectName,fieldToQuery,'id =: recordId',null,'','','' );
        system.debug( '::::soql:::::' + soql );
        sObject AP = database.query( soql );
        for( string str : fieldToQuery ){
            if( str.contains('__r') ){
                sObject childObj = AP.getSObject( str.substringBefore( '.' ) );
                returnMap.put( str,string.valueOf( childObj.get( str.substringAfter( '.' ) ) ) );    
            }else{
            	returnMap.put( str,string.valueOf( AP.get( str ) ) );    
            }
            
        }
        system.debug( '::::returnMap::::' + returnMap );
        return returnMap;
    }
    
    public static string getPrimaryBussinessName( string recordId ){    
        List<Account_Profile_Settings__c> apRec =  [SELECT id,Name FROM Account_Profile_Settings__c WHERE id =: recordId ];
        if(apRec!=null && apRec.size()>0)
            return apRec[0].Name;
        else
            return null;
    }
    
    public static string getAccountProfileId( string recordName ){
        List<Account_Profile__c>APList = new List<Account_Profile__c>([ SELECT id FROM Account_Profile__c WHERE Name =: recordName ]);
        if( APList != null && APList.size() > 0 )
            return APList[0].id; 
        else
            return null;
    }
    
    public static Map<string,string>getDefaultValues( List<AP_Section_Fields__mdt>listOfFields,Map<string,string>fetchData ){
        Map<string,string>returnMap = new Map<string,string>();
        for( AP_Section_Fields__mdt val : listOfFields ){
            if( val.MasterLabel == 'Mohawk Purchases' ){
                returnMap.put( val.MasterLabel,( fetchData.get( val.Field_Name__c ) != null ? fetchData.get( val.Field_Name__c ) : '0' ) );                
            }
            if( val.MasterLabel == 'Wholesale Purchases' ){
                returnMap.put( val.MasterLabel,( fetchData.get( val.Field_Name__c ) != null ? fetchData.get( val.Field_Name__c ) : '0' ) );
            }
            if( val.MasterLabel == 'Green Opportunity' ){
                returnMap.put( val.MasterLabel,( fetchData.get( val.Field_Name__c ) != null ? fetchData.get( val.Field_Name__c ) : '0' ) );
            }
            if( val.MasterLabel == 'Yellow Opportunity' ){
                returnMap.put( val.MasterLabel,( fetchData.get( val.Field_Name__c ) != null ? fetchData.get( val.Field_Name__c ) : '0' ) );
            }
            if( val.MasterLabel == 'Red Opportunity' ){
                returnMap.put( val.MasterLabel,( fetchData.get( val.Field_Name__c ) != null ? fetchData.get( val.Field_Name__c ) : '0' ) );
            }
        }
        return returnMap;
    }
    
    public static string calculateGreenOpportunity( decimal AMP,decimal GOPP ){
         if( AMP < GOPP )
             return string.valueOf( GOPP - AMP );
         else
             return '0';
       
    }
    
    public static string calculateYellowOpportunity( decimal AMP,decimal GOPP,decimal YOPP ){
    
            string yellowOpportunityValue;
            if( AMP >= ( GOPP + YOPP ) )        
                yellowOpportunityValue = '0';
            else if( AMP <= ( GOPP + YOPP ) && AMP > GOPP )
                yellowOpportunityValue = string.valueOf( ( GOPP + YOPP ) - AMP );
            else if( AMP <= GOPP )
                yellowOpportunityValue = string.valueOf( YOPP );
            return yellowOpportunityValue;        
 
    }
    
    public static string calculateRedOpportunity( decimal AMP,decimal EWP,decimal GOPP, decimal YOPP,decimal ROPP ){
       
            string redOpportunityValue;
            if( AMP >= EWP )
                redOpportunityValue = '0';
            else if( AMP < EWP && AMP >= ( GOPP + YOPP ) && AMP > 0)
                redOpportunityValue = string.valueOf( EWP - AMP );
            else if( AMP <= ( GOPP + YOPP ) )
                redOpportunityValue = string.valueof( ROPP );
            return redOpportunityValue;
     
    }
    
    public static boolean isNullChk( string val ){
        if( val == null )
            return true;
        else
            return false;
    }
}