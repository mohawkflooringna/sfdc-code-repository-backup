/**************************************************************************

Name	:	createExpandableSectionController 

===========================================================================
Purpose	:	This class is used in lightning component which is used in 
			Account Profile Detail Page.
===========================================================================
History:
--------
VERSION		AUTHOR		DATE			DETAIL		DESCRIPTION
1.0        	Mudit		05/June/2018     			Created
***************************************************************************/
public without sharing class createExpandableSectionController{
	
    /*********************************************************************
	* This Method is use to check wether to show Account Profile Details
		or MAT details on Account Profile
	*********************************************************************/
	@AuraEnabled
    public static string showMatScreenOnAP( string recordId ){
        if( recordId.substring(0,3) == '001' ){
        	return null;    
        }else{
            List< Mohawk_Account_Team__c>MATList = new  List< Mohawk_Account_Team__c>(
            	[ SELECT id FROM Mohawk_Account_Team__c WHERE Account_Profile__c =: recordId AND User__c =: userinfo.getUserId() ORDER BY Createddate ASC ]
            );
            if( MATList.size() > 0 ){
                return MATList[0].id;
            }else{
                return null;
            }
        }        
    }
    
	/*********************************************************************
	* This Method is use to get Account Profile Details.
	*********************************************************************/
	@AuraEnabled
    public static Account_Profile__c getAccountProfileDetail( string recordId ){
		system.debug( '::::recordId:::' + recordId );
        string soql = customDetailUtility.generateSOQLQuery( 'Account_Profile__c',new List<string>{ 'Id','Name','Multi_Channel__c','Primary_Business__r.Name','Retail_Channel_Split__c','Builder_Multi_Channel_split__c','Chain_Number__c' },'id =: recordId',null,null,null,'1' );
		sobject obj = database.query( soql );
		if( obj == null )
			return null;
		system.debug( '--->' + ( Account_Profile__c )obj );
		return ( Account_Profile__c )obj;
    }
	
    @AuraEnabled
    public static Mohawk_Account_Team__c getMohawkAccountTeam( string recordId ){
		string soql = customDetailUtility.generateSOQLQuery( 'Mohawk_Account_Team__c',new List<string>{ 'Id','Name','Brands__c','Product_Types__c' },'id =: recordId',null,null,null,'1' );
		sobject obj = database.query( soql );
		if( obj == null )
			return null;
		
		return ( Mohawk_Account_Team__c )obj;
    }
    
	/*********************************************************************
	* This Method is use to get the Section Header and List of Fields.
	* This Method is use in parent component. 
	*********************************************************************/
	@AuraEnabled
    public static List<expendableSectionWrapper> createExpandableSections( string BusinessType,string device,string recordId  ){
        List<expendableSectionWrapper>expendableSectionWrapperList = new List<expendableSectionWrapper>();
        string sectionObject = customDetailUtility.sectionObjectMap.get( customDetailUtility.getObjectName( recordId ) );
		
		expendableSectionWrapperList.addAll( createMandatorySection( sectionObject,recordId ) );
		
		//string whereCondition = 'Section_Object__c =: sectionObject';
		string whereCondition = 'Section_Object__c = '+ '\'' + sectionObject + '\'';
        if( sectionObject == 'Account Profile' ){
			//whereCondition += ' AND Business_Type__c =: BusinessType'; 
			whereCondition += ' AND Business_Type__c = ' + '\'' + BusinessType + '\'';
            whereCondition += ' AND Business_Type__c != null';
        }
		
		expendableSectionWrapperList.addAll( createOtherSection( sectionObject,whereCondition ) );
		system.debug( '--->' + expendableSectionWrapperList );
        return expendableSectionWrapperList;
    }
	
	/*********************************************************************
	* This Method is use create Other Expandable Section. 
	*********************************************************************/
	public static List<expendableSectionWrapper>createOtherSection( string sectionObject,string conditions ){
		List<expendableSectionWrapper>expendableSectionWrapperList = new List<expendableSectionWrapper>();
		string soql = customDetailUtility.generateSOQLQuery( customDetailUtility.AP_SECTION_OBJECT_NAME,customDetailUtility.AP_SECTION_OBJECT_FIELD_LIST,conditions,customDetailUtility.AP_SECTION_FIELD_FIELD_LIST,customDetailUtility.retationShipName,'','' );		
        system.debug('::::soql::::' + soql);
        List<AP_Section__mdt>result = database.query( soql );
		for( AP_Section__mdt otherSection : result ){
            if( otherSection.Parent_Section__c == null || otherSection.Parent_Section__c == '' ){
                expendableSectionWrapper ESW = new expendableSectionWrapper();	
                ESW.sectionSequence = string.valueof( otherSection.Sequence__c );
                ESW.sectionHeader = otherSection.MasterLabel;
                ESW.sectionDisplayType = otherSection.Display_As__c;
                ESW.isSectionDefaultOn = otherSection.ExpandableDefaultON__c;
                ESW.isSectionDefaultOn_PHONE = otherSection.ExpandableDefaultON_PHONE__c;
                ESW.isSectionDefaultOn_TABLET = otherSection.ExpandableDefaultON_TABLET__c;
                ESW.isSectionAlwaysShow = otherSection.Always_Show__c;
                ESW.blockSectionContent = otherSection.AP_Section_Fields1__r;
                for( AP_Section__mdt childSection : result ){
                    if( childSection.Parent_Section__c == otherSection.MasterLabel ){
                        tableSectionContentWrapper TSCW = new tableSectionContentWrapper();
                        TSCW.listOfTotal = otherSection.AP_Section_Fields1__r;
                        TSCW.APSectionMasterLabel = childSection.MasterLabel;
                        TSCW.APSectionMasterSequence = childSection.Sequence__c;
                        TSCW.listOfFields = childSection.AP_Section_Fields1__r;
                        ESW.tableSectionContent.add( TSCW );
                    }
                }
                expendableSectionWrapperList.add( ESW );
            }
        }
        system.debug( '--->' + expendableSectionWrapperList );
		return expendableSectionWrapperList;
	}
	
	
	/*********************************************************************
	* This Method is use create Mandatory Expandable Section. 
	*********************************************************************/
	public static List<expendableSectionWrapper>createMandatorySection( string sectionObject,string recordId ){
		List<expendableSectionWrapper>expendableSectionWrapperList = new List<expendableSectionWrapper>();
		string whereClause = 'Section_Object__c =: sectionObject AND Always_Show__c = true ';
		string soql = customDetailUtility.generateSOQLQuery( customDetailUtility.AP_SECTION_OBJECT_NAME,customDetailUtility.AP_SECTION_OBJECT_FIELD_LIST,whereClause,customDetailUtility.AP_SECTION_FIELD_FIELD_LIST,customDetailUtility.retationShipName,'','' );
        for( AP_Section__mdt mandatorySection : database.query( soql ) ){        
            if( mandatorySection.Display_As__c != 'Link' ){
                expendableSectionWrapper ESW = new expendableSectionWrapper();
                ESW.sectionSequence = string.valueof( mandatorySection.Sequence__c );
                ESW.sectionHeader = mandatorySection.MasterLabel;
                ESW.sectionDisplayType = mandatorySection.Display_As__c;
                ESW.isSectionDefaultOn = mandatorySection.ExpandableDefaultON__c;
                ESW.isSectionDefaultOn_PHONE = mandatorySection.ExpandableDefaultON_PHONE__c;
                ESW.isSectionDefaultOn_TABLET = mandatorySection.ExpandableDefaultON_TABLET__c;
                ESW.isSectionAlwaysShow = mandatorySection.Always_Show__c;
                ESW.blockSectionContent = mandatorySection.AP_Section_Fields1__r;    
                expendableSectionWrapperList.add( ESW );
            }else{
                expendableSectionWrapper ESW = new expendableSectionWrapper();
                ESW.sectionSequence = string.valueof( mandatorySection.Sequence__c );
                ESW.sectionHeader = mandatorySection.MasterLabel;
                ESW.sectionDisplayType = mandatorySection.Display_As__c;
                ESW.isSectionDefaultOn = mandatorySection.ExpandableDefaultON__c;
                ESW.isSectionDefaultOn_PHONE = mandatorySection.ExpandableDefaultON_PHONE__c;
                ESW.isSectionDefaultOn_TABLET = mandatorySection.ExpandableDefaultON_TABLET__c;
                for( AP_Section_Fields__mdt link : mandatorySection.AP_Section_Fields1__r ){
                    ESW.linkSectionContent.add( new linkSectionContentWrapper( link.Sequence__c,link.MasterLabel,string.valueof(link.Field_Name__c),recordId ) );
                }
                ESW.linkSectionContent.sort();    
                expendableSectionWrapperList.add( ESW );
            }
            
        }
        system.debug( '--->' + expendableSectionWrapperList );
		return expendableSectionWrapperList;
	}
	
    public class expendableSectionWrapper{
        @AuraEnabled
        public string sectionSequence;
        
        @AuraEnabled
        public string sectionHeader;
        
        @AuraEnabled
        public string sectionDisplayType;
        
        @AuraEnabled
        public boolean isSectionDefaultOn;
        
        @AuraEnabled
        public boolean isSectionDefaultOn_PHONE;
        
        @AuraEnabled
        public boolean isSectionDefaultOn_TABLET;
        
        @AuraEnabled
        public boolean isSectionAlwaysShow;
        
        @AuraEnabled
        public List<AP_Section_Fields__mdt>blockSectionContent;
        
        @AuraEnabled
        public List<tableSectionContentWrapper>tableSectionContent;
        
        @AuraEnabled
        public List<linkSectionContentWrapper>linkSectionContent;
        
        public expendableSectionWrapper( ){
            this.blockSectionContent = new List<AP_Section_Fields__mdt>();
            this.tableSectionContent = new List<tableSectionContentWrapper>();
            this.linkSectionContent = new List<linkSectionContentWrapper>();
        }
    }
	
	public class tableSectionContentWrapper{
        @AuraEnabled
        public string APSectionMasterLabel;
        
        @AuraEnabled
        public decimal APSectionMasterSequence;
        
        @AuraEnabled
        public List<AP_Section_Fields__mdt>listOfTotal;
        
        @AuraEnabled
        public List<AP_Section_Fields__mdt>listOfFields;
        
        public tableSectionContentWrapper(){
            this.listOfTotal = new List<AP_Section_Fields__mdt>();
            this.listOfFields = new List<AP_Section_Fields__mdt>();
        }
    }
	
    public class linkSectionContentWrapper implements Comparable{
        @AuraEnabled
        public decimal linkSequence;
        
        @AuraEnabled
        public string linkLabel;
        
        @AuraEnabled
        public string linkURL;
        
        public linkSectionContentWrapper( decimal ls,string ll,string lu,string recordId  ){
            this.linkSequence = ls;
            this.linkLabel = ll;
            if( lu.contains('<param1>') )
            this.linkURL = lu.replace('<param1>', recordId);
            else
                this.linkURL = lu;
        }
        
        public Integer compareTo(Object objToCompare) {
            return Integer.valueOf(linkSequence - ((linkSectionContentWrapper)objToCompare).linkSequence);               
        }
        
    }
    
}