/**************************************************************************

Name : MashupUtility_CC_Test

===========================================================================
Purpose : This tess class is used for MashupUtility_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         19/May/2017     Created 
2.0        Susmitha       14/March/2018   Modified
***************************************************************************/
@isTest
private class MashupUtility_CC_Test {
    static List<Mashup_Management__c> mashupManList; 
    
    public static void init(){
        mashupManList  = Utility_Test.getMashupManagementCusSetting();
    }
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.ADMIN_USER) {
            Test.startTest();
            MashupUtility_CC.isRecordTypeAvailOnObject('ABC');
            MashupUtility_CC.isRecordTypeAvailOnObject('Account');
            
            Mashup_Management__c data = new Mashup_Management__c( Name = 'Create Product Order - Project', Target_URL__c='https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/AccountId/view-catalog?site=mhkflooring',
                                                                 Query_String__c = 'AccountId=Id;;REMOTEID=Global_Account_Number__c', Object__c='Project', Group__c='Product Order', Sort_Order__c = 1, No_Query_String__c = true,
                                                                 Is_Publisher_Action__c = false, Type__c = 'Hybris', Is_Active__c = true, Display_Name__c = 'Create Product Order', Profile__c = 'Residential');
            
            insert data;
            List<Account> accList =  Utility_Test.createAccountsForNonInvoicing(true, 1);
            
            MashupUtility_CC.getFinalUrl(accList[0],data, true, false );
            system.assert(data.Id!=null);
            Test.stopTest();
        }
    }
    
    // for no query string
    static testMethod void testMethod2() {
        System.runAs(Utility_Test.COMMERCAIL_USER) {
            Test.startTest();
            
            Mashup_Management__c data = new Mashup_Management__c( Name = 'Create Product Order - Project', Target_URL__c='https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/AccountId/view-catalog?site=mhkflooring',
                                                                 Query_String__c = 'AccountId=Global_Account_Number__c;;REMOTEID=Global_Account_Number__c', Object__c='Project', Group__c='Product Order', Sort_Order__c = 1, No_Query_String__c = true,
                                                                 Is_Publisher_Action__c = false, Type__c = 'Hybris', Is_Active__c = true, Display_Name__c = 'View Quote', Profile__c = 'Commercial');
            
            insert data;
            system.assert(data.Id!=null);
            
            List<Account> accList =  Utility_Test.createAccountsForNonInvoicing(true, 1);
            
            MashupUtility_CC.getFinalUrl(accList[0],data, true, false );
            
            Test.stopTest();
        }
    }
    
    // for with query string
    static testMethod void testMethod3() {
        System.runAs(Utility_Test.ADMIN_USER) {
            Test.startTest();
            
            List<Account> accList =  Utility_Test.createAccountsForNonInvoicing(true, 1);
            
            Mashup_Management__c data = new Mashup_Management__c( Name = 'Create Product Order - Project', Target_URL__c='https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/view-catalog?site=mhkflooring',
                                                                 Query_String__c = 'AccountId=Id', Object__c='Project', Group__c='Product Order', Sort_Order__c = 1, No_Query_String__c = false,
                                                                 Is_Publisher_Action__c = false, Type__c = 'Hybris', Is_Active__c = true, Display_Name__c = 'Create Product Order', Profile__c = 'Residential');
            
            insert data;
            
            MashupUtility_CC.getFinalUrl(accList[0],data, true, false );
            
            data = new Mashup_Management__c( Name = 'Create Product Order - Project', Target_URL__c='https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/view-catalog',
                                            Query_String__c = 'AccountId=Global_Account_Number__c&site=mhkflooring', Object__c='Project', Group__c='Product Order', Sort_Order__c = 1, No_Query_String__c = false,
                                            Is_Publisher_Action__c = false, Type__c = 'Hybris', Is_Active__c = true, Display_Name__c = 'Create Product Order', Profile__c = 'All');
            
            insert data;
            system.assert(data.Id!=null);
            
            MashupUtility_CC.getFinalUrl(accList[0],data, true, false );
            
            Test.stopTest();
        }
    }
    
    static testMethod void testMethod4() {
        System.runAs(Utility_Test.ADMIN_USER) {
            Test.startTest();
            
            Mashup_Management__c data = new Mashup_Management__c( Name = 'Create Product Order - Project', Target_URL__c='https://ycommercebotst.mohawkind.com/samlsinglesignon/saml/mhkflooringstorefront/en/AccountId/view-catalog?site=mhkflooring',
                                                                 Query_String__c = 'AccountId=Id;;REMOTEID=Global_Account_Number__c', Object__c='Project', Group__c='Product Order', Sort_Order__c = 1, No_Query_String__c = true,
                                                                 Is_Publisher_Action__c = false, Type__c = 'Hybris', Is_Active__c = true, Display_Name__c = 'Create Product Order', Profile__c = 'Residential');
            
            insert data;
            system.assert(data.Id!=null);
            
            List<Account> accList =  Utility_Test.createAccountsForNonInvoicing(false, 2);
            accList[1].RecordTypeId = UtilityCls.getRecordTypeInfo('Account', UtilityCls.INVOICING_ACCOUNT);
            insert accList;
            
            Map<String, Map<String,list<Mashup_Management__c>>> result = MashupUtility_CC.getRecordsByMashupType(accList[1], 'Hybris', 'Account', 'Invoicing', false, false);
            
            result = MashupUtility_CC.getRecordsByMashupType(accList[0], 'Hybris', 'Product2', 'Non_Invoicing', false, false);
            result = MashupUtility_CC.getRecordsByMashupType(accList[0], 'Hybris', 'Opportunity', 'Non_Invoicing', false, false);
            result = MashupUtility_CC.getRecordsByMashupType(accList[0], 'Hybris', 'Order', 'Non_Invoicing', false, false);
            
            Test.stopTest();
        }
    }
    
}