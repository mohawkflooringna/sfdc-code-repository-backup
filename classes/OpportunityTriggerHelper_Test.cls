@isTest
public class OpportunityTriggerHelper_Test 
{
    static testmethod void testMethodNew() 
    {		rdcc__Insight_Company__c iCom2 = new rdcc__Insight_Company__c();
     iCom2.Name = 'test1';
     iCom2.rdcc__Company_ID__c = '12341';
     insert iCom2;
     System.assertEquals('12341',iCom2.rdcc__Company_ID__c);
     Test.startTest();
     
     
     Map<String,Id> mapOfAcc = OpportunityTriggerHelper.convertAccount(new List<String>{iCom2.rdcc__Company_ID__c});
     Map<String, String> compIdAccountIdMap = new Map<String, String>();
     for(String sr : mapOfAcc.keySet())
     {
         compIdAccountIdMap.put(sr,String.valueOf(mapOfAcc.get(sr)));
     }
     DataConvertHelper.afterCompanyConversion(new List<rdcc__Insight_Company__c>{iCom2},compIdAccountIdMap);
     
     //OpportunityTriggerHandler.getCountryName('TestCountry');
     //OpportunityTriggerHandler.getStateName('TestState');
     
     Test.stopTest();
    }
    
    static testmethod void testMethodNew2() 
    {		
        
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Invoicing').getRecordTypeId();
        //Creating config settings
        rdcc__Configurable_Settings__c configSett = new rdcc__Configurable_Settings__c(rdcc__Account_Additional_Fields_on_Conversion__c = 'Name;NumberOfEmployees',
                                                                                      	rdcc__Automated_SF_Account_Creation__c = 'Yes',
                                                                                      	rdcc__Allow_Future_ConstructConnect_Updates__c='Yes',
                                                                                      	rdcc__Convert_Primary_Contact_only__c=true,
                                                                                      	rdcc__Account_Record_Type__c=RecordTypeIdAccount,
                                                                                       rdcc__Allow_Future_Updates_to_Account_Contact__c='Yes');
        insert configSett;
    	
        rdcc__ReedProject__c prj=new rdcc__ReedProject__c();
        prj.Name='TestProject';
        insert prj;
        System.assertEquals('TestProject', prj.Name); 
        
        rdcc__ReedProject__c prj2=new rdcc__ReedProject__c();
        prj2.Name='TestProject2';
        insert prj2;
        System.assertEquals('TestProject2', prj2.Name); 
        
        rdcc__Insight_Company__c iCom2 = new rdcc__Insight_Company__c();
        iCom2.Name = 'test1';
        iCom2.rdcc__Company_ID__c = '12341';
        //iCom2.rdcc__Shipping_Country__c = 'Canada';
        insert iCom2;
        System.assertEquals('12341',iCom2.rdcc__Company_ID__c);
        
        rdcc__Insight_Company__c iCom3 = new rdcc__Insight_Company__c();
        iCom3.Name = 'test3';
        iCom3.rdcc__Company_ID__c = '123412';
        iCom3.rdcc__Shipping_Country__c = 'Canada';
        iCom3.rdcc__Shipping_State__c = 'Ontario';
        insert iCom3;
        System.assertEquals('123412',iCom3.rdcc__Company_ID__c);
        
        Account acc = new Account();
        acc.Name = 'Test';
        acc.rdcc__Company_ID__c = '123456789';
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Test 2';
        acc2.rdcc__Company_ID__c = '12345678';
        insert acc2;
        
        Account acc3 = new Account();
        acc3.Name = 'Test 2';
        //acc2.rdcc__Company_ID__c = '12345678';
        insert acc3;
        
        rdcc__ProjectCompanyRole__c compRole=new rdcc__ProjectCompanyRole__c();
        compRole.rdcc__CMD_Comapany_Id__c='12345678';
        compRole.rdcc__Primary_Contact__c = 'Test Primary';
        //compRole.rdcc__Insight_Company__c = iCom2.Id;
        compRole.rdcc__Account__c = acc2.Id;
        compRole.rdcc__Project__c=prj.Id;
        insert compRole;
        System.assertEquals('12345678', compRole.rdcc__CMD_Comapany_Id__c);
        
        rdcc__ProjectCompanyRole__c compRole2=new rdcc__ProjectCompanyRole__c();
        compRole2.rdcc__CMD_Comapany_Id__c='123456789';
        compRole2.rdcc__Primary_Contact__c = 'Test Primary';
        compRole.rdcc__Insight_Company__c = iCom3.Id;
        //compRole2.rdcc__Account__c = acc2.Id;
        compRole2.rdcc__Project__c=prj2.Id;
        insert compRole2;
        System.assertEquals('123456789', compRole2.rdcc__CMD_Comapany_Id__c);
        
        
        Opportunity opp = new Opportunity();
        opp.rdcc__Project__c = prj.Id;
        opp.StageName = 'Bidding';
        opp.Status__c = 'On Hold';
        opp.AccountId = acc.Id;
        opp.Location__c = 'Test USA';
        opp.Market_Segment__c = 'Workplace';
        opp.Name = 'Test Opportunity';
        opp.CloseDate = date.today();
        
        Opportunity opp2 = new Opportunity();
        opp2.rdcc__Project__c = prj2.Id;
        opp2.StageName = 'Bidding';
        opp2.Status__c = 'On Hold';
        opp2.AccountId = acc3.Id;
        opp2.Location__c = 'Test USA';
        opp2.Market_Segment__c = 'Workplace';
        opp2.Name = 'Test Opportunity 2';
        opp2.CloseDate = date.today();
        
        Test.startTest();
        insert opp;
        insert opp2;
        OpportunityTriggerHelper.getCountryName('Canada');
        OpportunityTriggerHelper.getAllowedRecordTypeIds('Opportunity');
        OpportunityTriggerHelper.isStateCountryPicklistEnabled();
        OpportunityTriggerHelper.getRecordTypeMap('Opportunity');
        OpportunityTriggerHelper.getStateName('Ontario');
        OpportunityTriggerHelper.mapAccountFields(iCom2,'Yes');
        OpportunityTriggerHelper.mapAccountFields(iCom3,'No');
        UtilityClass.getRecordTypeMap('Contact');
        UtilityClass.getAllowedRecordTypeIds('Contact');
        Test.stopTest();
    }
}