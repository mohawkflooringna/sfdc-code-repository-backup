public with sharing class ChartComponentController_Helper {


    public static String COMMERCIAL_BROADLOOM =     'Commercial Broadloom';
    public static String RESIDENTIAL_BROADLOOM =    'Residential Broadloom';
    public static String RESILIENT_SHEET =          'Resilient Sheet';
    public static String CARPET_TILE =              'Carpet Tile';
    public static String CUSHION =                  'Cushion';
    public static String SOLID_WOOD =               'Solid Wood';
    public static String TEC_WOOD =                 'Tec Wood';
    public static String REV_WOOD =                 'Rev Wood';
    public static String SOLIDWOOD =                'SolidWood';
    public static String TECWOOD =                  'TecWood';
    public static String REVWOOD =                  'RevWood';
    public static String TILE =                     'Tile';
    public static String RESILIENT_TILE =           'Resilient Tile';
    public static String RESILIENT_TILE_PLANK =     'Resilient Tile and Plank'; //Added by MB - Bug 71514 - 3/12/19
    public static String INSTALLATION_ACCESSORIES = 'Installation Accessories';
    public static String CARE_AND_MAINTENANCE =     'Care and Maintenance';
    public static String CARE_MAINTENANCE =         'Care & Maintenance'; //Added by MB - Bug 71514 - 3/12/19
    
    
    
	//***************************************************************************************
    // Build The Query String required on each chart
	//***************************************************************************************

    public static String getDataQueryStr_Chart1(String region, String district, String territory, String objName, String styleNum) {
        return '';
    }
    public static String getDataQueryStr_Chart1(String region, String district, String territory, String objName, String styleNum, String inventoryNum) {
        
        //Build a list of condition to be used in the WHERE clause
        List<String> whereConditions = new List<String>();
        //whereConditions.add('Price_Level__c in (\'> TM1\',\'TM1\',\'TM2\',\'TM3\',\'DM\',\'RVP\',\'< RVP\')');
        if (String.isNotEmpty(styleNum)) whereConditions.add('Selling_Style_Num__c = \'' + styleNum + '\'');
        if (String.isNotEmpty(region)) whereConditions.add('CPL_Product_Territory__r.Region_Code__c = \'' + region + '\'');
        if (String.isNotEmpty(district)) whereConditions.add('CPL_Product_Territory__r.District_Code__c  = \'' + district + '\'');
        if (String.isNotEmpty(territory)) whereConditions.add('CPL_Product_Territory__r.Sales_Group_TM__c = \'' + territory + '\'');
        
        //Add the reference to Product__r.Inventory_Style_Num__c for Cushion and Price Grid
        if (String.isNotEmpty(inventoryNum)) whereConditions.add('Product__r.Inventory_Style_Num__c = \'' + inventoryNum + '\'');
        
        whereConditions.add(getPriceLevelINStatement('Price_Level__c'));
        whereConditions.add('Is_Current_Price__c = true'); //Added by MB - Bug 71514 - 3/12/19
        String queryStr = '';
        queryStr += ' SELECT Count(Id),Price_Level__c';
        queryStr += ' FROM ' + objName;
        if (whereConditions.size() > 0)
            queryStr += ' WHERE ' + String.join(whereConditions, ' AND ');
        queryStr += ' GROUP BY Price_Level__c';
        
        return queryStr;
    }
    

    public static String getDataQueryStr_Chart2(String region, String district, String territory, String objName, String styleNum, String timePeriod, String inventoryNum) {
        
        //Build the list of Account the user has access to
        String queryStr = '';
        Set<String> setUniqueChainNumber = new Set<String>();
		String chainQuery = 'SELECT account__r.chain_number__c FROM ' + objName + ' WHERE Selling_Style_Num__c = : styleNum';
        List<SObject> chainList = Database.query(chainQuery);
        
        for (SObject so : chainList){
            SObject account = so.getSObject('account__r');
            String chainNumber = (String) account.get('Chain_Number__c');
            if (String.isNotEmpty(chainNumber)) setUniqueChainNumber.add(chainNumber);
        }        
        
        System.debug('### getDataQueryStr_Chart2 - setUniqueChainNumber(' + setUniqueChainNumber.size() + '): ' + setUniqueChainNumber);
        
        if(chainList != null && chainList.size()>0){ //Added condition if no CPL data, do not display Sales Item in the Chart - MB - Bug 71514 - 3/12/19
            //Build a list of condition to be used in the WHERE clause
            List<String> whereConditions = new List<String>();
            
            if (String.isNotEmpty(styleNum)) whereConditions.add('Selling_Style_Num__c = \'' + styleNum + '\'');
            if (String.isNotEmpty(region)) whereConditions.add('Region_Code__c = \'' + region + '\'');
            if (String.isNotEmpty(district)) whereConditions.add('District_Code__c  = \'' + district + '\'');
            if (String.isNotEmpty(territory)) whereConditions.add('Territory_Manager_Id__c = \'' + territory + '\'');
            if (setUniqueChainNumber.size() > 0) whereConditions.add('Division_Customer_No_SFX_Id__c IN ' + '(\'' + String.join(new List<String>(setUniqueChainNumber), '\', \'') + '\')' );
            
            //Add the reference to Product__r.Inventory_Style_Num__c for Cushion and Price Grid
            if (String.isNotEmpty(inventoryNum)) whereConditions.add('Product__r.Inventory_Style_Num__c = \'' + inventoryNum + '\'');
            
            whereConditions.add(getPriceLevelINStatement(timePeriod + '_Price_Level__c'));
            
            
            queryStr += ' SELECT ' + timePeriod + '_Price_Level__c, SUM(Net_Sales_' + timePeriod + '__c)';
            queryStr += ' FROM CPL_Sales_Item__c'; 
            if (whereConditions.size() > 0)
                queryStr += ' WHERE ' + String.join(whereConditions, ' AND ');
            queryStr += ' GROUP BY ' + timePeriod + '_Price_Level__c';
        }
        System.Debug('### queryStr chart 2: ' + queryStr);
        return queryStr;
    }

    public static String getDataQueryStr_Chart3(String territory, String objName, String accountId) {
        
        // Get Chain Account Id for Inherited Price Records
        String ChainAccountId = ResidentialPricingGridUtility.getChainAccountId(accountId);
        Set<String> setInheritedProdIds = New Set<String>();
        Set<String> setIds = New Set<String>();
        Account a = [Select CAMS_Account_Number__c From Account Where Id=:accountId Limit 1];
        Boolean suffixAccount = false;
        if (a != null){
            String strAccountNumber = a.CAMS_Account_number__c;
            if (!strAccountNumber.contains('.0000')){
                suffixAccount = true;
                // Get Suffix Account Prod Ids
                String strSOQL = 'Select Id, Product__c From ' + objName  + ' Where Account__c = \'' + accountId + '\' And Price_Level__c in (\'Greater_than_TM1\',\'TM1\',\'TM2\',\'TM3\',\'DM\',\'RVP\',\'Less_than_RVP\')';
                if (String.isNotEmpty(territory)) strSOQL += ' AND CPL_Product_Territory__r.Sales_Group_TM__c = \'' + territory + '\'';
                for (sObject so : database.query(strSOQL)){
                    String ProdId = so.get('Product__c').toString();
                    String soId = so.get('Id').toString();
                    if (String.isNotEmpty(ProdId)) {
                        setInheritedProdIds.add(ProdId);
                        setIds.add(soId);
                    }
                }
                strSOQL = 'Select Id, Product__c From ' + objName  + ' Where Account__c = \'' + ChainAccountId + '\'  And Price_Level__c in (\'Greater_than_TM1\',\'TM1\',\'TM2\',\'TM3\',\'DM\',\'RVP\',\'Less_than_RVP\')';
                if (String.isNotEmpty(territory)) strSOQL += ' AND CPL_Product_Territory__r.Sales_Group_TM__c = \'' + territory + '\'';
                for (sObject so : database.query(strSOQL)){
                    String ProdId = so.get('Product__c').toString();
                    if (String.isNotEmpty(ProdId)){
                        if (!setInheritedProdIds.contains(ProdId)) setIds.add(so.get('Id').toString());
                    } 
                }
            }
        }
        
        
        //Build a list of condition to be used in the WHERE clause
        List<String> whereConditions = new List<String>();
        
        if (suffixAccount) {
            String strIds = '(\'' + String.join(new List<String>(setIds), '\', \'') + '\')';
            if(setIds.size() > 0) whereConditions.add('Id in ' + strIds);
            system.debug('### strIds: ' + strIds);
        }
        else {
            Set<String> setAccountIds = New Set<String>();
            if(String.isNotEmpty(ChainAccountId)) setAccountIds.add(ChainAccountId);
            if(String.isNotEmpty(accountId)) setAccountIds.add(accountId);
            String strActIds = '(\'' + String.join(new List<String>(setAccountIds), '\', \'') + '\')';
            
            if(setAccountIds.size() > 0) whereConditions.add('Account__c in ' + strActIds);
            if(String.isNotEmpty(territory)) whereConditions.add('CPL_Product_Territory__r.Sales_Group_TM__c = \'' + territory + '\'');
            system.debug('### strActIds: ' + strActIds);
        }
        

        whereConditions.add(getPriceLevelINStatement('Price_Level__c'));
        
        String queryStr = '';
        queryStr += ' SELECT Price_Level__c, count(Id)';
		queryStr += ' FROM ' + objName;
        if (whereConditions.size() > 0)
	        queryStr += ' WHERE ' + String.join(whereConditions, ' AND ');
		queryStr += ' GROUP BY Price_Level__c';
        system.debug('query 3' + queryStr);
        return queryStr;
    }
    
    
    public static String getDataQueryStr_Chart4(String territory, String objName, String chainNumber, String timePeriod, String category) {

        //Build a list of condition to be used in the WHERE clause
        List<String> whereConditions = new List<String>();
        if (String.isNotEmpty(territory)) whereConditions.add('Territory_Manager_Id__c = \'' + territory + '\'');
        if (String.isNotEmpty(category)) whereConditions.add('Product__r.Salesforce_Product_Category__c  = \'' + category + '\'');
        if (String.isNotEmpty(chainNumber)) whereConditions.add('Division_Customer_No_SFX_Id__c  = \'' + chainNumber + '\'');

        whereConditions.add(getPriceLevelINStatement(timePeriod + '_Price_Level__c'));
        
        String queryStr = '';
        queryStr += ' SELECT ' + timePeriod + '_Price_Level__c, SUM(Net_Sales_' + timePeriod + '__c)';
        queryStr += ' FROM ' + objName; 
        if (whereConditions.size() > 0)
	        queryStr += ' WHERE ' + String.join(whereConditions, ' AND ');
        queryStr += ' GROUP BY ' + timePeriod + '_Price_Level__c';
        
        return queryStr;
    }
    
    
	//***************************************************************************************
    // Other Helpers
	//***************************************************************************************

    public static String getObjNameFromCategory(String category) {

        String objName = 'CPL_Residential_Broadloom__c';
        
        if      (category == RESIDENTIAL_BROADLOOM)     {objName = 'CPL_Residential_Broadloom__c';}
        else if (category == COMMERCIAL_BROADLOOM)      {objName = 'CPL_Commercial_Broadloom__c';}
        else if (category == CARPET_TILE)               {objName = 'CPL_Carpet_Tile__c';}
        else if (category == CUSHION)                   {objName = 'CPL_Cushion__c';}
        else if (category == SOLID_WOOD)                {objName = 'CPL_Solid_Wood__c';}
        else if (category == TEC_WOOD)                  {objName = 'CPL_Tec_Wood__c';}
        else if (category == REV_WOOD)                  {objName = 'CPL_Rev_Wood__c';}
        else if (category == SOLIDWOOD)                 {objName = 'CPL_Solid_Wood__c';}
        else if (category == TECWOOD)                   {objName = 'CPL_Tec_Wood__c';}
        else if (category == REVWOOD)                   {objName = 'CPL_Rev_Wood__c';}
        else if (category == TILE)                      {objName = 'CPL_Tile__c';}
        else if (category == RESILIENT_SHEET)           {objName = 'CPL_Resilient_Sheet__c';}
        else if (category == RESILIENT_TILE)            {objName = 'CPL_Resilient_Tile__c';}
        else if (category == RESILIENT_TILE_PLANK)      {objName = 'CPL_Resilient_Tile__c';} //Added by MB - Bug 71514 - 3/12/19
        else if (category == INSTALLATION_ACCESSORIES)  {objName = 'CPL_Installation_Accessories__c';}
        else if (category == CARE_AND_MAINTENANCE)      {objName = 'CPL_Care_and_Maintenance__c';}
        else if (category == CARE_MAINTENANCE)          {objName = 'CPL_Care_and_Maintenance__c';} //Added by MB - Bug 71514 - 3/12/19
        
        return objName;
    }


    public static Map<String, decimal> processResults(List<AggregateResult> groupedResults, String fieldName, List<String> labels) {
        Map<String, decimal> pLevelAmount = new Map<String, decimal>();

        //Build data matrix with 0 amount for each Price Level
        //Required to diplay columns with no records
        for (String label : labels) 
            pLevelAmount.put(label, 0);
        
        //Set actual amount on each Price Level
        for (AggregateResult row : groupedResults) {
            String label = string.valueof(row.get(fieldName));
            label = ChartComponentController_Helper.shortLabel(label);
            decimal amount = (decimal)row.get('expr0');
            pLevelAmount.put(label, amount);
        }  

        return pLevelAmount;
    }
    
    public static String getPriceLevelINStatement(String fieldName) {
        List<String> priceLevels = new List<String>{'Greater_than_TM1', 'TM1', 'TM2', 'TM3', 'DM', 'RVP', 'Less_than_RVP'};
        String priceLevelStr = '(\'' + String.join(priceLevels, '\', \'') + '\')';
       	return fieldName + ' IN ' + priceLevelStr;              
    }
    
    public static List<String> retrievePriceLevelHeaders() {
        return new List<String>{'>TM1', 'TM1', 'TM2', 'TM3', 'DM', 'RVP', '<RVP'};
    }
        
    
    
    public static String shortLabel(String label) {
        
        //Replace longer prefix by > or <
        //Getting >TM1 or <RVP
        if (label != null) {
            label = label.replace('Greater_than_', '>');
            label = label.replace('Less_than_', '<');
        }
        
        return label;
    }
    
    
    
    public static String getColorForChart(String chartId) {
        //Blue color
        if (chartId == '1' || chartId == '3') return 'rgb(54, 162, 235)';
        
        //Orange color
        if (chartId == '2' || chartId == '4') return 'rgb(255, 159, 64)';
        
        //Default to grey color
        return 'rgb(201, 203, 207)';
    }

   

    
}