/**************************************************************************

Name : AccountProfileShare_Batch 

===========================================================================
Purpose : Batch class to Share account profile record with it's accounts mohawk account team members
========================================================================== =
History:
--------
VERSION    AUTHOR            DATE           DETAIL          DESCRIPTION
1.0        Anand       26/April/2017        Created          CSR:

***************************************************************************/
global class AccountProfileShare_Batch  {

}