public class ChartComponentController {
    
    public static String COMMERCIAL_BROADLOOM =     'Commercial Broadloom';
    public static String RESIDENTIAL_BROADLOOM =    'Residential Broadloom';
    public static String RESILIENT_SHEET =          'Resilient Sheet';
    public static String CARPET_TILE =              'Carpet Tile';
    public static String CUSHION =                  'Cushion';
    public static String SOLIDWOOD =                'SolidWood';
    public static String TECWOOD =                  'TecWood';
    public static String REVWOOD =                  'RevWood';
    public static String TILE =                     'Tile';
    public static String RESILIENT_TILE =           'Resilient Tile';
    public static String INSTALLATION_ACCESSORIES = 'Installation Accessories';
    public static String CARE_AND_MAINTENANCE =     'Care and Maintenance';
    
    
    @AuraEnabled
    public static ChartWrapper getData(String region, String district, String territory, String objName, String styleNum, String chartId, String timePeriod, String category, String accountId, String inventoryNum){
        ChartWrapper returndata; 
        
        //Default category (and object name) to "Residential Broadloom" if null
        if (String.isEmpty(category)) category = 'Residential Broadloom';
        objName = ChartComponentController_Helper.getObjNameFromCategory(category);
        
        //Default time period to YTD is null - Replacing YTD to R12 - Bug 70746 - MB - 3/5/19
        if (String.isEmpty(timePeriod)) timePeriod = 'R12';
        
        System.Debug('### ChartComponentController - getData - objName: ' + objName);
        System.Debug('### ChartComponentController - getData - chartId: ' + chartId);
        System.Debug('### ChartComponentController - getData - region: ' + region);
        System.Debug('### ChartComponentController - getData - district: ' + district);
        System.Debug('### ChartComponentController - getData - territory: ' + territory);
        System.Debug('### ChartComponentController - getData - accountId: ' + accountId);
        
        //Dispatch the query data to the right chart number 1, 2, 3 or 4
        if (chartId == '1')	{
            return getData_chart1(region, district, territory, objName, styleNum, chartId, inventoryNum);
        }
        else if (chartId == '2') {	
            return ChartComponentCont_HelperWithoutSharing.getData_chart2(region, district, territory, objName, styleNum, chartId, timePeriod, inventoryNum);
        }
        else if (chartId == '3') {
            return getData_chart3( territory,  category,  objName,  accountId,  chartId);
        }
        else if (chartId == '4') {	
            return ChartComponentCont_HelperWithoutSharing.getData_chart4(territory, category, 'CPL_Sales_Item__c', accountId, chartId, timePeriod);
        }
        
        //Return empty wrapper for unrecognized chart
        return new ChartWrapper();
        
    }
    
    
    public static ChartWrapper getData_chart1(String region, String district, String territory, String objName, String styleNum, String chartId, String inventoryNum){
        ChartWrapper returndata = new ChartWrapper();
        
        String queryStr = ChartComponentController_Helper.getDataQueryStr_Chart1( region,  district,  territory,  objName,  styleNum, inventoryNum);
        String chartColor = ChartComponentController_Helper.getColorForChart(chartId);
        
        System.debug('### query chart 1: ' + queryStr);
        
        AggregateResult[] groupedResults = Database.query(queryStr);
        returndata.dataLabels = ChartComponentController_Helper.retrievePriceLevelHeaders();
        
        Map<String, decimal> pLevelAmount = ChartComponentController_Helper.processResults(groupedResults, 'Price_Level__c', returndata.dataLabels);
        List<Double> amountList = new List<Double>();
        
        for (String label : returndata.dataLabels) {
            double amount = pLevelAmount.get(label);
            amountList.add(amount.intValue());
            returndata.dataValues.add(string.valueof(amount.intValue()));
            returnData.colorValues.add(chartColor);
        }
        
        amountList.sort();
        if (amountList.size() > 0)
            returnData.maxValue = amountList.get(amountList.size()-1);
        else
            returnData.maxValue = 0;
        
        
        //Get Selling Style Num's full name
        if (String.isNotEmpty(styleNum)) {
            queryStr ='SELECT Selling_Style_Concat__c FROM ' + objName + ' WHERE Selling_Style_Num__c = \'' + styleNum + '\' LIMIT 1';
            List<SObject> obj = Database.query(queryStr);
            if (obj != null && obj.size() > 0)
                returndata.styleNumFullName = (String) obj.get(0).get('Selling_Style_Concat__c');
            else
                returndata.styleNumFullName = ' (' + styleNum + ')';
        }
        
        return returndata;
    }
    
    public static ChartWrapper getData_chart2(String region, String district, String territory, String objName, String styleNum, String chartId, String timePeriod, String inventoryNum){
        ChartWrapper returndata = new ChartWrapper();
        
        String queryStr = ChartComponentController_Helper.getDataQueryStr_Chart2( region,  district,  territory,  objName,  styleNum, timePeriod, inventoryNum);
        
        AggregateResult[] groupedResults = Database.query(queryStr);
        
        String chartColor = ChartComponentController_Helper.getColorForChart(chartId);
        returndata.dataLabels = ChartComponentController_Helper.retrievePriceLevelHeaders();
        
        Map<String, decimal> pLeveAmount = ChartComponentController_Helper.processResults(groupedResults, timePeriod + '_Price_Level__c', returndata.dataLabels);
        
        List<Double> amountList = new List<Double>();
        
        for (String label : returndata.dataLabels) {
            
            double amount = pLeveAmount.get(label);
            amountList.add(amount.intValue());
            returndata.dataValues.add(string.valueof(amount.intValue()));
            returnData.colorValues.add(chartColor);
        }
        
        amountList.sort();
        if (amountList.size() > 0)
            returnData.maxValue = amountList.get(amountList.size()-1);
        else
            returnData.maxValue = 0;
        
        //Get Selling Style Num's full name
        
        if (String.isNotEmpty(styleNum)) {
            queryStr ='SELECT Name FROM Product2 WHERE  Product_style_number__c  = \'' + styleNum + '\' LIMIT 1';
            List<SObject> obj = Database.query(queryStr);
            
            if (obj != null && obj.size() > 0) {
                //Map<String,Object> fielMap = obj.get(0).getPopulatedFieldsAsMap();
                Product2 productMap = (Product2) obj.get(0);
                
                if (productMap != null)	
                    returndata.styleNumFullName = (String) productMap.get('Name') + ' (' + styleNum + ')';
                else 
                    returndata.styleNumFullName = ' (' + styleNum + ')';
            }
            else
                returndata.styleNumFullName = ' (' + styleNum + ')';
            
        }
        
        return returndata;
    }        
    
    
    public static ChartWrapper getData_chart3(String territory, String category, String objName, String accountId, String chartId){
        ChartWrapper returndata = new ChartWrapper();
        
        String queryStr = ChartComponentController_Helper.getDataQueryStr_Chart3(territory,  objName,  accountId);
        
        System.debug('### query chart 3: ' + queryStr);
        
        AggregateResult[] groupedResults = Database.query(queryStr);
        
        String chartColor = ChartComponentController_Helper.getColorForChart(chartId);
        
        returndata.dataLabels = ChartComponentController_Helper.retrievePriceLevelHeaders();
        
        Map<String, decimal> pLevelAmount = ChartComponentController_Helper.processResults(groupedResults, 'Price_Level__c', returndata.dataLabels);
        List<Double> amountList = new List<Double>();
        
        for (String label : returndata.dataLabels) {
            double amount = pLevelAmount.get(label);
            amountList.add(amount.intValue());
            returndata.dataValues.add(string.valueof(amount.intValue()));
            returnData.colorValues.add(chartColor);
        }
        
        
        amountList.sort();
        if (amountList.size() > 0)
            returnData.maxValue = amountList.get(amountList.size()-1);
        else
            returnData.maxValue = 0;
        
        //Get Selling Style Num's full name
        if (String.isNotEmpty(accountId)) {
            queryStr ='SELECT Id, Name, CAMS_Account_Number__c FROM Account WHERE Id = \'' + accountId + '\' LIMIT 1';
            List<SObject> obj = Database.query(queryStr);
            if (obj != null && obj.size() > 0)
                returndata.styleNumFullName = (String) obj.get(0).get('Name') + ' (' + (String) obj.get(0).get('CAMS_Account_Number__c') + ')';
            else
                returndata.styleNumFullName = ' ';
        }
        
        return returndata;
    }    
    
    public static ChartWrapper getData_chart4(String territory, String category, String objName, String accountId, String chartId, String timePeriod){
        
        ChartWrapper returndata = new ChartWrapper();
        
        //Get Selling Style Num's full name
        String chainNumber = '';
        if (String.isNotEmpty(accountId)) {
            String accQueryStr ='SELECT Id, Name, Chain_Number__c FROM Account WHERE Id = \'' + accountId + '\' LIMIT 1';
            List<SObject> obj = Database.query(accQueryStr);
            System.debug('### chart 4 obj: ' + obj);
            if (obj != null && obj.size() > 0) {
                chainNumber = (String) obj.get(0).get('Chain_Number__c');
                returndata.styleNumFullName = (String) obj.get(0).get('Name') + ' (' + (String) obj.get(0).get('Chain_Number__c') + ')';
                System.debug('### chart 4 chainNumber (1): ' + chainNumber);
            }
            else
                returndata.styleNumFullName = ' ';
        }
        
        
        System.debug('### chart 4 chainNumber (2): ' + chainNumber);
        
        String chartColor = ChartComponentController_Helper.getColorForChart(chartId);
        returndata.dataLabels = ChartComponentController_Helper.retrievePriceLevelHeaders();
        
        String queryStr = ChartComponentController_Helper.getDataQueryStr_Chart4(territory,  objName,  chainNumber, timePeriod, category);
        AggregateResult[] groupedResults = Database.query(queryStr);
        System.Debug('### chart 4 queryStr: ' + queryStr);
        System.Debug('### chart 4 groupedResults: ' + groupedResults);
        
        Map<String, decimal> pLeveAmount = new Map<String, decimal>();
        for (String label : returndata.dataLabels)
            pLeveAmount.put(label, 0);
        
        for (AggregateResult row : groupedResults) {
            String label = string.valueof(row.get(timePeriod + '_Price_Level__c'));
            label = ChartComponentController_Helper.shortLabel(label);
            decimal amount = (decimal)row.get('expr0');
            pLeveAmount.put(label, amount);
        }
        
        List<Double> amountList = new List<Double>();
        for (String label : returndata.dataLabels) {
            double amount = pLeveAmount.get(label);
            amountList.add(amount.intValue());
            returndata.dataValues.add(string.valueof(amount.intValue()));
            returnData.colorValues.add(chartColor);
        }
        
        amountList.sort();
        if (amountList.size() > 0)
            returnData.maxValue = amountList.get(amountList.size()-1);
        else
            returnData.maxValue = 0;
        
        return returndata;
    }        
    
    
    
    
    public static List<String> retrieveTMNumbers_chart1(String objName, String styleNum) {
        
        List<String> returnList = new List<String>();
        
        String queryStr = 'SELECT CPL_Product_Territory__r.Sales_Group_TM__c, Count(Id) FROM ' + objName;
        
        if (String.isNotEmpty(styleNum)) queryStr += ' WHERE Selling_Style_Num__c = \'' + styleNum + '\' AND ' + ChartComponentController_Helper.getPriceLevelINStatement('Price_Level__c');
        
        
        
        
        queryStr += ' GROUP BY CPL_Product_Territory__r.Sales_Group_TM__c';
        queryStr += ' ORDER BY CPL_Product_Territory__r.Sales_Group_TM__c';
        
        System.Debug('### retrieveTMNumbers_chart1 - queryStr: ' + queryStr);
        
        AggregateResult[] groupedResults = Database.query(queryStr);
        
        for (AggregateResult row : groupedResults) {
            String label = string.valueof(row.get('Sales_Group_TM__c'));
            returnList.add(label);
        }
        System.Debug('### retrieveTMNumbers_chart1 - returnList: ' + returnList);
        return returnList;
    }
    
    public static List<String> retrieveTMNumbers_chart3(String objName, String accountId) {
        
        List<String> returnList = new List<String>();
        
        String queryStr = 'SELECT CPL_Product_Territory__r.Sales_Group_TM__c, Count(Id) FROM ' + objName;
        
        if (String.isNotEmpty(accountId)) queryStr += ' WHERE Account__c = \'' + accountId + '\' AND '  + ChartComponentController_Helper.getPriceLevelINStatement('Price_Level__c');
        
        queryStr += ' GROUP BY CPL_Product_Territory__r.Sales_Group_TM__c';
        queryStr += ' ORDER BY CPL_Product_Territory__r.Sales_Group_TM__c';
        
        System.Debug('### retrieveTMNumbers_chart3 - querySter: ' + queryStr);
        
        AggregateResult[] groupedResults = Database.query(queryStr);
        
        for (AggregateResult row : groupedResults) {
            String label = string.valueof(row.get('Sales_Group_TM__c'));
            returnList.add(label);
        }
        
        return returnList;
    }
    
    
    @AuraEnabled
    public static Map<String, String> getTerritoryNameMap(List<String> tmNumbers) {
        
        Map<String, String> returnOptions = new Map<String, String>();
        Id ResidentialRecordTypeId = ResidentialPricingGridUtility.getRecordTypesByObject('Territory_User__c').get(ResidentialPricingGridUtility.TERRITORY_USER_RESIDENTIAL_RECORD_TYPE_NAME);
        List<Territory_User__c> territoryList = [Select Id,Name, User__r.Name From Territory_User__c Where Name IN : tmNumbers and RecordTypeId = :ResidentialRecordTypeId];
        
        for (Territory_User__c tu : territoryList ) {
            returnOptions.put(tu.Name, tu.User__r.Name);
        }
        
        return returnOptions;
    }
    
    
    public static FilterWrapper loadFilterData_chart1Wrapper(String region, String district, String territory, String objName, String styleNum, String timePeriod, String chartId) {
        
        System.debug('### loadFilterData_chart1Wrapper - region: ' + region);
        System.debug('### loadFilterData_chart1Wrapper - district: ' + district);
        System.debug('### loadFilterData_chart1Wrapper - territory: ' + territory);
        
        FilterWrapper filterWrapper = new FilterWrapper();
        Set<String> territoryNumberSet = new Set<String>();
        
        String queryStr = 'SELECT Region_Code__c, District_Code__c,Sales_Group_TM__c, Count(Id) ';
        queryStr += 'FROM Account_Product_Territory_Relationship__c ';
        queryStr += 'WHERE Id IN (SELECT CPL_Product_Territory__c FROM ' + objName + ' ';
        queryStr += 'WHERE Price_Level__c IN (\'Greater_than_TM1\', \'TM1\', \'TM2\', \'TM3\', \'DM\', \'RVP\', \'Less_than_RVP\') ';
        
        if (String.isNotEmpty(styleNum)) queryStr += 'AND Selling_Style_Num__c = \'' + styleNum + '\'';
        
        queryStr += ') ';
        
        if (String.isNotEmpty(region)) queryStr += 'AND Region_Code__c = \'' + region + '\' ';
        if (String.isNotEmpty(district)) queryStr += 'AND District_Code__c  = \'' + district + '\' ';
        
        
        queryStr += 'GROUP BY Region_Code__c, District_Code__c, Sales_Group_TM__c ';
        queryStr += 'ORDER BY Region_Code__c, District_Code__c, Sales_Group_TM__c ';
        
        system.debug('### loadFilterData (chart 1) queryStr: ' + queryStr);
        
        AggregateResult[] groupedResults = Database.query(queryStr);
        
        
        /*

Query structure:
SELECT Region_Code__c, District_Code__c,Sales_Group_TM__c, Count(Id) 
FROM Account_Product_Territory_Relationship__c 
WHERE id IN (SELECT CPL_Product_Territory__c 
FROM [objectName] 
WHERE Selling_Style_Num__c = '1I88' 
AND Price_Level__c IN ('Greater_than_TM1', 'TM1', 'TM2', 'TM3', 'DM', 'RVP', 'Less_than_RVP')) 
GROUP BY Region_Code__c,District_Code__c,Sales_Group_TM__c 
ORDER BY Region_Code__c,District_Code__c,Sales_Group_TM__c

*/
        
        for(AggregateResult ar : groupedResults)
            territoryNumberSet.add((String) ar.get('Sales_Group_TM__c'));
        
        List<String> tmNumbers = new List<String>(territoryNumberSet);
        Map<String, String> territoryNames = getTerritoryNameMap(tmNumbers);
        
        system.debug('### loadFilterData (chart 1) groupedResults: ' + groupedResults);
        system.debug('### loadFilterData (chart 1) territoryNames: ' + territoryNames);
        
        
        Set<String> regionSet = new Set<String>();
        Set<String> districtSet = new Set<String>();
        Map<String, String> territoryMap = new Map<String, String>();
        
        for (AggregateResult row : groupedResults) { 
            String regionVal = string.valueof(row.get('Region_Code__c'));
            String districtVal = string.valueof(row.get('District_Code__c'));
            String territoryCodeVal = string.valueof(row.get('Sales_Group_TM__c'));
            
            String territoryNameVal = territoryNames.get(territoryCodeVal);     
            
            territoryMap.put(territoryCodeVal, territoryNameVal);
            if (String.isNotEmpty(districtVal))districtSet.add(districtVal);
            if (String.isNotEmpty(regionVal)) regionSet.add(regionVal);
            
        }
        
        List<String> regionFilters = new List<String>(regionSet);
        regionFilters.sort();
        filterWrapper.regionFilters = regionFilters; 
        
        List<String> districtFilters = new List<String>(districtSet);
        districtFilters.sort();
        filterWrapper.ditrictFilters = districtFilters;
        
        filterWrapper.territoryFilters = territoryMap; 
        
        return filterWrapper;
        
    }        
    
    
    public static FilterWrapper loadFilterData_chart2Wrapper(String region, String district, String territory, String objName, String styleNum, String timePeriod, String chartId) {
        
        
        FilterWrapper filterWrapper = new FilterWrapper();
        
        System.debug('### loadFilterData_chart2Wrapper - region: ' + region);
        System.debug('### loadFilterData_chart2Wrapper - district: ' + district);
        System.debug('### loadFilterData_chart2Wrapper - territory: ' + territory);
        
        //Default Time Period if NULL 
        if (String.isEmpty(timePeriod))
            timePeriod = 'R12'; // Changed from YTD to R12 - MB - Bug 71913 - 3/18/19
        
        //Start of Code - MB - Bug 71913 - 3/18/19
        Set<String> setUniqueChainNumber = new Set<String>();
        if(objName != null && objName != ''){
            String chainQuery = 'SELECT account__r.chain_number__c FROM ' + objName + ' WHERE Selling_Style_Num__c = : styleNum';
            List<SObject> chainList = Database.query(chainQuery);
            
            for (SObject so : chainList){
                SObject account = so.getSObject('account__r');
                String chainNumber = (String) account.get('Chain_Number__c');
                if (String.isNotEmpty(chainNumber)) setUniqueChainNumber.add(chainNumber);
            }    
        }
        //End of Code - MB - Bug 71913 - 3/18/19
        //Default object name
        objName = 'CPL_Sales_Item__c'; 
        
        
        String queryStr = 'SELECT Region_Code__c, District_Code__c, Territory_Manager_User_Name__c,Territory_Manager_Id__c, Count(Id)';
        queryStr += ' FROM ' + objName;
        
        
        List<String> whereConditions = new List<String>();
        
        if (String.isNotEmpty(styleNum)) whereConditions.add('Selling_Style_Num__c = \'' + styleNum + '\'');
        if (String.isNotEmpty(region)) whereConditions.add('Region_Code__c = \'' + region + '\'');
        if (String.isNotEmpty(district)) whereConditions.add('District_Code__c  = \'' + district + '\'');
        if (setUniqueChainNumber.size() > 0) whereConditions.add('Division_Customer_No_SFX_Id__c IN ' + '(\'' + String.join(new List<String>(setUniqueChainNumber), '\', \'') + '\')' ); //Added By MB - Bug 71913 - 3/20/19
        whereConditions.add(ChartComponentController_Helper.getPriceLevelINStatement(timePeriod + '_Price_Level__c'));
        
        if (whereConditions.size() > 0) 
            queryStr += ' WHERE ' + String.join(whereConditions, ' AND ');
        
        queryStr += ' GROUP BY Region_Code__c, District_Code__c, Territory_Manager_User_Name__c, Territory_Manager_Id__c ';
        queryStr += ' ORDER BY Region_Code__c, District_Code__c, Territory_Manager_User_Name__c, Territory_Manager_Id__c ';
        
        //system.debug('### queryStr for loadFilterData (chart 2): ' + queryStr);
        
        //AggregateResult[] groupedResults = Database.query(queryStr); - Commented by MB - Bug 71913 - 3/18/19
        AggregateResult[] groupedResults = new List<AggregateResult>();
        if(setUniqueChainNumber.size()>0){
            groupedResults = ChartComponentCont_HelperWithoutSharing.ExecuteQueryDrillDownQuery(queryStr, ''); // Added By MB - Bug 71913 - 3/18/19
        }
        system.debug('### loadFilterData (chart 2) queryStr: ' + queryStr);
        system.debug('### loadFilterData (chart 2) groupedResults: ' + groupedResults);
        
        
        Set<String> regionSet = new Set<String>();
        Set<String> districtSet = new Set<String>();
        Map<String, String> territoryMap = new Map<String, String>();
        
        for (AggregateResult row : groupedResults) { 
            String regionVal = string.valueof(row.get('Region_Code__c'));
            String districtVal = string.valueof(row.get('District_Code__c'));
            String territoryNameVal = string.valueof(row.get('Territory_Manager_User_Name__c'));
            String territoryCodeVal = string.valueof(row.get('Territory_Manager_Id__c'));
            
            
            territoryMap.put(territoryCodeVal, territoryNameVal);
            if (String.isNotEmpty(districtVal))districtSet.add(districtVal);
            if (String.isNotEmpty(regionVal)) regionSet.add(regionVal);
            
        }
        
        List<String> regionFilters = new List<String>(regionSet);
        regionFilters.sort();
        filterWrapper.regionFilters = regionFilters; 
        
        List<String> districtFilters = new List<String>(districtSet);
        districtFilters.sort();
        filterWrapper.ditrictFilters = districtFilters;
        
        filterWrapper.territoryFilters = territoryMap; 
        
        return filterWrapper;
        
    }        
    
    
    
    public static FilterWrapper loadFilterData_chart3Wrapper(String territory, String objName, String accountId, String timePeriod, String chartId) {
        
        FilterWrapper filterWrapper = new FilterWrapper();
        Set<String> territoryNumberSet = new Set<String>();
        String ChainAccountId = ResidentialPricingGridUtility.getChainAccountId(accountId);
        Set<String> setInheritedProdIds = New Set<String>();
        Set<String> setIds = New Set<String>();
        Account a = [Select CAMS_Account_Number__c From Account Where Id=:accountId Limit 1];
        Boolean suffixAccount = false;
        if (a != null){
            String strAccountNumber = a.CAMS_Account_number__c;
            if (!strAccountNumber.contains('.0000')){
                suffixAccount = true;
                // Get Suffix Account Prod Ids
                String strSOQL = 'Select Id, Product__c From ' + objName  + ' Where Account__c = \'' + accountId + '\' And Price_Level__c in (\'Greater_than_TM1\',\'TM1\',\'TM2\',\'TM3\',\'DM\',\'RVP\',\'Less_than_RVP\')';
                for (sObject so : database.query(strSOQL)){
                    String ProdId = so.get('Product__c').toString();
                    String soId = so.get('Id').toString();
                    if (String.isNotEmpty(ProdId)) {
                        setInheritedProdIds.add(ProdId);
                        setIds.add(soId);
                    }
                }
                strSOQL = 'Select Id, Product__c From ' + objName  + ' Where Account__c = \'' + ChainAccountId + '\'  And Price_Level__c in (\'Greater_than_TM1\',\'TM1\',\'TM2\',\'TM3\',\'DM\',\'RVP\',\'Less_than_RVP\')';
                for (sObject so : database.query(strSOQL)){
                    String ProdId = so.get('Product__c').toString();
                    if (String.isNotEmpty(ProdId)){
                        if (!setInheritedProdIds.contains(ProdId)) setIds.add(so.get('Id').toString());
                    } 
                }
            }
        }
        
        //Clean up of filters on lower level to allow always all options available when upper level is selected
        territory = '';
        String strIds = '(\'' + String.join(new List<String>(setIds), '\', \'') + '\')';
        
        Map<String,String> territoryMap = new Map<String,String>();
        
        String queryStr = 'SELECT CPL_Product_Territory__r.Sales_Group_TM__c, Count(Id)';
        queryStr += ' FROM ' + objName;
        
        List<String> whereConditions = new List<String>();
        
        if (suffixAccount) {
            if(setIds.size() > 0) whereConditions.add('Id in ' + strIds);
        }
        else {
            if (String.isNotEmpty(accountId)) whereConditions.add('Account__c = \'' + accountId + '\'');
            whereConditions.add(ChartComponentController_Helper.getPriceLevelINStatement('Price_Level__c'));
        }
        
        if (whereConditions.size() > 0) 
            queryStr += ' WHERE ' + String.join(whereConditions, ' AND ');
        
        queryStr += ' GROUP BY CPL_Product_Territory__r.Sales_Group_TM__c';
        queryStr += ' ORDER BY CPL_Product_Territory__r.Sales_Group_TM__c';
        
        AggregateResult[] groupedResults = Database.query(queryStr);
        
        for(AggregateResult ar : groupedResults)
            territoryNumberSet.add((String) ar.get('Sales_Group_TM__c'));
        
        List<String> tmNumbers = new List<String>(territoryNumberSet);
        Map<String, String> territoryNames = getTerritoryNameMap(tmNumbers);
        
        system.debug('### loadFilterData (chart 3) queryStr: ' + queryStr);
        system.debug('### loadFilterData (chart 3) groupedResults: ' + groupedResults);
        system.debug('### loadFilterData (chart 3) territoryNames: ' + territoryNames);
        
        List<String> categoryList = new List<String>{
            RESIDENTIAL_BROADLOOM,
                COMMERCIAL_BROADLOOM,
                CARPET_TILE,
                CUSHION,
                SOLIDWOOD,
                TECWOOD,
                REVWOOD,
                TILE,
                RESILIENT_SHEET,
                RESILIENT_TILE,
                INSTALLATION_ACCESSORIES,
                CARE_AND_MAINTENANCE};
                    
                    for (AggregateResult row : groupedResults) { 
                        String territoryVal = string.valueof(row.get('Sales_Group_TM__c'));
                        
                        
                        String tName = territoryNames.get(territoryVal);
                        
                        territoryMap.put(territoryVal, tName);
                    }
        
        filterWrapper.territoryFilters = territoryMap;
        filterWrapper.categoryFilters = categoryList;
        
        
        system.debug('### filterWrapper: ' + filterWrapper);
        return filterWrapper;
    }        
    
    
    @AuraEnabled
    public static FilterWrapper loadFilterData(String region, String district, String territory, String objName, String accountId, String styleNum, String timePeriod, String chartId, String category) {
        
        System.debug('### loadFilterData - region: ' + region);
        System.debug('### loadFilterData - district: ' + district);
        System.debug('### loadFilterData - territory: ' + territory);
        
        
        FilterWrapper chartFilters = new FilterWrapper();
        
        if (String.isEmpty(category)) category = 'Residential Broadloom';
        objName = ChartComponentController_Helper.getObjNameFromCategory(category);
        
        if (chartId == '1') {
            chartFilters =   loadFilterData_chart1Wrapper ( region,  district,  territory,  objName,  styleNum,  timePeriod,  chartId);
            System.Debug('### filter data Chart 1: ' + chartFilters);
        }
        if (chartId == '2') {
            chartFilters =   loadFilterData_chart2Wrapper ( region,  district,  territory,  objName,  styleNum,  timePeriod,  chartId);
            System.Debug('### filter data Chart 2: ' + chartFilters);
        }
        if (chartId == '3') {
            chartFilters =  loadFilterData_chart3Wrapper( territory,  objName,  accountId,  timePeriod,  chartId);
            System.Debug('### filter data Chart 3: ' + chartFilters);
        }
        if (chartId == '4') {
            chartFilters =  loadFilterData_chart3Wrapper( territory,  objName,  accountId,  timePeriod,  chartId);
            System.Debug('### filter data Chart 4: ' + chartFilters);
        }
        
        return chartFilters;
    }
    
    //**********************************
    //Wrapper Classes
    //**********************************
    
    public class FilterWrapper {
        
        @AuraEnabled
        public List<String> regionFilters {get; set;}
        
        @AuraEnabled
        public List<String> ditrictFilters {get; set;}
        
        @AuraEnabled
        public Map<String, String> territoryFilters {get; set;}
        
        @AuraEnabled
        public List<String> timePeriodFilters {get; set;}
        
        @AuraEnabled
        public List<String> categoryFilters {get; set;}
        
        public FilterWrapper() {
            regionFilters = new List<String>();
            ditrictFilters = new List<String>();
            territoryFilters = new Map<String, String>();
            timePeriodFilters = new List<String>();
            categoryFilters = new List<String>();
            
        }
    }    
    
    public class ChartWrapper{
        @AuraEnabled
        public Double maxValue {get; set;}
        
        @AuraEnabled
        public String styleNumFullName {get; set;}
        
        @AuraEnabled
        public list<string> dataLabels{get;set;}
        
        @AuraEnabled
        public list<string> dataValues{get;set;}
        
        @AuraEnabled
        public list<string> colorValues{get;set;}
        public ChartWrapper(){
            styleNumFullName = '';
            dataLabels =new list<string> ();
            dataValues = new list<string> ();
            colorValues = new list<string> ();
        }
    }    
    
    
}