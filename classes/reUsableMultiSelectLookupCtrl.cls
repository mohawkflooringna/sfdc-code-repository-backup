public class reUsableMultiSelectLookupCtrl {
	@AuraEnabled
    public static List < Contact > fetchLookUpValues(String searchKeyWord, String ObjectName,
                                                     List<sObject> ExcludeitemsList, String filterField, String filterVal) {
        String searchKey = '%' + searchKeyWord + '%';
        List < Contact > returnList = new List < Contact > ();
 
        List<string> lstExcludeitems = new List<string>();
        for(sObject item : ExcludeitemsList ){
            lstExcludeitems.add(item.id);
        }
        String sQuery;
        if(filterField != null && filterField != '' && 
           filterField.equalsIgnoreCase('AccountId'))
        {
            squery = 'select id, Name from ' +ObjectName + 
                ' where Name LIKE: searchKey AND Id NOT IN : lstExcludeitems' + ' AND '+filterField+' = \''+ filterVal+
            '\' ';
        }
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5 and exclude already selected records  
        //String sQuery =  'select id, Name from ' +ObjectName + ' where Name LIKE: searchKey AND Id NOT IN : lstExcludeitems order by createdDate DESC limit 5';
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add((Contact)obj);
        }
        return returnList;
    }
}