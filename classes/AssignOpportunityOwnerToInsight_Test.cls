@isTest
public class AssignOpportunityOwnerToInsight_Test {

    @isTest
    private static void AssignOpportunityOwnerToInsightTest1()
    {	
        List<Id> projectIdsList = new List<Id>();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
		
        /*user u = new user();
        u.Email = 'test@mhktest.com';
        u.Username = 'test@mhkUserName.com';
        u.LastName = 'testLastNameUser';
        u.Alias = 'testAls';
        u.CommunityNickname = 'NickMHK';
        //u.TimeZoneSidKey = 'America/New_York';User__c
        u.TimeZoneSidKey = 'Europe/London';
		u.LocaleSidKey = 'en_GB';
		u.LanguageLocaleKey = 'en_US';
		u.EmailEncodingKey = 'ISO-8859-1';
        u.ProfileId = p.Id;
        Insert u;
        system.assertEquals('test@mhktest.com', u.Email);*/
        List<User> userList = Utility_Test.createTestUsers(true, 1, 'System Administrator');
        User u = userList[0];
        System.runAs(u) {
            rdcc__ReedProject__c prj1 = new rdcc__ReedProject__c();
			prj1.name ='testProj1';
			prj1.rdcc__Project_ID__c = '12345';
			insert prj1;
			system.assertEquals('testProj1', prj1.name);
			rdcc__Search_Name__c crmsearchname = new rdcc__Search_Name__c();
            crmsearchname.rdcc__User__c = u.id;
            crmsearchname.name  = 'Test';
            insert crmsearchname;
            system.assertnotEquals(crmsearchname.ID, null);
            
			rdcc__Search_Project_Bridge__c crmPrjDtl = new rdcc__Search_Project_Bridge__c();
			crmPrjDtl.rdcc__Project__c = prj1.ID;
            crmPrjDtl.rdcc__Search_Name__c = crmsearchname.ID;
			insert crmPrjDtl;
			//system.assertEquals(u.ID, crmPrjDtl.CreatedById);
			system.assertnotEquals(crmPrjDtl.ID, null);
			
			rdcc__ReedProject__c prj11 = new rdcc__ReedProject__c();
			prj11.name ='testProj11';
			prj11.rdcc__Project_ID__c = '22345';
			insert prj11;
			system.assertEquals('testProj11', prj11.name);
			
			projectIdsList.add(prj1.ID);
			projectIdsList.add(prj11.ID);
			
			AssignOpportunityOwnerToInsight.opportunityOwnerToInsight(projectIdsList);
        }
        
    }
}