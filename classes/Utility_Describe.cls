//
// Utility class to enhance and optimize use of the Salesforce Describe object
//
// January 9, 2016     George Acker     original

public without sharing class Utility_Describe{
    // property to get globalDescribe once and persist
    public static Map<String,Schema.SObjectType> globalDescribe{
        get{
            if (globalDescribe == null){
                globalDescribe = Schema.getGlobalDescribe();
            }
            return globalDescribe;
        }
        set;
    }
    
    // pass in an expression like Price_Grid__c.Product__r.name and get back the Schema.sObjectField representing Product Name
    public static Schema.sObjectField getSObjectFieldType(String fldExpression){
        //system.debug('fldExpression::' + fldExpression);
        String[] pieces = fldExpression.split('\\.');
        Schema.sObjectType objType = globalDescribe.get(pieces[0]);
        if (objType == null){
            return null;
        }
        // If Field is an ID Field or Relatioship, handle here
        for (integer i=1; i<pieces.size()-1; i++){
            String fldExp = getIdFieldNameFromRelationship(pieces[i]);
            Schema.sObjectField fldType = objType.getDescribe().fields.getMap().get(fldExp);
            //system.debug(fldType);
            if (fldType == null){
                return null;
            }
            objType = fldType.getDescribe().getReferenceTo()[0];
        }
        Schema.sObjectField fldType = objType.getDescribe().fields.getMap().get(pieces[pieces.size()-1]);
        //system.debug(fldType);
        if (fldType == null){
            return null;
        }
        return fldType;
    }
    
    public static Boolean isFieldAccessible(String fldExpression){
        return getSObjectFieldType(fldExpression).getDescribe().isAccessible();
    }
    
    // method that takes a field relationship name and returns the API name of the ID field
    public static String getIdFieldNameFromRelationship(String fld)
    {
        if (fld.right(3) == '__r')
            return fld.replace('__r','__c');
        else
            return fld + 'Id';
    }
}