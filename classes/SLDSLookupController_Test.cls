@isTest public class SLDSLookupController_Test {

    @isTest public static void testFunction()
    { 
        
        Test.startTest();
        /*Opportunity opp=new Opportunity();
        opp.Name='TestData';
        opp.StageName='CloseDate';
        opp.CloseDate=Date.today();
        insert opp;*/
        account acc = new account();
        acc.Name = 'testAcc';
        insert acc;
        system.assertEquals('testAcc', acc.Name);
        
        Opportunity opp = new Opportunity();
        opp.Name = 'testOpp2';
        opp.CloseDate = Date.newInstance(2018, 17, 9);
        opp.CurrencyIsoCode = 'USD';
        opp.Location__c = 'USA';
        opp.StageName='Bidding';
        opp.Status__c = 'In Process';
        opp.AccountId = acc.Id;
        opp.Market_Segment__c = 'Government';
        insert opp;
        system.assertEquals('testOpp2', opp.Name);
        
        SLDSLookupController slds=new SLDSLookupController();
        slds.objectName='Opportunity';
        slds.objectLabelPlural='Name';
        slds.label = 'Name';
        slds.uniqueComponentId='Name';
      
     
        System.assertNotEquals(0,SLDSLookupController.search('Opportunity', 'Name', 'Name', 'field->','Test').size() );
        
        Account a=new Account();
        a.Name='TestAccount';
        insert a;
        
        SLDSLookupController slds1=new SLDSLookupController();
        slds1.objectName='Account';
        slds1.objectLabelPlural='Name';
        slds1.label = 'Name';
        slds1.uniqueComponentId='Name';
        
       
        System.assertNotEquals(0, SLDSLookupController.search('Account', 'Name', 'Name', 'url->url','Test').size() );
        Test.stopTest();
    }
}