public class genricLookupController {
    
    @AuraEnabled 
    public static String searchDB(String objectName, String fld_API_Text, String fld_API_Val,Integer lim,
                                  String fld_API_Search,String searchText,string filterField,String filterVal ){
        searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
        String query='';
                                      
        if(filterField != null && filterField != '' && 
           (filterField.equalsIgnoreCase('Role_C__c') || filterField.equalsIgnoreCase('AccountId')))
        {
            query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val+ ', '+filterField +
            ' FROM '+objectName+
            ' WHERE '+fld_API_Search+' LIKE '+searchText+ ' AND '+filterField+' = \''+ filterVal+
            '\' LIMIT '+lim;
        }
                                      
        else 
        {
        	query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val+
            ( filterField != null && filterField != '' ? ', '+filterField : '' )+
            ' FROM '+objectName+
            ' WHERE '+fld_API_Search+' LIKE '+searchText+ 
            ' LIMIT '+lim;
        }
        system.debug(':::query::::' +query );
        List<sObject> sobjList = Database.query(query);
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
        
        for(SObject s : sobjList){
            ResultWrapper obj = new ResultWrapper();
            obj.objName = objectName;
            obj.text = String.valueOf(s.get(fld_API_Text)) ;
            obj.val = String.valueOf(s.get(fld_API_Val))  ;
            if( filterField != null && filterField != '' ){
                obj.filterVal = String.valueOf(s.get(filterField));
            }
            lstRet.add(obj);
        } 
        return JSON.serialize(lstRet) ;
    }
    
    @AuraEnabled 
    public static String searchDBMultipleFields(String objectName, String fields, String whereClause, Integer lim ){
		String query='';
        if(objectName != null && fields != null && fields != '' && whereClause != null && whereClause != '')
        {
            query = 'SELECT '+fields+' FROM '+objectName+' WHERE '+whereClause+' LIMIT '+lim;
        }
                                      
        system.debug(':::query::::' +query );
        List<sObject> sobjList = new List<sObject>();
        try{
            sobjList = Database.query(query);
        }catch(Exception ex){
            System.debug('Error: ' + ex.getMessage());
        }
        return JSON.serialize(sobjList) ;
    }
    
    public class ResultWrapper{
        public String objName {get;set;}
        public String text{get;set;}
        public String val{get;set;}
        public string filterVal{get;set;}
    }
    
}