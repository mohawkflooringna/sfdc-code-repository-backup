/**************************************************************************

Name : ContactTriggerHandler_Test
===========================================================================
Purpose : Test class for ContactTriggerHandler
===========================================================================
History:
--------
VERSION    AUTHOR         DATE             DETAIL          DESCRIPTION
1.0        Susmitha      07/nov/2017       Create  
***************************************************************************/
@isTest
public class ContactTriggerHandler_Test {
    
    static testMethod void  testMethod1(){
        System.runAs(Utility_Test.ADMIN_USER){
            Utility_Test.getDefaultConfCusSetting();
            List<Account> accList = Utility_Test.createAccountsForNonInvoicing(true, 1);
            List<Contact> contList = Utility_Test.createContacts(true, 1, accList);
            List<Contact> contList1 = Utility_Test.createContacts(true, 1);
            update contList1;
            system.assert(contList1.size()>0);
            ContactTriggerHandler.updateContact(contList,contList,FALSE,TRUE);
            
            //contList[0].Delete_Contact__c = true;
            //update contList;
        }
    }
    static testMethod void  test_checkOpenProject(){
        List<Contact> contList1 = new List<Contact>();
        List<Opportunity> oppList1 = new List<Opportunity>();
        
        Id  commRTId = UtilityCls.getRecordTypeInfo('Contact', 'Commercial');
        User commUser = [SELECT Id,Email, ProfileId FROM User WHERE Profile.Name = 'Commercial Sales User' AND IsActive=true LIMIT 1 ];
        System.runAs(commUser){
            //Utility_Test.getDefaultConfCusSetting();
            List<Account> accList = Utility_Test.createAccountsForNonInvoicing(true, 1);
            List<Opportunity> oppList = Utility_Test.createOpportunities(false, 1, accList);
            List<Contact> contList = Utility_Test.createContacts(false, 1, accList);
            for(Contact c: contList){
                c.RecordTypeid =commRTId;
                contList1.add(c);
            }
            insert contList1;
            for(Opportunity opp: oppList){
                opp.Status__c = 'In Process';
                opp.Contact__c = contList1[0].id;
                oppList1.add(opp);
            }
            insert oppList1;
            try{
                update contList1;
            }catch(Exception e){
                String errMsg = e.getMessage();
            }
        }   
    }
}