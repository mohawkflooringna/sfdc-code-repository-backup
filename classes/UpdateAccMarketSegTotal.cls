global class UpdateAccMarketSegTotal implements Database.Batchable<sObject> 
{
    global final String Query;
    //global final String recorId;
    global final String value;
    global UpdateAccMarketSegTotal(String q)
    {
        Query=q;
        //recorId = v;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        
        List<String> stageNames = new List<String>();
        stageNames.add('Closed Lost');
        stageNames.add('Closed Won');
        
        List<Account> UpdateAccList = new List<Account>();
        Map<String,Account> AccMap = new Map<String,Account>();
        System.debug('scope :'+scope);
        for(Sobject s : scope)
        {
            AccMap.put(s.id, (Account)s);
        }
        
        List<Related_Account__c> relAccList = [Select id,Account__c,Opportunity__c, Opportunity__r.Applied_Revenue__c,Opportunity__r.Market_Segment_Group__c 
                                               from Related_Account__c 
                                               where Account__c IN: AccMap.keySet() 
                                               and Opportunity__r.StageName Not In :stageNames];
        
        System.debug('relAccList :'+relAccList);
        System.debug('rel Size: '+relAccList.size());
        Map<String, Related_Account__c> relAccKey = new Map<String, Related_Account__c>();
        for(Related_Account__c ra: relAccList){
            String oppAcc = ra.Opportunity__c + '_' + ra.Account__c;
            if(!relAccKey.containsKey(oppAcc)){
                relAccKey.put(oppAcc, ra);
            }
        }
        
        
        Map<Id , List<Decimal> > accOppMap = new Map<Id , List<Decimal> >();
        
        For(Related_Account__c rel : relAccKey.values())
        {
            
            List<Decimal> totalList;
            
            if( accOppMap.containsKey(rel.Account__c) )
            {
                totalList = accOppMap.get(rel.Account__c);    
            }else
                totalList = New Decimal[]{0,0,0};
                    
                    if(rel.Opportunity__r.Market_Segment_Group__c == 'Education/Govt')
                {
                    totalList[0] += rel.Opportunity__r.Applied_Revenue__c;
                    
                }else if(rel.Opportunity__r.Market_Segment_Group__c == 'Healthcare/Sr Living')
                {
                    totalList[1] += rel.Opportunity__r.Applied_Revenue__c;
                    
                }else if(rel.Opportunity__r.Market_Segment_Group__c == 'Workplace/Retail')
                {
                    totalList[2] += rel.Opportunity__r.Applied_Revenue__c;
                }
            
            
            
            System.debug('totalList : '+totalList);
            accOppMap.put(rel.Account__c,totalList );
            
        }
        
        For(String aId: accOppMap.keySet())
        {
            Account upAcc = AccMap.get(aId);
            List<Decimal> totalList = accOppMap.get(aId);
            upAcc.Ed_Pipeline__c = totalList[0];
            upAcc.Hc_Pipeline__c = totalList[1];
            upAcc.Wp_Pipeline__c = totalList[2];
            if(upAcc.Business_Type__c == null)
            	upAcc.Business_Type__c = 'Commercial';
            
                UpdateAccList.add(upAcc);
        }
        System.debug('UpdateAccList : '+UpdateAccList);
        update UpdateAccList;
        
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
    
}