/**************************************************************************

Name : InsertClaimApprovers_Rest_TEst
===========================================================================
Purpose : Test class for InsertClaimApprovers_Rest rest service class
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha    16/Nov/2017       Create  
***************************************************************************/

@isTest
private class InsertClaimApprovers_Rest_Test{
    
    
    static testMethod void testMethod1(){
        
        Account a = new Account(Name='Test Account', Global_Account_Number__c='1234567');
        insert a;
        
        User ur = new User(
            Alias        = 'newUser',
            Email        = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random()),
            EmailEncodingKey  = 'UTF-8',
            LastName      = 'Testing',
            LanguageLocaleKey  = 'en_US',
            LocaleSidKey    = 'en_US',
            CAMS_Employee_Number__c='9876543',
            Business_Type__c='Commercial',
            ProfileId       = [SELECT Id FROM Profile WHERE Name='Standard User'].Id,
            
            Geography__c   ='Arizona',
            TimeZoneSidKey    = 'America/Los_Angeles',
            UserName      = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random())
        );
        insert ur;
        
        system.assert(ur!=null);
        
        InsertClaimApprovers_Rest.SalesUsers su = new InsertClaimApprovers_Rest.SalesUsers();
        system.assert(su!=null);
        
        su.emailID='debasmitha@gmail.com';
        su.employeeID=ur.CAMS_Employee_Number__c;
        
        InsertClaimApprovers_Rest.SalesUsers su1 = new InsertClaimApprovers_Rest.SalesUsers();
        su1.emailID='wrongEmail.com';
        su1.employeeID=ur.CAMS_Employee_Number__c;
        list<InsertClaimApprovers_Rest.Claims> NullclaimsList= new list<InsertClaimApprovers_Rest.Claims>();
        
        list<InsertClaimApprovers_Rest.Claims> claimsList= new list<InsertClaimApprovers_Rest.Claims>();
        InsertClaimApprovers_Rest.Claims clms=new InsertClaimApprovers_Rest.Claims();
        clms.shipToGlobalID=a.Global_Account_Number__c;
        clms.claimNumber='Test JSON1';
        clms.claimTypeCode='PRICING';
        clms.claimSubmitDate='02/02/2017';
        clms.claimAmount='100';
        clms.claimStatus='RESOLVED';
        clms.Company='c';
        clms.salesUsers=new list<InsertClaimApprovers_Rest.SalesUsers>();
        clms.salesUsers.add(su);
        clms.salesUsers.add(su1);
        claimsList.add(clms);
        
        String json=' {"claims": [{"shipToGlobalID" : "0000193180",  "claimNumber" : "3.8", "claimTypeCode" : "FREIGHT","claimSubmitDate" : "02/02/2017","claimAmount" : "100","claimStatus" : "RESOLVED","Company"  : "c","salesUsers": [{"emailID": "test6@gmail.com","employeeID": "23604" }]}]}';
        
        String response=InsertClaimApprovers_Rest.postClaimApprover(claimsList);
        response=InsertClaimApprovers_Rest.postClaimApprover(claimsList);  
        
        response=InsertClaimApprovers_Rest.postClaimApprover(NullclaimsList);                                       
        
        
    }
    
    
    static testMethod void testMethod2(){
        
        Account a = new Account(Name='Test Account', Global_Account_Number__c='1234567');
        insert a;
        system.assert(a!=null);
        
        User ur = new User(
            Alias        = 'newUser',
            Email        = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random()),
            EmailEncodingKey  = 'UTF-8',
            LastName      = 'Testing',
            LanguageLocaleKey  = 'en_US',
            LocaleSidKey    = 'en_US',
            CAMS_Employee_Number__c='9876543',
            Business_Type__c='Commercial',
            ProfileId       = [SELECT Id FROM Profile WHERE Name='Standard User'].Id,
            
            Geography__c   ='Arizona',
            TimeZoneSidKey    = 'America/Los_Angeles',
            UserName      = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random())
        );
        insert ur;
        
        system.assert(ur!=null);
        
        
        
        
        InsertClaimApprovers_Rest.SalesUsers su = new InsertClaimApprovers_Rest.SalesUsers();
        su.emailID='debasmitha@gmail.com';
        su.employeeID=ur.CAMS_Employee_Number__c;
        
        
        
        list<InsertClaimApprovers_Rest.Claims> claimsList= new list<InsertClaimApprovers_Rest.Claims>();
        InsertClaimApprovers_Rest.Claims clms=new InsertClaimApprovers_Rest.Claims();
        clms.shipToGlobalID=a.Global_Account_Number__c;
        clms.claimNumber='Test JSON1';
        clms.claimTypeCode='WrongTypeCode';
        clms.claimSubmitDate='02/02/2017';
        clms.claimAmount='100';
        clms.claimStatus='RESOLVED';
        clms.Company='c';
        clms.salesUsers=new list<InsertClaimApprovers_Rest.SalesUsers>();
        clms.salesUsers.add(su);
        
        claimsList.add(clms);
        system.assert(claimsLIST!=null);
        
        String response=InsertClaimApprovers_Rest.postClaimApprover(claimsList);           
        
        
        
        InsertClaimApprovers_Rest.Claims newClm= new InsertClaimApprovers_Rest.Claims();
        newClm=clms;
        newClm.salesUsers.add(su);                      
        claimsList.add(newClm);
        response=InsertClaimApprovers_Rest.postClaimApprover(claimsList);  
        
        
    }
    
    static testMethod void testMethod3(){
        
        Account a = new Account(Name='Test Account', Global_Account_Number__c='1234567');
        insert a;
        
        User ur = new User(
            Alias        = 'newUser',
            Email        = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random()),
            EmailEncodingKey  = 'UTF-8',
            LastName      = 'Testing',
            LanguageLocaleKey  = 'en_US',
            LocaleSidKey    = 'en_US',
            CAMS_Employee_Number__c='9876543',
            Business_Type__c='Commercial',
            ProfileId       = [SELECT Id FROM Profile WHERE Name='Standard User'].Id,
            
            Geography__c   ='Arizona',
            TimeZoneSidKey    = 'America/Los_Angeles',
            UserName      = 'AccountShareActionsTest@'+String.valueOf(Math.random())+'.'+String.valueOf(Math.random())
        );
        insert ur;
        
        system.assert(ur!=null);
        
        
        InsertClaimApprovers_Rest.SalesUsers su = new InsertClaimApprovers_Rest.SalesUsers();
        su.emailID='debasmitha@gmail.com';
        su.employeeID=ur.CAMS_Employee_Number__c;
        
        InsertClaimApprovers_Rest.SalesUsers su1 = new InsertClaimApprovers_Rest.SalesUsers();
        su1.emailID='wrongEmail.com';
        su1.employeeID=ur.CAMS_Employee_Number__c;
        list<InsertClaimApprovers_Rest.Claims> NullclaimsList= new list<InsertClaimApprovers_Rest.Claims>();
        
        list<InsertClaimApprovers_Rest.Claims> claimsList= new list<InsertClaimApprovers_Rest.Claims>();
        InsertClaimApprovers_Rest.Claims clms=new InsertClaimApprovers_Rest.Claims();
        clms.shipToGlobalID=a.Global_Account_Number__c;
        clms.claimNumber='Test JSON1';
        clms.claimTypeCode='PRICING';
        clms.claimSubmitDate='02/02/2017';
        clms.claimAmount='100';
        clms.claimStatus='RESOLVED';
        clms.Company='r';
        clms.salesUsers=new list<InsertClaimApprovers_Rest.SalesUsers>();
        clms.salesUsers.add(su);
        clms.salesUsers.add(su1);
        claimsList.add(clms);
        system.assert(claimsList!=null);
        
        
        String response=InsertClaimApprovers_Rest.postClaimApprover(claimsList);
        response=InsertClaimApprovers_Rest.postClaimApprover(claimsList);  
        
        response=InsertClaimApprovers_Rest.postClaimApprover(NullclaimsList);                                       
        
        
    }
    
}