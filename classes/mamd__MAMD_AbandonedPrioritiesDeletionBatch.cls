/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MAMD_AbandonedPrioritiesDeletionBatch implements Database.Batchable<SObject> {
    global MAMD_AbandonedPrioritiesDeletionBatch() {

    }
    global void execute(Database.BatchableContext bc, List<mamd__Multiday_Object_Priority__c> scope) {

    }
    global void finish(Database.BatchableContext bc) {

    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return null;
    }
}
