/**************************************************************************

Name : TerritoryUserTriggerHandler_Test

===========================================================================
Purpose : This tess class is used for TerritoryUserTriggerHandler and TerritoryUserTrigger  
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Susmitha      6/Nov/2017     Created             
***************************************************************************/
@isTest
private class TerritoryUserTriggerHandler_Test {
    
    public static Id residentialRT=Schema.SObjectType.Territory__c.getRecordTypeInfosByName().get('Residential').getRecordTypeId(); 
    public static Id terrUsrResRTId = Schema.SObjectType.Territory_User__c.getRecordTypeInfosByName().get('Residential').getRecordTypeId();
    public static List<Territory_User__c> terrUsrList = new List<Territory_User__c>();
    public static List<Territory__c> terrList = Utility_Test.createTerritories(false, 2);
    public static List<User> userList = new List<User>();
    
    @isTest
    static void testMethod1() {
        
        Default_Configuration__c dc = Utility_Test.getDefaultConfCusSetting();
        userList = Utility_Test.createTestUsers(false, 2, 'Residential Sales User');
        userList[0].Business_Type__c = 'Residential';
        insert userList;

        terrList[1].RecordTypeId = terrList[0].RecordTypeId = residentialRT;
        terrList[0].Name = '030';
        terrList[1].Name = '031';
        insert terrList;

        Territory_User__c terrUser = new Territory_User__c();
        terrUser.name = 'TM-001'; 
        terrUser.Territory__c = terrList[0].id;
        terrUser.user__c = userList[0].id;
        terrUser.Role__c = 'Territory Manager';
        terrUser.RecordTypeId = terrUsrResRTId;
        terrUser.External_ID__c = '254_251_030_Territory_Manager_TM-001';
        terrUsrList.add(terrUser);
        insert terrUsrList;
        
        terrUsrList.clear();
        terrUser = new Territory_User__c();
        terrUser.name = 'TM-001'; 
        terrUser.Territory__c = terrList[1].id;
        terrUser.user__c = userList[0].id;
        terrUser.Role__c = 'Territory Manager';
        terrUser.RecordTypeId = terrUsrResRTId;
        terrUser.External_ID__c = '254_251_031_Territory_Manager_TM-001';
        terrUsrList.add(terrUser);
        insert terrUsrList;
        
        try{
            Id tUserId = [SELECT Id from Territory_User__c Where Territory__c =: terrList[0].Id].Id;
            System.assertEquals(null, tUserId);
        }
        catch(Exception ex){
            
        }
        
    }
}