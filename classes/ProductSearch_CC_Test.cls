/**************************************************************************

Name : ProductSearch_CC_Test

===========================================================================
Purpose : This tess class is used for ProductSearch_CC
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Sasi Naik     2/May/2017     Created         CSR:    
1.1        Susmitha      6/Nov/2017     Modified
***************************************************************************/
@isTest
private class ProductSearch_CC_Test {
    
    static List<Opportunity> oppList;
    static List<Account> resAccListForInvoicing; 
    static List<Product2> prod2List;
    static List<User> UserList = new List<User>();
    static User commUser = new User();
    static User resiUser = new User();
    static Id priceBookId;
    
    public static void init(){
        Utility_Test.getTeamCreationCusSetting();
        commUser = Utility_Test.getTestUser('Commercial.User', 'Commercial Sales User');
        commUser.Business_Type__c = 'Commercial';
        UserList.add(commUser);
        resiUser = Utility_Test.getTestUser('Residential.User', 'Residential Sales User');
        resiUser.Business_Type__c = 'Residential';
        UserList.add(resiUser);
        insert UserList;
        system.assert(UserList.size()>0);
        PriceBook2 pb = new PriceBook2();
        pb.Name = 'Standard Price Book';
        pb.CurrencyIsoCode = 'USD';
        insert pb;
        priceBookId = pb.Id;
    }
    
    
    
    // Utility method that can be called by Apex tests to create price book entries.
    static testMethod void addPricebookEntriesResidential() {
        init();
        System.runAs(resiUser) {
            test.startTest();
            
            resAccListForInvoicing = Utility_Test.createAccountsForNonInvoicing(true, 1);
            for(Integer i=0;i<resAccListForInvoicing.size();i++){
                resAccListForInvoicing[i].Business_Type__c = UtilityCls.RESIDENTIAL;    
            }
            update resAccListForInvoicing;
            oppList = Utility_Test.createOpportunities(true, 1, resAccListForInvoicing);
            
            String SPricebook = Test.getStandardPricebookId();
            
            Prod2List = Utility_Test.createProduct2(false, 1);
            
            for(Integer i=0;i<Prod2List.size();i++){
                Prod2List[i].RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get(UtilityCls.RESIDENTIALPRODUCT).getRecordTypeId();
                Prod2List[i].Sub_Category__c = 'Woven';
            }
            insert Prod2List;
            
            string SelectedProducts = '[{"pr":{"Id":"'+Prod2List[0].Id+'","Quantity":"'+1+'","Price":"'+2+'","isSelected":"'+true+'"}}]';
            string jsonString='[{"isSelected":true,"pr":{"Id":"'+Prod2List[0].Id+'"},"price":9,"quantity":4}]';
            ProductSearch_CC   ps = new ProductSearch_CC();
            
            ProductSearch_CC.getProductCollectionNamePicklistValues();
            ProductSearch_CC.getProductCollectionBrandPicklistValues();
            ProductSearch_CC.getProductProductTypePicklistValues();
            ProductSearch_CC.getSubCategoryPickListvalues();
            ProductSearch_CC.getLoggedinuserBusinessTypeInfo();
            ProductSearch_CC.getproductlist('Test', '123','Mohawk Group', true,'123','Type 1', false,'hhhh', 'Woven');
            Boolean susscessvalue = ProductSearch_CC.AddProductstoProject(oppList[0].Id,jsonString);
           
         ProductSearch_CC.wrapproduct wp=new ProductSearch_CC.wrapproduct(Prod2List[0],true);
            system.assert(wp!=null);
            test.stopTest();
        }
    } 
     static testMethod void addPricebookEntriesComm() {
        init();
        System.runAs(commUser) {
            test.startTest();
            
            resAccListForInvoicing = Utility_Test.createAccountsForNonInvoicing(true, 1);
            for(Integer i=0;i<resAccListForInvoicing.size();i++){
                resAccListForInvoicing[i].Business_Type__c = UtilityCls.COMMERCIAL;    
            }
            update resAccListForInvoicing;
            oppList = Utility_Test.createOpportunities(false, 1, resAccListForInvoicing);
            oppList[0].RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Commercial Traditional').getRecordTypeId();
            oppList[0].PriceBook2Id = priceBookId;
            oppList[0].CurrencyIsoCode = 'USD';
            oppList[0].StageName = 'Discovery';
            insert oppList;
            String SPricebook = Test.getStandardPricebookId();

            Prod2List = Utility_Test.createProduct2(false, 2);
            
            for(Integer i=0;i<Prod2List.size();i++){
                Prod2List[i].RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get(UtilityCls.COMMERCIALPRODUCT).getRecordTypeId();
                Prod2List[i].Sub_Category__c = 'Woven';
                Prod2List[i].Name = 'Test';
                Prod2List[i].Product_Style_Number__c = '123';
                Prod2List[i].Collection_Name__c = 'Class Act';
                Prod2List[i].Promotion__c = 'Turn Up the Volume';
                Prod2List[i].Face_Weigh__c = 12.50;
                Prod2List[i].Drop_Date__c = System.today();
                 Prod2List[i].IsActive =true;
            }
            
            Prod2List[1].Generic_Product__c = true;
            Prod2List[1].Backing_Description__c = 'none';
            Prod2List[1].QuickShip_Product__c = true;
            Prod2List[1].Custom_Product__c = true;
            Prod2List[1].Mid_Market_USD__c = Prod2List[1].Mid_Market_CAN__c = 5.00;
            insert Prod2List;
            //Below code covers the code when Opp don't have OLI
            List<Product2> prodList = Utility_Test.createProduct2(false, 2);
            for(Integer i=0;i<prodList.size();i++){
                prodList[i].RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get(UtilityCls.COMMERCIALPRODUCT).getRecordTypeId();
                prodList[i].Sub_Category__c = 'Woven';
                prodList[i].Name = 'Test';
                prodList[i].Product_Style_Number__c = '123';
                prodList[i].Collection_Name__c = 'Class Act';
                prodList[i].Promotion__c = 'Turn Up the Volume';
                prodList[i].Face_Weigh__c = 12.50;
                prodList[i].Drop_Date__c = System.today();
                 prodList[i].IsActive =true;
                 prodList[i].Generic_Product__c = true;
                prodList[i].Backing_Description__c = 'none';
                prodList[i].QuickShip_Product__c = true;
                prodList[i].Custom_Product__c = true;
                prodList[i].Mid_Market_USD__c = Prod2List[1].Mid_Market_CAN__c = 5.00;
            
            }
            insert prodList;
            
            List<PricebookEntry> pbid=Utility_Test.createPricebookEntry(true, 2,  Prod2List,priceBookId);
            OpportunityLineItem OppLineItem = new OpportunityLineItem();
            OppLineItem.opportunityid = oppList[0].id;
            OppLineItem.unitprice = 50;
            OppLineItem.quantity = 10;  
            OppLineItem.pricebookentryid = pbid[1].id;
            //OppLineItem.Product2Id = Prod2List[1].id;
            insert OppLineItem;
            system.assert(Prod2List.size()>0);
            System.debug('Product: ' + Prod2List);
            string SelectedProducts = '[{"pr":{"Id":"'+Prod2List[0].Id+'","Quantity":"'+1+'","Price":"'+2+'","isSelected":"'+true+'"}}]';
            string jsonString='[{"isSelected":true,"pr":{"Id":"'+Prod2List[0].Id+'"},"price":9,"quantity":4},{"isSelected":true,"pr":{"Id":"'+Prod2List[1].Id+'"},"price":9,"quantity":4,"oppLineItemId":"'+OppLineItem.id+'"}]';
            ProductSearch_CC   ps = new ProductSearch_CC();
            
            ProductSearch_CC.getProductCollectionNamePicklistValues();
            ProductSearch_CC.getProductCollectionBrandPicklistValues();
            ProductSearch_CC.getProductProductTypePicklistValues();
            ProductSearch_CC.getSubCategoryPickListvalues();
            ProductSearch_CC.getLoggedinuserBusinessTypeInfo();
            ProductSearch_CC.getproductlist('Test', '123','Mohawk Group', true,'123','Type 1', false,'hhhh', 'Woven');
            
            /* Start of Code - MB - 07/26/18 */
            ProductSearch_CC.ProductInfo prodInfo = ProductSearch_CC.initialize();
            prodInfo.fieldValuesMap.put('styleName', 'Te');
            prodInfo.fieldValuesMap.put('styleNumber', '12');
            prodInfo.fieldValuesMap.put('collection', 'Class Act');
            prodInfo.fieldValuesMap.put('promotion', 'Turn Up the Volume');
            prodInfo.fieldValuesMap.put('subCategory', 'Woven');
            prodInfo.fieldBooleanMap.put('genericProduct', true);
            prodInfo.fieldBooleanMap.put('customProduct', true);
            prodInfo.fieldBooleanMap.put('quickShip', true);
            prodInfo.fieldValuesMap.put('minPrice', '1');
            prodInfo.fieldValuesMap.put('maxPrice', '25');
             prodInfo.fieldValuesMap.put('backing', 'none');
            prodInfo.selectedFaceWeightList = new List<String>();
            prodInfo.selectedFaceWeightList.add('12');
            String prodInfoStr = JSON.serialize(prodInfo);
            ProductSearch_CC.getProductListNew(prodInfoStr, oppList[0].Id);
            /* End of Code - MB - 07/26/18 */
            Boolean susscessvalue = ProductSearch_CC.AddProductstoProject(oppList[0].Id,jsonString);
            //prodInfo = ProductSearch_CC.initialize();
           // prodInfo.fieldValuesMap.put('styleName', 'Te');
            prodInfoStr = JSON.serialize(prodInfo);
           ProductSearch_CC.getProductListNew(prodInfoStr, oppList[0].Id);
            
         ProductSearch_CC.wrapproduct wp=new ProductSearch_CC.wrapproduct(Prod2List[0],true);
            test.stopTest();
        }
    } 
}