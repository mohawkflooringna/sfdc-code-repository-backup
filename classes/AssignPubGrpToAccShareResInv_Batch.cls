global class AssignPubGrpToAccShareResInv_Batch implements Database.Batchable<sObject>,Schedulable{
    
    Static String resRole;
    public static String residential;
    public static Id invMATRTId;
    
    Static {       
        resRole = UtilityCls.CS_RESROLE.Role__c;
        residential  = UtilityCls.RESIDENTIAL;
        invMATRTId  = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //Need to add condition to retrieve recent modified users last 1 DAY
                        
        /*--Modified codes by Susmitha on April 2 for Notifying error--*/
        String Query;
        Batch_Query__mdt [] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'AssignPubGrpToAccShareResInv_Batch'];
        If(QureyList.size() > 0) {
            query = QureyList[0].Query_String__c ;
        } else {
            String fields = 'Id, IsDeleted, Name, RecordTypeId, Account__c, User__c, Territory__c, Territory_User_Number__c ';
            String whereCond = 'RecordTypeId = :invMATRTId AND ( LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY ) '; 
            query = 'SELECT ' + fields + ' FROM Mohawk_Account_Team__c WHERE ' + whereCond + ' LIMIT 500000';
            System.debug('Query: ' + query);
        }
        return Database.getQueryLocator(Query);
    }
    
    global static void execute(Database.BatchableContext BC, List<Mohawk_Account_Team__c> scope){
        List<Mohawk_Account_Team__c> newMATList = new List<Mohawk_Account_Team__c>();
        List<Mohawk_Account_Team__c> delMATList = new List<Mohawk_Account_Team__c>();
        
        Set<Id> delMATAccIds = new Set<Id>();
        Set<Id> newMATAccIds = new Set<Id>(); //Added By Mudit - Bug 71601 - 14/03/19
        for(Mohawk_Account_Team__c mat: scope){
            if(mat.IsDeleted){ // If TM has been removed from Mohawk Account Team
                //delMATList.add(mat);
                delMATAccIds.add(mat.Account__c);
            }else{ // If TM has been modified or added in Mohawk Account Team
                //newMATList.add(mat);
                newMATAccIds.add( mat.Account__c );//Added By Mudit - Bug 71601 -  14/03/19
            }
        }
        if(delMATAccIds != null && delMATAccIds.size()>0){
            String fieldsql = UtilityCls.getObjectFieldsForSql('Mohawk_Account_Team__c');
            delMATList = Database.query('SELECT ' + fieldsql + ' FROM Mohawk_Account_Team__c WHERE Account__c IN : delMATAccIds');
        }
        
        //Start Added By Mudit - Bug 71601 - 14/03/19
        if( newMATAccIds != null && newMATAccIds.size() > 0 ){
            String fieldsql = UtilityCls.getObjectFieldsForSql('Mohawk_Account_Team__c');
            newMATList = Database.query('SELECT ' + fieldsql + ' FROM Mohawk_Account_Team__c WHERE Account__c IN : newMATAccIds');
        }
        //End Added By Mudit - Bug 71601 - 14/03/19
        
        if(newMATList != null & newMATList.size()>0){
            ProcessBatchUpdate(newMATList);
        }
        
        if(delMATList != null & delMATList.size()>0){
            ProcessBatchUpdate(delMATList);
        }
    }
    
    global static void ProcessBatchUpdate(List<Mohawk_Account_Team__c> mhkATList){
        List<AccountShare> addAccShareList = new List<AccountShare>();
        List<Id> delAccShareIdsList = new List<Id>();
        
        Map<Id, List<String>> accNewTMMap = new Map<Id, List<String>>();
        Map<Id, List<String>> accDelTMMap = new Map<Id, List<String>>();
        Map<String, String> tmTerrMap = new Map<String, String>();
        Map<String, Id> grpNameMap = new Map<String, Id>();
        Map<Id, Set<Id>> existingAccGroupMap = new Map<Id, Set<Id>>();
        Map<Id, List<Id>> delAccShareMap = new Map<Id, List<Id>>();
        Map<Id, Set<Id>> accGroupMap = new Map<Id, Set<Id>>();

        Set<Id> accIds = new Set<Id>();
        Set<Id> delAccShareIds = new Set<Id>();
        Set<Id> delAccSharePGIds = new Set<Id>();
        Set<Id> publicGroupIds = new Set<Id>();
        Set<String> TMNumSet = new Set<String>();
        Set<String> grpNameSet = new Set<String>();
        // copied from 
        
        for(Mohawk_Account_Team__c mat: mhkATList){
            String tm = '';
            List<String> tmNumbers = new List<String>();
            accIds.add(mat.Account__c);
            if(mat.Territory_User_Number__c != null && mat.Territory_User_Number__c != ''){
                tm = mat.Territory_User_Number__c;
                if(tm.contains(';')){
                    tmNumbers = tm.split(';');
                }else{
                    tmNumbers.add(tm);
                }
                for(String tmNumber: tmNumbers){
                    String tmNum = '';
                    tmNum = tmNumber.replaceAll('\\s+', '');
                    TMNumSet.add(tmNum);
                    if(accNewTMMap.containsKey(mat.Account__c)){
                        accNewTMMap.get(mat.Account__c).add(tmNumber);
                    }else{
                        List<String> tempTM = new List<String>();
                        tempTM.add(tmNum);
                        accNewTMMap.put(mat.Account__c, tempTM);
                    }
                }
            }
        }
        System.debug('Account TM Map: ' + accNewTMMap);
        System.debug('Territory Set: ' + TMNumSet);
        if(TMNumSet.size()>0){
            for(List<Territory_User__c> terrUserList: [SELECT Id, Name, Territory__r.Name, User__c
                                                       FROM Territory_User__c
                                                       WHERE Name IN :TMNumSet
                                                       AND Role__c = 'Territory Manager']){
                                                           System.debug('Territory List: ' + terrUserList);
                                                           for(Territory_User__c terrUser: terrUserList){
                                                               tmTerrMap.put(terrUser.Name, terrUser.Territory__r.Name);
                                                               grpNameSet.add(terrUser.Territory__r.Name);
                                                           }
                                                       }
        }    
        System.debug('Territory Map: ' + tmTerrMap);
        System.debug('Group Name Set: ' + grpNameSet);
        if(accIds.size()>0){
            Default_Configuration__c defConfig = Default_Configuration__c.getOrgDefaults();
            String groups = defConfig.Groups__c;
            List<String> devNameList = groups.split(';');
            List<Id> exclGrpIds = new List<Id>();
            for(List<Group> grpList : [SELECT Id, Name, DeveloperName FROM Group WHERE DeveloperName IN : devNameList]){
                for(Group grp: grpList){
                    exclGrpIds.add(grp.Id);
                }
            }
            for(List<AccountShare> accShareList : [SELECT Id, AccountId, UserOrGroupId 
                                                   FROM AccountShare
                                                   WHERE AccountId IN :accIds
                                                   AND UserOrGroupId IN (SELECT Id FROM Group Where Id NOT IN :exclGrpIds AND Type = 'Regular')]){
                                                       for(AccountShare accShare: accShareList){
                                                           String grpValue = String.valueOf(accShare.UserOrGroupId);
                                                           if(grpValue.startsWith('00G')){
                                                               publicGroupIds.add(accShare.UserOrGroupId);
                                                               if(existingAccGroupMap.containsKey(accShare.AccountId)){
                                                                   existingAccGroupMap.get(accShare.AccountId).add(accShare.UserOrGroupId);
                                                               }else{
                                                                   Set<Id> userOrGroupIds = new Set<Id>();
                                                                   userOrGroupIds.add(accShare.UserOrGroupId);
                                                                   existingAccGroupMap.put(accShare.AccountId, userOrGroupIds);
                                                               }
                                                           }
                                                       }
                                                   }
        }
        System.debug('Existing Public Group: ' + existingAccGroupMap);
        System.debug('Group Ids: ' + publicGroupIds);
        if(grpNameSet.size()>0 || publicGroupIds.size()>0){
            System.debug('Group Name Set: ' + grpNameSet);
            for(List<Group> grpList : [SELECT Id, Name FROM Group 
                                       WHERE ( Id IN :publicGroupIds
                                              OR Name IN :grpNameSet )
                                       AND Type = 'Regular']){
                                           System.debug('Group List: ' + grpList.size());
                                           for(Group grp: grpList){
                                               grpNameMap.put(grp.Name, grp.Id);
                                               System.debug('Group: ' + grpNameMap);
                                           }
                                       }
        }
        System.debug('Groups: ' + grpNameMap);
        for(Id accId: accIds){
            List<String> tmNumbers = new List<String>();
            tmNumbers = accNewTMMap.get(accId);
            if(tmNumbers != null){
                for(String tmNum: tmNumbers){
                    tmNum = tmNum.replaceAll('\\s+', '');
                    String terrName = tmTerrMap.get(tmNum);
                    System.debug('tmNum: ' + tmNum + ', Terr. Name: ' + terrName);
                    Id grpId = grpNameMap.get(terrName); //territory name should be exactly equal to group name
                    System.debug('grpId: ' + grpId);
                    if(accGroupMap.containsKey(accId)){
                        accGroupMap.get(accId).add(grpId);
                    }else{
                        Set<Id> groupIdList = new Set<Id>();
                        groupIdList.add(grpId);
                        accGroupMap.put(accId, groupIdList);
                    }
                }
            }
        }
        System.debug('MAT Group: ' + accGroupMap);
        System.debug('existingAccGroupMap: ' + existingAccGroupMap);
        for(Id accId: accIds){
            Set<Id> existingAccShareGrpIds = new Set<Id>();
            Set<Id> MHKAccTeamShareGrpIds = new Set<Id>();
            Set<Id> delPGSet = new Set<Id>();   // copied
            Set<Id> addPGSet = new Set<Id>();   // copied
            Boolean PGExist = false;
            Boolean removePG = true;

            existingAccShareGrpIds = existingAccGroupMap.get(accId);
            MHKAccTeamShareGrpIds = accGroupMap.get(accId);
            
            if(existingAccShareGrpIds != null && existingAccShareGrpIds.size()>0){
                for(Id existAccShareId: existingAccShareGrpIds){
                    if(MHKAccTeamShareGrpIds != null && MHKAccTeamShareGrpIds.size()>0){
                        for(Id mhkAccShareId: MHKAccTeamShareGrpIds){
                            if(existAccShareId == mhkAccShareId){
                                PGExist = true;
                                break;
                            }else{
                                PGExist = false;
                            }
                        }
                        if(!PGExist){
                            delPGSet.add(existAccShareId); // Remove PG if it exists in Account Share and not in MHK Account Team Share
                        }
                    }else{
                        delPGSet.add(existAccShareId); // Remove PG if it exists in Account Share and not in MHK Account Team Share
                    }
                }
            }
            
            PGExist = false;
            if(MHKAccTeamShareGrpIds != null && MHKAccTeamShareGrpIds.size()>0){
                for(Id mhkAccShareId: MHKAccTeamShareGrpIds){
                    if(existingAccShareGrpIds != null && existingAccShareGrpIds.size()>0){
                        for(Id existAccShareId: existingAccShareGrpIds){
                            if(mhkAccShareId == existAccShareId){
                                PGExist = true;
                                break;
                            }else{
                                PGExist = false;
                            }
                        }
                        if(!PGExist){
                            addPGSet.add(mhkAccShareId); // Add PG if it exists in MHK Account Team Share and not in Account Share
                        }
                    }else{
                        addPGSet.add(mhkAccShareId); // Remove PG if it exists in MHK Account Team Share and not in Account Share
                    }
                }
            }
            
            System.debug('To be deleted PG: ' + delPGSet);
            System.debug('To be added PG: ' + addPGSet);
            if(delPGSet.size()>0){
                delAccShareIds.add(accId);
                delAccSharePGIds.addAll(delPGSet);           
            }
            if(addPGSet.size()>0){
                for(Id pgId: addPGSet){
                    AccountShare accShare = new AccountShare();
                    accShare.AccountId = accId;
                    accShare.AccountAccessLevel = 'Edit';
                    accShare.ContactAccessLevel = 'Edit';
                    accShare.OpportunityAccessLevel = 'Edit';
                    accShare.UserOrGroupId = pgId;
                    addAccShareList.add(accShare);
                }
            }
        }
        System.debug('Add Account Share List: ' + addAccShareList);
        //Get the Id of AccountShare for deletion
        if(delAccShareIds.size()>0 && delAccSharePGIds.size()>0){
            for(List<AccountShare> accShareList : [SELECT Id, AccountId, UserOrGroupId FROM AccountShare
                                                   WHERE AccountId IN :delAccShareIds
                                                   AND UserOrGroupId IN :delAccSharePGIds]){
                                                       for(AccountShare accShare : accShareList){
                                                           delAccShareIdsList.add(accShare.Id);
                                                       }
                                                   }
        }
        System.debug('Delete Account Share List: ' + delAccShareIdsList);
        
        //DML Operation
        if(addAccShareList != null && addAccShareList.size()>0){
            try{
                List<Database.SaveResult> results = Database.insert(addAccShareList, false);
                for (Database.SaveResult result : results) {
                    if (result.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted Acccount Share: ' + result.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : result.getErrors()) {
                            System.debug('Error while inserting Account Share ' + err.getMessage());
                        }
                    }
                } 
            }Catch(Exception dmle){
                System.debug('!!!!!!!Error Occurred with Line Number'+dmle.getLineNumber() +' --'+dmle ); 
            }
        }
        
        if(delAccShareIdsList != null && delAccShareIdsList.size()>0){
            try{
                Database.DeleteResult[] results = Database.delete(delAccShareIdsList, false);
                for (Database.DeleteResult result : results) {
                    if (result.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully deleted AccountShare: ' + result.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : result.getErrors()) {
                            System.debug('Error while deleting AccountShare: ' + err.getMessage());
                        }
                    }
                } 
            }
            catch (System.DMLException dmle){
                for(Integer i=0; i<dmle.getNumDml(); i++){
                    system.debug(dmle.getDmlMessage(i));
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
    // Schedulable batch
    global void execute(SchedulableContext sc){
        //Database.executeBatch(new AssignPubGrpToAccShareResInv_Batch());
        //Start Added By Mudit - Bug 71601 - 14/03/19
        List<Batch_Query__mdt> queryList = [SELECT Batch_Size__c FROM Batch_Query__mdt where Class_Name__c = 'AssignPubGrpToAccShareResInv_Batch']; //default query 
        if(queryList != null && queryList.size()>0 && queryList[0].Batch_Size__c != null && queryList[0].Batch_Size__c != 0){
            Integer batchSize = (Integer)queryList[0].Batch_Size__c;
            System.debug('batchSize: ' + batchSize);
        	Database.executeBatch(new AssignPubGrpToAccShareResInv_Batch(), batchSize);
        }else{
            Database.executeBatch(new AssignPubGrpToAccShareResInv_Batch(), 200);
        }
        //End Added By Mudit - Bug 71601 - 14/03/19
    }
}