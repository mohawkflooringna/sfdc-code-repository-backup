/**************************************************************************

Name : FreightRateCalloutMockNew_Test 

===========================================================================
Purpose :  Create JSON for Freight Rate
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
***************************************************************************/
@isTest
global class FreightRateCalloutMockNew_Test implements HttpCalloutMock  {

     global HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setStatus('OK');
        res.setStatusCode(200);
         res.setBody('{'

                        

                            +'"ERRORCODE": "R.018129.0000",'

                            +'"MESSAGE": "J A Lotourneau & Fils Inc",'
                            +'"PRIMEWHSE": "J A Lotourneau & Fils Inc",'
                            +'"CARPETDLVDAYS": "J A Lotourneau & Fils Inc",'
                            +'"PALLETDLVDAYS": "J A Lotourneau & Fils Inc",'

                            +'"ODATETIME": "*February 10 - 2017, 01:47pm EST*",'
                            +'"DATETIMESFDC":"*January 28,2019 - 03:13 PM EST*",'


                            +'"FRIEGHTARRAY": ['

                              +'{'

                                +'"CATEGORY": "Soft Surface",'

                                +'"PRODTYPE": "Residential",'

                                +'"WCFRTRTE": "5/30 N40",'
                                +'"WCMINFRT": "5/30 N40",'
                                +'"MDFRTRTE": "5/30 N40",'

                                +'"MDMINFRT": "Y"'

                              +'}'

                            
                            +']'

                          

                        +'}');
           return res;
      }//end of Method
}//End of Class