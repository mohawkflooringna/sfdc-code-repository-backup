/**************************************************************************
Name : MATMainProfile_Batch 
===========================================================================
Purpose :           
========================================================================== =
History:
--------
VERSION    AUTHOR            DATE           DETAIL          DESCRIPTION
1.0        Nagendra       29/01/2019         Created          

***************************************************************************/
global class MATMainProfile_Batch implements Database.Batchable<sObject>,Schedulable{
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
         String query;
          List<MAT_Batch_Setting__c> batchSettingList = new List<MAT_Batch_Setting__c>( [ SELECT id,Batch_Query__c,Last_Run_At__c,Batch_Size__c,MinutesDelay__c FROM MAT_Batch_Setting__c WHERE NAME = 'MATMainProfile_Batch' LIMIT 1 ] );
        DateTime lastRunTime =System.today();
         system.debug('batchSettingList'+batchSettingList.size());
        if(batchSettingList.size()>0){
            if(  batchSettingList[0].Last_Run_At__c != null ){
              lastRunTime = batchSettingList[0].Last_Run_At__c;
                if(batchSettingList[0].Batch_Query__c !=null){
                   query = batchSettingList[0].Batch_Query__c  +' And  CreatedDate >= : lastRunTime'; 
                }else
                  query = 'Select Id,Chain_Number_UserId__c,Main_Profile__c,CreatedDate from Mohawk_Account_Team__c where CreatedDate >= : lastRunTime And Main_Profile__c =false ';   
            }
        }else{             
            query = 'Select Id,Chain_Number_UserId__c,Main_Profile__c,CreatedDate from Mohawk_Account_Team__c where CreatedDate >= : lastRunTime And Main_Profile__c =false ';
        }
        
        return Database.getQueryLocator(query);
        
    }
    global void execute(Database.BatchableContext BC, list<Mohawk_Account_Team__c> scope) {
         list<Mohawk_Account_Team__c>  MATList = scope;
        UpdateMainProfile.checkMainProfileCheckBox(scope);
    }
    global void finish(Database.BatchableContext BC){
        AsyncApexJob a = [SELECT Id, Status, CompletedDate FROM AsyncApexJob WHERE Id =: BC.getJobId()];
        MAT_Batch_Setting__c custoSettingVal =  MAT_Batch_Setting__c.getInstance('MATMainProfile_Batch');
        if (custoSettingVal != null) {
            custoSettingVal.Last_Run_At__c = a.CompletedDate;
            update custoSettingVal;
        }else{
            custoSettingVal = new MAT_Batch_Setting__c(name = 'MATMainProfile_Batch', Last_Run_At__c = a.CompletedDate);
            insert custoSettingVal;
        }
    }
    global void execute(SchedulableContext SC){
        database.executeBatch(new MATMainProfile_Batch());
    }
}