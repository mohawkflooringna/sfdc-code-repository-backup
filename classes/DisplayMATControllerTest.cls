@isTest
public class DisplayMATControllerTest {
     
    static testmethod void dataSetUp()
    {
      Account_Profile__c apRec;
      Account accRec;
      Mohawk_Account_Team__c matRec;
        apRec = new Account_Profile__c();
        apRec.Name = 'test record';
        apRec.Multi_Channel__c = true;
        insert apRec;
        
        accRec = new Account();
        accRec.name = 'test Account';
        accRec.Account_Profile__c = apRec.id;
        insert accRec;
        
        matRec = new Mohawk_Account_Team__c();
        matRec.account__c=accRec.id;
        matRec.Account_Profile__c = apRec.id;
        matRec.main_profile__c = true;
        insert matRec;
        test.startTest();
           DisplayMATController.getMAT(apRec.id);
        test.stopTest();
    }
}