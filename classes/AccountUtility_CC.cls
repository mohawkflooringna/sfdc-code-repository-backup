public without sharing class AccountUtility_CC {
	
    @AuraEnabled
    public static WrapArchitectFolder getAccountInfo(Id accId){
        Account acc = new Account();
        //List<String> ArchTectPickListValues = new List<String>();
        WrapArchitectFolder archFolder = new WrapArchitectFolder();
        acc = [SELECT Architech_Folder__c, Mohawk_Group_Soft_Surface__c , Mohawk_Group_Hard_Surface__c , 
                      AF_Last_Updated_By__c, 
               	      Last_Updated__c, AF_MG_Soft_Surface_Updated__c, AF_MG_Hard_Surface_Updated__c 
                      FROM Account
                      WHERE Id =: accId LIMIT 1];
        archFolder.ArchTectPickList = UtilityCls.getPicklistValues(UtilityCls.Account, UtilityCls.ArchitectFolder);
        archFolder.AF_MG_Soft_Surface_PickList = UtilityCls.getPicklistValues(UtilityCls.Account, 'Mohawk_Group_Soft_Surface__c');
        archFolder.AF_MG_Hard_Surface_PickList = UtilityCls.getPicklistValues(UtilityCls.Account, 'Mohawk_Group_Hard_Surface__c');
        archFolder.account = acc;
        //archFolder.ArchTectPickList = ArchTectPickListValues;
        System.debug(archFolder.account);
        return archFolder;
    }
    
    @AuraEnabled
    public static WrapArchitectFolder updateArchitectFolder(Account acc, Map<String, List<String>> archFoldersMap, String device){
        String archFolderPickList = '';
        //Account acc = new Account();
        Boolean error = false;
        String errorMsg = '';
        WrapArchitectFolder err = new WrapArchitectFolder();
        System.debug('Device: ' + device);
        System.debug('archFoldersMap: ' + archFoldersMap);
        List<String> archFolders = archFoldersMap.containsKey('archFolders') ? archFoldersMap.get('archFolders') : null;
        if(archFolders != null && archFolders.size()>0 && acc.Id != null && device == 'DESKTOP'){
            acc.Architech_Folder__c = '';
            for(String archFolder: archFolders){
                archFolderPickList = archFolderPickList + archFolder + ';'; 
            }
            //acc.Id = accId;
            acc.Architech_Folder__c = archFolderPickList;
            acc.Last_Updated__c = System.now();
        }else if((archFolders == null || archFolders.size()==0) && acc.Id != null && device == 'DESKTOP'){
            acc.Architech_Folder__c = '';
        }
        /* Start of Code - MB - Bug 61730 - 11/8/18 */
        List<String> archFoldersMGSS = archFoldersMap.containsKey('MGSS_ArchFolders') ? archFoldersMap.get('MGSS_ArchFolders') : null;
        if(archFoldersMGSS != null && archFoldersMGSS.size()>0 && acc.Id != null && device == 'DESKTOP'){
            acc.Mohawk_Group_Soft_Surface__c = '';
            for(String archFolder: archFoldersMGSS){
                archFolderPickList = archFolderPickList + archFolder + ';'; 
            }
            //acc.Id = accId;
            acc.Mohawk_Group_Soft_Surface__c = archFolderPickList;
            acc.AF_MG_Soft_Surface_Updated__c  = Date.today();
        }else if((archFolders == null || archFolders.size()==0) && acc.Id != null && device == 'DESKTOP'){
            acc.Mohawk_Group_Soft_Surface__c = '';
        }

        List<String> archFoldersMGHS = archFoldersMap.containsKey('MGHS_ArchFolders') ? archFoldersMap.get('MGHS_ArchFolders') : null;
        if(archFoldersMGHS != null && archFoldersMGHS.size()>0 && acc.Id != null && device == 'DESKTOP'){
            acc.Mohawk_Group_Hard_Surface__c = '';
            for(String archFolder: archFoldersMGHS){
                archFolderPickList = archFolderPickList + archFolder + ';'; 
            }
            //acc.Id = accId;
            acc.Mohawk_Group_Hard_Surface__c = archFolderPickList;
            acc.AF_MG_Hard_Surface_Updated__c  = Date.today();
        }else if((archFoldersMGHS == null || archFoldersMGHS.size()==0) && acc.Id != null && device == 'DESKTOP'){
            acc.Mohawk_Group_Hard_Surface__c = '';
        }
        /* End of Code - MB - Bug 61730 - 11/8/18 */
        System.debug('Account: ' + acc);
        List<Account> AccL=new list<Account>();
        AccL.add(acc);
      //  Database.SaveResult result = Database.update(acc);
        
        List<Database.SaveResult> results = Database.update(accL,false);
            UtilityCls.createDMLexceptionlog(accL,results,'AccountUtility_CC','updateArchitectFolder');
        for (Integer i = 0; i < results.size(); i++) {
            if (!results[i].isSuccess()) {
                err.errorMsg = results[i].getErrors()[0].getMessage(); 
                err.error = true;
            }
        }
        return err;
    }
    /*
    @AuraEnabled
    public static WrapDeleteRecord checkDeleteAccess(Id recordId, String objName){
        WrapDeleteRecord deleteRecord = new WrapDeleteRecord();
        
        switch on objName{
            when 'Account'{
                Mohawk_Account_Team__c mhk = [SELECT Id FROM Mohawk_Account_Team__c 
                                              WHERE Account__c =: recordId AND User__c =: UserInfo.getUserId() LIMIT 1];
                
            }
            when 'Contact'{
                
            }
                
        }
        return deleteRecord;
    }
    */
	/**** Created By Mudit - on 27-11-2018 For Account and Contact Delete ****/	
    @AuraEnabled
    public static WrapDeleteRecord checkDeleteAccess( Id recordId, String objName ){
        WrapDeleteRecord deleteRecord = new WrapDeleteRecord();
        string objectName = UtilityCls.getObjectNameById( recordId );
        String deleteMsg = Label.DeleteAccountContactMessage;
        String errMessage = Label.DeleteAccountContactErrorMessage;
        switch on objName{
            when 'Account'{
                List<Mohawk_Account_Team__c> mhkList = new List<Mohawk_Account_Team__c>( [SELECT Id FROM Mohawk_Account_Team__c 
                                              WHERE Account__c =: recordId AND User__c =: UserInfo.getUserId() LIMIT 1] );
                if( mhkList != null && mhkList.size() > 0 ){
                    deleteRecord.error = false;
                    deleteMsg = deleteMsg.replace('<Object>', 'Account');
                    deleteRecord.message = deleteMsg;
                }else{
                    deleteRecord.error = true;
                    errMessage = errMessage.replace('<Object>', 'Account');
                    deleteRecord.errorMessage = errMessage;
                }
            }
            when 'Contact'{
                deleteRecord.error = false;
                deleteMsg = deleteMsg.replace('<Object>', 'Contact');
                deleteRecord.message = deleteMsg;
            }
        }
        return deleteRecord;
    }
    
    /**** Created By Mudit - on 27-11-2018 For Account and Contact Delete ****/	
    @AuraEnabled
    public static void deleteRecord( Id recordId ){
        database.delete(recordId);
    }
    
    public class WrapArchitectFolder {
        @AuraEnabled
        public Account account;
        @AuraEnabled
        public List<String> ArchTectPickList;
        @AuraEnabled
        public List<String> AF_MG_Soft_Surface_PickList;
        @AuraEnabled
        public List<String> AF_MG_Hard_Surface_PickList;
        @AuraEnabled
        public String errorMsg = '';
        @AuraEnabled
        public Boolean error = false;
        
        public WrapArchitectFolder(){
            account = new Account();
            ArchTectPickList = new List<String>();
            AF_MG_Soft_Surface_PickList = new List<String>();
            AF_MG_Hard_Surface_PickList = new List<String>();
        }
    }
    
    public class WrapDeleteRecord {
        @AuraEnabled public String message = '';
        @AuraEnabled public Boolean error = false;
        @AuraEnabled public String errorMessage = '';
    }
    
}