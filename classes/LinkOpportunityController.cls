public class LinkOpportunityController 
{
    public Quote quote{get;set;}
    
    public LinkOpportunityController()
    {
        quote = new Quote();
    }
    
    public PageReference save()
    {
        PageReference p;
        System.debug('Op>>'+quote.OpportunityId);
        if(quote.OpportunityId != null && String.isNotBlank(quote.OpportunityId))
        {
            Opportunity opp = [select id,rdcc__Project__c,StageName from Opportunity where id = :quote.OpportunityId];
            opp.rdcc__Project__c = Id.valueOf(ApexPages.currentPage().getParameters().get('id'));
            try
            {
                update opp;
                p = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
        		p.setRedirect(true);
            }
            catch(Exception e)
            {	
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,' '+e));
               	p = null;
            }
            
        }
        else
        {
			if(quote.OpportunityId == null)
       			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Please select Opportunity before saving'));
			p = null;          
        }
        
        return p;
    }
    
    public PageReference onCancel()
    {
        PageReference p = new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
        p.setRedirect(true);
        return p;
    }
}