global class UpdateUserGeography_Batch implements Database.Batchable<sObject>,Schedulable {
	
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id, Geography__c FROM User WHERE Business_Type__c = \'Commercial\' ';
        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<User> scope) {
        Set<Id> userIds = new Set<Id>();
        Map<Id, Territory_User__c> userTerrMap = new Map<Id, Territory_User__c>();
        List<User> updUserList = new List<User>();
        System.debug('Scope: ' + scope);
        for(User usr: scope){
            userIds.add(usr.Id);
        }
        for(Territory_User__c terrUser: [SELECT Id, Territory__r.Name, Territory__r.Region__c, Territory__r.Sector__c, Role__c, User__c
                                         FROM Territory_User__c WHERE User__c IN :userIds]){
                                             userTerrMap.put(terrUser.User__c, terrUser);
                                         }
        System.debug('Territory_User__c: ' + userTerrMap);
        for(User usr: scope){
            if(userTerrMap.containsKey(usr.Id)){
                Territory_User__c terrUser = userTerrMap.get(usr.Id);
                if(terrUser.Role__c == UtilityCls.AE && usr.Geography__c != terrUser.Territory__r.Name){
                    usr.Geography__c = terrUser.Territory__r.Name;
                    updUserList.add(usr);
                }else if(terrUser.Role__c == UtilityCls.RVP && usr.Geography__c != terrUser.Territory__r.Region__c){
                    usr.Geography__c = terrUser.Territory__r.Region__c;
                    updUserList.add(usr);
                }else if(terrUser.Role__c == UtilityCls.SVP && usr.Geography__c != terrUser.Territory__r.Sector__c){
                    usr.Geography__c = terrUser.Territory__r.Sector__c;
                    updUserList.add(usr);
                } 
            }
        }
        if(updUserList.size()>0){
            System.debug('Users to be Updated: ' + updUserList);
            try{
                Database.update(updUserList, false);
            }
            catch(Exception dmle){
                  System.debug('**Error in DML operation:'+ dmle.getMessage());
                  System.debug('**line number'+dmle.getLineNumber());
                  System.debug('**line number'+dmle);
             }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
                        
    }

     //Schedulable 
    global void execute(SchedulableContext SC){
        String jobId = Database.executeBatch(new UpdateUserGeography_Batch(),200); 
        System.debug('*******Job id***'+jobId);
    }
}