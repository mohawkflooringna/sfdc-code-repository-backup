global class AssignPubGrpToAccountSharing_Batch implements Database.Batchable < sObject > , Database.Stateful, Schedulable{

    global static string ErrorRecords;
    global static string SuccessRecords;
    global string download_Or_Update = 'Download'; //Default
    global string tempErrorRecs='';
    global string tempSuccessRecs='';
    global string tempFinalRecords='';
    global static string finalstr = 'Id, Name, UserOrGroupId, GrpName  \r\n';

    
    // Batch Constructor
    global AssignPubGrpToAccountSharing_Batch(String downld_Or_Updte) {
        download_Or_Update = downld_Or_Updte;
    }
    
    /*global Database.QueryLocator start(Database.BatchableContext bc) {
//String soql = 'SELECT Id, Name, Territory__r.Name FROM Account WHERE Business_Type__c = \'Commercial\' and Territory__c <> NULL LIMIT 500000';
return Database.getQueryLocator([SELECT Id, Name, Territory__r.Name FROM Account WHERE Business_Type__c = 'Commercial' and Territory__c <> NULL LIMIT 500000]);
}*/
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String Query;
        Batch_Query__mdt [] QureyList = [SELECT Query_String__c FROM Batch_Query__mdt where Class_Name__c = 'AccWithTerritoryAssignedButNoAccShare'];
        If(QureyList.size() > 0)
            query = QureyList[0].Query_String__c ;
        
        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<Account> scope){
        Set<Id> terrIds_C = new Set<Id>();
        Set<Id> accIds_C = new Set<Id>();
        Set<Id> publicGroupIds = new Set<Id>();
        Set<String> terrName = new Set<String>();
        Set<String> grpNameSet = new Set<String>();
        Map<String, Id> grpIdNameMap = new Map<String, Id>();
        Map<Id,List<Id>> accPublicGroupMap = new Map<Id,List<Id>>();
        Map<Id, String> publicGroupNameMap = new Map<Id, String>();
        Map<Id, Set<String>> accPGNameMap = new Map<Id, Set<String>>();
        Map<Id, String> terrNameMap = new Map<Id, String>();
        Map<Id, Set<String>> accTerrNameMap = new Map<Id, Set<String>>();
        List<AccountShare> newAccShareList = new List<AccountShare>();

        for(Account acc: scope){
            if(acc.Strategic_Account__c == false && acc.Business_Type__c == 'Commercial'){
                if(acc.Territory__c != null){
                    terrIds_C.add(acc.Territory__c);
                    terrNameMap.put(acc.Territory__c, acc.Territory__r.Name);
                    terrName.add(acc.Territory__r.Name);
                    if(accTerrNameMap.containsKey(acc.Id)){
                        accTerrNameMap.get(acc.Id).add(acc.Territory__r.Name);
                    }else{
                        Set<String> terrNameSet = new Set<String>();
                        terrNameSet.add(acc.Territory__r.Name);
                        accTerrNameMap.put(acc.Id, terrNameSet);
                    } 
                }
                if(acc.Education_Govt_Territory__c != null){
                    terrIds_C.add(acc.Education_Govt_Territory__c);
                    terrNameMap.put(acc.Education_Govt_Territory__c, acc.Education_Govt_Territory__r.Name);
                    terrName.add(acc.Education_Govt_Territory__r.Name);
                    if(accTerrNameMap.containsKey(acc.Id)){
                        accTerrNameMap.get(acc.Id).add(acc.Education_Govt_Territory__r.Name);
                    }else{
                        Set<String> terrNameSet = new Set<String>();
                        terrNameSet.add(acc.Education_Govt_Territory__r.Name);
                        accTerrNameMap.put(acc.Id, terrNameSet);
                    }
                }
                if(acc.Healthcare_Sr_Living_Territory__c != null){
                    terrIds_C.add(acc.Healthcare_Sr_Living_Territory__c);
                    terrNameMap.put(acc.Healthcare_Sr_Living_Territory__c, acc.Healthcare_Sr_Living_Territory__r.Name);
                    terrName.add(acc.Healthcare_Sr_Living_Territory__r.Name);
                    if(accTerrNameMap.containsKey(acc.Id)){
                        accTerrNameMap.get(acc.Id).add(acc.Healthcare_Sr_Living_Territory__r.Name);
                    }else{
                        Set<String> terrNameSet = new Set<String>();
                        terrNameSet.add(acc.Healthcare_Sr_Living_Territory__r.Name);
                        accTerrNameMap.put(acc.Id, terrNameSet);
                    }
                }
                if(acc.Workplace_Retail_Territory__c != null){
                    terrIds_C.add(acc.Workplace_Retail_Territory__c);
                    terrNameMap.put(acc.Workplace_Retail_Territory__c, acc.Workplace_Retail_Territory__r.Name);
                    terrName.add(acc.Workplace_Retail_Territory__r.Name);
                    if(accTerrNameMap.containsKey(acc.Id)){
                        accTerrNameMap.get(acc.Id).add(acc.Workplace_Retail_Territory__r.Name);
                    }else{
                        Set<String> terrNameSet = new Set<String>();
                        terrNameSet.add(acc.Workplace_Retail_Territory__r.Name);
                        accTerrNameMap.put(acc.Id, terrNameSet);
                    }
                }
                accIds_C.add(acc.Id);
            }
            //
        }
        
        if(accIds_C.size()>0){
            Default_Configuration__c defConfig = Default_Configuration__c.getOrgDefaults();
            String groups = defConfig.Groups__c;
            List<String> devNameList = groups.split(';');
            List<Id> exclGrpIds = new List<Id>();
            for(List<Group> grpList : [SELECT Id, Name, DeveloperName FROM Group WHERE DeveloperName IN : devNameList]){
                for(Group grp: grpList){
                    exclGrpIds.add(grp.Id);
                }
            }
            for(List<AccountShare> accShareList : [SELECT Id, AccountId, UserOrGroupId FROM AccountShare
                                                   WHERE AccountId IN :accIds_C
                                                   AND RowCause = 'Manual'
                                                   AND UserOrGroupId IN (SELECT Id FROM Group Where Id NOT IN :exclGrpIds AND Type = 'Regular')]){
                                                       system.debug('###accShareList:'+accShareList);
                                                       for(AccountShare accShare : accShareList){
                                                           String grpValue = String.valueOf(accShare.UserOrGroupId);
                                                           if(grpValue.startsWith('00G')){
                                                               publicGroupIds.add(accShare.UserOrGroupId);
                                                               if(accPublicGroupMap.containsKey(accShare.AccountId)){
                                                                   accPublicGroupMap.get(accShare.AccountId).add(accShare.UserOrGroupId);
                                                               }else{
                                                                   List<Id> userOrGroupIds = new List<Id>();
                                                                   userOrGroupIds.add(accShare.UserOrGroupId);
                                                                   accPublicGroupMap.put(accShare.AccountId, userOrGroupIds);
                                                               }
                                                           }
                                                       }
                                                   }
        }
        //Get the Group Names
        if(grpNameSet.size()>0 || publicGroupIds.size()>0 || terrName.size()>0){
            for(List<Group> grpList: [SELECT Id, Name, Type from Group
                                      Where ( Id IN :publicGroupIds 
                                             OR Name IN :grpNameSet OR Name IN: terrName ) 
                                      AND type = 'Regular']){
                                          system.debug('###grpList'+grpList);
                                          for(Group grp: grpList){
                                              if(grp.Type == 'Regular'){
                                                  publicGroupNameMap.put(grp.Id, grp.Name);
                                                  grpIdNameMap.put(grp.Name, grp.Id);
                                              }
                                          }
                                      }
        }
        /*for(Group grp: [SELECT Id, Name FROM Group WHERE Name IN :terrName
AND Type = 'Regular']){
grpIdNameMap.put(grp.Name, grp.Id);
}*/    
        for(Id accId: accPublicGroupMap.keySet()){
            List<String> accPubGrpList = accPublicGroupMap.get(accId);
            for(String accPubGrp: accPubGrpList){
                String pubgrpName = publicGroupNameMap.get(accPubGrp);
                if(pubgrpName != null && pubgrpName != ''){
                    if(accPGNameMap.containsKey(accId)){
                        accPGNameMap.get(accId).add(pubgrpName);
                    }else{
                        Set<String> PGNameSet = new Set<String>();
                        PGNameSet.add(pubgrpName);
                        accPGNameMap.put(accId, PGNameSet);
                    }
                }
            }
        }
        //string header = 'Id, Name, UserOrGroupId, GrpName  \r\n';
        //string finalstr = header;
        for(Account acc: scope){
            //Id pubgrpId = grpIdNameMap.get(acc.Territory__r.Name);
            //AccountShare accShare = new AccountShare();
            Set<String> accTerrNameSet = new Set<String>();
            Set<String> accPGNameSet = new Set<String>();
            Set<String> grpToBeAddedSet = new Set<String>();
            Boolean addgrp = true;
            //Boolean 
            List<Id> accpublicGroupIds = accPublicGroupMap.get(acc.Id);
            accTerrNameSet = accTerrNameMap.get(acc.Id);
            accPGNameSet = accPGNameMap.get(acc.Id);
            system.debug('Account Territories: '+accTerrNameSet);
            if(accTerrNameSet != null){
                system.debug(accpublicGroupIds);
                System.debug(accPGNameMap);
                if(accPGNameSet != null){
                    for(String tName: accTerrNameSet){
                        //String grpName = publicGroupNameMap.get(publicGroupId);
                        if(!accPGNameSet.contains(tName)){
                            grpToBeAddedSet.add(tName);
                        }
                    }
                }else{
                    grpToBeAddedSet.addAll(accTerrNameSet);
                }
                System.debug('Groups to be added: ' + grpToBeAddedSet);
                if(grpToBeAddedSet.size()>0){
                    for(String grp: grpToBeAddedSet){
                        Id grpId = grpIdNameMap.get(grp);
                        if(grpId != null){
                            if(download_Or_Update == 'Update'){
                                AccountShare accShare = new AccountShare();
                                accShare.AccountId = acc.Id;
                                accShare.AccountAccessLevel = 'Edit';
                                accShare.ContactAccessLevel = 'Edit';
                                accShare.OpportunityAccessLevel = 'Edit';
                                accShare.UserOrGroupId = grpId;
                                system.debug('###accShare :'+accShare);
                                newAccShareList.add(accShare);
                            }else if(download_Or_Update == 'Download'){
                                string recordString = acc.id + ',' + acc.Name + ',' + grpId + ',' + grp + '\r\n';
                                finalstr = finalstr + recordString;
                            }
                        }
                    }
                }
            }

            /*if(pubgrpId != null){
accShare.AccountId = acc.Id;
accShare.AccountAccessLevel = 'Edit';
accShare.ContactAccessLevel = 'Edit';
accShare.OpportunityAccessLevel = 'Edit';
accShare.UserOrGroupId = pubgrpId;
newAccShareList.add(accShare);
}*/
        }
        if(newAccShareList.size() > 0 && download_Or_Update == 'Update'){
            try{
                List<Database.SaveResult> results = Database.insert(newAccShareList, false);
                for (Integer i = 0; i < results.size(); i++) {
                    if (!results.get(i).isSuccess()) {
                        if (ErrorRecords == null) {
                            ErrorRecords = 'Operation, Object Name, Record, Status Code, Error Message \r\n';
                        }
                        for (Database.Error theError: results.get(i).getErrors()) {
                            string recordString = 'Insert' + ',' + 'Account Share' + ',' + newAccShareList.get(i) + ',' + theError.getStatusCode() + ',' + theError.getMessage() + '\r\n';
                            ErrorRecords = ErrorRecords + recordString;
                        }
                    }else if(results.get(i).isSuccess()){
                        if (SuccessRecords == null) {
                            SuccessRecords = 'Operation, Object Name, Record, Status Code, Message \r\n';
                        }
                        string recordString = 'Insert' + ',' + 'Account Share' + ',' + newAccShareList.get(i) + ',' + 'Success' + ',' + 'Record Created' +  '\r\n';
                        SuccessRecords = SuccessRecords + recordString;
                    }
                    System.debug('Error Records' + ErrorRecords);
                    System.debug('Success Records' + SuccessRecords);   
                } 
            }Catch(Exception dmle){
                System.debug('!!!!!!!Error Occurred with Line Number'+dmle.getLineNumber() +' --'+dmle ); 
            }
        }
        
        
        tempErrorRecs+=ErrorRecords;
        tempSuccessRecs+=SuccessRecords;
        tempFinalRecords+=finalstr;
        
    }
    
    global void finish(Database.BatchableContext BC) {
     System.debug('!!!!!!Error Records tempErrorRecs' + tempErrorRecs );
        System.debug('!!!!!!Success Records tempSuccessRecs' + tempSuccessRecs );
        System.debug('!!!!!!Batch Job Finished'+tempFinalRecords ); 
       // String[] emailAddressList  = new list<string> {'susmitha.josephine@ust-global.com'};
        if(download_Or_Update == 'Download' && tempFinalRecords!='' ){
            List<String> emailAddressList = Label.Batch_Class_Error_Notification_Email.split(';');  
            
            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            blob csvBlob = Blob.valueOf(tempFinalRecords);
            string csvname = 'Account.csv';
            csvAttc.setFileName(csvname);
            csvAttc.setBody(csvBlob);
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            String subject = 'Accounts without Public Group Assignment';
            email.setSubject(subject);
            email.setToAddresses(emailAddressList);
            email.setPlainTextBody('Accounts without Public Group Assignment ');
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {
                csvAttc
            });
            Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                email
            });
        }
        if(download_Or_Update == 'Update'){
        List<String> emailAddressList = Label.Batch_Class_Error_Notification_Email.split(';'); 
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String subject = 'Account Public Group Assignment Log';
        email.setSubject(subject);
        email.setToAddresses(emailAddressList);
        email.setPlainTextBody('Account Public Group Assignment Log');
        if (tempErrorRecs!= ''){
            Messaging.EmailFileAttachment csvAttcErr = new Messaging.EmailFileAttachment();
            blob csvBlob = Blob.valueOf(tempErrorRecs);
            string csvname = 'ErrorRecords.csv';
            csvAttcErr.setFileName(csvname);
            csvAttcErr.setBody(csvBlob);
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {
                csvAttcErr
                    });
        }
        if (tempSuccessRecs!= ''){
            Messaging.EmailFileAttachment csvAttcSuccess = new Messaging.EmailFileAttachment();
            blob csvBlob = Blob.valueOf(tempSuccessRecs);
            string csvname = 'SuccessRecords.csv';
            csvAttcSuccess.setFileName(csvname);
            csvAttcSuccess.setBody(csvBlob);
            email.setFileAttachments(new Messaging.EmailFileAttachment[] {
                csvAttcSuccess
                    });
        }
        Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
            email
                });
                }

       
    }
    
    // Schedulable batch
    global void execute(SchedulableContext sc){
        //Database.executeBatch(new AssignPubGrpToAccountSharing_Batch());
    } 
}