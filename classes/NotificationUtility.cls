/**************************************************************************

Name : NotificationUtility
===========================================================================
Purpose : To send Email Notifications whenever there is an exception occuring 
          Batch class
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mohan       20/March/2018    Create  
***************************************************************************/
public class NotificationUtility {
    
    /* Sample code for the method to be implemented in the Batch 
     * for(Database.SaveResult dsr : dsrs){
            if(!dsr.isSuccess()){
                String errMsg = dsr.getId() + ': ' + dsr.getErrors()[0].getMessage();
                errorList.add(errMsg);
            }   
       } 
    */

    public static void sendBatchExceptionNotification(Id batchJobId, String ErrorRecords, String AttName){
        
        AsyncApexJob currentJob = [SELECT Id, ApexClassID, JobType, MethodName, Status FROM AsyncApexJob WHERE Id =: batchJobId];
        ApexClass batchClass = [SELECT Id, Name FROM ApexClass WHERE Id =: currentJob.ApexClassID];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // Strings to hold the email addresses to which you are sending the email.
        List<String> emailAddressList = Label.Batch_Class_Error_Notification_Email.split(';');         
        IF ( emailAddressList.size() > 0 && ErrorRecords != null ){
            
         String[] toAddresses = emailAddressList; 
        //String[] ccAddresses = new String[] {'smith@gmail.com'};
          
        
        // Assign the addresses for the To and CC lists to the mail object.
        mail.setToAddresses(toAddresses);
        //mail.setCcAddresses(ccAddresses);        
        
        // Specify the name used as the display name.
        mail.setSenderDisplayName('Mohawk Developement Team');
        
        // Specify the subject line for your email address.
        mail.setSubject('Batch Class Execution Error: ' + batchClass.Name);  
        
        String body = 'batch job '
            + batchClass.Name
            + 'has finished. \n' 
            + 'Please find the error list attached.';        
        
        // Creating the CSV file
        // String finalstr = 'Error Message \n';
         attName = (attName == null) ? 'Batch Errors.csv' : attName ;
            
       /* for(String errorMsg  : errorList){
            string recordString = '"'+ errorMsg +'"\n';
            finalstr = finalstr +recordString;
        }  */ 
        
        // Create the email attachment    
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName(attName);
        efa.setBody(Blob.valueOf(ErrorRecords));        
        
        mail.setPlainTextBody( body );
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        
        /*
        // Specify the text content of the email.
        mail.setPlainTextBody('There was an error executing the batch class. Below are the details regarding the failure: \n' +
                              'Batch Class Name: ' + batchClass.Name + '\n' +
                              'Method Name: ' + AsyncApexJob.MethodName + '\n' +
                              'Error Details: ' + e.getMessage() + '\n LineNumber: ' + e.getLineNumber() + 
                              '\n Exception Type: ' + e.getTypeName() + '\n Cause: ' + e.getCause() + 
                              '\nStack Trace ' + e.getStackTraceString());  
        */
        
        // Send the email you have created.
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });        
    } 
  }
      
}