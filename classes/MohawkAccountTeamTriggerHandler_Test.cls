/**************************************************************************

Name : MohawkAccountTeamTriggerHandler_Test 
===========================================================================
Purpose : Uitlity class to MohawkAccountTeamTriggerHandler test data 
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Sasi Naik     17/Mar/2017    Create  
2.0        Susmitha      27/March/2018  Modified
***************************************************************************/

@isTest
private class MohawkAccountTeamTriggerHandler_Test {
    static List<Account> resAccListForInvoicing;
    static List<Territory__c> terrList;
    static User userList;
    static List<Mohawk_Account_Team__c> mohAccTeam;
    static List<Mohawk_Account_Team__c> mohAccTeam1;
    static List<Action_List__c> actListForResidentialFL;
    static List<Action_List__c> actListForResidentialSAL;
    static List<Action_List__c> actListForResidential = new List<Action_List__c>();
    static List<SAL_Territory__c>  salTerr;
    static List<Territory_User__c>  terrUser;
    static List<Territory_User__c>  terrUser1;
    static Id resiNonInvRTId = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Non-Invoicing');
    static Id resiInvRTId    = UtilityCls.getRecordTypeInfo('Mohawk_Account_Team__c', 'Residential Invoicing');
    
    public static void init(){   
        Utility_Test.getTeamCreationCusSetting(); 
        AP_Trigger_Settings__c APSC = new AP_Trigger_Settings__c();
        APSC.Name = 'Two Team Account';
        APSC.Exclude_Chain_Numbers_1__c = '11';
        APSC.Exclude_Chain_Numbers_2__c = '12';
        insert APSC;
    }
    
    static testMethod void testMethod1() {
        System.runAs(Utility_Test.RESIDENTIAL_USER) {
            test.startTest(); 
            init();
            //List<Account_Profile_Settings__c> accProfileSettings = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
            List<Account_Profile__c> accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false); // Added by MB - 07/10/17
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1); 
            //resAccListForInvoicing = Utility_Test.createAccountsForInvoicing(true, 1); // Added by MB - 07/07/17
            terrList = Utility_Test.createTerritories(true, 2);
            system.assert(terrList.size()>0);
            mohAccTeam = Utility_Test.createMohawkAccountTeam(true, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiNonInvRTId, accProfile, 'Horizon');
            MohawkAccountTeamTriggerHandler.initAccountprofile(mohAccTeam);
            delete mohAccTeam; 
            mohAccTeam = Utility_Test.createMohawkAccountTeam(true, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId, accProfile, 'Karastan');
            delete accProfile; delete mohAccTeam;
            accProfile = Utility_Test.createAccountProfile(true, 1, 'R.900124', false); // Added by MB - 07/10/17
            mohAccTeam = Utility_Test.createMohawkAccountTeam(true, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId, accProfile, 'Aladdin');
            Map<Id,Mohawk_Account_Team__c> mohAccTeamMap = new Map<Id,Mohawk_Account_Team__c>(mohAccTeam);
            MohawkAccountTeamTriggerHandler.updateAccountMHKOwner(mohAccTeamMap);
            delete mohAccTeam;
            
            
            
            test.stopTest();
        }
    }
    
    static testMethod void testMethod2() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);
            terrList = Utility_Test.createTerritories(false, 2);
            for(Territory__c ter :terrList){
                ter.Type__c = UtilityCls.BUILDERMULTIFAMILY;
            }
            insert terrList;
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
            for(Mohawk_Account_Team__c mat:mohAccTeam){
                mat.Account__c=resAccListForInvoicing[0].id;
            }
            insert mohAccTeam;
            system.assert(mohAccTeam.size()>0);
            
            try {
                mohAccTeam1 = Utility_Test.createMohawkAccountTeam(true, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
                
            } catch (Exception e) {
                System.Assert(e.getMessage().contains(System.Label.Related_Mohawk_Account_Team_Duplication_ErrMsg));
            }
            
            test.stopTest();
        }
    }
    
    
    static testMethod void testMethod3() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for(Account a :resAccListForInvoicing){
                a.Total_Account_Chain_Sales__c=50;
            }
            insert resAccListForInvoicing;
            terrList = Utility_Test.createTerritories(true, 2);
            system.assert(terrList.size()>0);
            
            /***Creating Account Profile Setting and Account Profile***8/22/2017****/
            List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
            apSetting[0].Yellow_Threshold_Carpet__c = 10;
            apSetting[0].Red_Threshold_Carpet__c = 10;
            apSetting[0].Yellow_Threshold_Cushion__c = 10;
            apSetting[0].Red_Threshold_Cushion__c = 10;
            apSetting[0].Yellow_Threshold_Hardwood__c = 10;
            apSetting[0].Red_Threshold_Hardwood__c = 10; 
            apSetting[0].Yellow_Threshold_Laminate__c = 10; 
            apSetting[0].Red_Threshold_Laminate__c = 10;
            apSetting[0].Yellow_Threshold_Tile__c = 10;
            apSetting[0].Red_Threshold_Tile__c = 10;
            apSetting[0].Yellow_Threshold_Resilient__c = 10;
            apSetting[0].Red_Threshold_Resilient__c = 10;
            apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
            apSetting[0].Red_Threshold_Mainstreet__c = 10;
            apSetting[0].Retail_Sales_Carpet__c = 10;
            update apSetting[0];
            List<Account_Profile__c> apList = new List<Account_Profile__c>();
            Account_Profile__c ap = new Account_Profile__c();
            ap.Primary_Business__c = apSetting[0].Id;
            ap.Annual_Retail_Sales__c = 300000;
            ap.Annual_Retail_Sales_Non_Flooring__c = 10;
            ap.Chain_Number__c = '3213111';
            apList.add(ap);
            Insert aplist;
            
            //update Account with Account profile information
            resAccListForInvoicing[0].Chain_Number__c = '3213311';
            resAccListForInvoicing[0].Account_Profile__c = aplist[0].Id;
            update resAccListForInvoicing;
            /***END Creating Account Profile Setting and Account Profile****/
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
            for(Mohawk_Account_Team__c mat:mohAccTeam){
                mat.Account__c=resAccListForInvoicing[0].id;
            }
            insert mohAccTeam;
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Brands__c = 'Horizon; Hard Surface; Portico';
                mohAccTeam[i].Product_Types__c = 'Carpet; Cushion; Hardwood; Laminate; Tile; Resilient';
            }
            update mohAccTeam;
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Brands__c = 'Aladdin; Karastan; Aladdin Commercial';
                mohAccTeam[i].Product_Types__c = 'Cushion; Hardwood; Laminate; Tile; Resilient';
            }
            update mohAccTeam;
            
            /*userList = Utility_Test.getTestUser('Sasi','System Administrator');
List<AccountTeamMember> accteammembers = createAccountTeamMember(true,mohAccTeam.size(),resAccListForInvoicing,
for(Integer i=0;i<mohAccTeam.size();i++){
mohAccTeam[i].User__c = userList.Id;
mohAccTeam[i].Brands__c = null;
mohAccTeam[i].Product_Types__c = null;
}
update mohAccTeam; */ 
            
            
            test.stopTest();
        }
    }
    static testMethod void testMethod7() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for(Account a :resAccListForInvoicing){
                a.Total_Account_Chain_Sales__c=50;
            }
            insert resAccListForInvoicing;
            terrList = Utility_Test.createTerritories(true, 2);
            system.assert(terrList.size()>0);
            
            /***Creating Account Profile Setting and Account Profile***8/22/2017****/
            List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
            apSetting[0].Yellow_Threshold_Carpet__c = 10;
            apSetting[0].Red_Threshold_Carpet__c = 10;
            apSetting[0].Yellow_Threshold_Cushion__c = 10;
            apSetting[0].Red_Threshold_Cushion__c = 10;
            apSetting[0].Yellow_Threshold_Hardwood__c = 10;
            apSetting[0].Red_Threshold_Hardwood__c = 10; 
            apSetting[0].Yellow_Threshold_Laminate__c = 10; 
            apSetting[0].Red_Threshold_Laminate__c = 10;
            apSetting[0].Yellow_Threshold_Tile__c = 10;
            apSetting[0].Red_Threshold_Tile__c = 10;
            apSetting[0].Yellow_Threshold_Resilient__c = 10;
            apSetting[0].Red_Threshold_Resilient__c = 10;
            apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
            apSetting[0].Red_Threshold_Mainstreet__c = 10;
            apSetting[0].Retail_Sales_Carpet__c = 10;
            update apSetting[0];
            List<Account_Profile__c> apList = new List<Account_Profile__c>();
            Account_Profile__c ap = new Account_Profile__c();
            ap.Primary_Business__c = apSetting[0].Id;
            ap.Annual_Retail_Sales__c = 300000;
            ap.Annual_Retail_Sales_Non_Flooring__c = 10;
            ap.Chain_Number__c = '3213111';
            apList.add(ap);
            Insert aplist;
            
            //update Account with Account profile information
            resAccListForInvoicing[0].Chain_Number__c = '3213311';
            resAccListForInvoicing[0].Account_Profile__c = aplist[0].Id;
            update resAccListForInvoicing;
            /***END Creating Account Profile Setting and Account Profile****/
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER, resiInvRTId);
            for(Mohawk_Account_Team__c mat:mohAccTeam){
                mat.Account__c=resAccListForInvoicing[0].id;
                
            }
            insert mohAccTeam;
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Brands__c = 'Karastan';
                mohAccTeam[i].Product_Types__c = 'Cushion';
            }
            update mohAccTeam;
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Brands__c = 'Horizon; Hard Surface; Portico';
                mohAccTeam[i].Product_Types__c = 'Hardwood; Laminate; Tile; Resilient';
            }
            update mohAccTeam;
            try{
                for(Integer i=0;i<mohAccTeam.size();i++){
                    //mohAccTeam[i].Account_Access__c = 'Edit';
                    mohAccTeam[i].Brands__c = 'Aladdin Commercial'; 
                    mohAccTeam[i].Product_Types__c = 'Hardwood; Laminate; Tile; Resilient';
                    
                }
                update mohAccTeam;
            }
            catch(Exception ex){
            }
            
            test.stopTest();
            try{
                userList = Utility_Test.getTestUser('Sasi','System Administrator');
                for(Integer i=0;i<mohAccTeam.size();i++){
                    mohAccTeam[i].User__c = userList.Id;
                    mohAccTeam[i].Brands__c = null;
                    mohAccTeam[i].Product_Types__c = null;
                }
                update mohAccTeam;  
            }
            catch(Exception ex){}
            
            
        }
        
    }
    
    static testMethod void testMethod6() {
        System.runAs(Utility_Test.ADMIN_USER) {
            
            init();
            resAccListForInvoicing = Utility_Test.createAccountsForNonInvoicing(true, 2);
            for(Integer i=0;i<resAccListForInvoicing.size();i++){
                resAccListForInvoicing[i].Business_Type__c = 'Residential';
            }
            update resAccListForInvoicing;
            system.assert(resAccListForInvoicing.size()>0);
            
            test.startTest();  
            NonInvoiceGLoablAccNumber_Batch  cls = new NonInvoiceGLoablAccNumber_Batch ();
            database.executeBatch(cls,10);
            test.stopTest();
        }
    }
    
    /* Commented by MB - 07/10/17
static testMethod void testMethod4() {
System.runAs(Utility_Test.ADMIN_USER) {

init();
resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);

terrList = Utility_Test.createTerritories(true, 1);
for(Integer i=0;i<resAccListForInvoicing.size();i++){
resAccListForInvoicing[i].Business_Type__c = 'Residential';
resAccListForInvoicing[i].Territory__c = terrList[i].Id;
}
update resAccListForInvoicing;
terrUser = Utility_Test.createTerritoryUsers(false,1);
for(Integer j=0;j<terrList.size();j++){
for(Integer i=0;i<terrUser.size();i++){
terrUser[i].Territory__c = terrList[j].Id;
terrUser[i].Role__c = 'Business Development Manager';
}
}
insert terrUser;

terrUser1 = Utility_Test.createTerritoryUsers(false,1);
for(Integer j=0;j<terrList.size();j++){
for(Integer i=0;i<terrUser1.size();i++){
terrUser1[i].Territory__c = terrList[j].Id;
terrUser1[i].Role__c = 'Business Development Manager';
terrUser1[i].User__c = Utility_Test.ADMIN_USER.Id;
}
}
insert terrUser1;

mohAccTeam = Utility_Test.createMohawkAccountTeam(true, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER, resiInvRTId);
for(Integer i=0;i<mohAccTeam.size();i++){
mohAccTeam[i].Account_Access__c = 'Edit';
mohAccTeam[i].Source_CAMS__c = true;
}
update mohAccTeam;

test.startTest();  
TerritoryCheckMohawAccTeam_Batch  cls = new TerritoryCheckMohawAccTeam_Batch ();
database.executeBatch(cls,200);
test.stopTest();
}
}

static testMethod void testMethod5() {
System.runAs(Utility_Test.ADMIN_USER) {

init();
resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);

terrList = Utility_Test.createTerritories(true, 1);
for(Integer i=0;i<resAccListForInvoicing.size();i++){
resAccListForInvoicing[i].Business_Type__c = 'Residential';
resAccListForInvoicing[i].Territory__c = terrList[i].Id;
}
update resAccListForInvoicing;
terrUser = Utility_Test.createTerritoryUsers(false,1);
for(Integer j=0;j<terrList.size();j++){
for(Integer i=0;i<terrUser.size();i++){
terrUser[i].Territory__c = terrList[j].Id;
terrUser[i].Role__c = 'Business Development Manager';
}
}
insert terrUser;

terrUser1 = Utility_Test.createTerritoryUsers(false,1);
for(Integer j=0;j<terrList.size();j++){
for(Integer i=0;i<terrUser1.size();i++){
terrUser1[i].Territory__c = terrList[j].Id;
terrUser1[i].Role__c = 'Business Development Manager';
}
}
//insert terrUser1;

mohAccTeam = Utility_Test.createMohawkAccountTeam(true, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER, resiInvRTId);
for(Integer i=0;i<mohAccTeam.size();i++){
mohAccTeam[i].Account_Access__c = 'Edit';
mohAccTeam[i].Source_CAMS__c = true;
}
update mohAccTeam;

Delete terrUser;

test.startTest();  
TerritoryCheckMohawAccTeam_Batch  cls = new TerritoryCheckMohawAccTeam_Batch ();
database.executeBatch(cls,200);
test.stopTest();
}
}



static testMethod void testMethod7() {
System.runAs(Utility_Test.ADMIN_USER) {

init();
resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(true, 1);

terrList = Utility_Test.createTerritories(true, 1);
for(Integer i=0;i<resAccListForInvoicing.size();i++){
resAccListForInvoicing[i].Business_Type__c = 'Residential';
resAccListForInvoicing[i].Territory__c = terrList[i].Id;
}
update resAccListForInvoicing;
terrUser = Utility_Test.createTerritoryUsers(false,1);
for(Integer j=0;j<terrList.size();j++){
for(Integer i=0;i<terrUser.size();i++){
terrUser[i].Territory__c = terrList[j].Id;
terrUser[i].Role__c = 'Business Development Manager';
}
}
insert terrUser;

terrUser1 = Utility_Test.createTerritoryUsers(false,1);
for(Integer j=0;j<terrList.size();j++){
for(Integer i=0;i<terrUser1.size();i++){
terrUser1[i].Territory__c = terrList[j].Id;
terrUser1[i].Role__c = 'Business Development Manager';
terrUser1[i].User__c = Utility_Test.ADMIN_USER.Id;
}
}
insert terrUser1;

//  mohAccTeam = Utility_Test.createMohawkAccountTeam(true, 1, resAccListForInvoicing, terrList, Utility_Test.ADMIN_USER);
// for(Integer i=0;i<mohAccTeam.size();i++){
//     mohAccTeam[i].Account_Access__c = 'Edit';
//     mohAccTeam[i].Source_CAMS__c = true;
// }
// update mohAccTeam;

test.startTest();  
TerritoryCheckMohawAccTeam_Batch  cls = new TerritoryCheckMohawAccTeam_Batch ();
database.executeBatch(cls,200);
test.stopTest();
}
} End of Comment - MB- 07/10/17 */
    static testMethod void testMethod4() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            
            terrList = Utility_Test.createTerritories(false, 2);
            for(Territory__c ter : terrList){
                ter.type__c='Builder / Multi-Family';
            }
            insert terrList;
            User u=Utility_Test.getTestUser('TestUser',null);
            insert u;
            List<Territory_User__c> terUsers=Utility_Test.createTerritoryWithUsers(true,terrList,u,2);
            
            /***Creating Account Profile Setting and Account Profile***8/22/2017****/
            List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
            apSetting[0].Yellow_Threshold_Carpet__c = 10;
            apSetting[0].Red_Threshold_Carpet__c = 10;
            apSetting[0].Yellow_Threshold_Cushion__c = 10;
            apSetting[0].Red_Threshold_Cushion__c = 10;
            apSetting[0].Yellow_Threshold_Hardwood__c = 10;
            apSetting[0].Red_Threshold_Hardwood__c = 10; 
            apSetting[0].Yellow_Threshold_Laminate__c = 10; 
            apSetting[0].Red_Threshold_Laminate__c = 10;
            apSetting[0].Yellow_Threshold_Tile__c = 10;
            apSetting[0].Red_Threshold_Tile__c = 10;
            apSetting[0].Yellow_Threshold_Resilient__c = 10;
            apSetting[0].Red_Threshold_Resilient__c = 10;
            apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
            apSetting[0].Red_Threshold_Mainstreet__c = 10;
            apSetting[0].Retail_Sales_Carpet__c = 10;
            update apSetting[0];
            List<Account_Profile__c> apList = new List<Account_Profile__c>();
            Account_Profile__c ap = new Account_Profile__c();
            ap.Primary_Business__c = apSetting[0].Id;
            ap.Annual_Retail_Sales__c = 300000;
            ap.Annual_Retail_Sales_Non_Flooring__c = 10;
            ap.Chain_Number__c = '3213111';
            apList.add(ap);
            Insert aplist;
            
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for(Account a :resAccListForInvoicing){
                a.Total_Account_Chain_Sales__c=50;
                a.Account_Profile__c=aplist[0].id;
            }
            insert resAccListForInvoicing;
            system.assert(resAccListForInvoicing.size()>0);
            
            //update Account with Account profile information
            resAccListForInvoicing[0].Chain_Number__c = '3213311';
            resAccListForInvoicing[0].Account_Profile__c = aplist[0].Id;
            update resAccListForInvoicing;
            /***END Creating Account Profile Setting and Account Profile****/
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
            for(Mohawk_Account_Team__c mat:mohAccTeam){
                mat.Account__c=resAccListForInvoicing[0].id;
                mat.User__c=u.id;
            }
            insert mohAccTeam;
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Brands__c = 'Aladdin;Hard Surface; Portico';
                mohAccTeam[i].Product_Types__c = 'Carpet; Cushion; Hardwood; Laminate; Tile; Resilient';
            }
            update mohAccTeam;
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Brands__c = 'Aladdin; Karastan';
                mohAccTeam[i].Product_Types__c = 'Cushion; Hardwood; Laminate; Tile; Resilient';
            }
            update mohAccTeam;
            
            /*userList = Utility_Test.getTestUser('Sasi','System Administrator');
List<AccountTeamMember> accteammembers = createAccountTeamMember(true,mohAccTeam.size(),resAccListForInvoicing,
for(Integer i=0;i<mohAccTeam.size();i++){
mohAccTeam[i].User__c = userList.Id;
mohAccTeam[i].Brands__c = null;
mohAccTeam[i].Product_Types__c = null;
}
update mohAccTeam; */ 
            
            
            test.stopTest();
        }
    }
    static testMethod void testMethod9() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            
            terrList = Utility_Test.createTerritories(false, 2);
            for(Territory__c ter : terrList){
                ter.type__c='Builder / Multi-Family';
            }
            insert terrList;
            User u=Utility_Test.getTestUser('TestUser',null);
            insert u;
            List<Territory_User__c> terUsers=Utility_Test.createTerritoryWithUsers(true,terrList,u,2);
            
            /***Creating Account Profile Setting and Account Profile***8/22/2017****/
            List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
            apSetting[0].Yellow_Threshold_Carpet__c = 10;
            apSetting[0].Red_Threshold_Carpet__c = 10;
            apSetting[0].Yellow_Threshold_Cushion__c = 10;
            apSetting[0].Red_Threshold_Cushion__c = 10;
            apSetting[0].Yellow_Threshold_Hardwood__c = 10;
            apSetting[0].Red_Threshold_Hardwood__c = 10; 
            apSetting[0].Yellow_Threshold_Laminate__c = 10; 
            apSetting[0].Red_Threshold_Laminate__c = 10;
            apSetting[0].Yellow_Threshold_Tile__c = 10;
            apSetting[0].Red_Threshold_Tile__c = 10;
            apSetting[0].Yellow_Threshold_Resilient__c = 10;
            apSetting[0].Red_Threshold_Resilient__c = 10;
            apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
            apSetting[0].Red_Threshold_Mainstreet__c = 10;
            apSetting[0].Retail_Sales_Carpet__c = 10;
            update apSetting[0];
            List<Account_Profile__c> apList = new List<Account_Profile__c>();
            Account_Profile__c ap = new Account_Profile__c();
            ap.Primary_Business__c = apSetting[0].Id;
            ap.Annual_Retail_Sales__c = 300000;
            ap.Annual_Retail_Sales_Non_Flooring__c = 10;
            ap.Chain_Number__c = '3213111';
            apList.add(ap);
            Insert aplist;
            
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for(Account a :resAccListForInvoicing){
                a.Total_Account_Chain_Sales__c=50;
                a.Account_Profile__c=aplist[0].id;
            }
            insert resAccListForInvoicing;
            system.assert(resAccListForInvoicing.size()>0);
            
            //update Account with Account profile information
            resAccListForInvoicing[0].Chain_Number__c = '3213311';
            resAccListForInvoicing[0].Account_Profile__c = aplist[0].Id;
            update resAccListForInvoicing;
            /***END Creating Account Profile Setting and Account Profile****/
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
            for(Mohawk_Account_Team__c mat:mohAccTeam){
                mat.Account__c=resAccListForInvoicing[0].id;
                mat.User__c=u.id;
            }
            insert mohAccTeam;
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Brands__c = 'Aladdin;Karastan;Hard Surface;Portico';
                mohAccTeam[i].Product_Types__c = 'Carpet;Cushion';
            }
            update mohAccTeam;
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Brands__c = 'Horizon;Hard Surface;Portico';
                mohAccTeam[i].Product_Types__c = 'Hardwood; Laminate; Tile; Resilient';
            }
            update mohAccTeam;
            try{
                for(Integer i=0;i<mohAccTeam.size();i++){
                    //mohAccTeam[i].Account_Access__c = 'Edit';
                    mohAccTeam[i].Brands__c = 'Aladdin Commercial'; 
                    mohAccTeam[i].Product_Types__c = 'Hardwood; Laminate; Tile; Resilient';
                    
                }
                update mohAccTeam;
            }
            catch(Exception ex){
            }
            /*userList = Utility_Test.getTestUser('Sasi','System Administrator');
            List<AccountTeamMember> accteammembers = createAccountTeamMember(true,mohAccTeam.size(),resAccListForInvoicing,
            for(Integer i=0;i<mohAccTeam.size();i++){
            mohAccTeam[i].User__c = userList.Id;
            mohAccTeam[i].Brands__c = null;
            mohAccTeam[i].Product_Types__c = null;
            }
            update mohAccTeam; */ 
            
            
            test.stopTest();
        }
    }
    
    static testMethod void testMethod10() {
        System.runAs(Utility_Test.ADMIN_USER) {
            test.startTest();
            init();
            
            terrList = Utility_Test.createTerritories(false, 2);
            /********
            for(Territory__c ter : terrList){
                ter.type__c='Builder / Multi-Family';
            }
            insert terrList;
            *******/
            terrList[0].type__c='Builder / Multi-Family';
            terrList[1].type__c='Soft Surface';
            insert terrList;
            
            User u=Utility_Test.getTestUser('TestUser',null);
            insert u;
            List<Territory_User__c> terUsers=Utility_Test.createTerritoryWithUsers(false,terrList,u,2);
            for(Integer i=0;i<terUsers.size();i++){
                terUsers[i].Name = 'TEST 123'+i;
            }
            insert terUsers;
            
            /***Creating Account Profile Setting and Account Profile***8/22/2017****/
            List<Account_Profile_Settings__c> apSetting = Utility_Test.createAccountProfileSettings(true, 1, 'Builder');
            apSetting[0].Yellow_Threshold_Carpet__c = 10;
            apSetting[0].Red_Threshold_Carpet__c = 10;
            apSetting[0].Yellow_Threshold_Cushion__c = 10;
            apSetting[0].Red_Threshold_Cushion__c = 10;
            apSetting[0].Yellow_Threshold_Hardwood__c = 10;
            apSetting[0].Red_Threshold_Hardwood__c = 10; 
            apSetting[0].Yellow_Threshold_Laminate__c = 10; 
            apSetting[0].Red_Threshold_Laminate__c = 10;
            apSetting[0].Yellow_Threshold_Tile__c = 10;
            apSetting[0].Red_Threshold_Tile__c = 10;
            apSetting[0].Yellow_Threshold_Resilient__c = 10;
            apSetting[0].Red_Threshold_Resilient__c = 10;
            apSetting[0].Yellow_Threshold_Mainstreet__c = 10;
            apSetting[0].Red_Threshold_Mainstreet__c = 10;
            apSetting[0].Retail_Sales_Carpet__c = 10;
            update apSetting[0];
            List<Account_Profile__c> apList = new List<Account_Profile__c>();
            Account_Profile__c ap = new Account_Profile__c();
            ap.Primary_Business__c = apSetting[0].Id;
            ap.Annual_Retail_Sales__c = 300000;
            ap.Annual_Retail_Sales_Non_Flooring__c = 10;
            ap.Chain_Number__c = '3213111';
            apList.add(ap);
            Insert aplist;
            
            resAccListForInvoicing = Utility_Test.createResidentialAccountsForInvoicing(false, 1);
            for(Account a :resAccListForInvoicing){
                a.Total_Account_Chain_Sales__c=50;
                a.Account_Profile__c=aplist[0].id;
            }
            insert resAccListForInvoicing;
            system.assert(resAccListForInvoicing.size()>0);
            
            //update Account with Account profile information
            resAccListForInvoicing[0].Chain_Number__c = '3213311';
            resAccListForInvoicing[0].Account_Profile__c = aplist[0].Id;
            update resAccListForInvoicing;
            /***END Creating Account Profile Setting and Account Profile****/
            mohAccTeam = Utility_Test.createMohawkAccountTeam(false, 1, resAccListForInvoicing, terrList, Utility_Test.RESIDENTIAL_USER, resiInvRTId);
            for(Mohawk_Account_Team__c mat:mohAccTeam){
                mat.Account__c=resAccListForInvoicing[0].id;
                mat.User__c=u.id;
            }
            insert mohAccTeam;
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Brands__c = 'Aladdin;Karastan;Hard Surface;Portico';
                mohAccTeam[i].Product_Types__c = 'Carpet;Cushion';
            }
            update mohAccTeam;
            for(Integer i=0;i<mohAccTeam.size();i++){
                mohAccTeam[i].Account_Access__c = 'Edit';
                mohAccTeam[i].Brands__c = 'Horizon;Hard Surface;Portico';
                mohAccTeam[i].Product_Types__c = 'Hardwood; Laminate; Tile; Resilient';
                mohAccTeam[i].Territory_User_Number__c = 'TEST 1231';
            }
            update mohAccTeam;
            try{
                for(Integer i=0;i<mohAccTeam.size();i++){
                    //mohAccTeam[i].Account_Access__c = 'Edit';
                    mohAccTeam[i].Brands__c = 'Aladdin Commercial'; 
                    mohAccTeam[i].Product_Types__c = 'Hardwood; Laminate; Tile; Resilient';
                    
                }
                update mohAccTeam;
                MohawkAccountTeamTriggerHandler.updateBMFUserCheckBox( mohAccTeam );
            }
            catch(Exception ex){
            }
            test.stopTest();
        }
    }
}