/**
* Class Name: Rest_MATAccountProfile
* Author: Suresh Shanmugam - Mohawk
* Date: Feb 22, 2016
* Description: This class is used to save the MAT Account Profile information from the API
**/
@RestResource(urlMapping = '/v1/MATAccountProfile/*')
global with sharing class Rest_MATAccountProfile {
    @HttpPost
    global static response SaveContract(Mohawk_Account_Team__c MAT){
            
        Response result = new response();
        if ( MAT != null && MAT.Chain_Number_UserId__c != null ){
            list<Mohawk_Account_Team__c> MATList = new list<Mohawk_Account_Team__c>();
			map<id,Mohawk_Account_Team__c> MATMap = new map<id,Mohawk_Account_Team__c>([select id, Chain_Number_UserId__c from  Mohawk_Account_Team__c where Chain_Number_UserId__c = :  MAT.Chain_Number_UserId__c]);
            system.debug('MatMap'  + MATMap);
            for (id  matId : MATMap.keyset()){
				Mohawk_Account_Team__c matCopy = MAT.clone();
				matCopy.id = matId;
				MATList.add (matCopy);
			}
			
            system.debug('MatList'  + MATList);
			if ( MATList.size() > 0 ) {
			    list<Database.SaveResult> updateMATResultList = Database.update(MATList, false);
                for(Database.SaveResult updateMATResult : updateMATResultList) {
                   if(updateMATResult.isSuccess()) {
                        result.Success = 'true';
                        system.debug('MHK Debug --> Save MHK Team: Update the MHK Team : ' + MAT); 
                        result.Message = 'MAT -- ' + MAT.Chain_Number_UserId__c +  ' - is Updated ';   
                    } else {
                        Database.Error err = updateMATResult.getErrors()[0];
                        system.debug('MHK Debug --> Save MHK Team: ' + err);
                        result.Message = err.getMessage();
                        result.Success = 'false';  
                        result.errorCode = string.valueof(err.getStatusCode());
                    }       
                }
                   
            } else {
                result.Success = 'false';
			    result.Message = 'Chain Number User id - ' + MAT.Chain_Number_UserId__c + ' - Not Found';
			    result.errorCode = 'Invalid ID';                
            }
			
		} else {			
			result.Success = 'false';
			result.Message = 'Invlalid Chain Number User id - ' + MAT.Chain_Number_UserId__c;
			result.errorCode = 'Invalid ID';
		}
           
        return result;
        
    }
        
    
    global class response{
      global String Success;
      global String Message;
      global string errorCode;
    }      
    
    
}