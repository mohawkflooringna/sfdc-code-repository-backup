/**************************************************************************

Name : SamplePerformanceInventoryHandler_Test

===========================================================================
Purpose :   This class is used for SamplePerformanceInventoryHandler
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Mudit        27/September/2017     Created 
***************************************************************************/
@isTest
private class SamplePerformanceInventoryHandler_Test{
    static testMethod void testMethod1() {
        List<Account>accList = Utility_Test.createAccounts( true,1 );
        Sample_Display_Inventory__c SDI = new Sample_Display_Inventory__c();
        SDI.Name = 'TEST';
        SDI.Account__c = accList[0].id; 
        SDI.Dealer__c = accList[0].id; 
        SDI.Customer_Group__c = 'TEST';
        //SDI.RecordTypeId = [SELECT id FROM RecordType WHERE SobjectType =: 'Sample_Display_Inventory__c' AND DeveloperName =: 'Sample_Commercial' ].id;
        SDI.RecordTypeID = Schema.SObjectType.Sample_Display_Inventory__c.getRecordTypeInfosByName().get( UtilityCls.SAMPLEREDIDENTALDISPLAY ).getRecordTypeId() ;
        SDI.Architech_Folder__c = 'ANALOGUE';        
        insert SDI;
        
        System.assert(SDI != null);
        SamplePerformanceInventoryHandler.UpdateAccountCustomerGroupValues( new Map<id, Sample_Display_Inventory__c>( [ SELECT id,Account__c,Dealer__c,Customer_Group__c,RecordTypeId,Architech_Folder__c FROM Sample_Display_Inventory__c ] ) );
        
    }
    
}