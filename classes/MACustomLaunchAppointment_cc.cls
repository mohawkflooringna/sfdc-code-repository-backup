global with sharing class MACustomLaunchAppointment_cc {
    public MACustomLaunchAppointment_cc() {
    }
     
    @RemoteAction
    public static String getEventId( String RecId ) {
     String Eventid = Appointments_CC.validateRecordId( Recid );
     if ( UtilityCls.getObjectNameById(Eventid ) == 'Event' )
        Return (Eventid);
     else
        Return ('Error');
    }
    
}