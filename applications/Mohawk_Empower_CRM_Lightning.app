<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Invoicing_Account_Residential</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Account</pageOrSobjectType>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Case_Record_Page4</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Case</pageOrSobjectType>
    </actionOverrides>
    <brand>
        <headerColor>#0070D2</headerColor>
        <logo>MHK360_header_logo2</logo>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Residential Flooring</label>
    <navType>Standard</navType>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>BU_IS_Account_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Non_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>BU IS Users</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Project_Record_Page_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.Residential_Multi_Family</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.New_Residential</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.MHK_Group</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Existing_Account_Commercial</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Existing_Account_Commercial</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Existing_Account_Commercial</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Existing_Account_Commercial</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Existing_Account_Commercial</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Existing_Account_Commercial</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Existing_Account_Residential</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Project_Record_Page_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.Residential_Multi_Family</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Project_Record_Page_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.Residential_Traditional</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Project_Record_Page_Custom</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.Commercial_Traditional</recordType>
        <type>Flexipage</type>
        <profile>Administrator</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Invoicing_Account_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Invoicing</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Invoicing_Account_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Invoicing</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Invoicing_Account_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Invoicing</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Non_Invoicing_Account_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Non_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.New_Commercial</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.New_Commercial</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Non_Invoicing_Account_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Non_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Non_Invoicing_Account_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Non_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Account_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Competitors</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Account_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Competitors</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Account_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Competitors</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Non_Invoicing_Account_Commercial</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Non_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.New_Residential</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Project_Record_Page_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.Residential_Master</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Project_Record_Page_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.Residential_Master</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.MHK_Group</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Existing_Account_Residential</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Project_Record_Page_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.Residential_Master</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Existing_Account_Residential</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Non_Invoicing_Account_Commercial</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Non_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.MHK_Group</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.MHK_Group</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.MHK_Group</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.MHK_Group</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.New_Residential</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Non_Invoicing_Account_Commercial</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Non_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.New_Commercial</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Existing_Account_Residential</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Existing_Account_Residential</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Existing_Account_Residential</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.New_Commercial</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.New_Commercial</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.New_Commercial</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.New_Residential</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.New_Residential</recordType>
        <type>Flexipage</type>
        <profile>Commercial Sales User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.New_Residential</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Project_Record_Page_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.Residential_Multi_Family</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>BU_IS_Account_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Invoicing</recordType>
        <type>Flexipage</type>
        <profile>BU IS Users</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Project_Record_Page_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.Residential_Traditional</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales Operation</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Project_Record_Page_Residential</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
        <recordType>Opportunity.Residential_Traditional</recordType>
        <type>Flexipage</type>
        <profile>Residential Sales User</profile>
    </profileActionOverrides>
    <tabs>standard-home</tabs>
    <tabs>standard-Account</tabs>
    <tabs>Scheduling_Assistant</tabs>
    <tabs>standard-Task</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-ContentNote</tabs>
    <tabs>standard-File</tabs>
    <tabs>standard-Feed</tabs>
    <tabs>standard-CollaborationGroup</tabs>
    <tabs>Price_Grid</tabs>
    <tabs>Buying_Group_Grid</tabs>
    <tabs>Mobile_Pricing</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>standard-OtherUserProfile</tabs>
    <tabs>Action_List__c</tabs>
    <tabs>Account_Profile_Settings__c</tabs>
    <uiType>Lightning</uiType>
</CustomApplication>
