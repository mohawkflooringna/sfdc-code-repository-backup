<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>My_Account_Plan_for_approval</fullName>
        <description>My Account Plan for approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Notification_Templates/My_Account_Plan_for_approval</template>
    </alerts>
    <alerts>
        <fullName>My_Account_Plan_has_approved</fullName>
        <description>My Account Plan has approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Notification_Templates/My_Account_Plan_has_approved</template>
    </alerts>
    <alerts>
        <fullName>Send_notification_mail_when_rejected</fullName>
        <description>Send notification mail when rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Notification_Templates/My_Account_Plan_is_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Submit_for_approval</fullName>
        <field>Status__c</field>
        <literalValue>Sent for Approval</literalValue>
        <name>Submit for approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Checkbox</fullName>
        <description>Account Plan - update the approval checkbox as true</description>
        <field>Approval__c</field>
        <literalValue>1</literalValue>
        <name>Update Approval Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Old_Segment_Type</fullName>
        <field>Old_Segment_Type__c</field>
        <formula>IF ( ISBLANK(TEXT(PRIORVALUE(Segments__c))), TEXT(Segments__c),TEXT(PRIORVALUE(Segments__c)))</formula>
        <name>Update Old Segment Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Old_goal_value</fullName>
        <field>Old_Goal_Value__c</field>
        <formula>IF ( ISBLANK(PRIORVALUE(Goal_Opportunity__c)), Goal_Opportunity__c ,PRIORVALUE(Goal_Opportunity__c))</formula>
        <name>Update Old goal value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unlock_False</fullName>
        <field>Unlock__c</field>
        <literalValue>0</literalValue>
        <name>Update Unlock False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unlock_True</fullName>
        <field>Unlock__c</field>
        <literalValue>1</literalValue>
        <name>Update Unlock True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_for_approval</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update status for approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_for_recall</fullName>
        <field>Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Update status for recall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_status_for_reject</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update status for reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Old Segment Type and Goal</fullName>
        <actions>
            <name>Update_Old_Segment_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Old_goal_value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Old Segment Type</description>
        <formula>AND( ISCHANGED(Status__c),                             (TEXT(PRIORVALUE(Status__c))  = &apos;Approved&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
