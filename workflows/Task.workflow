<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Coaching_Conversation_Subject</fullName>
        <field>Subject</field>
        <formula>&apos;Coaching - &apos; + Owner:User.FirstName + &apos; &apos; + Owner:User.LastName + &apos; &apos; + 
Text(Month(Start_Date__c)) + &apos;/&apos; + 
Text(Day(Start_Date__c)) + &apos;/&apos; + 
Text(Year(Start_Date__c))</formula>
        <name>Update Coaching Conversation Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Subject based on Coaching Conversation</fullName>
        <actions>
            <name>Update_Coaching_Conversation_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Coaching Conversation</value>
        </criteriaItems>
        <description>Default Coaching Conversation subject to &quot;Coaching - &lt;Assigned to Field&gt;&lt;Start Date&gt;&quot;</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
