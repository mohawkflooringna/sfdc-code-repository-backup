<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SCA_Result_of_User_Story_for_Priority_Count</fullName>
        <description>SCA Result of User Story for Priority Count</description>
        <protected>false</protected>
        <recipients>
            <recipient>mamatha_kothapalli@mohawkind.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mohanbabu_kj@mohawkind.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>suresh_shanmugam@mohawkind.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>swapnil_borkar@mohawkind.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>copado__Copado_Deployer/Copado_SCA</template>
    </alerts>
</Workflow>
