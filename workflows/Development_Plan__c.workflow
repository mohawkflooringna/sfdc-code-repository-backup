<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Completed_Date</fullName>
        <description>Dev plan completed date</description>
        <field>Completed_Date__c</field>
        <formula>NOW()</formula>
        <name>Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reviewed_Date</fullName>
        <description>Dev plan review date</description>
        <field>Reviewed_Date__c</field>
        <formula>NOW()</formula>
        <name>Reviewed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Dev_Plan_Status</fullName>
        <description>When the focus complete fields are true, update the dev plan status to Complete</description>
        <field>Status__c</field>
        <literalValue>Complete</literalValue>
        <name>Update Dev Plan Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Dev Plan Complete</fullName>
        <actions>
            <name>Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Development_Plan__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Development plan is complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Dev Plan Reviewed</fullName>
        <actions>
            <name>Reviewed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Development_Plan__c.Status__c</field>
            <operation>equals</operation>
            <value>Reviewed</value>
        </criteriaItems>
        <description>Dev plan is reviewed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Dev Plan Status</fullName>
        <actions>
            <name>Update_Dev_Plan_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Development_Plan__c.Focus_Complete1__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Development_Plan__c.Focus_Complete2__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
