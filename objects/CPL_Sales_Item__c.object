<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Backing_Code__c</fullName>
        <externalId>false</externalId>
        <label>Backing Code</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CPL_Sales__c</fullName>
        <externalId>false</externalId>
        <label>CPL Sales</label>
        <referenceTo>CPL_Sales__c</referenceTo>
        <relationshipLabel>CPL Sales Items</relationshipLabel>
        <relationshipName>CPL_Sales_Items</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Company__c</fullName>
        <externalId>false</externalId>
        <label>Company</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>District_Code__c</fullName>
        <externalId>true</externalId>
        <label>District Code</label>
        <length>10</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>District_Description__c</fullName>
        <externalId>false</externalId>
        <label>District Description</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Division_Customer_No_SFX_Id__c</fullName>
        <externalId>true</externalId>
        <label>Division Customer No SFX Id</label>
        <length>40</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Employee_Id__c</fullName>
        <externalId>false</externalId>
        <label>Employee Id</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>External_Id__c</fullName>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>100</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Gross_Avg_Price_MTD__c</fullName>
        <description>Gross Average Price Month to Date</description>
        <externalId>false</externalId>
        <inlineHelpText>Gross Average Price Month to Date</inlineHelpText>
        <label>Avg MTD</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Gross_Avg_Price_R12__c</fullName>
        <description>Gross Average Price R12</description>
        <externalId>false</externalId>
        <inlineHelpText>Gross Average Price R12</inlineHelpText>
        <label>Average R12</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Gross_Avg_Price_YTD__c</fullName>
        <description>Gross Average Price Year to Date</description>
        <externalId>false</externalId>
        <inlineHelpText>Gross Average Price Year to Date</inlineHelpText>
        <label>Avg YTD</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>MTD_Price_Level__c</fullName>
        <externalId>false</externalId>
        <label>MTD Price Level</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Price_Levels</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Net_Sales_MTD__c</fullName>
        <description>Net Sales Month to Date</description>
        <externalId>false</externalId>
        <label>MTD</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Net_Sales_MTD_c__c</fullName>
        <description>Net Sales Month to Date</description>
        <externalId>false</externalId>
        <label>NTD</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Net_Sales_R12__c</fullName>
        <description>Net Sales R12</description>
        <externalId>false</externalId>
        <inlineHelpText>Net Sales R12</inlineHelpText>
        <label>R12</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Net_Sales_YTD__c</fullName>
        <description>Net Sales Year to Date</description>
        <externalId>false</externalId>
        <inlineHelpText>Net Sales Year to Date</inlineHelpText>
        <label>YTD</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Product_Categoy__c</fullName>
        <externalId>false</externalId>
        <label>Product Categoy</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Unique_Id__c</fullName>
        <externalId>false</externalId>
        <label>Product Unique Id</label>
        <length>30</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>CPL Sales Items</relationshipLabel>
        <relationshipName>CPL_Sales_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>R12_Price_Level__c</fullName>
        <externalId>false</externalId>
        <label>R12 Price Level</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Price_Levels</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Region_Code__c</fullName>
        <externalId>true</externalId>
        <label>Region Code</label>
        <length>10</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Region_Description__c</fullName>
        <externalId>false</externalId>
        <label>Region Description</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SVP_Code__c</fullName>
        <externalId>false</externalId>
        <label>SVP Code</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SVP_Description__c</fullName>
        <externalId>false</externalId>
        <label>SVP Description</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Selling_Style_Num__c</fullName>
        <externalId>true</externalId>
        <label>Selling Style #</label>
        <length>40</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Size_Code__c</fullName>
        <externalId>false</externalId>
        <label>Size Code</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Territory_Manager_Id__c</fullName>
        <externalId>true</externalId>
        <label>Territory Manager Id</label>
        <length>40</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Territory_Manager_User_Name__c</fullName>
        <externalId>true</externalId>
        <label>Territory Manager User Name</label>
        <length>100</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>YTD_Price_Level__c</fullName>
        <externalId>false</externalId>
        <label>YTD Price Level</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Price_Levels</valueSetName>
        </valueSet>
    </fields>
    <label>CPL Sales Item</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>All_CPL_Sales_Items</fullName>
        <columns>NAME</columns>
        <columns>Division_Customer_No_SFX_Id__c</columns>
        <columns>CPL_Sales__c</columns>
        <columns>Product__c</columns>
        <columns>Territory_Manager_Id__c</columns>
        <columns>Territory_Manager_User_Name__c</columns>
        <columns>Gross_Avg_Price_R12__c</columns>
        <columns>Gross_Avg_Price_MTD__c</columns>
        <columns>Gross_Avg_Price_YTD__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Division_Customer_No_SFX_Id__c</field>
            <operation>equals</operation>
            <value>R.406240</value>
        </filters>
        <filters>
            <field>Product_Unique_Id__c</field>
            <operation>contains</operation>
            <value>1T29</value>
        </filters>
        <label>All CPL Sales Items</label>
    </listViews>
    <nameField>
        <displayFormat>CPLSAITEM-{0000000000}</displayFormat>
        <label>CPL Sales Item Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>CPL Sales Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
