<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <compactLayouts>
        <fullName>rdcc__Company_Role_Layout</fullName>
        <fields>rdcc__Account__c</fields>
        <fields>rdcc__Project__c</fields>
        <label>Company Role Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>An object to store Project Company Roles in respect to a Project.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>rdcc__Account_Phone__c</fullName>
        <deprecated>false</deprecated>
        <description>Standard Salesforce Account&apos;s Phone Number</description>
        <externalId>false</externalId>
        <formula>IF(NOT(ISBLANK(rdcc__Account__c)),rdcc__Account__r.Phone,  rdcc__Insight_Company__r.rdcc__Phone__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The Standard Salesforce account&apos;s phone number for the company participating in the project.</inlineHelpText>
        <label>Account Phone</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Standard Salesforce Account</description>
        <externalId>false</externalId>
        <inlineHelpText>The Standard Salesforce Account record participating in the project.</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Project Participant - Companies</relationshipLabel>
        <relationshipName>Companies</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>rdcc__CMD_Comapany_Id__c</fullName>
        <deprecated>false</deprecated>
        <description>The CMD ID for the company listing in Insight.</description>
        <externalId>false</externalId>
        <inlineHelpText>The CMD ID for the company listing in Insight.</inlineHelpText>
        <label>CMD Comapany Id</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Company_Detail_URL__c</fullName>
        <deprecated>false</deprecated>
        <description>ConstructConnect&apos;s Insight Platform Company Detail URL</description>
        <externalId>false</externalId>
        <formula>IF( AND(rdcc__Account__c &lt;&gt; null,rdcc__Account__r.rdcc__URL__c &lt;&gt; null),(HYPERLINK( IF(BEGINS(rdcc__Account__r.rdcc__URL__c,&apos;http&apos;),rdcc__Account__r.rdcc__URL__c,(&apos;http://&apos; &amp; rdcc__Account__r.rdcc__URL__c) ), rdcc__Account__r.rdcc__URL__c ,&quot;_blank&quot;)),IF(AND( rdcc__Insight_Company__c  &lt;&gt; null,rdcc__Insight_Company__r.rdcc__URL__c &lt;&gt; null) ,(HYPERLINK( IF(BEGINS(rdcc__Insight_Company__r.rdcc__URL__c,&apos;http&apos;),rdcc__Insight_Company__r.rdcc__URL__c,(&apos;http://&apos; &amp; rdcc__Insight_Company__r.rdcc__URL__c) ), rdcc__Account__r.rdcc__URL__c ,&quot;_blank&quot;)) ,&apos;Not Available&apos;))</formula>
        <inlineHelpText>The URL for the company listing in Insight. Click to display the details for the company in Insight (Insight user access is required).</inlineHelpText>
        <label>Company Detail URL</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Company_Display__c</fullName>
        <deprecated>false</deprecated>
        <description>The company name for</description>
        <externalId>false</externalId>
        <formula>IF(NOT(ISBLANK( rdcc__Account__c )), HYPERLINK(&apos;/&apos; &amp; rdcc__Account__r.Id , rdcc__Account__r.Name,&apos;_parent&apos;), HYPERLINK(&apos;/&apos; &amp; rdcc__Insight_Company__r.Id , rdcc__Insight_Company__r.Name ,&apos;_parent&apos;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The name of the company participating on the project as listed in the CMD database.</inlineHelpText>
        <label>Company</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Company_Role_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <description>ConstructConnect&apos;s Role ID for the company.  This allows for reference to Role text displayed.</description>
        <externalId>true</externalId>
        <inlineHelpText>The ID of the role played by the company on a project.</inlineHelpText>
        <label>Company Role Id</label>
        <length>38</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>rdcc__Create_Lead_from_Project__c</fullName>
        <deprecated>false</deprecated>
        <description>Allows user to create a lead from a specific project.</description>
        <externalId>false</externalId>
        <formula>IF(rdcc__Account__r.Id &lt;&gt; null,(HYPERLINK(&quot;/apex/rdcc__CreateLeadFromCompany?accountId=&quot;&amp; rdcc__Account__r.Id &amp; &quot;&amp;Projid=&quot;&amp; rdcc__Project__r.Id &amp; &quot;&amp;theme=&quot; &amp; $User.UIThemeDisplayed , &quot;Create Lead from Company&quot; ,&quot;_parent&quot;)),IF( rdcc__Insight_Company__r.Id &lt;&gt; null ,(HYPERLINK(&quot;/apex/rdcc__CreateLeadFromCompany?accountId=&quot;&amp; rdcc__Insight_Company__r.Id &amp; &quot;&amp;Projid=&quot;&amp; rdcc__Project__r.Id &amp; &quot;&amp;theme=&quot; &amp; $User.UIThemeDisplayed , &quot;Create Lead from Company&quot; ,&quot;_parent&quot;)) ,&apos;NA&apos;))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Allows user to create a lead from a specific project.</inlineHelpText>
        <label>Create Lead for Project</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Insight_Company__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The ConstructConnect Insight company associated with the role.</description>
        <externalId>false</externalId>
        <inlineHelpText>The ConstructConnect Insight company associated with the role.</inlineHelpText>
        <label>Insight Company</label>
        <referenceTo>rdcc__Insight_Company__c</referenceTo>
        <relationshipLabel>Insight Projects: Company Roles</relationshipLabel>
        <relationshipName>Company_Roles</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>rdcc__Insight_Role__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The role the company is playing on the project.  Note that Role,TenderRole, and BiddingRole are all mutually exclusive. You can only select one of these values.</description>
        <externalId>false</externalId>
        <inlineHelpText>The role the company is playing on the project.  Note that Role,TenderRole, and BiddingRole are all mutually exclusive. You can only select one of these values.</inlineHelpText>
        <label>Role</label>
        <referenceTo>rdcc__Insight_Role__c</referenceTo>
        <relationshipLabel>Company Roles</relationshipLabel>
        <relationshipName>Company_Roles</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>rdcc__Primary_Contact__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Primary Contact</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rdcc__Project__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The ConstructConnect Insight project associated with the company.</description>
        <externalId>false</externalId>
        <inlineHelpText>The ConstructConnect Insight project associated with the company.</inlineHelpText>
        <label>Project</label>
        <referenceTo>rdcc__ReedProject__c</referenceTo>
        <relationshipLabel>Project Participants - Company</relationshipLabel>
        <relationshipName>R00N40000001LLaZEAW</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Company Role</label>
    <nameField>
        <displayFormat>{0}</displayFormat>
        <label>Record Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Company Roles</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>rdcc__Validate_Account_Company</fullName>
        <active>true</active>
        <description>Either Account or Insight Company must be selected</description>
        <errorConditionFormula>AND(OR(ISNULL( rdcc__Account__c ),ISBLANK(rdcc__Account__c )),OR(ISNULL(  rdcc__Insight_Company__c ),ISBLANK(rdcc__Insight_Company__c )))</errorConditionFormula>
        <errorMessage>You must select either Account or Insight Company</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>rdcc__Validate_Insight_Project</fullName>
        <active>true</active>
        <description>Insight Project must be selected before saving the records.</description>
        <errorConditionFormula>OR(ISNULL(rdcc__Project__c ),ISBLANK(rdcc__Project__c ))</errorConditionFormula>
        <errorDisplayField>rdcc__Project__c</errorDisplayField>
        <errorMessage>You must enter a value</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>rdcc__Validate_SF_Account_and_Insight_Company</fullName>
        <active>true</active>
        <description>If user selects both SF account and Insight company, an error must be thrown to prevent</description>
        <errorConditionFormula>AND(NOT(OR(ISBLANK( rdcc__Account__c ),ISNULL( rdcc__Account__c ))),NOT(OR(ISBLANK( rdcc__Insight_Company__c ),ISNULL( rdcc__Insight_Company__c ))))</errorConditionFormula>
        <errorMessage>You cannot select both &quot;Account&quot; &amp; &quot;Insight Company&quot; together.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
    <webLinks>
        <fullName>rdcc__Create_Lead</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>Create Lead</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <url>/apex/rdcc__CreateLeadFromCompany?accountId={!IF(rdcc__ProjectCompanyRole__c.rdcc__AccountId__c &lt;&gt; null,rdcc__ProjectCompanyRole__c.rdcc__AccountId__c,rdcc__ProjectCompanyRole__c.rdcc__Insight_CompanyId__c)}&amp;Projid={!rdcc__ProjectCompanyRole__c.rdcc__ProjectId__c}&amp;theme={!$User.UIThemeDisplayed}</url>
    </webLinks>
</CustomObject>
