<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <fullName>Accessory_Grid_Label__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Accessory Grid Label</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Adhesives and Sealers</fullName>
                    <default>true</default>
                    <label>Adhesives and Sealers</label>
                </value>
                <value>
                    <fullName>Care Maintenance</fullName>
                    <default>false</default>
                    <label>Care Maintenance</label>
                </value>
                <value>
                    <fullName>Cushion</fullName>
                    <default>false</default>
                    <label>Cushion</label>
                </value>
                <value>
                    <fullName>Installation Items</fullName>
                    <default>false</default>
                    <label>Installation Items</label>
                </value>
                <value>
                    <fullName>Trims and Molding</fullName>
                    <default>false</default>
                    <label>Trims and Molding</label>
                </value>
                <value>
                    <fullName>Underlayments</fullName>
                    <default>false</default>
                    <label>Underlayments</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Accessory_Grid__c</fullName>
        <description>Determines which accessory grid this data will show on</description>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Accessory Grid</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>ADHESIVES_SEALERS</fullName>
                    <default>true</default>
                    <label>ADHESIVES_SEALERS</label>
                </value>
                <value>
                    <fullName>CARE_MAINTENANCE</fullName>
                    <default>false</default>
                    <label>CARE_MAINTENANCE</label>
                </value>
                <value>
                    <fullName>CUSHION</fullName>
                    <default>false</default>
                    <label>CUSHION</label>
                </value>
                <value>
                    <fullName>INSTALLATION_ITEMS</fullName>
                    <default>false</default>
                    <label>INSTALLATION_ITEMS</label>
                </value>
                <value>
                    <fullName>TRIMS_AND_MOLDING</fullName>
                    <default>false</default>
                    <label>TRIMS_AND_MOLDING</label>
                </value>
                <value>
                    <fullName>UNDERLAYMENTS</fullName>
                    <default>false</default>
                    <label>UNDERLAYMENTS</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Data_Type__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Data Type</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Text</fullName>
                    <default>true</default>
                    <label>Text</label>
                </value>
                <value>
                    <fullName>Number</fullName>
                    <default>false</default>
                    <label>Number</label>
                </value>
                <value>
                    <fullName>Currency</fullName>
                    <default>false</default>
                    <label>Currency</label>
                </value>
                <value>
                    <fullName>Date</fullName>
                    <default>false</default>
                    <label>Date</label>
                </value>
                <value>
                    <fullName>Datetime</fullName>
                    <default>false</default>
                    <label>Datetime</label>
                </value>
                <value>
                    <fullName>Boolean</fullName>
                    <default>false</default>
                    <label>Boolean</label>
                </value>
                <value>
                    <fullName>Custom</fullName>
                    <default>false</default>
                    <label>Custom</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Field_API_Name__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Field API Name</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Field_Label__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Field Label</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Grid_Display_Order__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Grid Display Order</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Grid_Product_Category_Object_Name__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>Grid Product Category Object Name</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Grid_Product_Category__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Grid Product Category</label>
        <referenceTo>Grid_Product_Category__mdt</referenceTo>
        <relationshipLabel>Grid Accessory Fields</relationshipLabel>
        <relationshipName>Grid_Accessory_Fields</relationshipName>
        <required>true</required>
        <type>MetadataRelationship</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Order__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>SubscriberControlled</fieldManageability>
        <label>Order</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Grid Accessory Field</label>
    <listViews>
        <fullName>All_fields</fullName>
        <columns>MasterLabel</columns>
        <columns>DeveloperName</columns>
        <columns>NamespacePrefix</columns>
        <columns>Field_API_Name__c</columns>
        <columns>Field_Label__c</columns>
        <filterScope>Everything</filterScope>
        <label>All fields</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Price_Grid</fullName>
        <columns>MasterLabel</columns>
        <columns>Accessory_Grid__c</columns>
        <columns>Accessory_Grid_Label__c</columns>
        <columns>DeveloperName</columns>
        <columns>Grid_Display_Order__c</columns>
        <columns>Order__c</columns>
        <columns>Grid_Product_Category__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>MasterLabel</field>
            <operation>startsWith</operation>
            <value>Price</value>
        </filters>
        <label>Price Grid</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Under_Layments_Records</fullName>
        <columns>MasterLabel</columns>
        <columns>DeveloperName</columns>
        <columns>NamespacePrefix</columns>
        <columns>Field_API_Name__c</columns>
        <columns>Field_Label__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Accessory_Grid_Label__c</field>
            <operation>equals</operation>
            <value>Underlayments</value>
        </filters>
        <filters>
            <field>Field_Label__c</field>
            <operation>equals</operation>
            <value>Roll Size</value>
        </filters>
        <label>Under Layments Records</label>
    </listViews>
    <pluralLabel>Grid Accessory Fields</pluralLabel>
    <visibility>Public</visibility>
</CustomObject>
