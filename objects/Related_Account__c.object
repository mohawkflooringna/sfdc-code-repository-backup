<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <compactLayouts>
        <fullName>MHK_Related_Accounts_Compact_Layout</fullName>
        <fields>Account__c</fields>
        <fields>Project_Role__c</fields>
        <fields>Opportunity__c</fields>
        <label>MHK Related Accounts Compact Layout</label>
    </compactLayouts>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_Name__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK( $Label.Account_Record_URL &amp; CASESAFEID( Account__c ) &amp; &apos;/view&apos; , Account__r.Name , &apos;_self&apos; )</formula>
        <label>Account Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Role__c</fullName>
        <externalId>false</externalId>
        <label>Account Role</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>End User</fullName>
                    <default>false</default>
                    <label>End User</label>
                </value>
                <value>
                    <fullName>Dealer</fullName>
                    <default>false</default>
                    <label>Dealer</label>
                </value>
                <value>
                    <fullName>General Contractor</fullName>
                    <default>false</default>
                    <label>General Contractor</label>
                </value>
                <value>
                    <fullName>AD</fullName>
                    <default>false</default>
                    <label>A &amp; D</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>Networking</fullName>
                    <default>false</default>
                    <label>Networking</label>
                </value>
                <value>
                    <fullName>Competitor</fullName>
                    <default>false</default>
                    <label>Competitor</label>
                </value>
                <value>
                    <fullName>Real Estate Service Provider</fullName>
                    <default>false</default>
                    <label>Real Estate Service Provider</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <externalId>false</externalId>
        <label>Account</label>
        <lookupFilter>
            <active>true</active>
            <booleanFilter>1 OR (2 AND 3) OR (4 AND 5)</booleanFilter>
            <filterItems>
                <field>Account.Role_C__c</field>
                <operation>equals</operation>
                <valueField>$Source.Project_Role__c</valueField>
            </filterItems>
            <filterItems>
                <field>$Source.Project_Role__c</field>
                <operation>equals</operation>
                <value>Strategic Account</value>
            </filterItems>
            <filterItems>
                <field>Account.Strategic_Account__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <filterItems>
                <field>$Source.Project_Role__c</field>
                <operation>equals</operation>
                <value>Specifier</value>
            </filterItems>
            <filterItems>
                <field>Account.Role_C__c</field>
                <operation>equals</operation>
                <value>End User, Dealer, General Contractor, Other, Networking, Real Estate Service Provider, Designer, Architect, Facility Planner, Purchasing Agent, Student, Mohawk Employee, Competitor, A &amp; D</value>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Projects</relationshipLabel>
        <relationshipName>Related_Accounts</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>CAMS_Employee_ID_of_logged_in_User__c</fullName>
        <externalId>false</externalId>
        <label>CAMS Employee ID of logged in User</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Comment_Migration__c</fullName>
        <description>This is used to store the comment from data migration from Navigator</description>
        <externalId>false</externalId>
        <label>Comment (Migration)</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Contact</description>
        <externalId>false</externalId>
        <label>Contact</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Please choose contact from Account selected</errorMessage>
            <filterItems>
                <field>Contact.AccountId</field>
                <operation>equals</operation>
                <valueField>$Source.Account__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Project Related Accounts</relationshipLabel>
        <relationshipName>Related_Accounts</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>External_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>External ID</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Notes__c</fullName>
        <description>Notes</description>
        <externalId>false</externalId>
        <label>Notes</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <externalId>false</externalId>
        <label>Project</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Project Related Accounts</relationshipLabel>
        <relationshipName>Related_Accounts</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Primary__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <inlineHelpText>Indicates if the End user or Dealer is primary</inlineHelpText>
        <label>Primary</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Project_Name__c</fullName>
        <externalId>false</externalId>
        <formula>HYPERLINK(  $Label.Opportunity_Record_URL  &amp;   CASESAFEID(Opportunity__c) &amp; &apos;/view&apos; , Opportunity__r.Name, &apos;_self&apos; )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Project Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Project_Old_Calculated_Revenue__c</fullName>
        <defaultValue>0</defaultValue>
        <externalId>false</externalId>
        <label>Project Old Calculated Revenue</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Project_Old_Market_Segment__c</fullName>
        <externalId>false</externalId>
        <label>Project Old Market Segment</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Project_Role__c</fullName>
        <externalId>false</externalId>
        <label>Project Role</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>End User</fullName>
                    <default>false</default>
                    <label>End User</label>
                </value>
                <value>
                    <fullName>Dealer</fullName>
                    <default>false</default>
                    <label>Dealer</label>
                </value>
                <value>
                    <fullName>General Contractor</fullName>
                    <default>false</default>
                    <label>General Contractor</label>
                </value>
                <value>
                    <fullName>AD</fullName>
                    <default>false</default>
                    <label>A &amp; D</label>
                </value>
                <value>
                    <fullName>Preferred Dealer</fullName>
                    <default>false</default>
                    <label>Preferred Dealer</label>
                </value>
                <value>
                    <fullName>Strategic Account</fullName>
                    <default>false</default>
                    <label>Strategic Account</label>
                </value>
                <value>
                    <fullName>Influencer</fullName>
                    <default>false</default>
                    <label>Influencer</label>
                </value>
                <value>
                    <fullName>Specifier</fullName>
                    <default>false</default>
                    <label>Specifier</label>
                </value>
                <value>
                    <fullName>00000023</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>00000023</label>
                </value>
                <value>
                    <fullName>Architect &amp; Designer</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Architect &amp; Designer</label>
                </value>
                <value>
                    <fullName>Z0000005</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Z0000005</label>
                </value>
                <value>
                    <fullName>Z0000007</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Z0000007</label>
                </value>
                <value>
                    <fullName>Z0000011</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Z0000011</label>
                </value>
                <value>
                    <fullName>Z0000013</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Z0000013</label>
                </value>
                <value>
                    <fullName>Z0000016</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Z0000016</label>
                </value>
                <value>
                    <fullName>Z0000018</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Z0000018</label>
                </value>
                <value>
                    <fullName>Z0000019</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Z0000019</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Record_type_of_Oppt__c</fullName>
        <externalId>false</externalId>
        <formula>Opportunity__r.RecordType.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Record type of Oppt </label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SAP_Account_Id__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.SAP_Account_GUID__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SAP Account Id</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SAP_Contact_Id__c</fullName>
        <externalId>false</externalId>
        <formula>Contact__r.SAP_Contact_GUID__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SAP Contact Id</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SAP_Opportunity_Id__c</fullName>
        <externalId>false</externalId>
        <formula>Opportunity__r.SAP_Opportunity_GUID__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SAP Opportunity Id</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Specifier__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Specifier</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Strategic_Account__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Strategic Account</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>End User</fullName>
                    <default>false</default>
                    <label>End User</label>
                </value>
                <value>
                    <fullName>Dealer</fullName>
                    <default>false</default>
                    <label>Dealer</label>
                </value>
                <value>
                    <fullName>General Contractor</fullName>
                    <default>false</default>
                    <label>General Contractor</label>
                </value>
                <value>
                    <fullName>Architect &amp; Designer</fullName>
                    <default>false</default>
                    <label>Architect &amp; Designer</label>
                </value>
                <value>
                    <fullName>Preferred Dealer</fullName>
                    <default>false</default>
                    <label>Preferred Dealer</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Project Related Account</label>
    <nameField>
        <displayFormat>RA-{0000}</displayFormat>
        <label>Related Accounts No</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Project Related Accounts</pluralLabel>
    <recordTypes>
        <fullName>Commercial</fullName>
        <active>true</active>
        <description>Commercial Record Type</description>
        <label>Commercial</label>
        <picklistValues>
            <picklist>Account_Role__c</picklist>
            <values>
                <fullName>AD</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Competitor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Dealer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>End User</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>General Contractor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Networking</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Other</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Real Estate Service Provider</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Project_Role__c</picklist>
            <values>
                <fullName>AD</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Dealer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>End User</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>General Contractor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Influencer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Preferred Dealer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Specifier</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Strategic Account</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Type__c</picklist>
            <values>
                <fullName>Architect %26 Designer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Dealer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>End User</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>General Contractor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Preferred Dealer</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Residential</fullName>
        <active>true</active>
        <description>Residential Record Type</description>
        <label>Residential</label>
        <picklistValues>
            <picklist>Account_Role__c</picklist>
            <values>
                <fullName>AD</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Competitor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Dealer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>End User</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>General Contractor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Networking</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Other</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Real Estate Service Provider</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Project_Role__c</picklist>
            <values>
                <fullName>AD</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Dealer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>End User</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>General Contractor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Influencer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Preferred Dealer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Specifier</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Strategic Account</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Type__c</picklist>
            <values>
                <fullName>Architect %26 Designer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Dealer</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>End User</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>General Contractor</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Preferred Dealer</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Select_Invoice_Accounts</fullName>
        <active>false</active>
        <errorConditionFormula>Text(Project_Role__c)== &apos;Dealer&apos;  &amp;&amp; Account__r.RecordType.Name != &apos;Invoicing&apos;</errorConditionFormula>
        <errorDisplayField>Account__c</errorDisplayField>
        <errorMessage>Please select invoicing accounts for type dealer</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Select_Non_Invoice_Accounts</fullName>
        <active>false</active>
        <errorConditionFormula>( ISNULL(Text(Project_Role__c)) == False  &amp;&amp; ( Text(Project_Role__c)== &apos;End User&apos; || Text(Project_Role__c)== &apos;General Contractor&apos;) ) &amp;&amp; Account__r.RecordType.Name !=&apos;Non-Invoicing&apos;</errorConditionFormula>
        <errorDisplayField>Account__c</errorDisplayField>
        <errorMessage>Please select non invoicing accounts for type  End User, General Contractor</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
