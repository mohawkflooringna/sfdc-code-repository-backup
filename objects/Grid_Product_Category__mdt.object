<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <description>Represents a Grid Type + Product Category</description>
    <fields>
        <fullName>Grid_Type__c</fullName>
        <description>The Grid this Product Category is a part of</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>The Grid this Product Category is a part of</inlineHelpText>
        <label>Grid Type</label>
        <referenceTo>Grid_Type__mdt</referenceTo>
        <relationshipLabel>Grid Product Categories</relationshipLabel>
        <relationshipName>Grid_Product_Categories</relationshipName>
        <required>true</required>
        <type>MetadataRelationship</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Category_Field_API_Name__c</fullName>
        <description>API Name of the Product Category field to be used for filtering results from a grid</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>API Name of the Product Category field to be used for filtering results from a grid</inlineHelpText>
        <label>Product Category Field API Name</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Category__c</fullName>
        <description>Product Category</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>Product Category</inlineHelpText>
        <label>Product Category</label>
        <required>true</required>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Residential Broadloom</fullName>
                    <default>true</default>
                    <label>Residential Broadloom</label>
                </value>
                <value>
                    <fullName>Commercial Broadloom</fullName>
                    <default>false</default>
                    <label>Commercial Broadloom</label>
                </value>
                <value>
                    <fullName>Carpet Tile</fullName>
                    <default>false</default>
                    <label>Carpet Tile</label>
                </value>
                <value>
                    <fullName>Cushion</fullName>
                    <default>false</default>
                    <label>Cushion</label>
                </value>
                <value>
                    <fullName>SolidWood</fullName>
                    <default>false</default>
                    <label>Solid Wood</label>
                </value>
                <value>
                    <fullName>TecWood</fullName>
                    <default>false</default>
                    <label>Tec Wood</label>
                </value>
                <value>
                    <fullName>RevWood</fullName>
                    <default>false</default>
                    <label>Rev Wood</label>
                </value>
                <value>
                    <fullName>Tile</fullName>
                    <default>false</default>
                    <label>Tile</label>
                </value>
                <value>
                    <fullName>Resilient Sheet</fullName>
                    <default>false</default>
                    <label>Resilient Sheet</label>
                </value>
                <value>
                    <fullName>Resilient Tile</fullName>
                    <default>false</default>
                    <label>Resilient Tile and Plank</label>
                </value>
                <value>
                    <fullName>Installation Accessories</fullName>
                    <default>false</default>
                    <label>Installation Accessories</label>
                </value>
                <value>
                    <fullName>Care and Maintenance</fullName>
                    <default>false</default>
                    <label>Care &amp; Maintenance</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SObject_Name__c</fullName>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <label>SObject Name</label>
        <length>50</length>
        <required>true</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sort_Order__c</fullName>
        <description>The order in which these Product Categories show up within a picklist</description>
        <externalId>false</externalId>
        <fieldManageability>DeveloperControlled</fieldManageability>
        <inlineHelpText>The order in which these Product Categories show up within a picklist</inlineHelpText>
        <label>Sort Order</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Grid Product Category</label>
    <pluralLabel>Grid Product Categories</pluralLabel>
    <visibility>Public</visibility>
</CustomObject>
