<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AccountProfileShare_BatchV1</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:type="xsd:double">200.0</value>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">AccountProfileShare_Batch</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">SELECT id,AccountId,UserOrGroupId,Account.Account_Profile__c,isDeleted FROM AccountShare WHERE Account.Business_Type__c = &apos;Residential&apos;  AND Account.Type = &apos;Invoicing&apos;  AND Account.Account_Profile__c != null AND (LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY) AND ( RowCause = &apos;Manual&apos;  OR RowCause = &apos;Team&apos; ) Order By Account.Account_Profile__c ASC</value>
    </values>
</CustomMetadata>
