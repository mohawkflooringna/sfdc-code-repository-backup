<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TrackMohawkAccountTeam_Batch</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:type="xsd:double">200.0</value>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">TrackMohawkAccountTeam_Batch</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">select id,Account__c,isDeleted ,Role__c,Account_Access__c,User__c from Mohawk_Account_Team__c where User__c !=null and (CreatedDate = Today or CreatedDate = Yesterday) All rows</value>
    </values>
</CustomMetadata>
