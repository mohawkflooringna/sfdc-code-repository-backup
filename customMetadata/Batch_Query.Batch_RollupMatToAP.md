<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Batch_RollupMatToAP</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">Batch_RollupMatToAP</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">SELECT Id,Account_Profile__c, LastModifiedDate FROM Mohawk_Account_Team__c WHERE (LastModifiedDate = LAST_N_DAYS: 1 OR LastModifiedDate = TODAY) and recordtype.name = &apos;Residential Invoicing&apos; and LastModifiedBy.Profile_Name__c = &apos;Integration User&apos;</value>
    </values>
</CustomMetadata>
