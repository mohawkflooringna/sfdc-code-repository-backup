<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RollupActualsFromMatToAP_Batch</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">RollupActualsFromMatToAP_Batch</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">SELECT Id,Account_Profile__c,Account_Profile__r.Multi_Channel__c,Actuals_Sent_from_BI__c,Brands__c,Product_Types__c, BMF_Team_Member__c, Actual_Mohawk_Purchases_Carpet__c , Actual_Mohawk_Purchases_Aladdin__c , Actual_Mohawk_Purchases_Aladdin_Comm__c , Actual_Mohawk_Purchases_Horizon__c , Actual_Mohawk_Purchases_Karastan__c , Act_MHK_Purch_Car_PP__c , Actual_Mohawk_Purchases_Cushion__c , Act_MHK_Purch_CU_ALA__c , Act_MHK_Purch_CU_HOR__c , Act_MHK_Purch_CU_KAR__c , Act_MHK_Purch_CU_ALA_Comm__c , Act_MHK_Purch_CU_PP__c , Actual_Mohawk_Purchases_Hardwood__c , Act_MHK_Purch_HW_RT__c , Act_MHK_Purch_HW_PP__c , Actual_Mohawk_Purchases_Laminate__c , Act_MHK_Purch_Lam_RT__c , Act_MHK_Purch_Lam_PP__c , Actual_Mohawk_Purchases_Tile__c , Act_MHK_Purch_Tile_RT__c , Act_MHK_Purch_Tile_PP__c , Actual_Mohawk_Purchases_Resilient__c , Act_MHK_Purch_Resi_PP__c , Actual_Mohawk_Purch_Resilient_Comm__c , Actual_Mohawk_Purch_Resilient_Retail__c , Actual_Mohawk_Purch_Aladdin_Comm_Total__c,Actual_Mohawk_Purchases_Total__c,Chain_Number_UserId__c,LR12_AMP_Al_REPO__c,LR12_AMP_Al_Comm_REPO__c,LR12_AMP_Ca_REPO__c,LR12_AMP_Cu_REPO__c,LR12_AMP_Ha_REPO__c,LR12_AMP_Ka_REPO__c,LR12_AMP_La_REPO__c,LR12_AMP_Mainstreet_Com_REPO__c,LR12_AMP_Prod_Cat_Other_REPO__c,LR12_AMP_Re_REPO__c,LR12_AMP_Re_Comm_REPO__c,LR12_AMP_Re_Retail_REPO__c,LR12_AMP_Ti_REPO__c,LR12_AMP_Car_PP_BI__c,LR12_AMP_CU_ALA_BI__c,LR12_AMP_CU_ALA_Comm_BI__c,LR12_AMP_CU_HOR_BI__c,LR12_AMP_CU_KAR_BI__c,LR12_AMP_CU_PP_BI__c,LR12_AMP_Lam_PP_BI__c,LR12_AMP_Lam_RT_BI__c,LR12_AMP_Resi_PP_BI__c,LR12_AMP_Tile_PP_BI__c,LR12_AMP_Tile_RT_BI__c,LR12_AMP_HW_PP_BI__c,LR12_AMP_HW_RT_BI__c FROM Mohawk_Account_Team__c WHERE (LastModifiedDate = LAST_N_DAYS: 1 OR LastModifiedDate = TODAY) AND Actuals_Sent_from_BI__c = true AND Account_Profile__r.Multi_Channel__c = true Order By Account_Profile__c ASC</value>
    </values>
</CustomMetadata>
