<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>APshare_Delete_Batch</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">APshare_Delete_Batch</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">Select Id, IsDeleted, Name, RecordTypeId, Account_profile__c, User__c, Territory__c, Territory_User_Number__c FROM Mohawk_Account_Team__c where recordtype.name = &apos;Residential Invoicing&apos; AND Account_profile__c != null AND isdeleted = true AND ( LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY ) ORDER BY Chain_Number__c ASC ALL Rows</value>
    </values>
</CustomMetadata>
