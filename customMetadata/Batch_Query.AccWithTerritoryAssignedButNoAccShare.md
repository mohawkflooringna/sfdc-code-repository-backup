<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AccWithTerritoryAssignedButNoAccShare</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:type="xsd:double">200.0</value>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">AccWithTerritoryAssignedButNoAccShare</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">SELECT Id, Name, Type, Business_Type__c, Strategic_Account__c, Territory__c, Territory__r.Name, Healthcare_Sr_Living_Territory__r.name,Healthcare_Sr_Living_Territory__c,Education_Govt_Territory__r.name,Education_Govt_Territory__c, Workplace_Retail_Territory__r.name,Workplace_Retail_Territory__c FROM Account where Business_Type__c=&apos;Commercial&apos; and Type=&apos;Non-Invoicing&apos; and Strategic_Account__c = false and ( Territory__c != null or Workplace_Retail_Territory__c!=null or Education_Govt_Territory__c!=null or Healthcare_Sr_Living_Territory__c!=null)</value>
    </values>
</CustomMetadata>
