<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MATDeletefromCAMS_Batch</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:type="xsd:double">200.0</value>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">MATDeletefromCAMS_Batch</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">SELECT id,name, Territory_User_Number__c from Mohawk_Account_Team__c where User__c != null and recordtype.name = &apos;Residential Invoicing&apos; and ( Territory_User_Number__c = null OR Territory_User_Number__c = &apos; &apos;)</value>
    </values>
</CustomMetadata>
