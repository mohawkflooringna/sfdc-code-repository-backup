<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Account_Profile__c</label>
    <protected>false</protected>
    <values>
        <field>Related_List_Names__c</field>
        <value xsi:type="xsd:string">Competitive_Assessment__c,AttachedContentNote,Assessment_Summary__c,Mohawk_Account_Team__c</value>
    </values>
</CustomMetadata>
