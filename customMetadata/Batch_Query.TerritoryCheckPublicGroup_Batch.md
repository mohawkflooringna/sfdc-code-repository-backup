<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TerritoryCheckPublicGroup_Batch</label>
    <protected>false</protected>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>Class_Name__c</field>
        <value xsi:type="xsd:string">TerritoryCheckPublicGroup_Batch</value>
    </values>
    <values>
        <field>Query_String__c</field>
        <value xsi:type="xsd:string">SELECT Id,User__c,Territory__c,RecordTypeId,IsDeleted,Role__c,Region__c,Territory__r.Name,Territory__r.RecordTypeId FROM Territory_User__c WHERE (LastModifiedDate = LAST_N_DAYS:1 OR LastModifiedDate = TODAY) AND User__c != NULL ALL ROWS</value>
    </values>
</CustomMetadata>
