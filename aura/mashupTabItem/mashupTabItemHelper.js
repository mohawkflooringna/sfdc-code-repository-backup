({
    buildQueryStringForUrl : function(resultSet, component) {// not in use as this logic handled in APEX class
		var url = resultSet.Target_URL__c;
		var queryString = resultSet.Query_String__c;
		
		var paramList = component.get('v.paramList');
		if(queryString && queryString!=''){
            if(resultSet.No_Query_String__c == true){
                var qryStrArr = queryString.split(';;');
                var urlTemp = url;
                for(var i=0;i<qryStrArr.length;i++){
                    if(qryStrArr[i]!=''){
                        var valueArr = qryStrArr[i].split('=');
                        if(urlTemp.indexOf(valueArr[0])!=-1){
                            urlTemp = urlTemp.replace(valueArr[0], paramList[valueArr[1]])
                        }
                    }
                }
                component.set('v.IframeUrl', urlTemp);
            } else {
    	        if(queryString.indexOf('=RECORD_ID')!=-1){
                    queryString = queryString.replace('=RECORD_ID', '='+paramList['recordId']);
                }
                if(queryString.indexOf('RECORD_ID')!=-1){
    	            queryString = queryString.replace('RECORD_ID', '='+paramList['recordId']);
    	        }
                component.set('v.IframeUrl', url + (queryString && queryString!='' ? (url.indexOf('?')!=-1?'&':'?')+queryString : ''));
            }
    	} else 
            component.set('v.IframeUrl', url);
	}
})