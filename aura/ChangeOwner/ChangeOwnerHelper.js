({
    search: function (component, event, helper) {
        // event.preventDefault();
        // debugger
        var resultTable = component.find('result-table');
        // $A.util.addClass(resultTable, 'slds-hide');
        // $A.util.removeClass(resultTable, 'slds-show');

        var name = document.getElementById('name').value;
        // var territory = document.getElementById('territory').value;
        var territory = document.querySelector('input[name="radio-options"]:checked').value;

        var action = component.get("c.getTerrUserOwnerlist");
        action.setParams({'Name': name, 'Territory': territory});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // console.log(response.getReturnValue());
                component.set('v.accountList', response.getReturnValue());
                // var resultTable = component.find('result-table');
                $A.util.addClass(resultTable, 'slds-show');
                $A.util.removeClass(resultTable, 'slds-hide');
            }
        });
        $A.enqueueAction(action);
    },
    getStatePickListvalues : function (component, event, helper) {
        var action = component.get("c.getMajorMetorsPickListvalues");
        // action.setParams({'Name': name, 'Territory': territory});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // console.log(response.getReturnValue());
                component.set('v.stateList', response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getMajorMetorsPickListvalues : function (component, event, helper) {
        var action = component.get("c.getMajorMetorsPickListvalues");
        // action.setParams({'Name': name, 'Territory': territory});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // console.log(response.getReturnValue());
                component.set('v.metrosList', response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
})