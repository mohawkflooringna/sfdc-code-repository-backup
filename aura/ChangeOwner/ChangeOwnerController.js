({
  doInit: function (component, event, helper) {
    // console.log(component.get('v.recordId'))
  },
  handleSendUserIdEvt: function (component, event, helper) {

    // debugger
    // var urlEvent = $A.get("e.force:navigateToURL");
    // urlEvent.setParams({
    //     "url": 'mobike://'
    // });
    // urlEvent.fire();
    // location.hash = '#/sObject/0012C000008fYS7QAM/view';
    // var OwnerName = localStorage.OwnerName;
    /* ceHandlerController.js */
    // var territoryValue = component.get('v.territoryValue');
    // console.log(territoryValue)
    // debugger
    var name = event.getParam('username');
    var userId = event.getParam('userId');

    // console.log(OwnerName + ' ' + UserId)
    // Owner.value;
    var methodName = '';
    var recordId = component.get('v.recordId');
    var recordIdCondition = recordId.substring(0, 3);
    if (recordIdCondition === '001') {
      // var action = component.get("c.ChangeAccountOwner");
      methodName = 'c.ChangeAccountOwner';
    } else if (recordIdCondition === '00Q') {
      methodName = 'c.ChangeLeadOwner';
    }
    // console.log(recordIdCondition)
    var action = component.get(methodName);
    action.setParams({'RecordId': recordId, 'UserId': userId});
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        // console.log(response.getReturnValue());
        var context = component.get("v.UserContext");
        if (context != undefined) {
          if (context == 'Theme4t' || context == 'Theme4d') {
            console.log('VF in S1 or LEX');
            // sforce.one.navigateToSObject(component.get("v.contact").Id);
            sforce.one.navigateToSObject(recordId, 'detail');
          } else {
            console.log('VF in Classic');
            // var contactId = component.get("v.contact").Id;
            // window.location.href = 'https://mohawkdev--pwcdev.cs59.my.salesforce.com/0012C000008fYS7';
            // window.location.assign('/' + recordId);
            sforce.one.navigateToSObject(recordId, 'detail');

            // window.location.reload()
          }
        } else {
          console.log('standalone Lightning Component');
          var event = $A.get("e.force:navigateToSObject");
          event.setParams({"recordId": recordId});
          event.fire();
        }
      } else {
        alert('SYSTEM ERROR')
      }
    });
    $A.enqueueAction(action);
  },
})