({
    initAccoutUser : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getAccountAppointmentUsers");
        action.setParams({ "recordId" : recordId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log('My Appointment Id'+JSON.stringify(response.getReturnValue()));
                component.set("v.accountUserItems", response.getReturnValue());
                //Start - Changes by Nagendra
                var hasAppointmentId;
                for(var i=0;i<response.getReturnValue().length;i++){
                   
                    if(response.getReturnValue()[i].name =='My Activities'){
                        console.log('My Activities');
                       this.isCurrentUserResidentialUser(component, event,helper,response.getReturnValue()[i].id);//Added by Nagendra 17 Jan 2019
                       // this.getAllActivitiesHelper(component, event,helper,response.getReturnValue()[i].id);//Commented by Nagendra 17 Jan 2019
                       break;
                    }else{
                       this.getAllActivitiesHelper(component, event,helper,null,null); 
                        break;
                    }
                }
                //End - Changes by Nagendra
              //  component.set("v.accountUserItems",this.sortData( component,true,'name',response.getReturnValue() ));

                //console.log(response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    //Start - Nagendra 17 Jan 2019
    isCurrentUserResidentialUser : function(component,event,helper,myAppointmentId){
        var action = component.get("c.isCurrentUserResidentialUser");
        action.setCallback(this, function(response) {
            
            if(response.getReturnValue()){
                //component.find("selectType").set("v.value",'Appointment');
                
                component.set("v.activityVal",'Appointment');
                
            }
            
               this.getAllActivitiesHelper(component, event,helper,myAppointmentId,response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    //End - Nagendra 17 Jan 2019
    //End - Nagendra 17 Jan 2019                      
    /*** Commented by Mudit - No Use
    getTypeHelper : function(component, event, helper) {
        var action = component.get("c.getAppointmentType");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.appointmentsTypeItems", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    ***/
     sortData : function (component, sortAsc,sortField,data) {
        data.sort(function(a,b){
            var t1,t2;
            
             t1 = a[sortField] == b[sortField],
                t2 = (!a[sortField] && b[sortField]) || (a[sortField] < b[sortField]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        return data;
    },
    resetData : function(cmp){
        cmp.set('v.appointmentsDetails', {});
        cmp.set('v.noData', true);
    },
    initUserAll : function(cmp, event, helper) {
        // var selectUserName = evt.currentTarget.value;
        var recordId = cmp.get("v.recordId");
        var action = cmp.get("c.getAccountAppointmentByUsers");
        // this.resetData(cmp);
        cmp.set('v.appointmentsDetails', []);
        cmp.set('v.noData', true);
        action.setParams({ 
            "recordId" : recordId,
            "userId" : ''
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                /*cmp.set("v.appointmentsDetails", response.getReturnValue());
                console.log(response.getReturnValue());*/
                var result = response.getReturnValue();
                if(result && result.length>0){
                    cmp.set('v.appointmentsDetails', result);
                    cmp.set('v.noData', false);
                }
            }
        });
        $A.enqueueAction(action);
    },
    selectUserName : function(cmp, evt, selectUserName) {
        // var selectUserName = evt.currentTarget.value;
        var recordId = cmp.get("v.recordId");
        var action = cmp.get("c.getAccountAppointmentByUsers");
        cmp.set('v.appointmentsDetails', []);
        cmp.set('v.noData', true);
        // debugger
        action.setParams({ 
            "recordId" : recordId,
            "userId" : selectUserName,
            "type":cmp.get("v.selectedTypeValue")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result && result.length>0){
                    cmp.set('v.noData', false);
                    result.sort(function(a,b){
                        var c = new Date(a.startDate);
                        var d = new Date(b.startDate);
                        return c-d;
                     });
                    cmp.set('v.appointmentsDetails', result);
                }
                else{
                    cmp.set('v.noData', true);
                }

            }
        });
        $A.enqueueAction(action);
        
    },
    goToAppoitment : function(cmp, evt, helper) {
        var recordId = cmp.get("v.recordId");
        var appointmentId = evt.currentTarget.dataset.index;
        location.hash='/sObject/'+appointmentId+'/view';
    },
    goToUser : function(cmp, evt, helper) {
        var recordId = cmp.get("v.recordId");
        var appointmentId = evt.currentTarget.dataset.index;
        location.hash='/sObject/'+appointmentId+'/view';
    },
    //Start - Changes done by Nagendra
    getAllActivitiesHelper : function(cmp, evt, helper,myAppointmentId,isResidentialUser) {
         var recordId = cmp.get("v.recordId");
         var action = cmp.get("c.allActivity");
         action.setParams({ 
            "recordId" : recordId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('result'+JSON.stringify(result));
                if(result && result.length>0){
                   result.sort(function(a,b){
                        var c = new Date(a.startDate);
                        var d = new Date(b.startDate);
                        return d-c;
                     });
                   cmp.set("v.allDataActivity",result);
                    console.log('myAppointmentId'+myAppointmentId);
                    if(myAppointmentId !== null && (!isResidentialUser)){
                       var appointmentsDetails = [];
                        for(var i=0;i<result.length;i++){
                            if(myAppointmentId === result[i].ownerId){
                                appointmentsDetails.push(result[i]);
                            }
                        }
                        cmp.set('v.appointmentsDetails', appointmentsDetails);
                        cmp.set("v.selectedATMValue", myAppointmentId); //Added by MB - Bug 67269 - 11/29/18
                    }else if(myAppointmentId !== null && isResidentialUser){//Start- Nagendra 17-01-2019
                       var appointmentsDetails = [];
                        for(var i=0;i<result.length;i++){
                            if(myAppointmentId === result[i].ownerId && result[i].type == 'Appointment' ){
                                appointmentsDetails.push(result[i]);
                            }
                        }
                        cmp.set('v.appointmentsDetails', appointmentsDetails);
                        cmp.set("v.selectedATMValue", myAppointmentId); //Added by MB - Bug 67269 - 11/29/18
                    }//End- Nagendra 17-01-2019
                    else{
                        cmp.set('v.appointmentsDetails', result);
                       cmp.set('v.noData', false);
                    }
                    console.log('appointmentsDetails'+JSON.stringify(cmp.get("v.appointmentsDetails")));
                    console.log('allDataActivity'+JSON.stringify(cmp.get("v.allDataActivity")));
                }
                else{
                    cmp.set('v.noData', true);
                }

            }
        });
        $A.enqueueAction(action);
    },
    populateData : function(cmp, evt, helper,userId,selectedType) {
        //alert(userId + ' '+selectedType);
        console.log('userId: ' + userId);
        console.log('selectedType: ' + selectedType);
        if(userId !== 'All Appointments'){
            var  appointmentsDetails =[];
            var allDataActivity = cmp.get("v.allDataActivity");
            for(var i=0;i<cmp.get("v.allDataActivity").length;i++){
                if(userId === allDataActivity[i].ownerId){
                    if(selectedType === 'All'){
                       appointmentsDetails.push(allDataActivity[i]); 
                    }else if(selectedType ==='Appointment'){
                        if(cmp.get("v.allDataActivity")[i].type === 'Appointment'){
                           appointmentsDetails.push(allDataActivity[i]);  
                        } 
                    }else if(selectedType === 'Email'){
                        if(cmp.get("v.allDataActivity")[i].type === 'Email'){
                           appointmentsDetails.push(allDataActivity[i]);  
                        } 
                    }else if(selectedType === 'Call'){
                        if(cmp.get("v.allDataActivity")[i].type === 'Call'){
                           appointmentsDetails.push(allDataActivity[i]);  
                        } 
                    }else if(selectedType === 'Task'){
                        var data =allDataActivity[i].type;
                        if(data !== 'Call' && data !== 'Email' && data !== 'Appointment'){
                           appointmentsDetails.push(allDataActivity[i]);  
                        } 
                    }
                }
            }
           if(appointmentsDetails.length>0){
               cmp.set('v.noData', false); 
               cmp.set('v.appointmentsDetails', appointmentsDetails); 
            }else{
               cmp.set('v.noData', true); 
               cmp.set('v.appointmentsDetails', ' '); 
            }
        }else {
             var  appointmentsDetails =[];
            for(var i=0;i<cmp.get("v.allDataActivity").length;i++){
                  
                  // var allDataActivity = cmp.get("v.allDataActivity");
                    if(selectedType=='All'){
                       appointmentsDetails.push(cmp.get("v.allDataActivity")[i]); 
                    }else if(selectedType=='Appointment'){
                        if(cmp.get("v.allDataActivity")[i].type == 'Appointment'){
                           appointmentsDetails.push(cmp.get("v.allDataActivity")[i]);  
                        } 
                    }else if(selectedType=='Email'){
                        if(cmp.get("v.allDataActivity")[i].type == 'Email'){
                           appointmentsDetails.push(cmp.get("v.allDataActivity")[i]);  
                        } 
                    }else if(selectedType=='Call'){
                        if(cmp.get("v.allDataActivity")[i].type == 'Call'){
                           appointmentsDetails.push(cmp.get("v.allDataActivity")[i]);  
                        } 
                    }else if(selectedType=='Task'){
                        var data =cmp.get("v.allDataActivity")[i].type;
                        if(data != 'Call' && data != 'Email' && data != 'Appointment'){
                           appointmentsDetails.push(cmp.get("v.allDataActivity")[i]);  
                        } 
                    }
            }
            if(appointmentsDetails.length>0){
                cmp.set('v.noData', false); 
               cmp.set('v.appointmentsDetails', appointmentsDetails); 
            }else{
               cmp.set('v.noData', true); 
               cmp.set('v.appointmentsDetails', ' '); 
            }
            
        }
    }
    //End - Changes done by Nagendra
})