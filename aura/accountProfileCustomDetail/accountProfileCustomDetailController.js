({
	onInit : function(component, event, helper) {
        component.set("v.spinner",true);
        var SOA = component.get("v.SOA");        
        if( SOA != undefined && (SOA == 'true' || SOA )){
            console.log( 'IF' );
          //  component.set('v.recordId',component.get('v.pageReference.state.recordId') )
        	helper.apNameHelper( component, event, helper );    
        }else{
            console.log( 'ELSE' );
            helper.showAPORMATHelper( component, event, helper );
        }
        //helper.picklistValueHelper( component, event, helper );        
	},
    onclickRadio : function(component, event, helper) {
        component.set("v.spinner",true);
        helper.onRadioClickHelper( component, event, helper );
	},
    onChangeProductType : function(component, event, helper) {
        component.set("v.spinner",true);
        helper.onChangeProductTypeHelper( component, event, helper );
    },
    handleComponentEvent : function(component, event, helper) {
        //helper.apNameHelper( component, event, helper );  
        var SOA = component.get("v.pageReference.state.showOriginalAP");
        if( SOA == 'true'){
            component.set('v.recordId',component.get('v.pageReference.state.recordId') )
            helper.apNameHelper( component, event, helper );    
        }else{
            component.set("v.MATId","");
            component.set("v.showMat",false);
            helper.showAPORMATHelper( component, event, helper );
        }
        
	},
    handleRefreshEvent : function(component, event, helper) {
        component.set("v.spinner",true);
        //helper.apNameHelper( component, event, helper );
        var SOA = component.get("v.pageReference.state.showOriginalAP");
        if( SOA == 'true'){
            component.set('v.recordId',component.get('v.pageReference.state.recordId') )
        	helper.apNameHelper( component, event, helper );    
        }else{
            component.set("v.MATId","");
            component.set("v.showMat",false);
            helper.showAPORMATHelper( component, event, helper );
        }
	},
})