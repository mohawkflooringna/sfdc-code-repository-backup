({
    doInit : function(component, event, helper) {
        
        var accountId = component.get("v.accountId");
        var gridType = component.get("v.gridType");
        console.log('### accountId: ' + accountId);
        console.log('### gridType: ' + gridType);

        helper.doCallout(component, "c.getAccessoryData", {
            "recordId" : component.get("v.recordId"),
            "productCategoryId" : component.get("v.productCategoryId"),
            "sObjectName" : component.get("v.sObjectName"),
            "accountId" : accountId,
            "gridType" : gridType
        }, function(response){
            console.log('in callback');
            //let set = new Set();
            var sectionValues=[];
            var _status = response.getState();
            if(_status === 'SUCCESS'){
                var viewData = response.getReturnValue();
                console.log('listOfObjectData==>'+JSON.stringify(viewData));
                console.log('in success');
                component.set("v.listOfObjectData",viewData);
                for(var data in viewData){
                    console.log('viewData[data].listOfData==>'+viewData[data].listOfData);
                    if(viewData[data].listOfData === undefined || viewData[data].listOfData.length == 0){
                        console.log('this tab is empty : '+viewData[data].tab) 
                        viewData[data].isEmpty = true;
                        //var str = viewData[data].tab;
                        //var headerValue = helper.toUpper(str);
                        viewData[data].message = 'No '+viewData[data].tab+' for this product';
                        //viewData[data] = helper.handlePageMessage(component,'No '+viewData[data].tab+' for this product','No Results',_viewdata);
                    }
                }
                console.log('viewData====>'+JSON.stringify(viewData));
                viewData = helper.initialSortonTrimsandMolding(viewData, helper); // Added by MB - 08/07/18 - Bug 63809 - To default Color sort for Trims and Molding
                component.set("v.listOfObjectData",viewData);
            }
        });
        
    },
    doSort: function(component, event, helper) {
        
        var _tab = event.getSource().get("v.name");
        var _data = component.get("v.listOfObjectData");
        var _sortField = event.getSource().get("v.value");
        var _listOfData;
        var viewData;
        for(var key in _data){
            if(_data[key].tab==_tab){
                viewData =  _data[key];
                
                var _currentSortField = viewData.sortField;
                var _currentSortOrder = viewData.sortOrder;
                var _sortOrder = (_sortField == _currentSortField && _currentSortOrder == 'asc')?'desc':'asc';
                viewData.sortField= _sortField;   
                viewData.sortOrder= _sortOrder;
                _listOfData = viewData.listOfData;
            }
        }
        console.log('Tab Selected: ' + _tab);
        console.log('Sort Field: ' + _sortField);
        //do sorting
        /* Start of Code - MB - 08/06/18 - Bug 63809 */
        if(_tab === 'Trims and Molding' && _sortField === 'Colors'){
            viewData.sortField = _sortField = 'Product Color Number';
            if(!$A.util.isUndefinedOrNull(viewData.sortField)){
                _listOfData.sort(function(a, b) {
                    var val1 = helper.getFieldValue(a,viewData.sortField);
                	var val2 = helper.getFieldValue(b,viewData.sortField);
                    if(viewData.sortOrder == 'asc'){
                    	return val1 - val2;
                    }else{
						return val2 - val1;                        
                    }
                    
                });
            }
        }
        /* End of Code - MB - 08/06/18 - Bug 63809 */
        if(!$A.util.isUndefinedOrNull(viewData.sortField) && _sortField !== 'Product Color Number'){
            _listOfData.sort(function(a, b) {
                var val1 = helper.getFieldValue(a,viewData.sortField);
                var val2 = helper.getFieldValue(b,viewData.sortField);
                
                if ($A.util.isUndefinedOrNull(val1) || val1 < val2) {
                    if (viewData.sortOrder == 'asc')
                        return -1;
                    else {
                        return 1;
                    }
                }
                
                if ($A.util.isUndefinedOrNull(val2) || val1 > val2) {
                    if (viewData.sortOrder == 'asc')
                        return 1;
                    else {
                        return -1
                    }
                }
                return 0;
            });
        }
        /* Start of Code - MB - 08/06/18 - Bug 63809 */
        if(_tab === 'Trims and Molding' && _sortField === 'Product Color Number'){
            viewData.sortField = _sortField = 'Colors';
        }
        /* End of Code - MB - 08/06/18 - Bug 63809 */
        for(var key in _data){
            if(_data[key].tab==viewData.tab){
                _data[key] =   viewData;
            }
        }
        component.set("v.listOfObjectData", _data);
    },
    

    
})