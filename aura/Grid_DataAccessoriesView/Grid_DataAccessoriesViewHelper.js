({
    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.mydata");
        var reverse = sortDirection !== 'asc';
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.mydata", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
    getFieldValue : function(data, field){
        var _fieldArray = [];
        if(field.indexOf('.')){
            while(field.indexOf('.') > -1){
                _fieldArray = field.split('.');
                data = data[_fieldArray[0]];
                field = _fieldArray[1];
            }
        }
        return data[field];;
    },
    toUpper : function(str) {
        return str.toLowerCase().split(' ').map(function(word) {
            return word[0].toUpperCase() + word.substr(1);
        }).join(' ');
    },
    /* Start of Code - MB - 08/07/18 - Bug 63809 */
    initialSortonTrimsandMolding : function(data, helper){
        var _tab = 'Trims and Molding';
        var _data = data;
        var viewData;
        var _listOfData;
        var trims_molding = false;
        
        for(var key in data){
            if(data[key].tab === _tab){
                viewData =  data[key];
                trims_molding = true;
                break;
            }
        }
        if(trims_molding){
            viewData.sortField = 'Product Color Number';
            _listOfData = viewData.listOfData;
            console.log('viewData.listOfData: ' + JSON.stringify(viewData.listOfData));
            if(!$A.util.isUndefinedOrNull(viewData.sortField)){
                _listOfData.sort(function(a, b) {
                    var val1 = helper.getFieldValue(a,viewData.sortField);
                    var val2 = helper.getFieldValue(b,viewData.sortField);
                    return val1 - val2;
                });
            }
            viewData.sortField = 'Colors';
            viewData.sortOrder = 'asc';
            for(var key in data){
                if(data[key].tab === _tab){
                    data[key] = viewData;
                }
            }
        }
        return data;
    }
    /* End of Code - MB - 08/07/18 - Bug 63809 */
})