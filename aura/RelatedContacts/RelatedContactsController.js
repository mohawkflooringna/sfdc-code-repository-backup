({
    doInit: function (component, event, helper) {
        helper.initData(component);
    },
    //set all selected status
    selectAll: function (cmp, evt, helper) {
        var contactList = cmp.get("v.contactList");
          /*  var selectAll = cmp.get("v.selectContactsAllChecked");
        cmp.set("v.selectContactsAllChecked", "checked");*/


        if(evt.currentTarget.checked){
            for (var i = 0, len = contactList.length; i < len; i++) {
                contactList[i].isSelect = true;
            }

        }else{
            for (var i = 0, len = contactList.length; i < len; i++) {
                contactList[i].isSelect = false;
            }

        }
        cmp.set("v.contactList", contactList);
        /*for(var i=0;i<$('input[name="options"]').length; i++){
            $('input[name="options"]')[i].checked = evt.currentTarget.checked;
        }*/
        /*if (selectAll == '') {
            cmp.set("v.selectContactsAllChecked", "checked");
            for (var i = 0, len = contactList.length; i < len; i++) {
                contactList[i].isSelect = true;
            }
            cmp.set("v.contactList", contactList);
        } else {
            cmp.set("v.selectContactsAllChecked", "")
            // cmp.set("v.isChecked", "");
            for (var i = 0, len = contactList.length; i < len; i++) {
                contactList[i].isSelect = false;
            }

            cmp.set("v.contactList", contactList);

        }*/
    },
    // set unselect status
    setSelectedContacts: function (cmp, evt, helper) {
        cmp.set("v.selectContactsAllChecked", "");

       var val = cmp.get('v.checkedList');
       var aid = evt.currentTarget.dataset.id;
       val.push(aid);
       cmp.set('v.checkedList', val);
    },
    saveToBackend: function (component, event,helper) {

        var eventId = component.get("v.recordId");
        var contactList = component.get("v.contactList");
        console.log(contactList);
        console.log('savetoBackend')
       //  var selectedList = [];
       //  selectedList = component.get('v.checkedList')
       // /* for (var i = 0, len = contactList.length; i < len; i++) {
       //      if (contactList[i].isSelect === true && contactList[i].isExistingDataInDB !== true) {
       //          selectedList.push(contactList[i].id);
       //      }
       //  }*/
       //  var unselectedList = [];
       //  unselectedList = component.get('v.uncheckedList');
       //
       //  //console.log(selectedList);
        var action = component.get("c.updateEventRelations");
        action.setParams({
            "eventId": eventId,
            "viewModel": JSON.stringify(contactList)

        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                //console.log(response.getReturnValue())
                //errors in this section
                 //component.set("v.contactList", response.getReturnValue());
                 //console.log(response.getReturnValue());

                /*var dismissActionPanel = $A.get("e.force:closeQuickAction");
                 dismissActionPanel.fire();
                 $A.get('e.force:refreshView').fire();*/
                var newselected = [];
                $A.util.removeClass(component.find('relsuccessmsg'), 'slds-hide');
                window.setTimeout(
                    $A.getCallback(function() {
                        $A.util.addClass(component.find('relsuccessmsg'), 'slds-hide');
                    }), 4000);
                /*setTimeout(function(){
                    $A.getCallback(function() {});
                    $A.util.addClass(component.find('relsuccessmsg'), 'slds-hide');
                },3000);
*/

                helper.initData(component);

                component.set('v.checkedList', newselected);
                component.set('v.uncheckedList', newselected);
            }
        });
        $A.enqueueAction(action);
        // for(var i = 0, len = product.length; i < len; i++ ) {
        // 	// if(product[i].isSelected === true) {
        // 		product[i].Id = product[i].pr.Id;
        // 		// delete product[i].isSelected;
        // 		// delete product[i].pr;
        // 	// } else {
        // 	// 	product.splice(i, 1);
        // 	// }
        // }
        // console.log(contactList)


    },
    // click unabled checked box
    returnFalse: function (cmp, evt, helper) {
        return false;
    },
    deleteContract: function (cmp, evt, helper) {
        var id = evt.currentTarget.dataset.id,
            index = evt.currentTarget.dataset.index,
            contractList = cmp.get("v.contactList");
            contractList[index].isSelect  = !contractList[index].isSelect;
            cmp.set('v.contractList', contractList);
      


       /* var contractList = cmp.get('v.contactList'),
            id=evt.currentTarget.dataset.id,
            target = null;

        for (var i=0,len=contractList.length; i<len; i++){
            if(contractList[i].id == id){
                target = contractList[i];
            }
        }
        if(target){
            target.sobjectType = 'Contact';

            console.log(target);

            helper.deleteContract(cmp, target);
        }*/
    },
    sortByName:function(cmp, evt, helper){
        evt.stopPropagation();

        var result = cmp.get('v.contactList'),
            container = cmp.find("sort3"),
            newSortOrder = cmp.get('v.sortOrder') === 'asc' ? 'desc' : 'asc',
            icon = newSortOrder === 'asc' ? 'arrowdown' : 'arrowup';

        $A.createComponent("c:svgIcon", {
                class: "slds-is-sortable__icon",
                svgPath: cmp.get('v.iconPath') + icon,
                category: "utility",
                size: "x-small",
                name: icon
            },
            function(element) {
                container.set("v.body", [element]);
            }
        );
        cmp.set('v.sortOrder', newSortOrder);

        //sort function
        if(newSortOrder === 'asc'){
            result.sort(function(a, b){
                if(a.name < b.name) return -1;
                if(a.name > b.name) return 1;
                return 0;
            });
        }else{
            result.sort(function(a, b){
                if(a.name > b.name) return -1;
                if(a.name < b.name) return 1;
                return 0;
            });
        }
        cmp.set('v.contactList', result);
    },
    goBack:function(cmp, evt, helper){
        cmp.set('v.isVisible',false);
    }

})