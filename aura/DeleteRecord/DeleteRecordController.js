({
    doInit : function(component, event, helper){
        var navService = component.find("navService");
        var recordId = component.get('v.recordId');
        var sObjectName = component.get('v.sObjectName');
        var pageReference = '';
        var action = component.get("c.checkDeleteAccess");
        if(sObjectName === 'Account'){            
            // Sets the route to /lightning/o/Account/home
            pageReference = {
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: 'Account',
                    actionName: 'home'
                }
            };
        }else if(sObjectName === 'Contact'){
            // Sets the route to /lightning/o/Contact/home
            pageReference = {
                type: 'standard__objectPage',
                attributes: {
                    objectApiName: 'Contact',
                    actionName: 'home'
                }
            };
        }
        component.set("v.pageReference", pageReference);
        // Set the URL on the link or use the default if there's an error
        var defaultUrl = "#";
        navService.generateUrl(pageReference)
        .then($A.getCallback(function(url) {
            component.set("v.url", url ? url : defaultUrl);
        }), $A.getCallback(function(error) {
            component.set("v.url", defaultUrl);
        }));
        action.setParams({
            'recordId': recordId,
            'objName': sObjectName
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (component.isValid() && state === "SUCCESS") {
                console.log( result );
                component.set('v.deleteAccess', result);                
            }else{
                console.log('Error occurred. Please try again or contact System Administrator');
            }
        });
        $A.enqueueAction(action);
    },

    handleOnOKClick : function ( component, event, helper ){
        var action = component.get("c.deleteRecord");
        var recordId = component.get('v.recordId');
        action.setParams({
            'recordId': recordId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                //Redirect
                var navService = component.find("navService");
                var pageReference = component.get("v.pageReference");
                event.preventDefault();
                navService.navigate(pageReference);
            }else{
                console.log('Error occurred. Please try again or contact System Administrator');
            }
        });
        $A.enqueueAction(action);
    },
    closeAlertMsg: function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire()
    }
})