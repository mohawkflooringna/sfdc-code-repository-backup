({
    setPicklistValues : function(component, event, helper){
        var action = component.get("c.getPicklistValues");
            action.setCallback(this, function (response) {
                var state = response.getState();
                var result = response.getReturnValue();
                console.log(state);
                var options = [];
                var selected = false;
                if (component.isValid() && state === "SUCCESS") {
                    component.set('v.addReplList', result.addReplList);
                    component.set('v.indExpList', result.indExpList);
                    console.log('v.indExpList' + result.indExpList);
                    component.set('v.indExpSelected', component.get('v.User.Industry_Experience__c'));
            	    component.set('v.addReplSelected', component.get('v.User.Addition_or_Replacement__c'));
                  
                }
            });
            $A.enqueueAction(action);     
    }
    
})