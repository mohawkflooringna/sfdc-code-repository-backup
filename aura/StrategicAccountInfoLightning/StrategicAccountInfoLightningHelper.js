({
	initHelperMethod : function(component, event, helper) { 
        try{
            var recordId = component.get("v.recordId");
            var action = component.get("c.IsCurrentUseAdmin");
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    component.set("v.isCurrentUserAdmin", response.getReturnValue());
                }
            });
            $A.enqueueAction(action);
            
            var action1 = component.get("c.getMVPList");
            action1.setCallback(this, function(response) {
                var state = response.getState();
                var selected = false;
                if (component.isValid() && state === "SUCCESS") {
                    var MVPCoordinator =[];
                    console.log('MVP:' + JSON.stringify(component.get('v.SelectedMVPCoordinator')));
                    console.log('return:' + JSON.stringify(response.getReturnValue()));
                    for(var i=0;i<response.getReturnValue().length;i++){
                        selected = "false";
                        if(component.get('v.SelectedMVPCoordinator') === response.getReturnValue()[i]){
                            selected = "true";
                            console.log('same entry:' + selected);
                        }
                        MVPCoordinator.push({
                            Value:response.getReturnValue()[i],
                            Name:response.getReturnValue()[i],
                            selected : selected
                        });
                        
                    }
                    /*MVPCoordinator.push({
                            Value:'',
                            Name:'None',
                            isSelected : selected
                        });*/
                    
                    component.set("v.MVPCoordinator", MVPCoordinator);
                    console.log('MVPList:' + JSON.stringify(MVPCoordinator));
                }
            });
            $A.enqueueAction(action1);
    
            var isLockAction = component.get("c.isAccountLocked");
            isLockAction.setParams({ "id" : component.get("v.recordId") });
            isLockAction.setCallback(this, function(response) {
                if (component.isValid() && response.getState() === "SUCCESS") {
                    var result = response.getReturnValue();
                    var recUi = event.getParam("recordUi");
                    if(recUi !== undefined){
                        var fields = recUi.record.fields;
                        //alert(JSON.stringify(fields.Strategic_Account__c));
                        console.log( ':::STATUS:::' + result );
                        if (fields.Strategic_Account__c.value || result === true ){ 
                            component.set("v.status", "view"); 
                        } 
                        else { 
                               component.set("v.status", "edit"); 
                               //component.set("v.isCurrentUserAdmin",true);//ignore admin if not strategic account
                             }
                        component.set('v.isStrategicAccount', fields.Strategic_Account__c.value);
                        if(fields !== undefined && 
                           (fields.Approval_Status__c.value === '' || fields.Approval_Status__c.value === undefined || 
                            fields.Approval_Status__c.value === 'None' || fields.Approval_Status__c.value === 'Rejected')){
                            component.set('v.submitForApproval', true);
                        }
                    }
                    component.set('v.isLocked', result);
                } else{
                    console.log('error');
                }
            });
            $A.enqueueAction(isLockAction);
        }catch(err){
            
        }
		 
	},
    ValidateContractDate : function(component,event,fields){
        var Contract_Start_date   = fields.Contract_Start_date__c;
        var Contract_End_Date   = fields.Contract_End_Date__c;
        if(Contract_Start_date != null && Contract_Start_date != '' &&
           Contract_End_Date != null && Contract_End_Date != '' &&
           Contract_End_Date < Contract_Start_date){
            //component.set('v.statusMessage', 'Contract End Date should be greater than Contract Start Date');
            component.set('v.isErrorContractDate', true);
            var errorMsg =component.get('v.contractErrorMsg');
            errorMsg.push('Contract End Date should be greater than Contract Start Date');
            component.set('v.contractErrorMsg', errorMsg);
            component.set('v.errorOccurred', true);
            return false;
            //component.set("v.toggleSpinner",false);
        }/*else if(Contract_Start_date === null || Contract_Start_date === '' || Contract_Start_date === undefined){
            component.set('v.isErrorContractDate', true);
            component.set('v.contractErrorMsg', 'Please enter Contract Start Date');
            return false;
            //component.set("v.toggleSpinner",false);
        }else if(Contract_End_Date === null || Contract_End_Date === '' || Contract_End_Date === undefined){
            component.set('v.isErrorContractDate', true);
            component.set('v.contractErrorMsg', 'Please enter Contract End Date');
            return false;
            //component.set("v.toggleSpinner",false);
        }*/
        return true;
    },
    ValidateRecord:function(component,event,fields){
        //alert(fields.Account_Geography__c);
        if( ( fields.Account_Geography__c === null || fields.Account_Geography__c === '' ) || 
            ( fields.Facilities__c === null || fields.Facilities__c === '') ||
            ( fields.Est_of_Orders_in_the_next_6_months__c === null || fields.Est_of_Orders_in_the_next_6_months__c === '' )
          ){        
            /*if (fields.Strategic_Account__c || component.get('v.isCurrentUserAdmin') == true) {
                if(component.get('v.isCurrentUserAdmin') == false){
                    component.set('v.statusMessage', 'You don\'t have permission to edit record');
					return false;
                }
            } else {
                component.set('v.statusMessage', 'No Strategic Account. You can not save the record.');
				return false;
            }
			return true;*/
            component.set('v.isErrorContractDate', true);
            component.set('v.contractErrorMsg', 'Please enter required fields');
            //component.set('v.errorOccurred', true);
            return false;
        }else{
            component.set('v.isErrorContractDate', false);
            component.set('v.contractErrorMsg', '');
        }
        return true;
    },
    getMVPInfoHelper : function(component, event,MVPCordinator,fields){
        var fieldAction = component.get("c.getMVPInfo");
        fieldAction.setParams({ "MVPName" : MVPCordinator });
        fieldAction.setCallback(this, function(response) {
            if ( component.isValid() && response.getState() === "SUCCESS" ) {
                component.set("v.MVPEmail",response.getReturnValue().MVP_Email__c);
                component.set("v.MVPPhone",response.getReturnValue().MVP_Phone__c);
                //component.set("v.account.MVP_Phone__c",response.getReturnValue().MVP_Phone__c);
                //component.set("v.account.MVP_Email__c",response.getReturnValue().MVP_Email__c);
                //component.set("v.toggleSpinner",false);                
            } else{
                //console.log('error');
                //component.set("v.toggleSpinner",false);
            }                
        });
        
        $A.enqueueAction(fieldAction);
    }
})