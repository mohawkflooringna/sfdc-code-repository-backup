({
    doInit : function(component, helper) {
        var obj = component.get('v.obj');
        var FieldName = component.get('v.fieldName');
        var outputText = component.find("outputTextId");
        var currencyFieldNames = component.get("v.currencyFieldNames");
        //console.log('### FieldName: ' + FieldName);
        if (FieldName.indexOf(".") >= 0) {
            
            var fieldValue;
            var ParentSobject = obj[FieldName.split(".")[0]];
            
            if(ParentSobject != undefined){
                //console.log('### FieldName: ' + FieldName);
                //console.log('### FieldName[0]: ' + FieldName.split(".")[0]);
                //console.log('### FieldName[1]: ' + FieldName.split(".")[1]);
                //console.log('### ---');
                fieldValue =  ParentSobject[FieldName.split(".")[1]];
            }
        }
        else{
            fieldValue = obj[FieldName];
        }
        
		if (FieldName == 'Selling Style' || FieldName == 'Account')
            component.set("v.thClass", "slds-text-align_left");
        else
            component.set("v.thClass", "slds-text-align_center");
        
        if ((typeof fieldValue) == 'number') {
            if (currencyFieldNames != null && currencyFieldNames.includes(FieldName)) {
                //component.set("v.thClass", "slds-text-align_right");
                var fname = FieldName.toLowerCase();
                
                if (fname == 'mtd sales' || fname == 'ytd sales' || fname == 'r12 sales') {
                    var dataString = '';
                    dataString = Math.round(fieldValue).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    outputText.set("v.value", '$' + dataString );
                }
                else {
		        	outputText.set("v.value", '$' + parseFloat(fieldValue).toFixed(2) );
                }
            }else {
		        outputText.set("v.value", parseFloat(fieldValue).toFixed(2) );
            }
        }
        else
	        outputText.set("v.value", fieldValue);
    }
})