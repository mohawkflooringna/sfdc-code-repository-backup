({
	itemSelected : function(component, event, helper) {
		helper.itemSelected(component, event, helper);
	}, 
    serverCall :  function(component, event, helper) {
        var formFieldMap = new Map();
        formFieldMap.newFilterVal = component.get('v.filterVal');
        console.log('newFilterVal: ' + formFieldMap.newFilterVal);
        component.set('v.formFieldMap', formFieldMap);
		helper.serverCall(component, event, helper);
	},
    clearSelection : function(component, event, helper){
        helper.clearSelection(component, event, helper);
    },
   closeMenu : function(component, event, helper) {
      // alert('entry');
       component.set("v.server_result",null);
    var comboParentDiv = document.getElementById('listbox-unique-id');
       if(comboParentDiv!=null)
    comboParentDiv.classList.remove('slds-is-open');
},
    handleNewRecordClick: function(component, event, helper) {
        event.preventDefault();
        event.stopPropagation();
        helper.addNewRecord(component, event, helper);
    },
})