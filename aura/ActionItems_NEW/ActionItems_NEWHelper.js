({
    
    getObjName: function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getObjectNameById");
        action.setParams({
            "recordId": recordId
        });
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.objName", response.getReturnValue());
                if (response.getReturnValue() == 'Event') {
                    component.set('v.isInEvent', 'true');
                    this.init(component, event, helper, response.getReturnValue());
                    
                } else {
                    this.init(component, event, helper);
                }
                //   this.retriveBrandSegment(component);
            }
        });
        
        $A.enqueueAction(action);
        
    },
     deviceOrentationHandler : function( component,event,helper ){
        //alert(window.orientation);
                    component.set("v.deviceOrentation",window.orientation);

       window.addEventListener("orientationchange", function() {
            // Announce the new orientation number
            //alert(window.orientation);
            //return window.orientation;
            component.set("v.deviceOrentation",window.orientation);
        }, false);
    },
    
    init: function(component, event, helper, objectName) {
        if (objectName == 'Event') {
            var action = component.get("c.getObjectRecord");
            var result;
            action.setParams({
                "apiName": 'Event',
                'recordId': component.get('v.recordId'),
                'fields': 'Id, WhatId, AccountId'
            });
            
            action.setCallback(this, function(response) {
                
                var state = response.getState();
                
                if (component.isValid() && state === "SUCCESS") {
                    result = response.getReturnValue();
                     // Begin of Changes - SS - 3/20/19 - 71906
                    if (result[0].AccountId !== undefined && (result[0].AccountId).substring(0, 3) == '001'){
                        component.set("v.whatId", result[0].AccountId);                        
                        this.retrieveSAL(component, event, helper);
                     // End of Changes - SS - 3/20/19 - 71906
                    } else if (result[0].WhatId !== undefined &&  (result[0].WhatId).substring(0, 3) == '001') {
                        component.set("v.whatId", result[0].WhatId);                        
                        this.retrieveSAL(component, event, helper);
                    } else {
                        component.set('v.isEventAndNonAccountRecord', 'true');
                        console.log('Account is not available in Related-To');
                    }
                    this.retriveBrandSegment(component);
                }
            });
            $A.enqueueAction(action);
        } else {
            this.retrieveSAL(component, event, helper);
            this.retriveBrandSegment(component);
            
        }
        
    },
    
    getBrandsFromMat: function(component, accountId) {
    },
    
    retriveBrandSegment: function(component) {
        var action = component.get('c.getPicklistValue'); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = response.getReturnValue();
                var segmentList = [],
                    segment = result.Segement,
                    brandList = [],
                    brand = result.Brand; 
                var isEvent = component.get("v.isInEvent");
                /* if(!isEvent){
                                 brandList.push({
                                    label: 'All',
                                    value: 'All'
                                });
                      segmentList.push({
                                    label: 'All',
                                    value: 'All'
                                });
                            }*/
                for (var i = 0; i < segment.length; i++) {
                    if (isEvent) {
                        segmentList.push({
                            label: segment[i],
                            value: segment[i],
                            selected: true
                        });
                    } else {
                        segmentList.push({
                            label: segment[i],
                            value: segment[i]
                        });
                    }
                }
                
                //Calling Second Method
                var action2 = component.get('c.getBrandsFromMAT');
                action2.setParams({
                    "accountId": component.get("v.whatId")
                });
                action2.setCallback(this, function(response) {
                    var state = response.getState();
                    if (component.isValid() && state === "SUCCESS") {
                        if(brand.length>0){
                         var resultSelected = response.getReturnValue();
                        for (var i = 0; i < brand.length; i++) {
                            if (resultSelected == '' && isEvent) {
                                brandList.push({
                                    label: brand[i],
                                    value: brand[i],
                                    selected: true
                                });
                                
                            } else if (resultSelected != '' && isEvent ) {
                                if(resultSelected!=null && resultSelected.includes(brand[i])){
                                 //   alert('check include conditions brand i')
                                    brandList.push({ 
                                        label:brand[i], 
                                        value:brand[i],
                                        selected:true
                                        
                                    });
                                    
                                }
                                else{
                                    brandList.push({ 
                                        label:brand[i], 
                                        value:brand[i] 
                                    });
                                } 
                                
                            } else if (!isEvent) {
                                brandList.push({
                                    label: brand[i],
                                    value: brand[i]
                                });
                            }
                        }
                           
                        component.set("v.brandOptions", brandList);
                    }
                    }
                });
                $A.enqueueAction(action2);
                
                component.set("v.segmentOptions", segmentList);
               
            }
                                   
        });
        $A.enqueueAction(action);
           
    },
    retrieveSAL: function(component, event, helper, brandlist, seglist) {        
         var id = component.get("v.recordId");
         // Begin of Changes - SS - 3/20/19 - 71906
         if (id.substring(0, 3) != '001'){
            id =  component.get("v.whatId");
         }
        // console.log('test'+id);
        var action = component.get("c.retrieveSALs");
        if (!brandlist || brandlist.length == 0) {
            brandlist = [];
        }
        if (!seglist || seglist.length == 0) {
            seglist = [];
        }
        action.setParams({
            "accountId": id,
            "brandlist": brandlist,
            "seglist": seglist
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.actionItems", response.getReturnValue());
                component.set("v.myAppointmentList", response.getReturnValue().myAppointmentList);
            }
        });
        $A.enqueueAction(action);
    },
    
    
    updateInfo: function(cmp, evt) {
        var eventId = cmp.get("v.recordId");
        var eventActionId = cmp.get("v.actionItems.eventActionId");
        var appointmentCheckboxList = document.getElementsByClassName("appointment-checkbox");
        var attachmentList = document.getElementsByClassName("include-attachment");
        // console.log(attachmentList)
        var updateList = [];
        var insertList = [];
        var actionList = [];
        // objectForInsert is the object for add object to insertList
        var objectForInsert = new Object;
        objectForInsert.Action_List__c = '',
            objectForInsert.Include_Attachments_In_Email__c = '';
        objectForInsert.Event_Action__c = '';
        // objectForUpdate is the object for add object to updatetList
        var objectForUpdate = new Object;
        objectForUpdate.Action_List__c = '';
        objectForUpdate.Include_Attachments_In_Email__c = '';
        objectForUpdate.Event_Action__c = '';
        objectForUpdate.Id = '';
        
        for (var i = 0; i < appointmentCheckboxList.length; i++) {
            objectForInsert = {};
            objectForUpdate = {};
            if (appointmentCheckboxList[i].type == "checkbox") {
                if (appointmentCheckboxList[i].checked && appointmentCheckboxList[i].disabled !== true) {
                    actionList.push(appointmentCheckboxList[i].dataset.index);
                    objectForInsert.Action_List__c = appointmentCheckboxList[i].dataset.index;
                    objectForInsert.Event_Action__c = eventActionId;
                    // objectForInsert.Include_Attachments_In_Email__c = attachmentList[i].checked;
                    insertList.push(objectForInsert);
                    
                } else if (appointmentCheckboxList[i].disabled === true) {
                    
                    objectForUpdate.Action_List__c = appointmentCheckboxList[i].dataset.index;
                    // objectForUpdate.Include_Attachments_In_Email__c = attachmentList[i].checked;
                    objectForUpdate.Event_Action__c = eventActionId;
                    objectForUpdate.Id = appointmentCheckboxList[i].dataset.id;
                    updateList.push(objectForUpdate);
                }
            }
        }
        console.log('insertMyAppList', insertList, 'updateList', updateList);
        var action = cmp.get("c.createMyAppointmentList");
        action.setParams({
            "eventId": eventId,
            "actionList": actionList,
            "insertMyAppList": JSON.stringify(insertList),
            "updateMyAppList": JSON.stringify(updateList)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (cmp.isValid() && state === "SUCCESS") {
                
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(action);
    },
    sortData: function(component, field, data, innerList, setValueIn, sortVariable, sortFieldName) {
        var sortAsc = component.get("v." + sortVariable),
            sortField = component.get("v." + sortFieldName);
        sortAsc = sortField != field || !sortAsc;
        console.log(data);
        data.sort(function(a, b) {
            var t1 = a[innerList][field] == b[innerList][field],
                t2 = (!a[innerList][field] && b[innerList][field]) || (a[innerList][field] < b[innerList][field]);
            return t1 ? 0 : (sortAsc ? -1 : 1) * (t2 ? 1 : -1);
        });
        component.set("v." + sortVariable, sortAsc);
        component.set("v." + sortFieldName, field);
        component.set("v." + setValueIn, data);
        console.log(data);
    },
    
})