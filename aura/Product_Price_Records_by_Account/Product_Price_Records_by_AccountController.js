({
   	handleChartBarClicked : function(component, event, helper) {
		component.set("v.region", event.getParam("region"));
		component.set("v.district", event.getParam("district"));
		component.set("v.territory", event.getParam("territory"));
		component.set("v.timePeriod", event.getParam("timePeriod"));
		component.set("v.category", event.getParam("category"));

		component.set("v.priceLevelClicked", event.getParam("priceLevelClicked"));
		component.set("v.sourceChartId", event.getParam("sourceChartId"));

        var timestamp = Date.now();
		component.set("v.timeStamp", timestamp);
		        
        console.log('### application handling the events for ' + component.get("v.priceLevelClicked") + 
                    ' comming from chart ' + component.get("v.sourceChartId") + 
                    ' with territory: ' + component.get("v.territory") + 
                    ', category: ' + component.get("v.category") + 
                    ', timePeriod: ' + component.get("v.timePeriod") );
	},
    backToRecord: function(component, event, helper){
        window.history.back();
    },    
})