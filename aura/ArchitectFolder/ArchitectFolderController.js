({
    initialize : function(component, event, helper) {
        helper.getAccountInfo(component, event);
    },
    
    handlefieldValueChange: function(component, event, helper){
        var acc = component.get('v.account');
        component.set('v.Last_Updated', acc.Last_Updated__c);
        console.log('Last Updated: ' + component.get('v.Last_Updated'));
    },
    
    handleListValueChange: function(component, event, helper){
        var selectedOptionValue = event.getParam("value");
        console.log('selectedOptions: ' + selectedOptionValue);
        component.set('v.updatedListOptions', selectedOptionValue);
    },
    
    save: function(component, event, helper){
        var selectedOptions = component.get('v.updatedListOptions');
        //if(selectedOptions.length>0){
        helper.saveRecord(component, selectedOptions);
        /*}else{
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
	        dismissActionPanel.fire();
        }*/
        console.log(component.get('v.updatedListOptions'));
    },
    
    cancel: function(component, event, helper){
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
    closeAlertMsg: function(component, event, helper){
        component.set("v.isErrorOccurred",false);
    }
    
})