({
    getAccountInfo: function(component, event){
        debugger;
        var recordId = component.get('v.recordId');
        var action = component.get("c.getAccountInfo");
        action.setParams({'accId': recordId});  
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (component.isValid() && state === "SUCCESS") {
                var optionsSelected = [];
                var options = [];
                var result = response.getReturnValue();
                var acc = result.account;
                console.log('Account:  ' + JSON.stringify(result.account));
                component.set('v.account', acc);
                var archFolder = result.ArchTectPickList;
                for (var i = 0; i < archFolder.length; i++) {
                        options.push({
                            label: archFolder[i],
                            value: archFolder[i],
                        });
                }
                component.set('v.listOptions', options);
                console.log('Account Last Updated: ' + component.get('v.account.Last_Updated__c'));
                var ldate = component.get('v.account.Last_Updated__c');
                if(ldate != undefined){
                    var date = new Date(ldate);
                    var lastUpdated = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
                    component.set('v.Last_Updated', lastUpdated);
                }
                console.log('Account Arch Folder:  ' + JSON.stringify(acc.Architech_Folder__c));
                options = [];
                if(acc.Architech_Folder__c != undefined){
                    optionsSelected = acc.Architech_Folder__c.split(';');
                    optionsSelected.forEach(function(element) {
    					options.push(element);
					});
                    console.log('Arch Folder:  ' + JSON.stringify(options));
                    component.set('v.selectedOptions', options);
                    console.log('Selected Options: ' + options);
                }
            }else{
                alert(state);
            }
        });
        $A.enqueueAction(action);
    },
    
    saveRecord: function(component, selectedOptions){
        var recordId = component.get('v.recordId');
        var account = component.get('v.account');
        var action = component.get("c.updateArchitectFolder");
        var device = component.get('v.device');
        debugger;
        action.setParams({'acc': account,
                          'archFolders' : selectedOptions,
                          'device' : device});  
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            var err = response.getReturnValue();
            console.log('response: ' + response.getReturnValue());
            if (component.isValid() && state === "SUCCESS") {
                if(err.error === false){
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                    this.showMyToast(component);
                }else if(err.error){
                    component.set('v.isErrorOccurred', err.error);
                    component.set('v.errorMessage', err.errorMsg);
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    showMyToast : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            type: 'success',
            mode: 'dismissible',
            message: 'Architect Folder has been updated successfully.'
        });
        toastEvent.fire();
    }
})