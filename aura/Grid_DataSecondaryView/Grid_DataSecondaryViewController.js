({
    doInit : function(component, event, helper) {
        console.log('in init');
        console.log('Sobject name==>'+component.get("v.sObjectName"));
        console.log('Product Id==>'+component.get("v.viewData.productId"));
        console.log('productCategoryId-->' + component.get("v.productCategoryId") );
        console.log('recordId-->' + component.get("v.recordId") );
        console.log('sObjectName-->' + component.get("v.sObjectName") );
        
        helper.doCallout(component, "c.fetchSecondaryDisplayData", {
            "recordId" : component.get("v.recordId"),
            "sObjectName" : component.get("v.sObjectName"),
            "productCategoryId" : component.get("v.productCategoryId"),
            "warehouseValue" : component.get("v.warehouseValue"),
            "AccountId" : component.get("v.accountId"),
            "productId" : component.get("v.productId"), //Added by MB - 07/30/18 - Bug 63005
            "gridType" : component.get("v.gridType")		//Added by MB - 07/30/18 - Bug 63005
        }, function(response){
            console.log('in callback');
            //let set = new Set();
            var sectionValues=[];
            var _status = response.getState();
            if(_status === 'SUCCESS'){
                //console.log('in success');
                var _resp = response.getReturnValue();
                
                for(var datakey in _resp){
                    //console.log(' doInit - key==>'+datakey+"value==>"+JSON.stringify(_resp[datakey]));
                    for(var data in _resp[datakey]){
                        if(data=='hasAccessories' && _resp[datakey][data]){
                            component.set("v.hasAccessories", true);
                        }
                        if(data=='listOfSecondaryFields'){
                            //console.log('in first loop');
                            for(var secKey in _resp[datakey][data]){
                                //console.log('in 2nd loop key===>'+secKey);
                                for(var fieldKey in _resp[datakey][data][secKey]){
                                    if(fieldKey=='field'){
                                        for(var fieldarr in _resp[datakey][data][secKey][fieldKey]){
                                            if(fieldarr=='Modal_Section__c'){
                                                //console.log('Sections==>'+_resp[datakey][data][secKey][fieldKey][fieldarr])
                                                var val = _resp[datakey][data][secKey][fieldKey][fieldarr];
                                                sectionValues.push(val);
                                            }
                                        }
                                    }   
                                }
                            } 
                        }
                    }
                }
                var uniqueSection=[];
                for (var dup in sectionValues){
                    if(uniqueSection.indexOf(sectionValues[dup])<0){
                        uniqueSection.push(sectionValues[dup]);
                    }
                }
                for (var i = 0; i<_resp.length;i++){
                    var singleItem = _resp[i];
                    singleItem.isExpandable = true;
                    singleItem.section = uniqueSection[i]; 
                }
                //console.log('set value for section ==>'+JSON.stringify(uniqueSection));
                //console.log('Display Data==>'+JSON.stringify(_resp));
                component.set("v.sections",uniqueSection);
                component.set("v.viewData", _resp);
            }
        });
    },
    
    sectionClick : function(component, event, helper) {
        var value = event.getSource().get("v.name");
        if(value)
            event.getSource().set("v.name",false);
        else
            event.getSource().set("v.name",true);   
    },
    doReportsAction: function(component, event, helper) {
        var _prodCatName = component.get('{!v.productCategoryName}');
        var _sellingStyleNum = component.get('{!v.sellingStyleNum}');
        var _sellingStyleName = component.get('{!v.sellingStyleName}');
        var _inventoryStyleNum = component.get('{!v.inventoryStyleNum}');
        var _inventoryStyleName = component.get('{!v.inventoryStyleName}');
        
        console.log('### doReportsAction - _inventoryStyleNum: ' + _inventoryStyleNum);
        console.log('### doReportsAction - _inventoryStyleName: ' + _inventoryStyleName);
        
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:Product_Price_Records",
            componentAttributes: {
                category : _prodCatName,
                sellingStyleNum : _sellingStyleNum,
                sellingStyleName : _sellingStyleName,
                inventoryNum : _inventoryStyleNum,
                inventoryName : _inventoryStyleName,
                timeStamp : Date.now()
            }
        });
        evt.fire();
        
    }, 
    doViewAccesoriesAction: function(component, event, helper) {
        
        var productCategoryId = component.get('v.productCategoryId');
        var recordId = component.get('v.recordId');
        var sObjectName = component.get('v.sObjectName');
        var _prodCatName ;
        if (_prodCatName == 'Rev Wood') _prodCatName = 'RevWood';
        if (_prodCatName == 'Tec Wood') _prodCatName = 'TecWood';
        if (_prodCatName == 'Solid Wood') _prodCatName = 'SolidWood';
        
        var accountId = component.get('v.accountId');
        var gridType = component.get('v.gridType');
        
        console.log('### calling accesory with gridType: ' + gridType);
        console.log('### calling accesory with accountId: ' + accountId);
        
        var modalBody;
        $A.createComponent("c:Grid_DataAccessoriesView", {
            "recordId": recordId,
            "productCategoryId": productCategoryId,
            "sObjectName": sObjectName,
            "gridType" : gridType,
            "accountId" : accountId
        },
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: "Accessories",
                                       body: modalBody,
                                       showCloseButton: true,
                                       cssClass: "my-modal"
                                   })
                                   
                               }
                           });
                               
    },
    doMohawkExchangeAction: function(component, event, helper) {
        var accountId = component.get('v.accountId');
        var recordId = component.get('v.recordId');
        var sObjectName = component.get('v.sObjectName');
        var gridType = component.get('v.gridType');
        var productCategoryId = component.get('v.productCategoryId');
        
        if(accountId==null || accountId==undefined){
            accountId='NA';
        }
        
        helper.doCallout(component, "c.getProductId", {
            "recordId": recordId,                
            "sObjectName": sObjectName
        }, function(response){
            console.log('in mohawk xchange call');
            //let set = new Set();
            var sectionValues=[];
            var _status = response.getState();
            if(_status === 'SUCCESS'){
                
                var productId = response.getReturnValue();
                console.log('in return statement from call'+productId);
                var productCategoryId = productCategoryId;
                var modalBody;
                console.log('gridtype===>'+gridType);
                var mashupName;
                if(gridType=='Customer_Price_List')
                    mashupName = 'CPL Grid';
                if(gridType=='Price_Grid')
                    mashupName = 'Price Grid';
                if(gridType=='Buying_Group_Grid')
                    mashupName = 'BG Grid';
                if(gridType=='Merchandizing_Vehicle_Price_List')
                    mashupName = 'MVPL Grid';
                
                $A.createComponent("c:mashupTabItem", {
                    "mashupName": mashupName,
                    "recordId": accountId,
                    "isFromRecordPage":true,
                    "gridProductId": productId
                },
                                   function(content, status) {
                                       if (status === "SUCCESS") {
                                           
                                           modalBody = content;
                                           component.find('overlayLib').showCustomModal({
                                               header: mashupName,
                                               body: modalBody,
                                               showCloseButton: true,
                                               cssClass: "my-modal"
                                           })
                                           
                                       }
                                   });
            }
        });        
    },
    backToRecord: function(component, event, helper){
            window.history.back();
    }
})