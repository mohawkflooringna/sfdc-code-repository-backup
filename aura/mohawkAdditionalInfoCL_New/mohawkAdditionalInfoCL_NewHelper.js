({
    //ADDED BY MUDIT - 02-11-2017
    navigateToMashup : function( component,Id,mashup ) {        
        /******* PREVIOUS CODE *******
        var container = component.find("MashupDiv");
        $A.createComponent(
            "c:mashupTabItem",
            {
                mashupName: mashup,
                recordId: Id,
                isFromRecordPage: true
               // iFrameWidth: 350 // ADDED BY MUDIT - PASSING IFRAME WIDTH
            },
            function(cmp) {
                var cmpMain = component.find("mainDiv");
                console.log( cmpMain );
				$A.util.addClass(cmpMain, 'slds-hide');                   
                container.set("v.body", [cmp]);
            }
        );      
        *******/
        //$A.util.removeClass(component.find("iframeBack"), 'slds-hide');   
        //$A.util.addClass(component.find("iframeBack"), 'slds-show');
        component.set("v.iframeCmpCheck",true);
        //component.find( "iframeCmp" ).set("v.recordId", Id );
        //component.find( "iframeCmp" ).set("v.mashupName", mashup );
        //component.set("v.mashupNameURL",mashup);
        //var cmpMain = component.find("mainDiv");
        //$A.util.addClass(cmpMain, 'slds-hide');
    },
    //ADDED BY MUDIT - 02-11-2017
    navigateToOpenLE : function( component,Id,componentName ) {        
        var container = component.find("openLEDiv");
        $A.createComponent(
            "c:"+componentName,
            {                
                recordId: Id,
                isFromRecordPage: true,
                gridType:'Customer_Price_List',
                accountId :component.get('v.recordId')
                
            },
            function(cmp) {
                var cmpMain = component.find("mainDiv");
                console.log( cmpMain );
				$A.util.addClass(cmpMain, 'slds-hide');                
                container.set("v.body", [cmp]);
            }
        );        
    }
})