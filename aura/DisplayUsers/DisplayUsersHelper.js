({
    /*getInitialized: function(component, event){
        var action = component.get("c.getUserInitialize");
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set('v.user', response.getReturnValue());
            }else{
                console.log('error');
            }
        });
        $A.enqueueAction(action);
    },*/
    
    getStatePickListvalues : function (component, event, helper) {
        var options = [];
        var action = component.get("c.getStatePickListvalues");
        // action.setParams({'Name': name, 'Territory': territory});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log(response.getReturnValue());
                options.push({
                    label: "--None--",
                    value: ""
                });
                for (var i = 0; i < response.getReturnValue().length; i++) {
                    options.push({
                        label: response.getReturnValue()[i],
                        value: response.getReturnValue()[i]
                    });
                }
                component.set('v.stateList', options);
            }
        });
        $A.enqueueAction(action);
    },
    getMajorMetorsPickListvalues : function (component, event, helper) {
        var options = [];
        var action = component.get("c.getMajorMetorsPickListvalues");
        // action.setParams({'Name': name, 'Territory': territory});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log(response.getReturnValue());
                options.push({
                    label: "--None--",
                    value: ""
                });
                for (var i = 0; i < response.getReturnValue().length; i++) {
                    options.push({
                        label: response.getReturnValue()[i],
                        value: response.getReturnValue()[i]
                    });
                }
                component.set('v.metrosList', options);
            }
        });
        $A.enqueueAction(action);
    },
    
    getUserList: function(component, event){
        component.set('v.statusMessage', '');
        var areaCode = component.find('area-code').get('v.value');
        var name = component.find('input-name').get('v.value');
        //var stateVal=component.get('v.selectedStates');
        //var state_des=stateVal.toString();
        var user = component.get('v.user');
        var state = component.find('stateId').get('v.value');
        //var state = '';
        // alert(state.toString());
        var metro = component.find('metroId').get('v.value');
        var corpUser = component.find('corpUser').get('v.checked');
        //var corpUser = document.getElementById('corpUser').checked;
        component.set('v.userList', []); // Added by MB - 09/07/17
        var action = component.get("c.getUserInfo");
        //console.log(user.State__c);
        //state = user.State__c;
        action.setParams({'Name': name, 
                          'AreacodeValue': areaCode, 
                          'StateValue': state, 
                          'Majormetrosvalue': metro,
                          'CorporateUser': corpUser});
        console.log(action.getParams());
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log(response.getReturnValue());
                if(response.getReturnValue().length > 0){
                    component.set('v.userList', response.getReturnValue());
                }else if(response.getReturnValue().length === 0){
                    component.set('v.statusMessage', 'No records found.');
                }
                console.log(component.get('v.statusMessage'));
            }else{
                alert('Not able to fetch users. Please try again.');
            }
        });
        $A.enqueueAction(action);
    }
})