({
    initHelperMethod : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.IsCurrentUseAdmin");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.isCurrentUserAdmin", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
        
        var action = component.get("c.getMVPList");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.MVPCoordinator", response.getReturnValue());
            }
        });
        $A.enqueueAction(action); 
        
        var action = component.get("c.getAccount");
        action.setParams({ "id" : component.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {                
                component.set("v.account",response.getReturnValue());                
                var selectedLookUpRecord = component.get("v.selectedLookUpRecord");
                selectedLookUpRecord.Id = response.getReturnValue().Real_Estate_Service_Provider__c; 
                if( response.getReturnValue().Real_Estate_Service_Provider__c != undefined ){
                    selectedLookUpRecord.Name = response.getReturnValue().Real_Estate_Service_Provider__r.Name;
                }
                /**
                var ServiceNeeds = response.getReturnValue().Service_Needs__c;                
                var PurchasingPlans = response.getReturnValue().Purchasing_Plans__c;
                if( ServiceNeeds != null && ServiceNeeds != '' ){
                    if( ServiceNeeds.split(";").length > 0 ){
                        component.set("v.SelectedServiceNeeds",ServiceNeeds.split(";").join(","));    
                    }
                }
                if( PurchasingPlans != null && PurchasingPlans != '' ){
                    if( PurchasingPlans.split(";").length > 0 ){
                        component.set("v.SelectedServiceNeeds",PurchasingPlans.split(";").join(","));   
                    }
                }
                this.multiPicklistHelperMethod( component, event, helper, 'Purchasing_Plans__c',response.getReturnValue().Purchasing_Plans__c );
        		this.multiPicklistHelperMethod( component, event, helper, 'Service_Needs__c',response.getReturnValue().Service_Needs__c );
                **/
                component.set("v.openNow", true);                
                //End Mohan Chander US-51134/Bug-49897 
            }
        });
        $A.enqueueAction(action);
        
        var isLocked = false;        
        var lAction = component.get("c.isAccountLocked");
        lAction.setParams({ "id" : component.get("v.recordId") });
        lAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var result = response.getReturnValue();
                console.log( ':::STATUS:::' + component.get("v.account.Strategic_Account__c") );
                console.log( ':::STATUS:::' + result );
                if (component.get("v.account.Strategic_Account__c") || result==true) { 
                    component.set("v.status", "view");
                } else {
                    component.set("v.status", "edit");
                }
                isLocked = result;
                //component.set('v.isAccountLoaded', true);
                component.set('v.isLocked', isLocked);
                console.log( ':::STATUS INIT ::::' + component.get("v.status") );
            } else
                console.log('error')
                });
        $A.enqueueAction(lAction);
    },
    getMVPInfoHelper : function(component, event){
        var fieldAction = component.get("c.getMVPInfo");
        fieldAction.setParams({ "MVPName" : event.getSource().get("v.value") });
        fieldAction.setCallback(this, function(response) {
            if ( component.isValid() && response.getState() === "SUCCESS" ) {
                component.set("v.account.MVP_Phone__c",response.getReturnValue().MVP_Phone__c);
                component.set("v.account.MVP_Email__c",response.getReturnValue().MVP_Email__c);
                component.set("v.toggleSpinner",false);                
            } else{
                console.log('error');
                component.set("v.toggleSpinner",false);
            }                
        });
        
        $A.enqueueAction(fieldAction);
    },
    validateContractDate: function(component, event, helper){
        var Contract_Start_date   = component.get("v.account.Contract_Start_date__c");
        var Contract_End_Date   = component.get("v.account.Contract_End_Date__c");
        if(Contract_Start_date != null && Contract_Start_date != '' &&
           Contract_End_Date != null && Contract_End_Date != '' &&
           Contract_End_Date < Contract_Start_date){
            //component.set('v.statusMessage', 'Contract End Date should be greater than Contract Start Date');
            component.set('v.isErrorContractDate', true);
            component.set('v.contractErrorMsg', 'Contract End Date should be greater than Contract Start Date');
            component.set('v.errorOccurred', true);
            component.set("v.toggleSpinner",false);
        }else{
            component.set('v.errorOccurred', false);
            component.set("v.toggleSpinner",false);
        }
        if((Contract_Start_date == null || Contract_Start_date == '') &&
           Contract_End_Date != null && Contract_End_Date != ''){
            component.set('v.isErrorContractDate', true);
            component.set('v.contractErrorMsg', 'Please enter Contract Start Date');
            component.set('v.errorOccurred', true);
            component.set("v.toggleSpinner",false);
        }
    },
    setStatusEmpty : function(component){
        component.set('v.statusMessage',  '');
        //Start of code - MB - 3/2/18 - Bug 55825
        component.set('v.isErrorContractDate', false);
        component.set('v.contractErrorMsg', '');
        component.set('v.errorOccurred', false);
        //End of code - MB - 3/2/18
    },
    isNull : function(text){// 03/18/2017
        return (text == undefined || text == '' ? true : false);
    },
    showErrorMessage : function(component, errorObj){
        var message = 'Unable to process your request.';
        if ( errorObj ) {
            if ( errorObj[0] && errorObj[0].fieldErrors ){
                var fErrors = errorObj[0].fieldErrors;
                console.log( fErrors );
                for( var field in fErrors ){   
                    console.log( field );
                    console.log(fErrors[field]);
                    message += ' Reason: Missing ' + field + ' - ' + fErrors[field][0].message;
                }
            }
            if (errorObj[0] && errorObj[0].pageErrors && !this.isNull(errorObj[0].pageErrors[0])) {
                if(!this.isNull(errorObj[0].pageErrors[0].message))
                    message += ' Reason: ' + errorObj[0].pageErrors[0].message;
            }
            component.set('v.statusMessage', message);
        } else {
            component.set('v.statusMessage', message);
        }
    },
    createApproval : function(component, account) {
        this.upsertApproval( component, account );
    },
    upsertApproval : function( component, account ){        
        var action = component.get("c.runApproval");
        action.setParams({
            "approvalAccount": account
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.statusMessage', 'Record submitted for Approval successfully');
                setTimeout(function(){
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                    $A.get('e.force:refreshView').fire();
                }, 1500);
            } else if (state === "ERROR") {
                this.showErrorMessage(component, response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    createVote : function(component, account) {
        this.upsertVote( component, account );
    },
    upsertVote : function(component, account ){
        var action = component.get("c.saveAccount");
        action.setParams({
            "existingAccount": account
        });
        //If upsert of the account is success I want to alert the account Id back to the user.
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.statusMessage', 'Record saved successfully');
                setTimeout(function(){
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                    $A.get('e.force:refreshView').fire();
                }, 1500);
            }
            else if (state === "ERROR") {
                this.showErrorMessage(component, response.getError());
            }
        });
        $A.enqueueAction(action);
    },
    multiPicklistHelperMethod : function(component, event, helper, fieldAPIName, previousVal) {
        var Action = component.get("c.populateMultiSelect");
        Action.setParams({ "fieldName" : fieldAPIName,"previousVal" : previousVal });
        Action.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                var result = response.getReturnValue();                
                console.log( result );
                if( fieldAPIName == 'Service_Needs__c' ){
                    component.set('v.ServiceNeeds', result);
                    component.set('v.toggleServiceNeeds', true);
                }
                if( fieldAPIName == 'Purchasing_Plans__c' ){
                    component.set('v.PurchasingPlans', result);
                    component.set('v.togglePurchasingPlans', true);
                }
                console.log( component.get("v.ServiceNeeds") );
                console.log( component.get("v.PurchasingPlans") );
            } else
                console.log('error')
                });
        $A.enqueueAction(Action);
    },
})