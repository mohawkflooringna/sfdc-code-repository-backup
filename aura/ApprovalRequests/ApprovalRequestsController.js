({
	doInit : function(component, event, helper) {
		helper.getWorkitemList(component, event);
	},
    openDetailPage: function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToSObject");
        urlEvent.setParams({
            "recordId" : event.currentTarget.dataset.id});
        urlEvent.fire();
  }
})