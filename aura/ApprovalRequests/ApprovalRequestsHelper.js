({
	getWorkitemList : function(component, event) {
		var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');
        $A.util.removeClass(spinner, 'slds-hide');
        var action = component.get('c.getApprovalWorkitems');
		action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === 'SUCCESS') {
                var resultList = response.getReturnValue();
                component.set('v.WorkitemList', resultList);
                console.log(component.get('v.WorkitemList'));
            }else{
                alert('Error occured. Please try after sometime or contact System Administrator');
            }
            console.log(component.get('v.WorkitemList'));
            $A.util.addClass(spinner, 'slds-hide');
        	$A.util.removeClass(spinner, 'slds-show');
        });
        $A.enqueueAction(action);
        
	}
})