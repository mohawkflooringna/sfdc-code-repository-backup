/**
 * Created by rchen180 on 17/4/21.
 */
({
  getRecordName : function (component, event) {
    var action = component.get("c.GetCurrentOwnerName");
    action.setParams({"recordId" : component.get('v.recordId')});
    // console.log(action.getParams());
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        // console.log(response.getReturnValue());
        component.set('v.ownerName', response.getReturnValue());

      }
    });
    $A.enqueueAction(action);
  },

})