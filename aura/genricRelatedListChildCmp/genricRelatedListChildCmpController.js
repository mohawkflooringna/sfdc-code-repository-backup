({
	onInit : function(component, event, helper) {
        alert("hello");
        console.log("in child "+component.get("v.relListName"));
        
          
	},
    openRelatedList: function(component, _event){
   var relatedListEvent = $A.get("e.force:navigateToRelatedList");
   relatedListEvent.setParams({
      "relatedListId": component.get("v.relListName"),
      "parentRecordId": component.get("v.recId")
   });
   relatedListEvent.fire();
	}
})