({
    onInit : function(component, event, helper) {
        component.set("v.spinner",true);
        helper.apNameHelper( component, event, helper );          
    },
    handleBackClick : function(component, event, helper) {
        helper.handleBackClickHelper(component, event, helper);
    },
  
    backToRecord: function(component, event, helper){

      


       /* var navEvt = $A.get("e.force:navigateToSObject");
        alert(component.get('v.recordId'));
        navEvt.setParams({
            "recordId": 'a112C000000N5gOQAS',
            "slideDevName": "related"
        });
    navEvt.fire();*/
        
         var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            window.history.back();
        }else if( device == 'PHONE' || device == 'TABLET' ){
            window.location.reload();
        }
    }
})