({
    initHelper : function(component, event, helper) {
        var action = component.get("c.getData"); 
        action.setParams({
            'eventId': component.get("v.eventId")
        });
        action.setCallback(this, function(response) {            
            component.set('v.dataList', response.getReturnValue() );
        });
        $A.enqueueAction(action);
    },
    onKeyUpTextHelper : function(component, event, helper) {
        var searchStr = component.get("v.searchString");
        var data = component.get('v.dataList');
        var searchOn = component.get("v.selectedObject");
        var filtredList = [];
        if( searchOn == 'Contact' ){
            if( data.contactList.length > 0 ){
                if (searchStr !== "" && searchStr !== null && searchStr !== undefined && data.contactList != null) {
                    for( var i=0; i < data.contactList.length; i++ ){
                        if( data.contactList[i].Name.toLowerCase().indexOf( searchStr.toLowerCase()) != -1){
                            filtredList.push( data.contactList[i] );    
                        }
                    }
                    component.set("v.showFilterList",true);
                    component.set("v.filterValues",filtredList);
                }else{
                    component.set("v.showFilterList",false);
                }
            }
        }else{
            if( data.userList.length > 0 ){
                if (searchStr !== "" && searchStr !== null && searchStr !== undefined && data.userList != null) {
                    for( var i=0; i < data.userList.length; i++ ){
                        if( data.userList[i].Name.toLowerCase().indexOf( searchStr.toLowerCase() ) != -1){
                            filtredList.push( data.userList[i] );    
                        }
                    }
                    component.set("v.showFilterList",true);
                    component.set("v.filterValues",filtredList);
                }else{
                    component.set("v.showFilterList",false);
                }                
            }
        }        
    },
    selectedEmailHelper: function(component, event, helper) {
        var previousData = component.get("v.selectedValues");
        var selectedEmailList = [];
        selectedEmailList.push(
            {
                Email:event.currentTarget.id,
                iconname:component.get("v.selectedObject") == 'Contact' ? 'standard:contact' : 'standard:user',
                type:component.get("v.Label")
            }
        );        
        component.set("v.selectedValues",previousData.concat(selectedEmailList));
        component.set("v.showSelectedList",true);
        component.set("v.showFilterList",false);
        component.set("v.searchString",'');
    },
    removePillHelper: function(component, event, helper) {
        var previousData = component.get("v.selectedValues");
        var removeInt = parseInt( event.currentTarget.id );
        var newData = [];
        
        for( var i=0;i<previousData.length;i++ ){
            if( i != removeInt ){
                newData.push( previousData[i] );
            }
        }
        component.set("v.selectedValues",newData);
    },
    fireTheEventHelper: function(component, event, helper){        
        var EventVar = $A.get("e.c:SendEmailLookupEvent");
        if(component.get("v.Label")=='TO'){
        EventVar.setParams({
            data: component.get('v.selectedValues'),
            toCCbcc: 'TO'
            

        });
        }
        else  if(component.get("v.Label")=='CC'){
        EventVar.setParams({
            data: component.get('v.selectedValues'),
            toCCbcc: 'CC'
            

        });
        }
         else  if(component.get("v.Label")=='BCC'){
        EventVar.setParams({
            data: component.get('v.selectedValues'),
            toCCbcc: 'BCC'
            

        });
        }
        EventVar.fire();
    }
})