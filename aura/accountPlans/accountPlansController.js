({
    //Function to load Data when component created.
    init: function (cmp, event, helper) {
        helper.deviceOrentationHandler( cmp, event, helper );
        
        var currentYear = (new Date()).getFullYear();
        cmp.set("v.fYear",currentYear);
        console.log('currentYear :'+currentYear);
        // Calling helper function to get Account Plans 
        helper.getAccountPlans(cmp,""+currentYear+"");
        // Calling helper function to get PickList Fiscal Year values
        helper.getFiscalYear(cmp);
        // Calling helper function to get Account Team member.
        helper.getMHKTeamMember(cmp);
        
    },
    backToRecord: function(component, event, helper){
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            window.history.back();
        }else if( device == 'PHONE' || device == 'TABLET' ){
            window.location.reload();
        }
    },
   
    //Function to Load the account plans based on dopr down value.
    getAccountPlansBasedFiscal : function(cmp, event, helper){
        console.log('getAccountPlansBasedFiscal in ');
        helper.getAccountPlans(cmp,cmp.get('v.fYear'));

    },
    //function called on click New button. to create Account Plan
    createAccountPlan : function(cmp, event, helper)
    {
        var isMob = cmp.get("v.isMobileApp");
       // if(isMob)
                    helper.createAccountPlan(cmp, event, helper);
        
    },
    redirect : function(cmp, event, helper)
    {
        console.log('Id', event.currentTarget.dataset.id);
        var isMob = cmp.get("v.isMobileApp");
        if(isMob)
        {
           // sforce.one.navigateToSObject(event.currentTarget.dataset.id );
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": event.currentTarget.dataset.id
            });
            navEvt.fire();
        }else
        {
            
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": event.currentTarget.dataset.id
            });
            navEvt.fire();
        }
        
        
    }
})