({
    sendMail: function(component, event, helper) {
        // when user click on Send button 
        
        // First we get all 3 fields values     
        var getEmail = component.get("v.email");
        var getSubject = component.get("v.subject");
        var getbody = component.get("v.body");
        // check if Email field is Empty or not contains @ so display a alert message 
        // otherwise call call and pass the fields value to helper method    
        
        // attachments as string
        var salAttachList = component.get('v.salAttachList');
        var salAttachListString = '';
        if(salAttachList && salAttachList.length>0){
            for(i=0; i<salAttachList.length; i++)
                salAttachListString += salAttachList[i].Id + '::';
        }
        var mailParams = {
            'to': getEmail,
            'cc': component.find('Cc').get('v.value'),
            'bcc': component.find('Bcc').get('v.value'),
            'subject': getSubject,
            'body': getbody,
            'attachments': salAttachListString
        };

        helper.sendHelper(component, mailParams);
        
        // helper.sendHelper(component, getEmail, getSubject, getbody);
    },
    // when user click on the close buttton on message popup ,
    // hide the Message box by set the mailStatus attribute to false
    // and clear all values of input fields.   
    closeMessage: function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        $A.get('e.force:refreshView').fire();
    },
    showCc: function(component, event, helper) {
        var cc = component.find("CC");
        $A.util.addClass(cc, 'show');
        $A.util.removeClass(cc, 'cc-hide');

        var cc_link = component.find("CC-link");
        $A.util.addClass(cc_link, 'cc-hide');
    },
    doInit: function(component, event, helper) {
        // var eventId = "00U2C000001AERt";
        var eventId = component.get("v.recordId");
        var action = component.get("c.appoinmentData");
        var appoinmentData = '';
        var templateData = '';
        action.setParams({ "eventId" : eventId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // component.set("v.salItems", response.getReturnValue());
                appoinmentData = response.getReturnValue();
                // console.log(appoinmentData.Include_Attachments__c)
                if(appoinmentData.Include_SAL_In_Mail__c==true)
                    helper.getAttachList(component, eventId)
                
            }
        });
        $A.enqueueAction(action);

        // new method to get email template
        /*var tAction = component.get("c.dynamicTemplate");
        tAction.setParams({ "templateId" : '00X410000016by5EAA' });
        tAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                templateData = response.getReturnValue();
                helper.mergeAppointWithTemplateAndAutoFillForm(component, appoinmentData, templateData);
            }
        });
        $A.enqueueAction(tAction);*/

        // to feed subject
        var subAction = component.get("c.setSubject");
        subAction.setParams({ "eventId" : eventId });
        subAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                component.set('v.subject', response.getReturnValue());
            }
        });
        $A.enqueueAction(subAction);

        var bodyAction = component.get("c.setEmailBody");
        bodyAction.setParams({ "eventId" : eventId });
        bodyAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                component.set('v.body', response.getReturnValue());
                helper.mergeAppointWithTemplateAndAutoFillForm(component, appoinmentData, '');
            }
        });
        $A.enqueueAction(bodyAction);

        var salAction = component.get("c.sALItems");
        salAction.setParams({ "eventId" : eventId });
        salAction.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                helper.mergeActionListItemsToBody(component, response.getReturnValue(), 'Sales Action List');
            }
        });
        $A.enqueueAction(salAction);

        var falAction = component.get("c.fundametalItems");
        falAction.setParams({ "eventId" : eventId });
        falAction.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                helper.mergeActionListItemsToBody(component, response.getReturnValue(), 'Fundamentals Action List');
            }
        });
        $A.enqueueAction(falAction);
    },
    removeFileItem: function(component, event, helper) {
        if(confirm("Are you sure to remove this attachment?")){
            var index = event.currentTarget.dataset.index;
            helper.removeItem(component, index);
        }
    }
})