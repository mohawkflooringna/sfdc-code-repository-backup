({
    
     getHistory : function(component) {
        var action = component.get("c.getFieldHistory");
        action.setParams({
            "recordId":component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            var state = response.getState();            
            if(state === 'SUCCESS'){
                var retResponse = response.getReturnValue();                
                console.log( JSON.stringify(retResponse) );
                console.log( JSON.stringify(retResponse.sObjectrecords) );
                
                component.set( "v.AlldataList",retResponse.sObjectrecords ); 
                component.set( "v.total",retResponse.total );
                
                ////INITIAL LOAD////
                var data = retResponse.sObjectrecords;
                var numberOfRecord = component.get("v.pageSize");
                var opts = [];
                var totalRecords = retResponse.total;
                for( var i=0; i < numberOfRecord ; i++ ){
                    if( i < totalRecords ){
                        opts.push( data[i] );
                    }
                }
                component.set( "v.dataList",opts );
                component.set( "v.pages",Math.ceil( retResponse.total / component.get("v.pageSize") ) );                
                ////INITIAL LOAD////
                component.set("v.toggleSpinner",false);
            }
        });
        $A.enqueueAction(action);
    },
    previousPageHelper : function(component, event, helper) {        
        var data = component.get( "v.AlldataList" );
        component.set("v.page",component.get("v.page") - 1);        
        var offset = parseInt( component.get("v.offset") ) - parseInt( component.get("v.pageSize") );
        var max_length = offset + parseInt( component.get("v.pageSize") );        
        var opts = [];
        for( var i = offset; i < max_length; i++ ){            
            opts.push( data[i] );            
        }
        component.set( "v.dataList",opts );
        component.set("v.offset",offset);
    },
    nextPageHelper : function(component, event, helper) {        
        var data = component.get( "v.AlldataList" );
        component.set("v.page",component.get("v.page") + 1);
        var offset = parseInt( component.get("v.offset") ) + parseInt( component.get("v.pageSize") );
        var max_length = offset + parseInt( component.get("v.pageSize") );
        var totalRecords = component.get("v.total");
        var opts = [];
        for( var i = offset; i < max_length; i++ ){
            if( i < totalRecords ){
                opts.push( data[i] );
            }
        }
        component.set( "v.dataList",opts );        
        component.set("v.offset",offset);
    },
    creatingPicklistHelper : function(component, event, helper) {
    	var action = component.get("c.createAnnualSalesPickList");
        action.setCallback(this,function(response){
            var state = response.getState();            
            if(state === 'SUCCESS'){
                var retResponse = response.getReturnValue();                
                var opts = [];
                for( var i=0; i < retResponse.length; i++ ){
                    opts.push({
                        key: retResponse[i].Key__c,
                        CorrespondingValue: retResponse[i].Value__c
                    }); 
                }
                component.set('v.options',opts);
            }
        });
        $A.enqueueAction(action);
    },
    //Added By Susmitha for Bug 53350 on feb 14
    totalValueBooleanHelper : function( component ) {
        var totalValue = 0;
        var retail100Percent;
      
        var cutOrderStockPercent;
        var channelAllocation;

        var ap = component.get('v.accountProfile');
        var totalChannelAllocationPercent=0;
        var totalPercentCutOrderStockPercent =0;
        var totalPercent=0;
        var errorMessageForChannelAllocation='Sum of Retail, Multi-Family, Builder, Alladin Commercial percentages'
        /*COMMENT BY MUDIT - IN REFERENCE OF BUG 62312
        var errorMessageForCutOrderAndStockingPercent = 'Sum of Cut Order and Stocking percentages ';
        */
        var errorMessageForCutOrderAndStockingPercent = 'Sum of Special order and Stocking percentages ';
        var errorMessage = 'Error: Sum of distribution % of ';
        
        if(ap.Product_Category_Allocation_Carpet__c!=0 && ap.Product_Category_Allocation_Carpet__c!='undefined' &&ap.Product_Category_Allocation_Carpet__c!=null){
            totalPercent+=parseInt(ap.Product_Category_Allocation_Carpet__c);}
        
        if(ap.Product_Category_Allocation_Cushion__c!=0 && ap.Product_Category_Allocation_Cushion__c!='undefined' &&ap.Product_Category_Allocation_Cushion__c!=null ){
            totalPercent+=parseInt(ap.Product_Category_Allocation_Cushion__c);}
        
        if(ap.Product_Category_Allocation_Wood__c!=0  && ap.Product_Category_Allocation_Wood__c!='undefined' &&ap.Product_Category_Allocation_Wood__c!=null){
            totalPercent+=parseInt(ap.Product_Category_Allocation_Wood__c);}
        
        if(ap.Product_Category_Allocation_Laminate__c!=0 && ap.Product_Category_Allocation_Laminate__c!='undefined' &&ap.Product_Category_Allocation_Laminate__c!=null){
            totalPercent+=parseInt(ap.Product_Category_Allocation_Laminate__c);}
        
        if(ap.Product_Category_Allocation_Resilient__c!=0 && ap.Product_Category_Allocation_Resilient__c!='undefined' &&ap.Product_Category_Allocation_Resilient__c!=null){
            totalPercent+=parseInt(ap.Product_Category_Allocation_Resilient__c);}
        
        if(ap.Product_Category_Allocation_Tile__c!=0 && ap.Product_Category_Allocation_Tile__c!='undefined' &&ap.Product_Category_Allocation_Tile__c!=null){
            totalPercent+=parseInt(ap.Product_Category_Allocation_Tile__c);}
        
        if(totalPercent==100 ||(ap.Product_Category_Allocation_Carpet__c==0 && ap.Product_Category_Allocation_Cushion__c==0 && ap.Product_Category_Allocation_Wood__c==0
                                && ap.Product_Category_Allocation_Laminate__c==0 && ap.Product_Category_Allocation_Resilient__c==0 && ap.Product_Category_Allocation_Tile__c==0)||(ap.Product_Category_Allocation_Carpet__c==null && ap.Product_Category_Allocation_Cushion__c==null && ap.Product_Category_Allocation_Wood__c==null
                                && ap.Product_Category_Allocation_Laminate__c==null && ap.Product_Category_Allocation_Resilient__c==null && ap.Product_Category_Allocation_Tile__c==null)){
            retail100Percent=true;
        }
        else{
            
            retail100Percent=false;
            errorMessage += 'Product Category Allocation\'s';
        }
        
        /******************************************************/
       
          totalPercentCutOrderStockPercent=0;

        if(ap.Cut_Order__c!=0 && ap.Cut_Order__c!="undefined" &&ap.Cut_Order__c!=null){
            totalPercentCutOrderStockPercent+=parseInt(ap.Cut_Order__c);}
        
        if(ap.Stocking__c!=0 && ap.Stocking__c!="undefined" &&ap.Stocking__c!=null){
            totalPercentCutOrderStockPercent+=parseInt(ap.Stocking__c);}
        if(totalPercentCutOrderStockPercent==100 ||(ap.Cut_Order__c==0 && ap.Stocking__c==0 ) || (ap.Cut_Order__c==null  && ap.Stocking__c==null) ){
            cutOrderStockPercent=true;
        }
        else{
            cutOrderStockPercent=false;
            if(retail100Percent)
                errorMessage = errorMessageForCutOrderAndStockingPercent;

            else
                errorMessage =errorMessage+','+errorMessageForCutOrderAndStockingPercent+'\'s';
            
        }
        /******************************************************/
         totalChannelAllocationPercent=0;
                    console.log('$$$$$:Retail__c:::'+ap.Retail__c);

        if(ap.Retail__c!=0 && ap.Retail__c!='undefined' &&ap.Retail__c!=null){
            totalChannelAllocationPercent+=parseInt(ap.Retail__c);}
        
        if(ap.Builder__c!=0 && ap.Builder__c!='undefined' &&ap.Builder__c!=null){
                        console.log('$$$$$:Builder__c:::'+ap.Builder__c);

            totalChannelAllocationPercent+=parseInt(ap.Builder__c);}
         if(ap.Multi_Family__c!=0 && ap.Multi_Family__c!='undefined' &&ap.Multi_Family__c!=null){
                        console.log('$$$$$:Multi_Family__c:::'+ap.Multi_Family__c);
            totalChannelAllocationPercent+=parseInt(ap.Multi_Family__c);
             console.log('$$$$$:totalChannelAllocationPercent:::Multi_Family__c:::'+totalChannelAllocationPercent);

         }
        
        if(ap.Aladdin_Commercial__c!=0 && ap.Aladdin_Commercial__c!='undefined' &&ap.Aladdin_Commercial__c!=null){
            console.log('$$$$$:Aladdin_Commercial__c:::'+ap.Aladdin_Commercial__c);
            totalChannelAllocationPercent+=parseInt(ap.Aladdin_Commercial__c);}
        
        if(totalChannelAllocationPercent==100 ||(ap.Retail__c==0 && ap.Builder__c==0 && ap.Multi_Family__c ==0 && ap.Aladdin_Commercial__c ==0)||(ap.Retail__c==null && ap.Builder__c==null && ap.Multi_Family__c ==null && ap.Aladdin_Commercial__c ==null)){
            console.log('$$$$$::::totalChannelAllocationPercent'+totalChannelAllocationPercent);
            
            channelAllocation=true;
        }
        else{
            channelAllocation=false;
            if(retail100Percent && cutOrderStockPercent){
                errorMessage = errorMessageForChannelAllocation;}

            else{
                errorMessage =errorMessage+','+errorMessageForChannelAllocation+'\'s';
            
            }
        }
        /******************************************************/

        console.log("errorMessage :"+errorMessage);
        if(retail100Percent && cutOrderStockPercent && channelAllocation)
        {
            return true;
        }
        else
        {
            component.set("v.errorMsg",errorMessage+' should be 100%.');
            console.log('>>>>>>>>>>>'+errorMessage);
            return false;
            
        }
    },
    checkChainNumberHelper: function(component, event, helper) {
        var action = component.get("c.isChainNumberExist");
        var ap = component.get('v.accountProfile');
        action.setParams({
            ap : ap,
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {                
                if( response.getReturnValue() == true ){
                	component.set("v.chainNumberExist",true);    
                    component.set("v.toggleSpinner",false);
                }else{
                    component.set("v.toggleSpinner",false);
                    component.set("v.finishFirstAPCall",true);
                }
                
            }
        });
        $A.enqueueAction(action);
    },
})