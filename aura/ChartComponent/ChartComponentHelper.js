({
	callToServer : function(component, method, callback, params) {
		var action = component.get(method);
        if(params){
            action.setParams(params);
        }
        console.log('****param to controller:'+JSON.stringify(params));
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback.call(this,response.getReturnValue());
            }else if(state === "ERROR"){
                
                var spinner = component.find("chartSpinner");
        		$A.util.toggleClass(spinner, "slds-hide");
                
                alert('Problem with connection. Please try again.');
            }
        });
		$A.enqueueAction(action);
    },
    
	generateChart : function(component, helper) {   
        
        var chartId = component.get("v.chartId");
        var accountId = component.get("v.accountId");
		var xAxeLabel = component.get("v.xAxeLabel");        
		var yAxeLabel = component.get("v.yAxeLabel");        

        var loadedChartData = component.get("v.chartData");
        component.set("v.styleNumFullName", loadedChartData.styleNumFullName);
        
        console.log('loadedChartData Labels :'+loadedChartData.dataLabels);
        console.log('loadedChartData Data :'+loadedChartData.dataValues);

		var stepSize = 2; 
        
		var barChartData = {
			labels: loadedChartData.dataLabels,
			datasets: [{
				label: yAxeLabel,
				backgroundColor: loadedChartData.colorValues,
				borderColor: loadedChartData.colorValues,
				borderWidth: 1,
				data: loadedChartData.dataValues
			}]

		};
		
		// Define a plugin to provide data labels
		
		Chart.plugins.register({
			afterDatasetsDraw: function(chart) {
				var ctx = chart.ctx;

                 var ranges = [
                    { divider: 1e6, suffix: 'M' },
                    { divider: 1e3, suffix: 'k' }
                 ];
                
                chart.data.datasets.forEach(function(dataset, i) {
					var meta = chart.getDatasetMeta(i);
					if (!meta.hidden) {
						meta.data.forEach(function(element, index) {
							// Draw the text in black, with the specified font
							ctx.fillStyle = 'rgb(150, 150, 150)';

							var fontSize = 12;
							var fontStyle = 'normal';
							var fontFamily = 'Helvetica Neue';
							ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

							var dataValue = dataset.data[index];
                            
							var dataString = '';
                            dataString = Math.round(dataValue).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            
							// Make sure alignment settings are correct
							ctx.textAlign = 'center';
							ctx.textBaseline = 'middle';

							var padding = 2;
							var position = element.tooltipPosition();
							ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
						});
					}
				}); 
			}
		});
        
        
        var el = component.find('chart').getElement();
        el.style.backgroundColor = 'rgba(255,255,255,255)';
        var ctx = el.getContext("2d");

        var maxValue = loadedChartData.maxValue;
        
        if 		(maxValue > 50000000)	stepSize = 10000000;
        else if (maxValue > 10000000)	stepSize = 5000000;
        else if (maxValue > 5000000) 	stepSize = 1000000;
        else if (maxValue > 1000000) 	stepSize = 500000;
        else if (maxValue > 500000) 	stepSize = 100000;
        else if (maxValue > 100000) 	stepSize = 50000;
        else if (maxValue > 50000) 		stepSize = 10000;
        else if (maxValue > 10000) 		stepSize = 5000;
        else if (maxValue > 5000) 		stepSize = 1000;
        else if (maxValue > 1000) 		stepSize = 500;
        else if (maxValue > 500) 		stepSize = 100;
        else if (maxValue > 100) 		stepSize = 50;
        else if (maxValue > 50) 		stepSize = 10;
        else if (maxValue > 10) 		stepSize = 5;
        else 							stepSize = 2;
        
        
        var rangeFormat = function(value) {
            var ranges = [
                { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: 'k' }
            ];
            function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                    if (n >= ranges[i].divider) {
                        return (n / ranges[i].divider).toString() + ranges[i].suffix;
                    }
                }
                return n;
            }
            return formatNumber(value);
        };
        
        var myNewChart = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                                return component.get("v.yAxeLabel") + ": " + Number(tooltipItem.yLabel).toFixed(0).replace(/./g, function(c, i, a) {
                                    return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
                                });
                            }
                        }
                    },
	                layout: {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 15,
                            bottom: 0
                        }
                    },
                    maintainAspectRatio: false,
                    responsive: true,
                    onClick: function (c, e) {
                        if (e.length > 0) {
                            var event = e[0];
                            //console.log('I am here 1' + event);
                            var chart = event._chart;
                            var barIndex = event._index;
                            var selectedLabel = loadedChartData.dataLabels[barIndex];
                            var selectedValue = loadedChartData.dataValues[barIndex];
                            
                            var chartBarClickedEvent = component.getEvent("chartBarClicked");
                            
                            var regionObj = component.find("selectRegion");
                            var districtObj = component.find("selectDistrict");
                            var territoryObj = component.find("selectTerritory");
                            var timePeriodObj = component.find("selectTimePeriod");
                            var categoryObj = component.find("selectCategory");
                            
                            var regionValue = '';
                            var districtValue = '';
                            var territoryValue = '';
                            var timePeriodValue = '';
                            var categoryValue = '';

                            if (regionObj != null)  regionValue = regionObj.get("v.value");
                            if (districtObj != null)  districtValue = districtObj.get("v.value");
                            if (territoryObj != null)  territoryValue = territoryObj.get("v.value");
                            if (timePeriodObj != null) timePeriodValue = timePeriodObj.get("v.value");
                            if (categoryObj != null) categoryValue = categoryObj.get("v.value");
                            
                            if (categoryValue == '')  {
	                            var productCategory = component.get("v.productCategory");
                                categoryValue = productCategory; 
                            }
                            // Changed default timePeriodValue from YTD to R12 - Bug 71461 - 3/11/19
                            if (timePeriodValue == undefined || timePeriodValue=='') timePeriodValue = 'R12'; 
                            
                            territoryValue = component.get("v.territoryValue");
                            
                			chartBarClickedEvent.setParams({
                    			priceLevelClicked: selectedLabel,
                    			sourceChartId : chartId,
                                region : regionValue,
                                district : districtValue,
                                territory : territoryValue,
                                accountId : accountId,
                                category : categoryValue,
                                timePeriod : timePeriodValue
                			}).fire();
                        }
                    },
                    scales : {
                        xAxes: [{
                            barThickness : 30, 
                            scaleLabel: {
                                display: true,
                                labelString: xAxeLabel
                            }                        
                        }],
                        yAxes: [{
                            display : true,
                            scaleLabel: {
                                display: true,
                                labelString: yAxeLabel
                            } ,                      
				            ticks: {
                				suggestedMin: 0,
                                stepSize: stepSize,
                                callback : rangeFormat
            				}                            
                        }],
                    },
					legend: {
                        display: false,
						position: 'top',
					},
					title: {
						display: false,
						text: 'Account Pricing'
					}
				}
			});
        
		component.set("v.chartObj", myNewChart);    
    },
    
 
    updateChart : function(component, helper) {   
        
	    var chartId = component.get("v.chartId");

		var loadedChartData = component.get("v.chartData");
        var chartObj = component.get("v.chartObj");

		var xAxeLabel = component.get("v.xAxeLabel");        
		var yAxeLabel = component.get("v.yAxeLabel");        
        
        var maxValue = loadedChartData.maxValue;
        
        var stepSize = 2;
        
        if 		(maxValue > 50000000)	stepSize = 10000000;
        else if (maxValue > 10000000)	stepSize = 5000000;
        else if (maxValue > 5000000) 	stepSize = 1000000;
        else if (maxValue > 1000000) 	stepSize = 500000;
        else if (maxValue > 500000) 	stepSize = 100000;
        else if (maxValue > 100000) 	stepSize = 50000;
        else if (maxValue > 50000) 		stepSize = 10000;
        else if (maxValue > 10000) 		stepSize = 5000;
        else if (maxValue > 5000) 		stepSize = 1000;
        else if (maxValue > 1000) 		stepSize = 500;
        else if (maxValue > 500) 		stepSize = 100;
        else if (maxValue > 100) 		stepSize = 50;
        else if (maxValue > 50) 		stepSize = 10;
        else if (maxValue > 10) 		stepSize = 5;
        else 							stepSize = 2;        
        
		var barChartData = {
			labels: loadedChartData.dataLabels,
			datasets: [{
				label: '# of Accounts',
				backgroundColor: loadedChartData.colorValues,
				borderColor: loadedChartData.colorValues,
				borderWidth: 1,
				data: loadedChartData.dataValues,
  			}]

		};        
        chartObj.data = barChartData;
        chartObj.options.responsive = true;
        chartObj.options.scales.yAxes = [{
                            display : true,
                            scaleLabel: {
                                display: true,
                                labelString: yAxeLabel
                            } ,                      
				            ticks: {
                				suggestedMin: 0,
                                stepSize: stepSize,
            				}                            
                        }];
		chartObj.options.layout = {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 15,
                            bottom: 0
                        }
                    };
        
        chartObj.update();
        
        component.set("v.chartObj", chartObj); 
        
    },
    
    processFilter : function(component, response, regionVal, districtVal, territoryVal, timePerioVal, categoryVal) {
        console.log('apex response :'+JSON.stringify(response));
        
        var optsRegion=[];
        var optsDistrict=[];
        var optsTerritory=[];
        var optsTimePeriod=[];
        var optsCategory=[];
        
        optsRegion.push({label: '- All -', value: ''}); 
        optsDistrict.push({label: '- All -', value: ''}); 
        optsTerritory.push({label: '- All -', value: ''}); 
		//Placing R12 at top for default selection - Bug 70746 - MB - 3/5/19
        optsTimePeriod.push({label: 'R365', value: 'R12', selected: (timePerioVal == 'R12')}); //Changed label from R12 to R365 - Bug 71369 - 3/20/19
        optsTimePeriod.push({label: 'YTD', value: 'YTD', selected: (timePerioVal == 'YTD')}); 
        optsTimePeriod.push({label: 'MTD', value: 'MTD', selected: (timePerioVal == 'MTD')}); 
        
        var isSelected;
        
		
		var regionFilters = response.regionFilters;
		var ditrictFilters = response.ditrictFilters;
		var territoryFilters = response.territoryFilters;
		var categoryFilters = response.categoryFilters;

        for (var index in regionFilters) {
            var region = regionFilters[index];
            isSelected = false;
            isSelected = (region == regionVal);
            optsRegion.push({label: region, value: region, selected: isSelected}); 
        }
        
        for (var index in ditrictFilters) {
            var district = ditrictFilters[index];
            isSelected = false;
            isSelected = (district == districtVal);
            optsDistrict.push({label: district, value: district, selected: isSelected}); 
        }
        
        for (var territory in territoryFilters) {
            isSelected = false;
            isSelected = (territory == territoryVal);
            var territoryLabel = territoryFilters[territory];
            optsTerritory.push({label: territoryLabel, value: territory, selected: isSelected});
        }
        
        //Order the territory filter
        optsTerritory.sort(function(a, b){ return a.label > b.label});
        
        for (var index in categoryFilters) {
            var category = categoryFilters[index];
            isSelected = false;
            isSelected = (category == categoryVal);
            optsCategory.push({label: category, value: category, selected: isSelected}); 
        }
        
        var regionObj = component.find("selectRegion");
        if (regionObj != null) 
            component.set("v.regionOptions", optsRegion);
        
        var districtObj = component.find("selectDistrict");
        if (districtObj != null) 
            component.set("v.districtOptions", optsDistrict);
        
        var territoryObj = component.find("selectTerritory");
        if (territoryObj != null)
            component.set("v.territoryOptions", optsTerritory);
    
        var timePeriodObj = component.find("selectTimePeriod");
        if (timePeriodObj != null)
            component.set("v.timePeriodOptions", optsTimePeriod);

        var categoryObj = component.find("selectCategory");
        if (categoryObj != null) 
            component.set("v.categoryOptions", optsCategory);
        
	},
    getParamValue : function( tabName, paramName ) {
 
        var url = window.location.href;
        var allParams = url.substr(url.indexOf(tabName) + tabName.length+1).split('&');
        var paramValue = '';
        for(var i=0; i<allParams.length; i++) {
            if(allParams[i].split('=')[0] == paramName)
                paramValue = allParams[i].split('=')[1];
        }
        console.log(paramValue);
        return paramValue;
    }    
})