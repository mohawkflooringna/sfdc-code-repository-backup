/**
 * Created by rchen180 on 17/4/21.
 */
({
    getProductCollectionNamePicklistValues : function (component, event, helper) {
        var action = component.get("c.getProductCollectionNamePicklistValues");
        // action.setParams({'Name': name, 'Territory': territory});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // console.log(response.getReturnValue());
                component.set('v.collectionList', response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getSubCategoryPickListvalues : function (component, event, helper) {
        var action = component.get("c.getSubCategoryPickListvalues");
        // action.setParams({'Name': name, 'Territory': territory});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                // console.log(response.getReturnValue());
                component.set('v.subCategoryList', response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getProductCollectionBrandPicklistValues : function (component, event, helper) {
        var action = component.get("c.getProductCollectionBrandPicklistValues");
        // action.setParams({'Name': name, 'Territory': territory});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set('v.brandList', response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getProductProductTypePicklistValues : function (component, event, helper) {
        var action = component.get("c.getProductProductTypePicklistValues");
        // action.setParams({'Name': name, 'Territory': territory});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set('v.productTypeList', response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getLoggedinuserBusinessTypeInfo : function (component, event, helper) {
        var action = component.get("c.getLoggedinuserBusinessTypeInfo");
        // action.setParams({'Name': name, 'Territory': territory});
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set('v.loginUserInfo', response.getReturnValue());
                var spinner = component.find('v.spinner');
                $A.util.addClass(spinner, 'slds-hide');
                $A.util.removeClass(spinner, 'slds-show');
                //Added by Krishnapriya on Nov-17-2017 Bug:41188
                if (response.getReturnValue()=='Residential') { 
                console.log('check profile name'+response.getReturnValue());
                component.set('v.userCheck', true);
                //Added by Krishnapriya on Nov-17-2017 Bug:41188
            }
            }
        });
        $A.enqueueAction(action);
    },
    initProductList : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, 'slds-show');
        $A.util.removeClass(spinner, 'slds-hide');
        component.set('v.selectProductAllChecked', '');
        component.set('v.hasMoreRecords', false); // Added by MB - 06/21/17
        var action = component.get('c.getproductlist');
        action.setParams({
            'StyleNames': '',
            'StyleNumber': '',
            'Collection': 'none',
            'GenericProduct': false
            
        });
        console.log(action.getParams())
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === 'SUCCESS') {
                component.set('v.productList', response.getReturnValue());
                console.log(response.getReturnValue());
                $A.util.addClass(spinner, 'slds-hide');
                $A.util.removeClass(spinner, 'slds-show');
                //Added by MB - 06/21/17
                if(response.getReturnValue().length === 20){
                    component.set('v.hasMoreRecords', true);
        		}
                // var productDetail = document.getElementById('productDetail');
                // productDetail.className = 'show';
                // var first = document.getElementsByClassName('first-td');
                //
                // first[0].className= 'none';
            }
        });
        $A.enqueueAction(action);
    },
    // start Added By Lakshman oct-27-2017
    redirectToProductRecord : function (component,recordId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId
        });
        navEvt.fire();
    
    // End Lakshman oct-27-2017
    },

    
})