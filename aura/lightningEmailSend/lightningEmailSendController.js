({
    sendMail: function(component, event, helper) {
        // when user click on Send button 
        
        // First we get all 3 fields values     
        var gettoEmail = component.get("v.email");
        var getCCEmail = component.get("v.ccemail");
        var getbccEmail =component.get("v.bccemail");
        
        
        // var getEmail = component.get("v.email");
        var getSubject = component.get("v.subject");
        var getbody = component.get("v.body");
        // check if Email field is Empty or not contains @ so display a alert message 
        // otherwise call call and pass the fields value to helper method    
        
      //  console.log('is null', getEmail, helper.isNull(getEmail));
        if(helper.isNull(gettoEmail)==false){
            // attachments as string
            var salAttachList = component.get('v.salAttachList');
            var salAttachListString = '';
            if(salAttachList && salAttachList.length>0){
                for(i=0; i<salAttachList.length; i++)
                    salAttachListString += salAttachList[i].Id + '::';
            }
            var mailParams = {
                'to': gettoEmail,
                'cc': getCCEmail/*component.find('Cc').get('v.value')*/,
                'bcc': getbccEmail/*component.find('Bcc').get('v.value')*/,
                'subject': getSubject,
                'body': getbody,
                'attachments': salAttachListString
            };
            
            helper.sendHelper(component, mailParams);
        } else {
            alert('Please enter Email Address')
        }
        // helper.sendHelper(component, getEmail, getSubject, getbody);
    },
    // when user click on the close buttton on message popup ,
    // hide the Message box by set the mailStatus attribute to false
    // and clear all values of input fields.   
    closeMessage: function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        // $A.get('e.force:refreshView').fire();
    },
    handleApplicationEventFired: function(component, event, helper) {
        var toEm = event.getParam("data");
        var listObject=JSON.stringify( toEm );
        var toCCbcc=event.getParam("toCCbcc");
                var getEmail='';
                var getccEmail='';
                var getbccEmail='';
        if(toCCbcc=='TO'){
            for(var i=0;i<toEm.length; i++){
                 if(i!=toEm.length){
                getEmail+=toEm[i].Email+',';
            }
            }
            getEmail=getEmail.slice(0,-1);

            console.log('getEmailto'+getEmail);
        component.set("v.email",getEmail);
            

        }
         if(toCCbcc=='CC'){
            for(var i=0;i<toEm.length; i++){
                 if(i!=toEm.length){
                getccEmail+=toEm[i].Email+',';
            }
            }
            getccEmail=getccEmail.slice(0,-1);

            console.log('getEmailcc'+getccEmail);
        component.set("v.ccemail",getccEmail);
            

        }
         if(toCCbcc=='BCC'){
            for(var i=0;i<toEm.length; i++){
                 if(i!=toEm.length){
                getbccEmail+=toEm[i].Email+',';
            }
            }
            getbccEmail=getbccEmail.slice(0,-1);

            console.log('getEmailbcc'+getbccEmail);
        component.set("v.bccemail",getbccEmail);
            

        }
        console.log( '::Received application event:: ' + JSON.stringify( toEm ) );
        //alert("Received application event with param to = "+ toEm);
    },
    handleComponentEvent: function(component, event, helper) {
        
        var toEm = event.getParam("data");
        alert("Received application event with param to = "+ toEm);
        /*component.set("v.email",toEm);
        var ccEm = event.getParam("ccemail");
        alert("Received application event with param cc= "+ ccEm);
        component.set("v.ccemail",ccEm);
        var bccEm = event.getParam("bccemail");
        alert("Received application event with param  bcc= "+ bccEm);
        component.set("v.bccemail",bccEm);*/
    },
    showCc: function(component, event, helper) {
        var cc = component.find("CC");
        $A.util.addClass(cc, 'show');
        $A.util.removeClass(cc, 'cc-hide');
        
        var cc_link = component.find("CC-link");
        $A.util.addClass(cc_link, 'cc-hide');
    },
    doInit: function(component, event, helper) {
       
        // var eventId = "00U2C000001AERt";
        
        var eventId = component.get("v.recordId");
        // var action = component.get("c.appointmentDataNew");
        var action = component.get("c.appoinmentData");
        var appoinmentData = '';
        var actionList = {};
        var templateData = '';
        action.setParams({ "eventId" : eventId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                
                console.log("appointment response", response.getReturnValue());
                appoinmentData = response.getReturnValue();
                
                /*var result = JSON.parse(response.getReturnValue());
                appoinmentData = result.eventData;
                actionList = result.actionList;*/
                // console.log(appoinmentData.Include_Attachments__c)
                
                if(appoinmentData.Include_SAL_In_Mail__c==true)
                    helper.getAttachList(component, eventId)
                    
                    }
        });
        $A.enqueueAction(action);
        helper.getEventContact(component, event, helper);
        
        // new method to get email template
        /*var tAction = component.get("c.dynamicTemplate");
        tAction.setParams({ "templateId" : '00X410000016by5EAA' });
        tAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                templateData = response.getReturnValue();
                helper.mergeAppointWithTemplateAndAutoFillForm(component, appoinmentData, templateData);
            }
        });
        $A.enqueueAction(tAction);*/
        
        // to feed subject
        var subAction = component.get("c.setSubject");
        subAction.setParams({ "eventId" : eventId });
        subAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                component.set('v.subject', response.getReturnValue());
            }
        });
        $A.enqueueAction(subAction);
        
        var bodyAction = component.get("c.setEmailBody");
        bodyAction.setParams({ "eventId" : eventId });
        bodyAction.setCallback(this, function(response) {
            if (component.isValid() && response.getState() === "SUCCESS") {
                component.set('v.body', response.getReturnValue());
                helper.mergeAppointWithTemplateAndAutoFillForm(component, appoinmentData, '', actionList);
            }
        });
        $A.enqueueAction(bodyAction);
        
        var salAction = component.get("c.sALItems");
        salAction.setParams({ "eventId" : eventId });
        salAction.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                helper.mergeActionListItemsToBody(component, response.getReturnValue(), 'Sales Action List');
            }
        });
        $A.enqueueAction(salAction);
        
        var falAction = component.get("c.fundametalItems");
        falAction.setParams({ "eventId" : eventId });
        falAction.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                helper.mergeActionListItemsToBody(component, response.getReturnValue(), 'Fundamentals Action List');
            }
        });
        $A.enqueueAction(falAction);
    },
    removeFileItem: function(component, event, helper) {
        if(confirm("Are you sure to remove this attachment?")){
            var index = event.currentTarget.dataset.index;
            helper.removeItem(component, index);
        }
    }
})