({
    doAccessoriesDataDisplay: function(component, event, helper){
        var value = event.getParam("value");
        console.log("menuitem called==>"+value);
        console.log("recordId==>"+component.get("v.recordId"));
        console.log("productCategoryId==>"+component.get("v.productCategoryId"));
        if(value=='View_Accessories'){
            console.log('in if condition');
            var modalBody;
            $A.createComponent("c:Grid_DataAccessoriesView",
                               {
                                   "recordId": component.get("v.recordId"),
                                   "productCategoryId": component.get("v.productCategoryId")
                               },
                               function(content, status) {
                                   if (status === "SUCCESS") {
                                       modalBody = content;
                                       component.find('overlayLibabc').showCustomModal({
                                           header: "Accessories",
                                           body: modalBody,
                                           showCloseButton: true,
                                           cssClass: "my-modal"
                                       })
                                       
                                   }
                                   
                               });
        }
    }
})