({
    onInit : function(component, event, helper) {
        //helper.apNameHelper( component, event, helper );
            
            component.set('v.SOA',component.get('v.pageReference.state.showOriginalAP') );
            var SOA = component.get("v.pageReference.state.showOriginalAP");
            if( SOA == 'true' || SOA ){
                component.set('v.apRecordId',component.get('v.pageReference.state.apRecordId') );
            }
     //   var SOA = component.get("v.pageReference.state.showOriginalAP");
     /*    if( SOA == 'true' || SOA ){
            console.log( 'IF' );
            component.set('v.apRecordId',component.get('v.pageReference.state.apRecordId') );
            component.set('v.SOA',component.get('v.pageReference.state.apRecordId') );
            SOA
        	helper.apNameHelper( component, event, helper );    
        }else{
            console.log( 'ELSE' ); */
            helper.apNameHelper( component, event, helper );
        
    },
    handlePrimaryInput : function(component, event, helper) {
        helper.handlePrimaryInputHelper( component, event, helper );
    },
    handleRefresh : function(component, event, helper) {
        helper.handleRefreshHelper( component, event, helper );
    },
    handleClosing : function(component, event, helper) {
        component.set("v.spinner",true); 
        var container = component.find("primaryInputDiv");
        $A.createComponent(
            "c:accountProfileCustomDetailPage",
            {
                //recordId:component.get("v.recordId"),
                recordId:component.get("v.apRecordId"),
                apRecordId:component.get("v.apRecordId"),
                accountId:component.get("v.accountId")
            },
            function(cmp) {
                var cmpMain = component.find("mainDiv");
                console.log( cmpMain );
                $A.util.addClass(cmpMain, 'slds-show');                
                container.set("v.body", [cmp]);
            }
        );   
    },
    handleRefreshEvent : function(component, event, helper) {
        component.set("v.spinner",true);
        console.log( 'HERE' );
        console.log( event.getParam("AccountProfileId") );
        var container = component.find("primaryInputDiv");
        $A.createComponent(
            "c:accountProfileCustomDetailPage",
            {
                apRecordId:event.getParam("AccountProfileId"),
                recordId:event.getParam("AccountProfileId"),
                accountId:event.getParam("AccountId")
            },
            function(cmp) {
                var cmpMain = component.find("mainDiv");
                console.log( cmpMain );
                $A.util.addClass(cmpMain, 'slds-show');                
                container.set("v.body", [cmp]);
            }
        );
	},
    
   backToRecord: function(component, event, helper){
            console.log(component.get('v.apRecordId'));
            var navService = component.find("navLink");
            var pageReference = {
                type: 'standard__recordPage',
                attributes: {
                    recordId: component.get('v.apRecordId') ,
                    objectApiName: 'Account_Profile__c',
                    actionName: 'view'
                }
            };
            event.preventDefault();
            navService.navigate( pageReference );    
    },
})