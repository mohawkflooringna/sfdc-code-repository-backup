({
    
    onInit : function(component, event, helper) {
        console.log("component.set(,true)");
        var action = component.get("c.getRelatedListNames");
        action.setParams({ 'recordId' : component.get('v.recordId') });
        action.setCallback(this, function(resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                var result = resp.getReturnValue();
                console.log( result );
                component.set("v.data",result);
                
            }            
        });
        
        $A.enqueueAction(action);
        
    },
    openRelatedList: function(component, event){
        
        var div = event.currentTarget;
        var recName = div.getAttribute('data-rName');
        console.log(recName);
        
        var relatedListEvent = $A.get("e.force:navigateToRelatedList");
        relatedListEvent.setParams({
            "relatedListId": recName,
            "parentRecordId": component.get("v.recordId")
        });
        relatedListEvent.fire();
    }
})