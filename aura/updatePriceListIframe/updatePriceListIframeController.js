({
    doInit : function(component, event, helper) {
        if(
            component.get('v.mashupName') !='' && 
            ( component.get('v.IframeUrl') == '' || component.get('v.IframeUrl') == undefined)
        ){
            var action = component.get("c.getMashupRecordNew");
            action.setParams({ 
                "mashupName" : component.get('v.mashupName'),
                "recordId" : component.get('v.recordId'),
                "gridProductId" : ""
            });
            
            action.setCallback(this, function(response){
                if(component.isValid() && response !== null && response.getState() == 'SUCCESS'){
                    var resultSet = response.getReturnValue();
                    console.log('URl :' + resultSet);                    
                    component.set('v.IframeUrl', resultSet);
                }
            });
            
            $A.enqueueAction(action); 
        }         
    },
    backToRecord: function(component, event, helper){

        $A.get("e.force:closeQuickAction").fire();
    },
})