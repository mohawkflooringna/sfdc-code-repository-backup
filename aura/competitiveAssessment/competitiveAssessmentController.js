({
    doInit: function (component, event, helper) {
        helper.doInitHelper( component, event, helper );    
        helper.deviceOrentationHandler( component, event, helper );
        
    },
    onclickTabSelection: function (component, event, helper) {
        component.set("v.activeTabName",event.getSource().get("v.selectedTabId"));
    },
    onclickNewButton: function (component, event, helper) {
        var tabName = component.get("v.activeTabName");
        if( tabName == 'PA' ){
            var method = component.get("c.showProductModal");
            $A.enqueueAction(method);
        }else{
            var method = component.get("c.showMarketModal");
            $A.enqueueAction(method);
        }
    },
    backToRecord: function(component, event, helper){
        //Force the navigation back to the account record instead of the previous window.history
        var _accountId = component.get('v.recordId');
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": _accountId
        });
        navEvt.fire();
    },
    onChangeSelectSegment: function (component, event, helper) {
        component.set('v.spinner',true);
        var selectedSegment = component.get('v.segmentValue'),selectedBrand = '';
        helper.getFilterList(component, selectedSegment, selectedBrand);
    },
    onChangeSelectItemProduct: function (component, event, helper) {
        component.set('v.spinner',true);
        var selectedSegment = component.get('v.segmentValue'),selectedBrand = '';
        helper.getFilterList(component, selectedSegment, selectedBrand);
    },
    productTableSorting : function (component, event, helper) {
        var fieldName = event.currentTarget.title;
        helper.sortData(component,fieldName,component.get("v.currentProductList"),'currentProductList','sortAscProduct','sortFieldProduct');
    },
    marketTableSorting : function (component, event, helper) {
        var fieldName = event.currentTarget.title;
        helper.sortData(component,fieldName,component.get("v.currentMarketList"),'currentMarketList','sortAscMarket','sortFieldMarket');
    },
    handleProductMenuSelect : function(component, event, helper) {
        var selectedMenuItemValue = event.getSource().get("v.value");
        if( selectedMenuItemValue.substr(0, selectedMenuItemValue.indexOf('_')) == 'Edit' ){
            var index = selectedMenuItemValue.substr( selectedMenuItemValue.indexOf('_')+1,selectedMenuItemValue.length );
            var resultList = component.get('v.initialResults.Product');
            var data = resultList[index];
            component.set('v.editProductDetails',data);
            var editVal = {};
            editVal.val = data.Competitor_Product__c;
            editVal.text = data.Competitor_Product__r.Name;
            editVal.objName = 'Competitor_Products__c';
            editVal.filterVal = component.get('v.CompetitorProductRecordTypeId');
            component.set("v.selectedLookUpRecordEDITProduct",editVal);
            
            var device = $A.get("$Browser.formFactor");
            if( device == 'DESKTOP' ){
                helper.showPopupHelper(component, 'editProductModal');
            }else{
                var desktop = component.find('mainDiv');
                $A.util.removeClass(desktop, 'slds-show');
                $A.util.addClass(desktop, 'slds-hide');
                
                var mobile = component.find('mobileDiv');
                $A.util.removeClass(mobile, 'slds-hide');
                $A.util.addClass(mobile, 'slds-show');
                helper.showPopupHelper(component, 'editProductModalMobile');	   
            }
        }else{
            var index = selectedMenuItemValue.substr( selectedMenuItemValue.indexOf('_')+1,selectedMenuItemValue.length ),
                result = component.get('v.initialResults.Product');            
            var params = {
                "curAss":JSON.stringify(result[index])
            };
            component.set('v.deleteProductParam', params);
            helper.showPopupHelper( component, 'deleteProductModal' );
        }
    },
    handleMarketMenuSelect : function(component, event, helper) {
        //var selectedMenuItemValue = event.getParam("value");
        var selectedMenuItemValue = event.getSource().get("v.value");
        if( selectedMenuItemValue.substr(0, selectedMenuItemValue.indexOf('_')) == 'Edit' ){
            var index = selectedMenuItemValue.substr( selectedMenuItemValue.indexOf('_')+1,selectedMenuItemValue.length );
            var resultList = component.get('v.initialResults.Market');
            var data = resultList[index];            
            component.set('v.editMarketDetails',data);
            var editVal = {};
            editVal.val = data.Competitor__c;
            editVal.text = data.Competitor__r.Name;
            editVal.objName = 'Account';
            editVal.filterVal = '';
            component.set("v.selectedLookUpRecordEDITMarket",editVal);
            
            var device = $A.get("$Browser.formFactor");
            if( device == 'DESKTOP' ){
                helper.showPopupHelper( component, 'editMarketModal' );
            }else{
                var desktop = component.find('mainDiv');
                $A.util.removeClass(desktop, 'slds-show');
                $A.util.addClass(desktop, 'slds-hide');
                
                var mobile = component.find('mobileDiv');
                $A.util.removeClass(mobile, 'slds-hide');
                $A.util.addClass(mobile, 'slds-show');
                helper.showPopupHelper(component, 'editMarketModalMobile');	   
            }
            
            
            
            
        }else{
            var index = selectedMenuItemValue.substr( selectedMenuItemValue.indexOf('_')+1,selectedMenuItemValue.length ),
                result = component.get('v.initialResults.Market');
            var params = {
                "curAss":JSON.stringify(result[index])
            };
            component.set('v.deleteMarketParam', params);
            helper.showPopupHelper( component, 'deleteMarketModal' );
        }
    },
    deleteProductRecords:function(component, event, helper){
        component.set('v.spinner',true);
        var params = component.get('v.deleteProductParam');
        var action = component.get("c.deleteAssessment");
        action.setParams(params);
        action.setCallback(this, function (resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                if(resp.getReturnValue()){
                    var seg = component.get('v.segmentValue');
                    helper.hidePopupHelper(component, 'deleteProductModal' , 'deletemodalproduct');
                    helper.getFilterList(component, seg, '');
                }
            }
        });
        $A.enqueueAction(action);
    },
    hideDeleteModalProduct:function(component, event, helper){
        helper.hidePopupHelper(component, 'deleteProductModal' , 'deletemodalproduct');
    },
    deleteMarketRecords:function(component, event, helper){
        component.set('v.spinner',true);
        var params = component.get('v.deleteMarketParam');
        var action = component.get("c.deleteAssessment");
        action.setParams(params);
        action.setCallback(this, function (resp) {
            var state = resp.getState();
            if (component.isValid() && state === "SUCCESS") {
                if(resp.getReturnValue()){
                    var seg = component.get('v.segmentValue');
                    helper.hidePopupHelper(component, 'deleteMarketModal', 'deletemodalmarket');
                    helper.getFilterList(component, seg, '');
                }
            }
        });
        $A.enqueueAction(action);
    },
    hideDeleteModalMarket:function(component, event, helper){
        helper.hidePopupHelper(component, 'deleteMarketModal', 'deletemodalmarket');
    },
    showProductModal: function (component, event, helper) {
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            helper.showPopupHelper( component, 'newProductModal' );
        }else{
            var desktop = component.find('mainDiv');
            $A.util.removeClass(desktop, 'slds-show');
            $A.util.addClass(desktop, 'slds-hide');
            
            var mobile = component.find('mobileDiv');
            $A.util.removeClass(mobile, 'slds-hide');
            $A.util.addClass(mobile, 'slds-show');
            helper.showPopupHelper( component, 'newProductModalMobile' ); 	   
        }
    },
    showMarketModal: function (component, event, helper) {
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            helper.showPopupHelper( component, 'newMarketModal' );
        }else{
            var desktop = component.find('mainDiv');
            $A.util.removeClass(desktop, 'slds-show');
            $A.util.addClass(desktop, 'slds-hide');
            
            var mobile = component.find('mobileDiv');
            $A.util.removeClass(mobile, 'slds-hide');
            $A.util.addClass(mobile, 'slds-show');
            helper.showPopupHelper( component, 'newMarketModalMobile' );
        }
        
    },
    handleButtonPress : function(component, event, helper) {
        var selectedCP = event.getParam("data");
        component.set("v.btnId",selectedCP[0].buttonId ); 
        helper.createNewCompetitorProductHelper( component, event, helper );
    },
    saveProduct: function (component, event, helper) {
        component.set('v.spinner',true);
        helper.saveProductHelper( component, event, helper );
    },
    saveMarket: function (component, event, helper) {
        component.set('v.spinner',true);
        helper.saveMarketHelper( component, event, helper );
    },
    hideProductModal: function (component, event, helper) {
        component.set("v.addProductDetails.Price",'');
        component.set("v.addProductDetails.Velocity",'');
        component.set("v.selectedLookUpRecord1Product",[]);
        component.set("v.errMsg1",'');
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            helper.hidePopupHelper( component,'newProductModal','newModalProduct' );    
        }else{ 
            helper.hidePopupHelper( component,'newProductModalMobile','newModalProductMobile' );
        }
        
    },
    hideMarketModal: function (component, event, helper) {
        component.set("v.addMarketDetails.Shares",'');
        component.set("v.selectedLookUpRecord1Market",[]);
        component.set("v.errMsg3",'');
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            helper.hidePopupHelper( component,'newMarketModal','newModalMarket' );  
        }else{ 
            helper.hidePopupHelper( component,'newMarketModalMobile','newModalMarketMobile' );
        }
        
    },
    handleCompetitorProduct : function(component, event, helper) {
        
        var selectedCP = event.getParam("newCompetitorProduct");
        console.log( '::::selectedCP TT :::::' + JSON.stringify( selectedCP ) );
        console.log( selectedCP ) ;
        var isCloseMobile = event.getParam("isClose");
        console.log( isCloseMobile ) ;
        //if( !isCloseMobile ){
            var selectdVal = {};
            selectdVal.val = selectedCP.Id;
            selectdVal.text = selectedCP.Name;
            selectdVal.objName = 'Competitor_Products__c';
            selectdVal.filterVal = '';
            console.log( selectdVal );
            console.log( component.get("v.btnId") );
            var btnId = component.get("v.btnId");
            if( btnId == 'ProductNew')
                component.set("v.selectedLookUpRecord1Product",selectdVal);
            if( btnId == 'ProductEdit')
                component.set("v.selectedLookUpRecordEDITProduct",selectdVal);
        //}
        
        if( isCloseMobile ){
            var mobile = component.find('mobileDiv');
            $A.util.removeClass(mobile, 'slds-hide');
            $A.util.addClass(mobile, 'slds-show');
        }
        
    },
    hideEditProductModal: function (component, event, helper) {
        component.set("v.editProductDetails.Price",'');
        component.set("v.editProductDetails.Velocity",'');
        component.set("v.selectedLookUpRecordEDITProduct",[]);
        component.set("v.errMsg2",'');
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            helper.hidePopupHelper( component,'editProductModal','editModalProduct' );
        }else{ 
            helper.hidePopupHelper( component,'editProductModalMobile','editModalProductMobile' );
        }
    },
    hideEditMarketModal: function (component, event, helper) {
        component.set('v.errMsg4', '');
        component.set("v.addMarketDetails.Shares",'');
        component.set("v.selectedLookUpRecord1Market",[]);
        var device = $A.get("$Browser.formFactor");
        if( device == 'DESKTOP' ){
            helper.hidePopupHelper( component,'editMarketModal','editModalMarket' );
        }else{ 
            helper.hidePopupHelper( component,'editMarketModalMobile','editModalMarketMobile' );
        }
    },
    updateProductAssessment:function(component, event,helper){
        component.set('v.spinner',true);
        helper.updateProductHelper(component, event,helper);
    },
    updateMarket:function(component, event,helper){
        component.set('v.spinner',true);
        helper.updateMarketHelper(component, event,helper);
    },
    productTableFirstPage: function(component, event, helper) {
        component.set("v.pageNumberProduct", 1);
        helper.renderProductTable(component,component.get("v.segmentValue"));
    },
    productTablePrevPage: function(component, event, helper) {
        component.set("v.pageNumberProduct", Math.max(component.get("v.pageNumberProduct")-1, 1));
        helper.renderProductTable(component,component.get("v.segmentValue"));
    },
    productTableNextPage: function(component, event, helper) {
        component.set("v.pageNumberProduct", Math.min(component.get("v.pageNumberProduct")+1, component.get("v.maxPageProduct")));
        helper.renderProductTable(component,component.get("v.segmentValue"));
    },
    productTableLastPage: function(component, event, helper) {
        component.set("v.pageNumberProduct", component.get("v.maxPageProduct"));
        helper.renderProductTable(component,component.get("v.segmentValue"));
    },
    marketTableFirstPage: function(component, event, helper) {
        component.set("v.pageNumberMarket", 1);
        helper.renderMarketTable(component);
    },
    marketTablePrevPage: function(component, event, helper) {
        component.set("v.pageNumberMarket", Math.max(component.get("v.pageNumberMarket")-1, 1));
        helper.renderMarketTable(component);
    },
    marketTableNextPage: function(component, event, helper) {
        component.set("v.pageNumberMarket", Math.min(component.get("v.pageNumberMarket")+1, component.get("v.maxPageMarket")));
        helper.renderMarketTable(component);
    },
    marketTableLastPage: function(component, event, helper) {
        component.set("v.pageNumberMarket", component.get("v.maxPageMarket"));
        helper.renderMarketTable(component);
    },
    openPage : function(component, event, helper) {
        var targetId = event.currentTarget.dataset.id;
        var device = $A.get("$Browser.formFactor");
        if( targetId !='' && device == 'DESKTOP'  ){            
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": targetId,
                'slideDevName': 'detail'
            });
            navEvt.fire();
        }else if( targetId != '' && ( device == 'PHONE' || device == 'TABLET' ) ){
            sforce.one.navigateToSObject( targetId ,"detail"); 
        }
            else {
                alert('No Record Id found. Unable to load the target page');
            }
    },
    
})