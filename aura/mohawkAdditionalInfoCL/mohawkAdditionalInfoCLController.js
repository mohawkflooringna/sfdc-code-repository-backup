({
    doInit : function(component, event, helper) {
        var listHeight = component.get('v.listHeight');
        listHeight = parseInt(listHeight)+10;
        component.set('v.listHeight', listHeight);
        console.log('Print : '+component.get('v.recordId'));
        //var action = component.get('c.getRecordById'); - Commented by MB - 5/31/17
        var action = component.get('c.getAccountProfileId');
        action.setParams({
            'recordId': component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            if(component.isValid() && response !== null && response.getState() == 'SUCCESS'){
                //component.set('v.Account', response.getReturnValue()); - Commented by MB - 5/31/17
                component.set('v.Account_Profile_Id', response.getReturnValue());
            }
            console.log('account obj', response.getReturnValue());
        });
        
        $A.enqueueAction(action);
    },
    openPage : function(component, event, helper) {
        var targetId = event.currentTarget.dataset.id;
        console.log('targetId', targetId);
        
        
        if(targetId!='' && targetId!= undefined && targetId!= 'null'){
            
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": targetId,
                'slideDevName': 'detail'
            });
            console.log('navEvt', navEvt);
            
            navEvt.fire();
        }
        
        
        
        else {
            alert('No Record Id found. Unable to load the target page');
        }
    },
    
    openLEModal : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        console.log('Component clicked : ' + event.currentTarget.dataset.comp);
        evt.setParams({
            componentDef : "c:"+event.currentTarget.dataset.comp,
            componentAttributes: {
                recordId : component.get('v.recordId'),
                isFromRecordPage: 'true'
            }
        });
        evt.fire();
    },
    openMashup : function(component, event, helper) {
        
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:mashupTabItem",
            componentAttributes: {
                recordId : component.get('v.recordId'),
                mashupName : event.currentTarget.dataset.title,
                isFromRecordPage : 'true'
            }
        });
        evt.fire();
    }
})