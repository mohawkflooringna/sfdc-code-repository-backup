({
    doInit: function (cmp, evt, helper) {
        helper.initData(cmp);
    },
    //select all
    selectAll: function (cmp, evt, helper) {
        /*var territoryItems = cmp.get('v.territoryItems'),
            selectAll = cmp.get('v.selectTerritoryAllChecked'),
            checkedListDefault = [],
            uncheckedListDefault = [];
		
        console.log('selectAll: ' + selectAll);
        console.log('territoryItems: ' + JSON.stringify(territoryItems));
        for (var i = 0; i < territoryItems.length; i++){
            if(selectAll)
                territoryItems[i].selected = true;
            else
                territoryItems[i].selected = false;
        }
        console.log('territoryItems: ' + JSON.stringify(territoryItems));
        cmp.set('v.territoryItems', territoryItems);
        for (var i = 0; i < territoryItems.length; i++) {
            if (territoryItems[i].selected) {
                uncheckedListDefault.push(territoryItems[i].salId);
            } else {
                checkedListDefault.push(territoryItems[i].Id);
            }
        }
        if (evt.currentTarget.checked) {
            cmp.set('v.checkedList', checkedListDefault);
        } else {
            cmp.set('v.uncheckedList', uncheckedListDefault);
        }*/
        helper.selectAllHelper(cmp, evt, helper);
    },
    //search territories
    search: function (component, evt, helper) {

        var type = document.getElementById("type");
        var territoryType =  type.options[type.selectedIndex].value;//component.find("select11").get("v.value");
        var territoryRegion = component.get('v.territoryRegion');
        console.log('territoryRegion: ' + territoryRegion);
        if (territoryType == 'All' && territoryRegion === [""] || territoryRegion.length == 0) {
            territoryType = component.get('v.typeRegionVal').Type;
            var terrRegion = component.get('v.selectAllType'),
                terrResult = [];
            for (var i = 0; i < terrRegion.length; i++) {
                terrResult.push(terrRegion[i].label);
            }
            territoryRegion = terrResult;
        } else {
            territoryType = new Array(territoryType);
        }
        console.log("territoryType: " + territoryType);
        if (territoryType === "" || territoryRegion === [""] || territoryRegion.length == 0) {
            if (territoryType === "") {
                var typeErrmsg = component.find('type-errmsg');
                $A.util.removeClass(typeErrmsg, 'hide');
                $A.util.addClass(typeErrmsg, 'show');
            } else {
                var typeErrmsg = component.find('type-errmsg');
                $A.util.removeClass(typeErrmsg, 'show');
                $A.util.addClass(typeErrmsg, 'hide');
            }

            if (territoryRegion.length == 0) {
                var regionErrmsg = component.find('region-errmsg');
                $A.util.removeClass(regionErrmsg, 'hide');
                $A.util.addClass(regionErrmsg, 'show');
            } else {
                var regionErrmsg = component.find('region-errmsg');
                $A.util.removeClass(regionErrmsg, 'show');
                $A.util.addClass(regionErrmsg, 'hide');
            }
        } else {
			console.log( territoryType );
            console.log( territoryRegion );
            var typeErrmsg = component.find('type-errmsg');
            $A.util.removeClass(typeErrmsg, 'show');
            $A.util.addClass(typeErrmsg, 'hide');
            var regionErrmsg = component.find('region-errmsg');
            $A.util.removeClass(regionErrmsg, 'show');
            $A.util.addClass(regionErrmsg, 'hide');
            var saId = component.get("v.recordId");
            var sortData=[];
            var action = component.get("c.retrieveTerritories");
            action.setParams({
                "saId": saId,
                "type": territoryType,
                "region": territoryRegion
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    var result = response.getReturnValue();
                    component.set("v.hasTerritory", "true");
                    for(i in result){
            sortData.push(
                {
                    "salId":result[i].salId != null ? result[i].salId : "",
                    "selected":result[i].selected != null ? result[i].selected : "",
                    "Role":result[i].Role != null ? result[i].Role : "",
                    "Id":result[i].territory.Id != null ? result[i].territory.Id : "",
                    "Name":result[i].territory.Name != null ? result[i].territory.Name : "",
                    "Type__c":result[i].territory.Type__c != null ? result[i].territory.Type__c : "",
                    "Territory_Code__c":result[i].territory.Territory_Code__c != null ? result[i].territory.Territory_Code__c : "",
                    "Region__c":result[i].territory.Region__c != null ? result[i].territory.Region__c : "",
                    "Sector__c":result[i].territory.Sector__c != null ? result[i].territory.Sector__c : ""

                }
            );}
                    component.set("v.territoryItems", sortData);
        component.set( "v.selectedTab", 'Region__c' );

            component.set("v.arrowDirection", 'arrowdown');

        helper.sortHelper(component, event, 'Region__c');
                    //console.log('territoryItems', result);
                    var checked = [], unchecked = [];
                    component.set('v.uncheckedList', unchecked);
                    component.set('v.checkedList', checked);
                }
            });
            $A.enqueueAction(action);
        }
    },
    //set all selected status
    setSelectedTerritory: function (cmp, evt, helper) {
        cmp.set("v.selectTerritoryAllChecked", false);
        
        var checkedList = cmp.get('v.checkedList'),
            uncheckedList = cmp.get('v.uncheckedList'),            
            //isChecked = $(evt.currentTarget).is(':checked'),
            isChecked = evt.currentTarget.checked,
            defaultChecked = evt.currentTarget.dataset.isselect;

        //logic to filter checked and unchecked items
        function contains(arr, obj) {
            var i = arr.length;
            while (i--) {
                if (arr[i] === obj) {
                    return true;
                }
            }
            return false;
        }

        function removeByValue(arr, val) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == val) {
                    arr.splice(i, 1);
                    break;
                }
            }
        }

        if (defaultChecked == 'false') {
            if (isChecked) {
                checkedList.push(evt.currentTarget.dataset.id);
            } else {
                if (contains(checkedList, evt.currentTarget.dataset.id)) {
                    removeByValue(checkedList, evt.currentTarget.dataset.id);
                }
            }

            cmp.set('v.checkedList', checkedList);
        } else if (defaultChecked == 'true') {
            if (isChecked) {
                if (contains(uncheckedList, evt.currentTarget.dataset.salid)) {
                    removeByValue(uncheckedList, evt.currentTarget.dataset.salid);
                }
            } else {
                uncheckedList.push(evt.currentTarget.dataset.salid);
            }

            cmp.set('v.uncheckedList', uncheckedList);
        }
        //console.log(cmp.get('v.checkedList'));
        //console.log(cmp.get('v.uncheckedList'));

        /* if (isChecked) {

         var val = cmp.get('v.checkedList');
         val.push(id);
         cmp.set('v.checkedList', val);
         }*/
    },
    //save to backend
    saveToBackend: function (cmp, evt) {

        var saId = cmp.get("v.recordId"),
            uncheckedList = cmp.get('v.uncheckedList'),
            checkList = cmp.get('v.checkedList');
        console.log(checkList, uncheckedList);
        /* var options = document.getElementById("territories").getElementsByTagName("input");
         for (var i = 0; i < options.length; i++) {
         if (options[i].type == "checkbox") {

         if (options[i].checked && options[i].disabled !== true) {
         checkList.push(options[i].dataset.index);
         }
         }
         }*/
        if (checkList.length !== 0 || uncheckedList !== 0) {
            var action = cmp.get("c.createSalTerritories");
            action.setParams({
                "saId": saId,
                "selectedTerIds": checkList,
                "uncheckedSalIds": uncheckedList
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (cmp.isValid() && state === "SUCCESS") {

                    // console.dir("SUCC")

                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();
                    window.location.hash = '#/sObject/' + saId + '/view';
                    window.location.reload();
                    $A.get('e.force:refreshView').fire();
                }
            });
            $A.enqueueAction(action);
        }
    },
    // click unabled checked box
    returnFalse: function (cmp, evt, helper) {
        return false;
    },
    onMultiSelectChange: function (cmp) {
        var selectCmp = cmp.find("InputSelectMultiple");
        var resultCmp = cmp.find("multiResult");
        resultCmp.set("v.value", selectCmp.get("v.value"));
    },
    getDependentVal: function (cmp, evt, helper) {
        var isFirst = cmp.get('v.isFirstChange');
        if (isFirst) {
            var result = cmp.get('v.dependList')[0],
                depentVal = cmp.find('select11').elements["0"].value,
                selectizeTerrtories = cmp.get('v.multiFn'),
                newRegionList = [],
                multiSelection = selectizeTerrtories[0];
            for (var item in result) {
                if (item == depentVal) {
                    var list = result[item];
                    for (var i = 1; i < list.length; i++) {
                        newRegionList.push({id: JSON.parse(list[i]).value});
                    }
                }
            }
            cmp.set('v.dependListResult', newRegionList);
            multiSelection.selectize.clearOptions();

            newRegionList.map(function (item) {
                multiSelection.selectize.addOption(item)
            });
            multiSelection.selectize.refreshOptions(true);
        } else {
            var depentVal = cmp.find('select11').elements["0"].value;
            var action = cmp.get('c.getDependentPicklistOptions');
            action.setParams({
                "pObjName": 'Territory__c',
                "pControllingFieldName": 'Type__c',
                "pDependentFieldName": 'Region__c',
                "controllingFieldValue": depentVal
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (cmp.isValid() && state === "SUCCESS") {
                    var result = response.getReturnValue();
                    cmp.set('v.isFirstChange', true);
                    cmp.set('v.dependList', result);
                    var newRegionList = [];
                    for (var item in result) {
                        if (item == depentVal) {
                            var list = result[item];
                            for (var i = 1; i < list.length; i++) {
                                newRegionList.push({id: JSON.parse(list[i]).value});
                            }
                        }
                    }
                    cmp.set('v.dependListResult', newRegionList);
                    var selectizeTerrtories = $('#select-beast').selectize({
                        create: false,
                        valueField: 'id',
                        labelField: 'id',
                        searchField: 'id',
                        options: newRegionList,
                        placeholder: 'Please select territory region...             ',
                        onChange: function (val) {
                            cmp.set('v.territoryRegion', val);
                        },
                        plugins: ['remove_button']
                    });
                    cmp.set('v.multiFn', selectizeTerrtories);
                }
            });
            $A.enqueueAction(action);

        }


    },
    getDependentValNew: function (cmp, evt, helper) {
        var result = cmp.get('v.typeRegionVal'),
            allSelectedVal = cmp.get('v.selectAllType'),
            depentVal,selectizeTerrtories = cmp.get('v.multiFn'),
            newRegionList = [],multiSelection;
		//cmp.set('v.multiSelectOptions', []);
        if(cmp.find('select11')!=null && cmp.find('select11').elements["0"]!=null){
            depentVal = cmp.find('select11').elements["0"].value;

        }
        if (depentVal == 'All') {
            newRegionList = allSelectedVal;
          //  multiSelection.selectize.disable(); //commented to avoid exception
            //cmp.set("v.isDisableMultiSelect",true);                                
            for (var item in result) {
                if (item == depentVal) {
                    var list = result[item];
                    for (var i = 0; i < list.length; i++) {
                        newRegionList.push({label: list[i],value:list[i]});
                    }
                }
            }
            cmp.set("v.isDisableMultiSelect",false);
            cmp.set("v.multiSelectOptions",helper.removeDuplicates(newRegionList,'label'));
            
            
        } else {
            //cmp.set('v.isSelectAll', false);
            for (var item in result) {
                if (item == depentVal) {
                    var list = result[item];
                    for (var i = 0; i < list.length; i++) {
                        newRegionList.push({label: list[i],value:list[i]});
                    }
                }
            }
            cmp.set("v.isDisableMultiSelect",false);
            cmp.set("v.multiSelectOptions",helper.removeDuplicates(newRegionList,'label'));
        }
        //cmp.set("v.multiSelectOptions",helper.removeDuplicates(newRegionList,'label'));  
        
    },
    unCheckSelected: function (component, event, helper) {
        var id = event.currentTarget.dataset.salid,
            isChecked = $(event.currentTarget).is(':checked');

        if (!isChecked) {

            var val = component.get('v.uncheckedList');

            val.push(id);
            component.set('v.uncheckedList', val);
        }

    },
    sorting: function(component, event, helper) {    
        console.log('test');
        console.log( event.currentTarget.id );
        component.set( "v.selectedTab", event.currentTarget.id );
        helper.sortHelper(component, event, event.currentTarget.id);
    },
    isSelectAll: function (component, event) {
        var state = component.get('v.isSelectAll');
        component.set('v.isSelectAll', !state);

    },
    handleSelectChange: function(component, event, helper) {
        var selectedData = event.getParam("data");
        component.set( "v.territoryRegion",selectedData[0].values );
    },

})