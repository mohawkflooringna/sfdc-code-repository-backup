({
    initData:function(cmp) {
        var action = cmp.get("c.getPicklistFieldValues");
        action.setParams({
            "strObjName": 'Territory__c',
            "strFieldName": 'Type__c'
        });
        action.setCallback(this, function (response) {
            if (cmp.isValid() && response.getState() === "SUCCESS") {
                var result = response.getReturnValue();
                cmp.set("v.typeList", result.Type);
                cmp.set("v.typeRegionVal", result);
                var allResultList = [];
                
                for (var item in result){
                    if(item !== 'Type' && item !== '' && item !== 'null'){
                        for(var i=0;i<result[item].length; i++){
                            allResultList.push({
                                label:result[item][i],
                                value:result[item][i]
                            })
                        }
                        
                    }
                    
                }
                cmp.set('v.selectAllType', allResultList);
                cmp.set('v.dependList', allResultList);
                cmp.set("v.isDisableMultiSelect",false);
                cmp.set("v.multiSelectOptions",this.removeDuplicates(allResultList,'label'));
            }
            
        });
        $A.enqueueAction(action);
    },
    sortHelper: function(component, event, sortFieldName) {
        var currentDir = component.get("v.arrowDirection");
        var AllData = component.get("v.territoryItems");
        console.log('all');
        console.log( AllData );
        var sortData =[];
        // alert('1');
        // alert(AllData[i].territory.Id);
        
        for(i in AllData){
            sortData.push(
                {
                    "salId":AllData[i].salId != null ? AllData[i].salId : "",
                    "selected":AllData[i].selected != null ? AllData[i].selected : "",
                    "Role":AllData[i].Role != null ? AllData[i].Role : "",
                    "Id":AllData[i].Id != null ? AllData[i].Id : "",
                    "Name":AllData[i].Name != null ? AllData[i].Name : "",
                    "Type__c":AllData[i].Type__c != null ? AllData[i].Type__c : "",
                    "Territory_Code__c":AllData[i].Territory_Code__c != null ? AllData[i].Territory_Code__c : "",
                    "Region__c":AllData[i].Region__c != null ? AllData[i].Region__c : "",
                    "Sector__c":AllData[i].Sector__c != null ? AllData[i].Sector__c : ""
                    
                }
            );
        }
        
        // alert('test');
        if ( currentDir == 'arrowdown' ) {
            // set the arrowDirection attribute for conditionally rendred arrow sign  
            component.set("v.arrowDirection", 'arrowup');
            // set the isAsc flag to true for sort in Assending order.  
            component.set("v.isAsc", true);
            sortData.sort(function(a,b){
                // alert(b[sortFieldName])
                if( !isNaN( a[sortFieldName] ) || !isNaN( b[sortFieldName] ) )
                    return (a[sortFieldName] > b[sortFieldName]) ? 1 : ((a[sortFieldName] < b[sortFieldName]) ? -1 : 0);
                else
                    return (a[sortFieldName].toLowerCase() > b[sortFieldName].toLowerCase()) ? 1 : ((a[sortFieldName].toLowerCase() < b[sortFieldName].toLowerCase()) ? -1 : 0);
            });
        } else {
            component.set("v.arrowDirection", 'arrowdown');
            component.set("v.isAsc", false);
            sortData.sort(function(a,b){                
                if( !isNaN( a[sortFieldName] ) || !isNaN( b[sortFieldName] ) )
                    return (b[sortFieldName] > a[sortFieldName]) ? 1 : ((b[sortFieldName] < a[sortFieldName]) ? -1 : 0);
                else
                    return (b[sortFieldName].toLowerCase() > a[sortFieldName].toLowerCase()) ? 1 : ((b[sortFieldName].toLowerCase() < a[sortFieldName].toLowerCase()) ? -1 : 0);
            });            
        }
        console.log('sort');
        console.log( sortData );
        var tempData=[];
        for(i in AllData){
            tempData.push(
                {
                    "salId":AllData[i].salId != null ? AllData[i].salId : "",
                    "selected":AllData[i].selected != null ? AllData[i].selected : "",
                    "Role":AllData[i].Role != null ? AllData[i].Role : "",
                    "territory":sortData!=null?sortData:""
                }
            );
        }
        console.log('tempData');
        console.log(tempData);
        
        component.set("v.territoryItems", sortData);
    },
    removeDuplicates:function(originalArray, objKey) {
        var trimmedArray = [];
        var values = [];
        var value;
        
        for(var i = 0; i < originalArray.length; i++) {
            value = originalArray[i][objKey];
            
            if(values.indexOf(value) === -1) {
                trimmedArray.push(originalArray[i]);
                values.push(value);
            }
        }
        
        return trimmedArray;
    },
    selectAllHelper : function( cmp, evt, helper ){
        var territoryItems = cmp.get('v.territoryItems'),
            selectAll = cmp.get('v.selectTerritoryAllChecked'),
            checkedListDefault = [],
            uncheckedListDefault = [];
        
        if( selectAll ){
            for (var i = 0; i < territoryItems.length; i++){
                territoryItems[i].selected = true;
                checkedListDefault.push(territoryItems[i].Id);
            }
            cmp.set('v.territoryItems', territoryItems);
        }else{
            for (var i = 0; i < territoryItems.length; i++){
                territoryItems[i].selected = false;
                uncheckedListDefault.push(territoryItems[i].salId);
            }
            cmp.set('v.territoryItems', territoryItems);
        }
        if (evt.currentTarget.checked) {
            cmp.set('v.checkedList', checkedListDefault);
        } else {
            cmp.set('v.uncheckedList', uncheckedListDefault);
        }
    },
})