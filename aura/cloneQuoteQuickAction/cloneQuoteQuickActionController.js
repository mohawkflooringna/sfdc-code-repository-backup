({
    doInit: function(component, event, helper) {
        helper.initHelper(component, event, helper);
    },
    selectQuote: function(component, event, helper) {
        helper.selectQuoteHelper(component, event, helper);
    },
    closeMessage: function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
    next: function(component, event, helper) {
        helper.hybrisMashup(component, event, helper);
    },
    previous: function(component, event, helper) {
        component.set('v.isHybris', false);
        component.set('v.selectedDealer', null);
        component.set('v.isDealerSelected', false);
        component.set('v.ExternalQuoteId', null);
    }
    
})