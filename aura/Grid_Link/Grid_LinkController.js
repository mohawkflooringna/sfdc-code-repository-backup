({
    doSecondaryDataDisplay: function(component, event, helper){
        var appEvent = $A.get("e.c:Grid_LaunchSecondaryModalEvent");
        appEvent.setParams({
            "sObjectName" : component.get("v.sObjectName"),
            "recordId": component.get("v.recordId"),
            "productCategoryId": component.get("v.productCategoryId"),
            "warehouseValue": component.get("v.warehouseValue")
        });
        appEvent.fire();
    }
})