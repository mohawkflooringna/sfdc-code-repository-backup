({
  search: function (component, event, helper) {
    component.set('v.hasInfo', true);
    component.set('v.hasMoreRecords', false); // Added by MB - 06/20/17
    var territoryValue = component.get('v.territoryValue');
    console.log('territoryValue before', territoryValue);
    if (territoryValue == undefined) {
      territoryValue = '';
    }
    console.log('territoryValue after', territoryValue);
    // var name = document.getElementById('name').value;
    var name = component.find('input-name').elements[0].value;
    var areaCode = component.find('area-code').elements[0].value;
    var state = component.find('state').elements[0].value;
    var metro = component.find('metro').elements[0].value;
    // var areaCode = document.getElementById('area-code').value;
    // var state = document.getElementById('state').value;
    // var metro = document.getElementById('metro').value;
    // var territory = document.getElementById('territory').value;
    // var territory = document.querySelector('input[name="radio-options"]:checked').value;
    var territoryList = component.find('territory');
    var territory = '';
    // for(var i = 0; i<territory.length; i++) {
    if(territoryList[0].elements[0].checked) {
      territory = 'All Users'
    } else if(territoryList[0].elements[0].checked){
      territory = 'All Users Within My Territory'
    }
    // }
    console.log(component.find('territory'), name, areaCode, metro, state, territory);
    var action = component.get("c.getTerrUserOwnerlist");
    action.setParams({'Name': name, 'Territory': territory, 'AreacodeValue': areaCode, 'StateValue': state, 'Majormetrosvalue': metro, 'Territoryvalue': territoryValue});
    console.log(action.getParams());
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        console.log(response.getReturnValue());
        // console.log(response.getReturnValue())
        if (response.getReturnValue().length == 0) {
          component.set('v.hasInfo', false);
        }
        component.set('v.accountList', response.getReturnValue());
		//Added by MB - 06/20/17
        if(response.getReturnValue().length === 100){
            component.set('v.hasMoreRecords', true);
        }
        // var resultTable = component.find('result-table');
        // $A.util.addClass(resultTable, 'slds-show');
        // $A.util.removeClass(resultTable, 'slds-hide');
      }
    });
    $A.enqueueAction(action);
  },
  getTerrUserOwnerlist: function (component, event, helper) {
    // console.log('innn---');
    // debugger
    component.set('v.hasMoreRecords', false); // Added by MB - 06/20/17
    var action = component.get("c.getTerrUserOwnerlist");
    action.setParams({'Name': '', 'AreacodeValue': '', 'StateValue' : '', 'Majormetrosvalue': '', 'Territory' :'All Users', 'Territoryvalue' : component.get('v.territoryValue')});
    // action.setParams({'Name': '', 'AreacodeValue': '', 'StateValue' : '', 'Majormetrosvalue': '', 'Territory' :'All Users', 'Territoryvalue' : ''});
    // console.log(action.getParams());
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        console.log(response.getReturnValue());
        component.set('v.accountList', response.getReturnValue());
        if (response.getReturnValue().length === 0) {
          component.set('v.hasInfo', 'false');
        }
        //Added by MB - 06/20/17
        if(response.getReturnValue().length === 100){
            component.set('v.hasMoreRecords', true);
        }
      }
    });
    $A.enqueueAction(action);
  },
  getTerrUserOwnerlistWithTerritory: function (component, event, helper) {
    var currentTime = Date.parse(new Date());
    component.set('v.hasInfo', true);
    component.set('v.hasMoreRecords', false); // Added by MB - 06/20/17
    var action = component.get("c.getTerrUserOwnerlist");
    action.setParams({'Name': '', 'Territory': 'All Users', 'AreacodeValue': '', 'StateValue': 'none', 'Majormetrosvalue': 'none', 'Territoryvalue': '', 'CurrentTime': currentTime});
    console.log(action.getParams());
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        console.log(response.getReturnValue());
        // console.log(response.getReturnValue())
        if (response.getReturnValue().length == 0) {
          component.set('v.hasInfo', false);
        }
        component.set('v.accountList', response.getReturnValue());
        //Added by MB - 06/20/17
        if(response.getReturnValue().length === 100){
            component.set('v.hasMoreRecords', true);
        }
        // var resultTable = component.find('result-table');
        // $A.util.addClass(resultTable, 'slds-show');
        // $A.util.removeClass(resultTable, 'slds-hide');
      }
    });
    $A.enqueueAction(action);
  },
  getStatePickListvalues : function (component, event, helper) {
    var action = component.get("c.getStatePickListvalues");
    // action.setParams({'Name': name, 'Territory': territory});
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        console.log(response.getReturnValue());
        component.set('v.stateList', response.getReturnValue());
      }
    });
    $A.enqueueAction(action);
  },
  getMajorMetorsPickListvalues : function (component, event, helper) {
    var action = component.get("c.getMajorMetorsPickListvalues");
    // action.setParams({'Name': name, 'Territory': territory});
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        // console.log(response.getReturnValue());
        component.set('v.metrosList', response.getReturnValue());
      }
    });
    $A.enqueueAction(action);
  },
  getTerritoryValue : function (component, event) {
    // debugger
    var action = component.get("c.ChangeOwner");
    action.setParams({"recordId" : component.get('v.recordId')});
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        // console.log(response.getReturnValue());
        // debugger
        console.log("c.ChangeOwner", response.getReturnValue());

        component.set('v.territoryValue', response.getReturnValue());
        if(response.getReturnValue() !== '') {

          component.set('v.isCommercial', false);
        } else {
          component.set('v.isCommercial', true);

        }
        console.log('territoryValue set', component.get('v.territoryValue'));
        this.getTerrUserOwnerlist(component, event);
      }
    });
    $A.enqueueAction(action);
  },
})