({
    initHelper : function(component, event, helper){
        var recId = component.get('v.recordId');
        var objName = component.get('v.sObjectName');
        var proId;
        var mashupName='';
        var businessOverView = false;
        if(objName == 'Account'){
            businessOverView = false;
            component.set('v.isAccountRecord',true); 
        }
        else{
            businessOverView = true;
            component.set('v.isAccountRecord',false);
            component.set('v.recordId','');
        }
        console.log('Business Overview: ' + businessOverView)
        var action = component.get("c.getMashupRecordDisplayNames");
        var recordId = null;
        //Added by MB - 3/23/18
        if(businessOverView == false){
            recordId = component.get('v.recordId');
        }
        action.setParams({
            'recordId': recordId
        });//End of code - MB - 3/23/18
        action.setCallback(this, function(response) {
            var wrapperClassValue = response.getReturnValue();
            var state = response.getState();
            var mashupRecords = [];
            if(component.isValid() && state === "SUCCESS"){
                if(businessOverView==true){
                    console.log(wrapperClassValue.BusinessOverviewMashupRecords);
                    mashupRecords = wrapperClassValue.BusinessOverviewMashupRecords;
                    //For initial loading of iframe in doInit
                    if(mashupRecords.length>0){
                        component.set('v.mashupRecords', mashupRecords);
                        component.set("v.mashupName",wrapperClassValue.BusinessOverviewMashupRecords[0].Name);
                        component.set('v.iframeUrl', wrapperClassValue.BusinessOverviewMashupRecords[0].Target_URL__c);
                        if(wrapperClassValue.BusinessOverviewMashupRecords[0].Name === $A.get("$Label.c.Select_Dashboard")){
                            component.set("v.isSelectaDashboard",true);
                        }
                    }
                }
                else{
                    mashupRecords = wrapperClassValue.AccountOverviewMashupRecords;
                    //For initial loading of iframe in doInit
                    if(mashupRecords.length>0){
                        component.set('v.mashupRecords', mashupRecords);
                        component.set("v.mashupName",wrapperClassValue.AccountOverviewMashupRecords[0].Name);
                        component.set("v.iframeUrl", wrapperClassValue.AccountOverviewMashupRecords[0].Target_URL__c);
                        mashupName=wrapperClassValue.AccountOverviewMashupRecords[0].Name;
                    }
                }
                var mashupMap=component.get('v.mashupRecords');
                var mapTemp=[];
                var i=0;
                console.log(mashupMap);
                if(mashupMap != undefined){
                    for(i=0;i<mashupMap.length;i++){
                        mapTemp.push({
                            'Name': mashupMap[i].Name,
                            'Target_URL__c': mashupMap[i].Target_URL__c
                        });
                    }
                }
            }else{
                console.log('Error Occurred');
            }
            component.set('v.mashupNameUrlMap',mapTemp);
            //this.forAccountRecordHelper(component, event, helper,mashupName);
        });
        $A.enqueueAction(action);
    },
    
    /*forAccountRecordHelper : function(component, event, helper,mashupName){
        var isAccountRecord=component.get('v.isAccountRecord');
        if(isAccountRecord==true){
            
            //  var mashupName=component.get('v.mashupName');
            //alert('mashupName:::'+mashupName+'::::recordId::::'+component.get('v.recordId'));
            
            var action = component.get("c.getMashupRecordNew");
            
            action.setParams({ 
                "mashupName" : component.get('v.mashupName'),
                "recordId" : component.get('v.recordId')
            });
            
            action.setCallback(this, function(response){
                var resultSet = response.getReturnValue();
                component.set('v.IframeUrl', resultSet);
                
            });
            $A.enqueueAction(action);
            
        }
        
    },*/
    
    setMashupNameMethodHelper: function(component, event, helper){
        var mashupMap=component.get('v.mashupNameUrlMap');
        var key= component.find("Select").get("v.value");
        console.log(mashupMap);
        if(key === 'Select a Dashboard'){
            component.set("v.isSelectaDashboard",true);
        }
        else{
            for(var i=0;i<mashupMap.length;i++){
                if(mashupMap[i].Name === key){
                    component.set("v.isSelectaDashboard",false);
                    component.set('v.iframeUrl',mashupMap[i].Target_URL__c);
                    //component.set('v.mashupName',mashupMap[i].Name);
                    console.log(mashupMap[i].Target_URL__c);
                }
            }
        }
        //console.log(mashupMap);
        //this.forAccountRecordHelper(component, event, helper,key);
    }
})