({
    doInit: function(component, event, helper) {
        helper.initHelper(component, event, helper);
    },
    
    setMashupNameMethod: function(component, event, helper) {
        helper.setMashupNameMethodHelper(component, event, helper);
    },
    //Not in use
    /*setMashupName : function(component, event, helper) {
        var val= component.find("Select").get("v.value");
       // alert(val);
      component.set("v.mashupName",val);
       // alert(val.Name);
        
        var action = component.get("c.getMashupRecord");
        action.setParams({
            'mashupName': val
        });
        action.setCallback(this, function(response) {
            var result = response.getReturnValue();

            component.set('v.iframeUrl', result.Target_URL__c);

          //  alert(result);
            
        });
        $A.enqueueAction(action);
        
    }*/
    backToRecord: function(component, event, helper) {
        sforce.one.navigateToURL('/'+component.get('v.recordId'));
        //window.open('lightning/r/Account/'+component.get('v.recordId')+'/view','_parent');
    }
    
})