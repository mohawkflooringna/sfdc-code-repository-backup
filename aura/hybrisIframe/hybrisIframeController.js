({
    doInit : function(component, event, helper) {
    	// console.log('in hybrisIframe controller', component.get('v.isFromRecordPage'));
    	/*var IframeUrl = component.get('v.IframeUrl');
    	if(IframeUrl)
    		component.set('v.IframeUrl', IframeUrl);*/
            console.log(component.get('v.mashupName'), '-Mashup')
        if(component.get('v.mashupName')!=''){
            var action = component.get("c.getUrlByName");
            action.setParams({ 
                "mashupName" : component.get('v.mashupName')
            });

            action.setCallback(this, function(response){
                if(component.isValid() && response !== null && response.getState() == 'SUCCESS'){
                    // console.log('iii')
                    var resultSet = response.getReturnValue();
                    helper.buildQueryStringForUrl(resultSet, component);
                    /* moved to helper
                    var url = resultSet.Url__c;
                    if(url.indexOf('=RECORD_ID')!=-1){
                        // console.log('record id', component.get('v.recordId'), url);
                        url = url.replace('=RECORD_ID', '='+component.get('v.recordId'));
                        console.log('record id', component.get('v.recordId'), url);
                    }
                    component.set('v.IframeUrl', url);*/
                    // component.set('v.IframeUrl', resultSet.Url__c);
                    // console.dir(response.getReturnValue());
                }
            });

            $A.enqueueAction(action);
        }
    },
    backToRecord: function(component, event, helper){
    	// to navigate/go back to record page
    	var navEvt = $A.get("e.force:navigateToSObject");
			navEvt.setParams({
				"recordId": component.get('v.recordId')
			});
			navEvt.fire();
    }
})