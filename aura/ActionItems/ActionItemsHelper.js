({
rerender : function(cmp, helper){
	this.superRerender();
// do custom rerendering here
},
//get Object Name to change the display info
getObjName : function (component, event, helper) {
	var recordId = component.get("v.recordId");
	var action = component.get("c.getObjectNameById");
	action.setParams({ "recordId" : recordId });
	action.setCallback(this, function(response) {
		var state = response.getState();
		if (component.isValid() && state === "SUCCESS") {
			component.set("v.objName", response.getReturnValue());
			if(response.getReturnValue() == 'Event')
				component.set('v.isInEvent', 'true');
			// console.log(response.getReturnValue());
			this.init(component, event, helper, response.getReturnValue());
		}
	});
	$A.enqueueAction(action);
},
retrieveSAL : function(component, event, helper) {
	var id = component.get("v.recordId");
	// console.log('test'+id);
	var action = component.get("c.retrieveSALs");
	action.setParams({ "id" : id });
	action.setCallback(this, function(response) {
		var state = response.getState();
		if (component.isValid() && state === "SUCCESS") {
			component.set("v.actionItems", response.getReturnValue());
			component.set("v.myAppointmentList", response.getReturnValue().myAppointmentList);
			// component.set("v.isCommercial", response.getReturnValue().isCommercial);
			console.log(response.getReturnValue());
		}
	});
	$A.enqueueAction(action);
},
init : function(component, event, helper, objectName) {
	// console.log(objectName)
	// if(component.get("v.objName")=='Event') {
	if(objectName=='Event') {
		var action = component.get("c.getObjectRecord");
		action.setParams({ "apiName" : 'Event', 'recordId': component.get('v.recordId'), 'fields': 'Id, WhatId' });
		action.setCallback(this, function(response) {
			var state = response.getState();
			// console.log(response.getReturnValue())
			if (component.isValid() && state === "SUCCESS") {
				var result = response.getReturnValue();
				// console.log(result[0].WhatId, (result[0].WhatId).substring(0,3))
				if((result[0].WhatId).substring(0,3)=='001')
					this.retrieveSAL(component, event, helper);
				else {
					component.set('v.isEventAndNonAccountRecord', 'true');
					console.log('Account is not available in Related-To');
				}
			}
		});
		$A.enqueueAction(action);
	} else
		this.retrieveSAL(component, event, helper);
	/*var id = component.get("v.recordId");
	// console.log('test'+id);
	var action = component.get("c.retrieveSALs");
	action.setParams({ "id" : id });
	action.setCallback(this, function(response) {
		var state = response.getState();
		if (component.isValid() && state === "SUCCESS") {
			component.set("v.actionItems", response.getReturnValue());
			component.set("v.myAppointmentList", response.getReturnValue().myAppointmentList);
			// component.set("v.isCommercial", response.getReturnValue().isCommercial);
			console.log(response.getReturnValue());
		}
	});
	$A.enqueueAction(action);*/
},
updateInfo : function(cmp, evt) {
	 var eventId = cmp.get("v.recordId");
	// console.log('test:'+eventId);
	var eventActionId = cmp.get("v.actionItems.eventActionId");
	var appointmentCheckboxList = document.getElementsByClassName("appointment-checkbox");
    var attachmentList = document.getElementsByClassName("include-attachment");
    // console.log(attachmentList)
    var updateList = [];
	var insertList = [];
    var actionList = [];
    // objectForInsert is the object for add object to insertList
    var objectForInsert  = new Object;
    objectForInsert.Action_List__c = '',
    objectForInsert.Include_Attachments_In_Email__c = '';
    objectForInsert.Event_Action__c = '';
	// objectForUpdate is the object for add object to updatetList
    var objectForUpdate = new Object;
    objectForUpdate.Action_List__c = '';
    objectForUpdate.Include_Attachments_In_Email__c = '';
    objectForUpdate.Event_Action__c = '';
    objectForUpdate.Id = '';
    
    for(var i=0; i<appointmentCheckboxList.length; i++) {
    	objectForInsert = {};
    	objectForUpdate = {};
        if (appointmentCheckboxList[i].type == "checkbox" ) {
            if (appointmentCheckboxList[i].checked && appointmentCheckboxList[i].disabled !== true) {
            	actionList.push(appointmentCheckboxList[i].dataset.index);
            	objectForInsert.Action_List__c = appointmentCheckboxList[i].dataset.index;
            	objectForInsert.Event_Action__c = eventActionId;
            	objectForInsert.Include_Attachments_In_Email__c = attachmentList[i].checked;
            	insertList.push(objectForInsert);
            } else if (appointmentCheckboxList[i].disabled === true) {
            	objectForUpdate.Action_List__c = appointmentCheckboxList[i].dataset.index;
            	objectForUpdate.Include_Attachments_In_Email__c = attachmentList[i].checked;
            	objectForUpdate.Event_Action__c = eventActionId;
            	objectForUpdate.Id = appointmentCheckboxList[i].dataset.id;
            	updateList.push(objectForUpdate);
            }
        }
    }
    console.log('insertMyAppList', insertList, 'updateList', updateList);
	var action = cmp.get("c.createMyAppointmentList");
	action.setParams({
		"eventId" : eventId,
		"actionList" : actionList,
		"insertMyAppList" : JSON.stringify(insertList),
		"updateMyAppList" : JSON.stringify(updateList)
	});
	action.setCallback(this, function(response) {
		var state = response.getState();
		if (cmp.isValid() && state === "SUCCESS") {

		 var dismissActionPanel = $A.get("e.force:closeQuickAction");
		 dismissActionPanel.fire();
		 $A.get('e.force:refreshView').fire();
		}
	});
	$A.enqueueAction(action);
  },
})