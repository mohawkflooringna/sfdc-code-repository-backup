trigger EventTrigger on Event (before insert,before update, after insert, after update, after delete) {

// Based on custom setting(Default_Configuration__c) value trigger will be executed
    if(UtilityCls.isTriggerExecute('Event') || Test.isRunningTest() ){ 
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                EventTriggerHandler.validateContact(Trigger.new);
                EventTriggerHandler.updateAppt(Trigger.new, true, false);
            }
            if(Trigger.isUpdate && EventTriggerHandler.runUpdateTrigger) {
                EventTriggerHandler.validateContact(Trigger.new);
                EventTriggerHandler.updateAppt(Trigger.new, false, true);
            }
        } 
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                EventTriggerHandler.updateAttendeeToName(Trigger.newMap.keySet());
                EventTriggerHandler.createEventAction(Trigger.newMap); //Added by MB - Bug 60273 - 08/09/18                
            }
            
            if(Trigger.isUpdate && EventTriggerHandler.runUpdateTrigger){
                EventTriggerHandler.updateAttendeeToName(Trigger.newMap.keySet());
                EventTriggerHandler.updateCustomActivity( Trigger.new,Trigger.newMap ); //Added by MS - Bug 70109 - 11/02/18
            } 
            
            /* Code start Added by MS - Bug 70109 - 11/02/18 */
            if( Trigger.isDelete ) 
                EventTriggerHandler.deleteCustomActivity( Trigger.oldMap.keySet() ); //Added by MS - Bug 70109 - 11/02/18    
            /* Code End Added by MS - Bug 70109 - 11/02/18 */
        }
    }
}