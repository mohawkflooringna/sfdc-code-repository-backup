/**************************************************************************

Name : OpportunityTeamMemberTrigger

===========================================================================
Purpose : This trigger is used for Opportunity Team Member.
===========================================================================
History:
--------
VERSION    AUTHOR               DATE           DETAIL          DESCRIPTION
1.0        Mohan Babu           06/08/2017     Created      Added method on Delete operation
1.1        Mohan Babu           08/04/2017     Modified     Added method on Update and Insert operation
***************************************************************************/

trigger OpportunityTeamMemberTrigger on OpportunityTeamMember (before insert,before update, before delete,after insert,after update,after delete) {
    
    if(UtilityCls.isTriggerExecute('OpportunityTeamMember') || Test.isRunningTest() 
      ){ // Based on custom setting(Default_Configuration__c) value trigger will be executed
        if(Trigger.isBefore){
            if(Trigger.isInsert){
               //OpportunityTeamMemberTriggerHandler.updateOppTeam(Trigger.new);
              OpportunityTeamMemberTriggerHandler.updateOppDetails(Trigger.new,false);  // Added Code SB 9-26-17
            }
            if(Trigger.isupdate){
                OpportunityTeamMemberTriggerHandler.updateOppTeam(Trigger.new);
                OpportunityTeamMemberTriggerHandler.updateOppDetails(Trigger.new,true);  // Added Code SB 9-26-17
            }
            if(Trigger.isDelete) {
                OpportunityTeamMemberTriggerHandler.DeleteOppShareAccess(Trigger.old); //Added code - MB - 08/04/2017
            }
        } 
        //system.debug('Outside of isInsert');
        if(Trigger.isAfter){
            //system.debug('Inside of isAfter');
            if(Trigger.isInsert) {
                OpportunityTeamMemberTriggerHandler.AddOppShareAccess(Trigger.new); //Added code - MB - 08/04/2017
                
            }
            if(Trigger.isUpdate){
                
            }
            if(Trigger.isDelete){
                OpportunityTeamMemberTriggerHandler.insOppTeamMember(Trigger.old); //Added by MB - 2/26/18
            }
        }
        //system.debug('Outside of isAfter');
    }
}