trigger ContentDocumentTrigger on ContentDocument (before insert, after insert, after update, before delete) {
    if(UtilityCls.isTriggerExecute('Notes')){
        if(trigger.isBefore){
            if(trigger.isDelete){
                ContentDocumentTriggerHandler.preventDeletion(Trigger.OldMap);
                System.debug('Content Document delete: ' + trigger.old);
            }
        }
    }
    
    // Start of Code - MB - Bug 70374 - 2/7/19
    if(trigger.isAfter){
        if(trigger.isInsert || trigger.isUpdate){
            ContentDocumentTriggerHandler.updateProject(trigger.new);
        }
    }
    // End of Code - MB - Bug 70374 - 2/7/19
}