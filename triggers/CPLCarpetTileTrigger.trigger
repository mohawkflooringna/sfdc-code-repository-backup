trigger CPLCarpetTileTrigger on CPL_Carpet_Tile__c(before insert, before update) {

    if(UtilityCls.isTriggerExecute('CPL_Carpet_Tile__c')  || Test.isRunningTest()){

        CPLObjectsTriggerHandler.updateCPLSalesRelationship(Trigger.new);
    }
    
}