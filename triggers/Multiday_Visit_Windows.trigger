trigger Multiday_Visit_Windows on mamd__Multiday_Visit_Windows__c (after insert,after update,after delete) {
    
    if( trigger.isInsert ){
        Multiday_Visit_WindowsHandler.isAfterInsertMultiDay( trigger.new );
    }
    if( trigger.isUpdate ){
        Multiday_Visit_WindowsHandler.isAfterUpdateMultiDay( trigger.new );
    }
    if( trigger.isDelete ){
        Multiday_Visit_WindowsHandler.isAfterDeleteMultiDay( trigger.old );
    }

}