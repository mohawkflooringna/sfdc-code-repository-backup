/**************************************************************************

Name : AccountTrigger

===========================================================================
Purpose : This class is used for Account Trigger.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         22/Dec/2016    Created
***************************************************************************/
trigger AccountTrigger on Account (before insert,before update, before delete, after insert, after update, after delete) {
    
    if(UtilityCls.isTriggerExecute('Account')  || Test.isRunningTest()){ // Based on custom setting(Default_Configuration__c) value trigger will be executed
        
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                AccountTriggerHandler.updateBusinessType(Trigger.new,true); 
                AccountTriggerHandler.ldsFlag(Trigger.new,false,true); // Added by SB - 08/21/17
                AccountTriggerHandler.updateTerritoryForInsert(Trigger.new);
                AccountTriggerHandler.updateAccountProfile(Trigger.new);
            }
            if(Trigger.isUpdate && !UtilityCls.AsycupdateFromMAT) {
          //      AccountTriggerHandler.ldsFlag(Trigger.new,true,false); // Added by SB - 08/21/17
                AccountTriggerHandler.updateBusinessType(Trigger.new,false); // Added by MB - 09/05/17
                //AccountTriggerHandler.updateTerritoryForUpdate(Trigger.new); - Commented by MB - 06/19/17 - Replace with updateTerritoryForInsert
                AccountTriggerHandler.updateTerritoryForInsert(Trigger.new); // Added by MB - 06/19/17
                //Method to check open activities and active projects validation
                AccountTriggerHandler.checkOpenItems(Trigger.newMap, Trigger.oldMap);
                AccountTriggerHandler.updateAccountProfile(Trigger.new);
                AccountTriggerHandler.validateStrAccUpdate(Trigger.new); //Added by MB - 06/01/18
            }
            if(Trigger.isDelete){                
                //added by Mohan : 1-jan-2018
                AccountTriggerHandler.updatePartnerAccountsOnParentAccountChanges(Trigger.oldMap, null, 'DELETE');
                AccountTriggerHandler.updatePartnerAccountsOnRealEstateServiceProviderAccountChanges(Trigger.oldMap, null, 'DELETE');                
                AccountTriggerHandler.deleteCPLSales( Trigger.oldMap ); // Added by Mudit - 30/11/18 - Bug 68213
            }            
        } 
        
        if(Trigger.isAfter){
            if(Trigger.isInsert){
                 AccountTriggerHandler.createAccountTeam(Trigger.newMap,null);
                AccountTriggerHandler.CollecttheAccountids(Trigger.new);
                //AccountTriggerHandler.createSamplePerformanceInventory(Trigger.newMap); - Commented by MB - 10/26/18 - Bug 66920
                //AccountTriggerHandler.AssignPublicGroupToAccShare_Commercial(Trigger.new); // Added by MB - 6/7/17 - commented on 06/19/17
                //AccountTriggerHandler.CallAssignPGToAccShare_Commercial(Trigger.new); // Added by MB - 06/09/17
                AccountTriggerHandler.AccessToTeamMemberOrSuperiors_NonInv(Trigger.new); // Added by MB - 06/19/17
                
                //added by Mohan : 1-jan-2018
                AccountTriggerHandler.updatePartnerAccountsOnParentAccountChanges(Trigger.newMap, null, 'CREATE');
                AccountTriggerHandler.updatePartnerAccountsOnRealEstateServiceProviderAccountChanges(Trigger.newMap, null, 'CREATE');
                AccountTriggerHandler.createAccountCPLSalesRelationships(Trigger.new);
            }
            
            if(Trigger.isUpdate && !UtilityCls.AsycupdateFromMAT && !AccountTriggerHandler.updatedFromRelAcc){
                AccountTriggerHandler.ChangePublicGroupAssignmentonTerrChange(Trigger.newMap, Trigger.oldMap); // Added by MB - 6/9/17
                //AccountTriggerHandler.CallChangeAssignPGToAccShare(Trigger.newMap, Trigger.oldMap); // Added by MB - 06/09/17
                AccountTriggerHandler.createAccountTeamOnUpdate(Trigger.newMap, Trigger.oldMap);
                //AccountTriggerHandler.createSamplePerformanceInventory(Trigger.newMap); - Commented by MB - 10/26/18 - Bug 66920
                AccountTriggerHandler.updateMHKOwnerOnChange(Trigger.newMap, Trigger.oldMap); // Added by MB - 5/30/18
                //added by Mohan : 1-jan-2018
               AccountTriggerHandler.updatePartnerAccountsOnParentAccountChanges(Trigger.newMap, Trigger.oldMap, 'UPDATE');   
               AccountTriggerHandler.updatePartnerAccountsOnRealEstateServiceProviderAccountChanges(Trigger.newMap, Trigger.oldMap, 'UPDATE');
               AccountTriggerHandler.createAccountCPLSalesRelationships(Trigger.new);
            }      
            
            if(Trigger.isDelete){
                AccountTriggerHandler.validateMHKAccTeamAfterMerge(Trigger.old); 
                AccountTriggerHandler.deleteAccountProfile(Trigger.oldMap); // Added by MB - 10/11/18 - Bug 66291
                OutBoundDeletion.onDeleteAccount(trigger.Old);//Added by - Nagendra 12-20-2018
            }
        }
    } 
    
}