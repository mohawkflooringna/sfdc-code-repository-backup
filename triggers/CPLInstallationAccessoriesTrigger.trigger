trigger CPLInstallationAccessoriesTrigger on CPL_Installation_Accessories__c(before insert, before update) {

    if(UtilityCls.isTriggerExecute('CPL_Installation_Accessories__c')  || Test.isRunningTest()){

        CPLObjectsTriggerHandler.updateCPLSalesRelationship(Trigger.new);
    }
    
}