trigger CPLTileTrigger on CPL_Tile__c(before insert, before update) {

    if(UtilityCls.isTriggerExecute('CPL_Tile__c')  || Test.isRunningTest()){

        CPLObjectsTriggerHandler.updateCPLSalesRelationship(Trigger.new);
    }
    
}