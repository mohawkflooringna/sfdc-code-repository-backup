trigger DeletedOppTeamMemberTrigger on Opportunity_Team_Member__c (after insert) {
	
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            DeletedOppTeamMemberTriggerHandler.deleteAfterInsert(Trigger.newMap);
        }
    }
}