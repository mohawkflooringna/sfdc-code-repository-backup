trigger DeletedOppLineItemTrigger on Opportunity_Line_Item__c (after insert) {
	
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            DeletedOpptLineItemTriggerHandler.deleteAfterInsert(Trigger.newMap);
        }
    }
}