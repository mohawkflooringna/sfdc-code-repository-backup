/**************************************************************************

Name : AccountProfileTrigger

===========================================================================
Purpose : This class is used for Account Profile Trigger.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Anand          08/10/2017    Created
***************************************************************************/
trigger AccountProfileTrigger on Account_Profile__c(before insert, before update, after update) {
          
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                 // AccountProfileTriggerHandlerTotal.validateFieldValues(Trigger.New);
            }

            if(Trigger.isUpdate){
                //    AccountProfileTriggerHandlerTotal.validateFieldValues(Trigger.New);

                //AccountProfileTriggerHandlerTotal.updateAccountProfile(Trigger.New, Trigger.oldMap);
                AccountProfileTriggerHandler.overallCalculation( Trigger.New, Trigger.oldMap );
            }
        }
}