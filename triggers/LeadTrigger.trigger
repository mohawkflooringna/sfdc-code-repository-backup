/**************************************************************************

Name : LeadTrigger

===========================================================================
Purpose : This class is used for Lead  Trigger.
===========================================================================
History:
--------
VERSION    AUTHOR         DATE           DETAIL          DESCRIPTION
1.0        Lester         22/Dec/2016    Created
           Mudit          06/Oct/2017    Updated        Call Lead Assignment Method.  
***************************************************************************/
trigger LeadTrigger on Lead (before insert,before update, after insert, after update) {
    
                List<Lead> LeadListTriggerNew=new List<Lead>();

    if(UtilityCls.isTriggerExecute('Lead')){ // Based on custom setting(Default_Configuration__c) value trigger will be executed
        
        if(Trigger.isBefore){
            if(Trigger.isInsert || Trigger.isUpdate) {           
                LeadTriggerHandler.UpdateTerritoryFromZipCode(trigger.new, Trigger.isInsert, Trigger.isUpdate);   
                LeadTriggerHandler.ChangeLeadOwner(Trigger.new, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate);
                //LeadTriggerHandler.ChangeLeadOwner(Trigger.new);
            }
            //Code added by Mudit on 06-12-2017
            if( Trigger.isInsert ){
                List<lead>commercialLeadList = new List<lead>();
                for( Lead lead : trigger.new ){
                    if( !lead.From_Other_System__c && lead.Account__c==null ){
                        commercialLeadList.add( lead );
                    }
                }
                //LeadTriggerHandler.handleMultipleTerritory( commercialLeadList,true,false );
            }
        }
        
        
        if(Trigger.isAfter){
            if(Trigger.isInsert || Trigger.isUpdate) {
                //Method to convert the lead automatically
                LeadTriggerHandler.ConvertLeads(Trigger.newMap); 
                
                
                //Calling LeadAssignmentMethod From SF1 Leads only.
                if( userinfo.getUiTheme() == 'Theme4t' )
                    LeadTriggerHandler.leadAssignMentForSF1( Trigger.newMap.keySet() );
                
                
                
                if(Trigger.isUpdate){
                LeadTriggerHandler.changeRTtoMasterOpportunity(Trigger.new); //Added By Susmitha on NOV 8,2017 for 41203 Bug
                }
                
            }
        }
    } 
}