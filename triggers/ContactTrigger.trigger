trigger ContactTrigger on Contact (before insert,before update,after insert,after update) {
	if(UtilityCls.isTriggerExecute('Contact') ||  Test.isRunningTest()){ // Based on custom setting(Default_Configuration__c) value trigger will be executed
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                ContactTriggerHandler.updateContact(Trigger.new, Trigger.old, true, false);
                //ContactTriggerHandler.ldsFlag(Trigger.new,false,true); - Commented by MB as we don't need it anymore - tied towards Bug 69429 - 3/18/19
            }
            if(Trigger.isUpdate) {
                ContactTriggerHandler.checkOpenProject(Trigger.new); //Added by Nagen - Bug 69429 - 3/15/19
                ContactTriggerHandler.updateContact(Trigger.new, Trigger.old, false, true);
                //System.Debug('Contact Update LDS Flags :' );
                //ContactTriggerHandler.ldsFlag(Trigger.new,true,false);  - Commented by MB as we don't need it anymore - tied towards Bug 69429 - 3/18/19
            }
        }
        if(Trigger.isAfter){
            //if((Trigger.isInsert || Trigger.isUpdate) && !ContactTriggerHandler.updatedBatch){
                //ContactTriggerHandler.validateAndUpdateContact(Trigger.oldMap, Trigger.newMap); //Added by MB - 09/12/18 
            //}
            /*if(Trigger.isUpdate && !ContactTriggerHandler.updatedBatch){
                ContactTriggerHandler.checkDeleteFlag(Trigger.newMap);
            }*/
        }
    }
}