trigger ProductCustomerGroupRelationshipTrigger on Product_Customer_Group_Relationship__c(after insert, after update) {

    if(UtilityCls.isTriggerExecute('Product_Customer_Group_Relationship__c')  || Test.isRunningTest()){

    	ProductCustGroupRelTriggerHandler.updateIndependentAlignedBuyingGroup(Trigger.new);
    }
    
}