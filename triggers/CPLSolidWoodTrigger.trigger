trigger CPLSolidWoodTrigger on CPL_Solid_Wood__c(before insert, before update) {

    if(UtilityCls.isTriggerExecute('CPL_Solid_Wood__c')  || Test.isRunningTest()){

        CPLObjectsTriggerHandler.updateCPLSalesRelationship(Trigger.new);
    }
    
}