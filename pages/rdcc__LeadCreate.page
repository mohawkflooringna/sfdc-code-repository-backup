<apex:page standardController="Lead" extensions="rdcc.LeadCreateController" id="thepg" tabStyle="Lead" action="{!createLead}">
    <apex:includeScript value="{!URLFOR($Resource.rdcc__ConstructConnect, 'js/jquery-1.11.1.min.js')}"/>
    <script>
    
        window.onload = function() {
             
            if('{!manualOppSetting}'== 'No'){
                if('{!$CurrentPage.parameters.theme}' == 'Theme3')
                    document.getElementById("ModalId_classic").style.display = "block";
                else
                    document.getElementById("ModalId_light").style.display = "block";
            }
            if('{!$CurrentPage.parameters.theme}' != 'Theme3'){            
                overridePageMessages();
            }
            
        }
    
        function overridePageMessages(){  
            var textureEffect = '';
            //Uncomment below line for texture effect on page messages
            textureEffect = 'slds-theme--alert-texture';
            
            var errMsgs = document.getElementsByClassName('errorM3');
            var infoMsgs = document.getElementsByClassName('infoM3');
            var confirmMsgs = document.getElementsByClassName('confirmM3');
            if(errMsgs.length > 0){
                errMsgs[0].className = 'message slds-notify slds-notify--alert slds-theme--error customMessage '+textureEffect;
            }
            if(infoMsgs.length > 0){
                infoMsgs[0].className = 'message slds-notify slds-notify--toast customMessage '+textureEffect;
            }
            if(confirmMsgs.length > 0){
                confirmMsgs[0].className = 'message slds-notify slds-notify--alert slds-theme--success  customMessage '+textureEffect;
            }
            
        }
        
        
        
        function hideError(){
            document.getElementById("errorDiv").style.display = "none";
        }
    
    </script>
    
    <style>
        
        .msgIcon {
            display: none!important
        }
        
        .customMessage * {
            color: #fff!important
        }
        
        .customMessage {
            margin: 5px 0!important;            
            opacity: 1!important;
            width: 100%;
            font-size: 12px;
            border: 0px;
            padding-left: 10px;
        }
        
        .message {
            opacity: 1;
        } 
        
        #loading {
            width: 100%;
            height: 100%;
            top: 0px;
            left: 0px;
            position: fixed;
            display: block;
            opacity: 0.7;
            background-color: #fff;
            z-index: 99;
            text-align: center;
            }
            
            #loading-content {
            position: absolute;
            top: 50%;
            left: 50%;
            text-align: center;
            z-index: 100;
            }
            abbr[title]{text-decoration: none;}
            .msgIcon {
            display: none!important
            }

    </style>
   
    
    <apex:form id="thefrm" rendered="{!AND($CurrentPage.parameters.theme == 'Theme3',manualOppSetting == 'Yes')}">
      <apex:sectionHeader title="New Lead" subtitle="Lead Edit"/>
       
        <apex:pageBlock title="Lead Edit" >
            <apex:pageMessages /><br/>
            <apex:pageBlockSection title="Mandatory Fields" rendered="{!isMandatoryFieldError}">               
                <apex:repeat value="{!leadMandFieldMap}" var="mandField">                    
                    <apex:inputField value="{!newLead[mandField]}"/>
                </apex:repeat>
            </apex:pageBlockSection>
            
            <apex:pageBlockTable value="{!duplicateRecords}" rendered="{!hasDuplicateResult}" var="item">
                <apex:column >
                    <apex:facet name="header">Name</apex:facet>
                    <apex:outputLink value="/{!item['Id']}">{!item['Name']}</apex:outputLink>
                </apex:column>
                <!--apex:column >
                    <apex:facet name="header">Company</apex:facet>
                    <apex:outputField value="{!item['Phone']}"/>
                </apex:column!-->
                
                <apex:column >
                    <apex:facet name="header">Owner</apex:facet>
                    <apex:outputField value="{!item['OwnerId']}"/>
                </apex:column>
                <apex:column >
                    <apex:facet name="header">Last Modified Date</apex:facet>
                    <apex:outputField value="{!item['LastModifiedDate']}"/>
                </apex:column>
            </apex:pageBlockTable>
             
            <apex:pageBlockButtons >
                <apex:commandButton value="Save" action="{!createLead}" rendered="{!isMandatoryFieldError}"/>
                <apex:commandButton value="Save (Ignore Alert)" action="{!forceSaveLead}" rendered="{!isDuplicateSaveAllowed}"/>
                <apex:commandButton value="Cancel" action="{!back}" immediate="true"/>
            </apex:pageBlockButtons>
        </apex:pageBlock>
       
    </apex:form>
    
    
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <script>
            function renderSpinner(){
                var element = document.getElementById('loading');
                //alert(element);
                if(element.style.display == 'none'){
                    element.style.display = 'block';
                }else{
                    element.style.display = 'none';
                }
            }
        </script>
        <apex:stylesheet value="{!URLFOR($Resource.rdcc__SLDS202, 'assets/styles/salesforce-lightning-design-system-vf.min.css')}" />
        <apex:form id="thefrmLight" rendered="{!AND(manualOppSetting='Yes',OR($CurrentPage.parameters.theme == 'Theme4d',$CurrentPage.parameters.theme == 'Theme4t'))}" styleClass="slds">            
            
            <div id="loading" style="display:none">
                <div id="loading-content">
                    <div role="status" class="slds-spinner slds-spinner--medium" id="spinner">
                        <span class="slds-assistive-text">Loading</span>
                        <div class="slds-spinner__dot-a"></div>
                        <div class="slds-spinner__dot-b"></div>
                    </div>
                </div>
            </div>
            <div class="slds-page-header slds-page-header--object-home">
                <div class="slds-grid">
                    <div class="slds-col slds-has-flexi-truncate">
                        <div class="slds-media slds-no-space slds-grow">
                            <div class="slds-media__figure">
                                <span class="slds-icon_container">
                                    <svg class="slds-icon slds-icon-standard-lead" aria-hidden="true">
                                        <use xlink:href="{!URLFOR($Resource.SLDS202, '/assets/icons/standard-sprite/svg/symbols.svg#lead')}"></use>
                                    </svg>
                                </span>
                            </div>
                            <div class="slds-media__body">
                                <p class="slds-text-title--caps slds-line-height--reset">Leads</p>
                                <h1 class="slds-page-header__title slds-p-right--x-small">
                                    <span class="slds-grid slds-has-flexi-truncate slds-grid--vertical-align-center">
                                        <span class="slds-truncate">Lead Edit</span>
                                        <svg class="slds-button__icon slds-button__icon--right slds-no-flex" aria-hidden="true">
                                            <use xlink:href="{!URLFOR($Resource.SLDS202, '/assets/icons/utility-sprite/svg/symbols.svg#down')}"></use>
                                        </svg>
                                    </span>        
                                </h1>
                            </div>
                        </div>
                    </div>
                    
                    <apex:outputPanel id="bttnGrp">
                    <div id="bttnGrp" class="slds-col slds-no-flex slds-grid slds-align-top slds-p-bottom--xx-small">
                    
                        <div class="slds-button-group" role="group">
                            <apex:commandButton value="Save" action="{!createLead}" reRender="theLink,pgMsg,bttnGrp" rendered="{!isMandatoryFieldError}" styleClass="slds-button slds-button--neutral" onclick="renderSpinner()" oncomplete="overridePageMessages();renderSpinner();"/>
                            <apex:commandButton value="Save (Ignore Alert)" reRender="theLink,pgMsg,bttnGrp" action="{!forceSaveLead}" rendered="{!isDuplicateSaveAllowed}" styleClass="slds-button slds-button--neutral" onclick="renderSpinner()" oncomplete="overridePageMessages();renderSpinner();"/>
                            <apex:commandButton value="Cancel" action="{!back}" styleClass="slds-button slds-button--neutral" />        
                        </div>
                    </div>
                    </apex:outputPanel>
                </div>
            </div>
            <apex:outputPanel id="pgMsg">
                <apex:pageMessages /><br/>
                
            </apex:outputPanel>
        <apex:outputPanel rendered="{!isMandatoryFieldError}" id="mandPanel">
            <div class="slds-panel slds-grid slds-grid--vertical slds-nowrap" >
                <div class="slds-form--stacked slds-grow">
                    <div class="slds-panel__section ">
                        <h2 class="slds-section-title--divider">Mandatory Fields</h2>
                        <div class="slds-grid slds-wrap slds-grid--pull-padded">
                            <!--apex:repeat value="{!additionalLeadFields}" var="f"-->
                            <apex:repeat value="{!wrapperListforMandatoryFields}" var="f">
                                
                                <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2" >                              
                                    <div style="float:left;margin-left:5px;padding:5px;width:80%" class="responsive-width">
                                        <div class="slds-form-element">
                                            <div class="slds-form-element__control" style="display:{!IF(f.apiName != 'OwnerId' && f.apiName != 'AccountId' && f.apiName != 'IsPrivate' && f.apiName != 'Name' && f.apiName != 'Name' && f.apiName != 'LeadSource' && f.apiName !='Type' && f.apiName !='Description' && f.apiName !='rdcc__project__c','block','none')}">                                                    
                                                <div style="display:{!If($ObjectType.Lead.fields[f.apiName].type !='reference','block','none')}">
                                                    <abbr class="slds-required" title="required" style="float:left;">*</abbr>
                                                    <label class="slds-form-element__label"> {!$ObjectType.Lead.fields[f.apiName].label}</label><br/>
                                                    <apex:inputField value="{!newLead[f.apiName]}" required="false" styleClass="{!If($ObjectType.Lead.fields[f.apiName].type !='boolean','slds-input','')}"/>   
                            
                                                </div>
                                                <apex:outputPanel rendered="{!IF($ObjectType.Lead.fields[f.apiName].type =='reference',true,false)}">
                                                    <div style="display:{!If($ObjectType.Lead.fields[f.apiName].type =='reference','block','none')}">
                                                        <abbr class="slds-required" title="required" style="float:left;">*</abbr>
                                                        <c:SLDSDynamicLookup id="fieldValue" SLDSResourceName="{!$Resource.rdcc__SLDS202}"
                                                                             ObjectApiName="{!leadLkpFieldRefToMap[f.apiName]}" DisplayFieldApiNames="Name" DisplayFieldsPattern="Name"  
                                                                             photo="" LabelName="{!$ObjectType.Lead.fields[f.apiName].label}" SetValueToField="{!newLead[f.apiName]}" SetIdField="{!f.recordValue}"/>
                                                    </div>
                                                </apex:outputPanel>
                                            </div>  
                                        </div>
                                        
                                    </div>
                                </div>                               
                            </apex:repeat> 
                        </div>
                    </div>
                </div>
            </div>
        </apex:outputPanel>
            <apex:outputPanel id="theLink">
            <apex:outputPanel title="Lead Edit" rendered="{!hasDuplicateResult}" >   
            <table class="slds-table slds-table--bordered slds-table--cell-buffer">
                <thead>
                    <tr class="slds-text-title--caps">
                        <th scope="col">
                            <div class="slds-truncate" title="Name"><b>Name</b></div>
                        </th>
                        <!--th scope="col">
                            <div class="slds-truncate" title="Phone"><b>Phone</b></div>
                        </th!-->
                        <th scope="col">
                            <div class="slds-truncate slds-medium-show" title="Owner"><b>Owner</b></div>
                        </th>
                        <th scope="col">
                            <div class="slds-truncate slds-medium-show" title="Last Modified Date"><b>Last Modified Date</b></div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <apex:repeat value="{!duplicateRecords}" var="item" id="theRepeatrow">
                        <tr>
                            <th scope="row" data-label="Name">
                                <div class="slds-truncate" title="{!item['Name']}"><apex:outputLink value="/{!item['Id']}">{!item['Name']}</apex:outputLink></div>
                            </th>
                            <!--td data-label="Phone">
                                <div class="slds-truncate" title="{!item['Phone']}"><apex:outputField value="{!item['Phone']}"/></div>
                            </td!-->
                            <td data-label="Owner">
                                <div class="slds-truncate slds-medium-show" title="{!item['OwnerId']}"><apex:outputField value="{!item['OwnerId']}"/></div>
                            </td>
                            <td data-label="Last Modified Date">
                                <div class="slds-truncate slds-medium-show" title="{!item['LastModifiedDate']}"><apex:outputField value="{!item['LastModifiedDate']}"/></div>
                            </td>
                        </tr>
                    </apex:repeat>
                    
                </tbody>
            </table>
            </apex:outputPanel>
           </apex:outputPanel> 
               
    </apex:form>
   
   
   <apex:form styleClass="slds">
            <div id="ModalId_classic" style="display:none">
                <div role="dialog" id ="boxId" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal--small slds-scrollable" aria-labelledby="header43">
                    <div class="slds-modal__container">
                        <div class="slds-modal__header">
                            
                        </div>
                        <div class="slds-modal__content slds-p-around--medium">                                
                            <div style="height:Auto;">
                                <div class="message errorM3" role="alert" id="pgmsg">
                                    <table border="0" cellpadding="0" cellspacing="0" class="messageTable" style="padding:0px;margin:0px;">
                                        <tbody><tr valign="top">
                                            <td>
                                                <img alt="ERROR" class="msgIcon" src="/s.gif" title="ERROR"/>
                                            </td>
                                            <td class="messageCell"><div id="textId" class="messageText"><span id="msgtype" style="color:#cc0000">
                                                <h4>Error:</h4></span><span id="msg">{!errMsg}</span><br/></div>
                                            </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                </div>
                            </div>
                        </div>
                        <div class="slds-modal__footer">
                                                                                    
                            <apex:commandButton value="Ok" action="{!back}"  style="width: 70px;margin-left: 10px;"/>                       
                        </div>
                    </div>                        
                </div>
                <div class="slds-backdrop slds-backdrop--open" id="backGroundSectionId"></div>
            </div>
        
        
        
        <div id="ModalId_light" style="display:none">
                <div role="dialog" id ="boxId" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal--small slds-scrollable" aria-labelledby="header43">
                    <div class="slds-modal__container">
                    <div class="slds-modal__header slds-theme--error slds-theme--alert-texture">
                      <button class="slds-button slds-modal__close slds-button--icon-inverse" title="Close">
                        <svg class="slds-button__icon slds-button__icon--large" aria-hidden="true">
                          <use xlink:href="/assets/icons/utility-sprite/svg/symbols.svg#close"></use>
                        </svg>
                        <span class="slds-assistive-text">Close</span>
                      </button>
                      <h2 class="slds-text-heading--medium" id="prompt-heading-id">Service Unavailable</h2>
                    </div>
                    <div class="slds-modal__content slds-p-around--medium">
                      <p>{!errMsg}</p>
                    </div>
                    <div class="slds-modal__footer slds-theme--default">

                                                                                                    
                    <apex:commandButton value="Ok" action="{!back}"  styleClass="slds-button slds-button--neutral" style="width: 70px;margin-left: 10px;"/>                       
                                      
                    </div>
                    </div>
                </div>
                <div class="slds-backdrop slds-backdrop--open" id="backGroundSectionId"></div>
        </div>
    </apex:form>
    </html>
</apex:page>