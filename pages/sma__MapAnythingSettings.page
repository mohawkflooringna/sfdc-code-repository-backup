<apex:page sidebar="false" title="MapAnything Settings" controller="sma.MapAnythingBaseObjects" setup="false" extensions="sma.MARemoteFunctions,sma.MAAdminAJAXResources">
    <apex:slds />
    <c:MASystem />
    <script type='text/javascript' src="{!URLFOR($Resource.MapAnything, 'js/jquery-2.2.4.min.js')}"></script>
    <script async="true" defer="true" type='text/javascript' src="{!URLFOR($Resource.MapAnything, 'jquery/jquery-ui.min.js')}"></script>
    <script type="text/javascript" src="{!URLFOR($Resource.MapAnything, 'toastr/toastr.min.js')}"></script>
    <script async="true" defer="true" type='text/javascript' src="{!URLFOR($Resource.MapAnythingJS, 'JSActionFramework.js')}"></script>
    <!-- async -->
    <script async="true" defer="true" type='text/javascript' src="{!URLFOR($Resource.MapAnything, 'async/async.min.js')}"></script>
    <link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.MapAnything, 'toastr/toastr.min.css')}"></link>
    <c:MA />
    <script type="text/javascript">
		if (!!navigator.userAgent.match(/Trident.*rv\:11\./)) {
            // polyfill remove function for IE
            (function() {
                function remove() { this.parentNode && this.parentNode.removeChild(this); }
                if (!Element.prototype.remove) Element.prototype.remove = remove;
                if (Text && !Text.prototype.remove) Text.prototype.remove = remove;
            })();

            console.log('detected IE11 ... loading polyfills');
            var head = document.getElementsByTagName('head')[0] || document.body;
            var script = document.createElement('script');
            script.src = "{!URLFOR($Resource.MapAnything, 'js/polyfill.min.js')}";
            head.appendChild(script);

            console.log('adding IE style override')
            var link = document.createElement("link");
            link.type = "text/css";
            link.rel = "stylesheet";
            link.href = "{!URLFOR($Resource.MapAnything, 'css/ie11.css')}";
            head.appendChild(link);
        } else if (!!navigator.userAgent.match(/MSIE 10.0.*/)) {
            window.location = '/apex/Oops?type=UnsupportedBrowser';
        }
	</script>
    <script>
        var MARemoting = {
            processAJAXRequest : '{!$RemoteAction.MARemoteFunctions.processAJAXRequest}',
            processAJAXRequestReadOnly : '{!$RemoteAction.MARemoteFunctions.processAJAXRequestReadOnly}',
            AdminStartUpAction: '{!$RemoteAction.MAAdminAJAXResources.AdminStartUpAction}',
            getPermissioRows: '{!$RemoteAction.MAAdminAJAXResources.getPermissioRows}'
        }

        function removeNamespace(namespace, obj) {
            try {
                var namespacePrefix = namespace + '__';
                $.each(obj, function (key, val) {
                    if (key.indexOf(namespacePrefix) === 0) {
                        obj[key.replace(namespacePrefix, '')] = val;
                        delete obj[key];

                        //go recursive if this is an object
                        if (obj[key.replace(namespacePrefix, '')] !== null && typeof obj[key.replace(namespacePrefix, '')] == 'object') {
                            removeNamespace(namespace, obj[key.replace(namespacePrefix, '')]);
                        }
                    } else if (typeof val == 'object') {
                        removeNamespace(namespace, val);
                    }
                });
            } catch (err) { } //this is most likely due to a null value being passed.  in any case, returning the original objects seems the correct action if we can't manipulate it

            return obj;
        }

        function getProperty(obj, prop, removeWorkspace)
        {
            prop = prop || '';

            if(removeWorkspace !== false)
            {
                //needed when working in our packaging org(s)
                if ( MA.Namespace == 'sma')
                {
                    obj = MA.Util.removeNamespace(obj,'sma__');

                    //remove from string prop as well
                    prop = prop.replace(/sma__/g,'');
                }
            }

            var arr = prop.split(".");
            while(arr.length && (obj = obj[arr.shift()]));
            return obj;
        }

        toastr.options = {
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "iconClasses" : {
                "success" : "toast-success",
                "error"   : "toast-error",
                "warning" : "toast-warning",
                "info"    : "toast-info"
            }
        };

        var MAToastMessages = {
            showMessage : function(options,type) {
                type = type || 'info';
                options = $.extend({
                    message : '',
                    subMessage : '',
                    timeOut : 3000,
                    extendedTimeOut : 1000,
                    position : 'toast-bottom-right',
                    closeButton : false,
                    onclick : null
                }, options || {});

                toastr.options.timeOut = options.timeOut;
                toastr.options.extendedTimeOut = options.extendedTimeOut;
                toastr.options.positionClass = options.position;
                toastr.options.closeButton = options.closeButton;
                toastr.options.onclick = options.onclick;

                if(type === 'loading') {
                    var $message = toastr['info'](options.subMessage,options.message);
                    $message.addClass('ma-toast-loading').removeClass('toast-info');
                    return $message;
                }
                else {
                    return toastr[type](options.subMessage,options.message);
                }


            },
            showSuccess : function(options) {
                return MAToastMessages.showMessage(options,'success');
            },
            showLoading : function(options) {
                return MAToastMessages.showMessage(options,'loading');
            },
            showWarning : function(options) {
                return MAToastMessages.showMessage(options,'warning');
            },
            showError : function(options) {
                return MAToastMessages.showMessage(options,'error');
            },
            hideMessage : function (toast) {
                toastr.clear(toast);
                toast.remove();
                toast = null;
            }
        } 
    </script>
    <!-- icon-font --> 
    <apex:stylesheet value="{!URLFOR($Resource.sma__MapAnythingJS, 'styles/styles.css')}"/>

    <!-- MapAnything Configuration Page Styling -->
    <apex:stylesheet value="{!URLFOR($Resource.sma__MapAnythingJS, 'styles/css/ma-configuration-ui.css')}"/>

    <div class="slds-scope">
        <div class="slds-brand-band slds-brand-band_cover slds-brand-band_medium slds-p-around_medium">
            <div class="flex-column full-height">
                <!-- Header -->
                <div class="slds-scope">
                    <div class="slds-page-header slds-has-bottom-magnet">
                        <div class="slds-grid">
                            <div class="slds-col slds-has-flexi-truncate">
                                <div class="slds-media slds-no-space slds-grow">
                                    <div class="slds-media__figure ma-slds-media__figure">
                                        <span class="slds-icon ma-icon ma-icon-mapanything"></span>
                                    </div>
                                    <div class="slds-media__body">
                                        <p class="slds-text-title--caps slds-line-height--reset">MapAnything</p>
                                        <h1 class="slds-page-header__title slds-m-right--small slds-align-middle slds-truncate">{!$Label.MA_Configuration}</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 

                <div class="flex-row flex-grow-1">
                    <div class="flex-shrink-0"> 
                        <!--navigation-->
                        <c:MAAdminHeader activeTab="settings" />
                    </div>
                    <div class="flex-grow-1">
                        <div id="ma-vue-root"></div>
                        <link type="text/css" rel="stylesheet" href="{!resourcePaths['MapAnythingJS']}/dist/admin/general-settings/styles.css" />
                        <script type="text/javascript" src="{!resourcePaths['MapAnythingJS']}/dist/admin/general-settings/bundle.js"></script>
                    </div>
                </div>
            </div>
        </div>
        <div class="templates" style="display:none;">
            <!-- Button Set Templates -->
            <div class='buttonset-section'>

                <!-- Header -->
                <div class='buttonset-section-header'>
                    <div class='buttonset-section-name editable'>{!$Label.MA_New_Section}</div>
                </div>

                <!-- Buttons -->
                <div class='buttonset-section-columns'>
                    <div class='buttoncolumn'>
                        <div class='button-dropzone'><div class='button-dropzone-handle'></div></div>
                    </div>
                    <div class='buttoncolumn'>
                        <div class='button-dropzone'><div class='button-dropzone-handle'></div></div>
                    </div>
                    <div class='buttoncolumn'>
                        <div class='button-dropzone'><div class='button-dropzone-handle'></div></div>
                    </div>
                </div>

                <!-- Dropzone -->
                <div class='section-dropzone'></div>

            </div>
            <div class='buttonset-button'>
                <div class='button-dropzone'><div class='button-dropzone-handle'></div></div>
            </div>
        </div>
    </div>
</apex:page>