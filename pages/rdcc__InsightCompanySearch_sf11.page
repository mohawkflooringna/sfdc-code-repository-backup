<apex:page controller="rdcc.InsightCompanySearchController" id="page" tabStyle="rdcc__Insight_Company__c" docType="html-5.0" sidebar="false" readOnly="true">
    <div class="slds">
        <div class="slds-spinner_container" id="loading" style="display:none;">
            <div id="loading-content">
                <div role="status" class="slds-spinner slds-spinner--medium" style="top:350px;">
                    <span class="slds-assistive-text">Loading</span>
                    <div class="slds-spinner__dot-a"></div>
                    <div class="slds-spinner__dot-b"></div>
                </div>
            </div>
        </div>
    </div>
    <apex:includescript value="{!URLFOR($Resource.rdcc__ConstructConnect, 'js/jquery-1.11.1.min.js')}" />
    <apex:includescript value="{!URLFOR($Resource.rdcc__ConstructConnect, 'js/jquery.dataTables.min.js')}" />
    <apex:stylesheet value="{!URLFOR($Resource.rdcc__ConstructConnect, 'css/jquery.dataTables.css')}"/>
    <apex:includeScript value="{!URLFOR($Resource.rdcc__ConstructConnect, 'js/InsightCompanySearch_sf11.js')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.rdcc__SLDS202, 'assets/styles/salesforce-lightning-design-system-vf.min.css')}" />   
    <apex:stylesheet value="{!URLFOR($Resource.rdcc__ConstructConnect, 'css/InsightCompanySearch_sf11.css')}"/>
    <style>
        .slds .slds-spinner:before, .slds .slds-spinner:after, 
        .slds .slds-spinner__dot-a:before, .slds .slds-spinner__dot-b:before, 
        .slds .slds-spinner__dot-a:after, .slds .slds-spinner__dot-b:after{
        background:#0070d2 !important;
        }
        @media only screen and (min-device-width: 320px) and (max-device-width: 1824px) {
	.sidenav{padding-top:30px;}
    </style>
    <script>
    document.getElementById("loading").style.display='block';
    window.onload = function() {
        document.getElementById("loading").style.display='none';
    }
    function clearProjectEstValue(){        
        document.getElementById("page:theForm:minProjectValue").value = '';
        document.getElementById("page:theForm:maxProjectValue").value = '';
        document.getElementById("btnProjectEstValuePanel").style.display = 'none';
        document.getElementById("mySidenav").style.width = "0";
        
        
        return false;
    }
    function expandSectionBelow(element){
        element.classList.toggle("active");
        panel = element.nextElementSibling;
        if (panel.style.display=="none" ){
            panel.style.display = 'block';
        } else {
            panel.style.display = 'none';
            hideErrorDiv();
        }
        hideOtherAccordian(element);
    }
    function hideErrorDiv(){
        document.getElementById('errMsgEstValue').style.display = 'none';   
    }
    function hideOtherAccordian(element){
        var acc = document.getElementsByClassName("active");
        for(var i=0;i<acc.length;i++){                               
            if(element != null && acc[i].id != element.id){
                hideAccordian(acc[i].nextElementSibling); 
                acc[i].classList.toggle("active");
            }
        }
    }
    function validateData(minId,maxId,elementId,type,SLDS202){           
        var minValue =  document.getElementById(minId).value;
        var maxValue =  document.getElementById(maxId).value;
        var isError = false;
        var errorMsg = '';
        if((minValue == null || minValue == '')){
            minValue = '0';
        }
        if((maxValue == null || maxValue == '')){
            maxValue = '0';
        }
        if( minValue == '0' && maxValue == '0'){
            isError = true;
            errorMsg = 'Min and Max both cannot be zero. please provide value of atleast one field';
        }
        else if(!minValue.match(/^\d+$/) || !maxValue.match(/^\d+$/)){
            isError = true;
            errorMsg = 'Only numeric value acceptable for both min and max field.';
        }
            else if(minValue > maxValue && !(document.getElementById(maxId).value =='' || document.getElementById(maxId).value == null)){
                isError = true;
                errorMsg = 'minValue must be smaller than maxValue.';
            }
        if(isError){
            document.getElementById(elementId).style.display = 'block';
            document.getElementById(elementId).innerText = errorMsg;
            onComplete();
            if(type == 'Estimated Value'){
                toggleSelectedDiv('selectedEstValueDiv','selectedEstValueContainer');
            }
            toggleOpenApply();
            document.getElementById("mySidenav").style.width = "100%";
            
        }else{
            document.getElementById(elementId).style.display = 'none';
            toggleCloseApply();
            document.getElementById("mySidenav").style.width = "0";
            if(type == 'Estimated Value')
                customSearch();
        }
        return false;        
    }
    function toggleOpenApply(){
        panel = document.getElementById("btnProjectEstValuePanel");
        panel.style.display = 'block';
    }
    function toggleCloseApply(){
        panel = document.getElementById("btnProjectEstValuePanel");
        panel.style.display = 'none';
    }
    
    function onChangeListView(){
        hideExpandedAccordian();
        uncheckedAllCheckbox();
        clearFilters();
        tooglePillsDiv();
        listViewChange();
        clearTextFilters();            
    }
    </script> 
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <!--apex:stylesheet value="{!URLFOR($Resource.rdcc__SLDS202, 'assets/styles/salesforce-lightning-design-system-vf.min.css')}" /-->
        
        <apex:form styleClass="slds" id="theForm">
            <apex:actionFunction name="searchServer" action="{!runSearch}" oncomplete="resultTableProperties();overridePageMessages();hideExpandedAccordian();onComplete();" status="spinnerStatus" rerender="results,debug,errors">
                <apex:param name="country" value=""/>
                <apex:param name="state" value=""/>
                <apex:param name="county" value=""/>      
                <apex:param name="type" value=""/>
                <apex:param name="searchText" value=""/>
                <apex:param name="listView" value=""/>
                <apex:param name="projectValueRange" value=""/>
            </apex:actionFunction>
            <apex:actionFunction name="listViewChange" 
                                 action="{!onListViewChange}" 
                                 oncomplete="overridePageMessages();onComplete();resultTableProperties();" 
                                 status="spinnerStatus" 
                                 rerender="results,debug,errors"/>
            
            <div id="msgDiv" style="display:none;margin-left:20px; margin-right:20px;">
                <apex:pageMessages id="errors"></apex:pageMessages>
            </div>
            
            <!--Header section--->
            <div class="slds-page-header slds-page-header--object-home slds-fixed-header" style="padding-top:.5rem;padding-bottom:.5rem;">
                <div class="slds-grid">
                    <div class="slds-col slds-has-flexi-truncate">
                        <div class="slds-media slds-no-space slds-grow">
                            <div class="slds-media__figure">
                                <span class="slds-icon_container slds-icon-standard-household">
                                    <svg class="slds-icon slds-icon_small" aria-hidden="true">
                                        <use xlink:href="{!URLFOR($Resource.SLDS202, '/assets/icons/standard-sprite/svg/symbols.svg#household')}"></use>
                                    </svg>
                                </span>
                            </div>
                            <div class="slds-media__body">
                                <div id="view-list" class="slds-dropdown-trigger--click slds-m-left--x-small" aria-expanded="false">
                                    <p class="slds-text-title--caps slds-line-height--reset">Insight Companies</p>
                                    <h1 class="slds-page-header__title slds-p-right--x-small">
                                        <span class="slds-grid slds-has-flexi-truncate slds-grid--vertical-align-center">
                                            <apex:selectList styleClass="searchTypeListView slds-scope slds-input" value="{!selectedListView}" id="theLV"  size="1" onchange="onClick();onChangeListView();closeNav();" style="min-height:25px;">
                                                <apex:selectOptions value="{!listViews}" />        
                                            </apex:selectList>
                                            <span class="slds-icon_container slds-icon_container--circle slds-icon-action-filter" title="Add Filter" onclick="openNav();hideExpandedAccordian();" style="margin-left:.5rem;padding: .3rem;">
                                                <svg class="slds-icon slds-icon--x-small" aria-hidden="true" style="width: 1.2rem;">
                                                    <use xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/action-sprite/svg/symbols.svg#filter')}"></use>
                                                </svg>
                                            </span>
                                        </span>
                                        <span class="slds-grid slds-has-flexi-truncate slds-grid--vertical-align-center">
                                            <apex:selectlist id="saveLV" style="background-color: #fff;min-height:25px;" size="1" onchange="onClick();loadSearchName(this.value);closeNav();" styleClass="slds-scope slds-input">                            
                                                <apex:selectoptions value="{!savedSearches}" />
                                            </apex:selectlist>
                                            <apex:outputPanel rendered="{!cs.rdcc__Allow_Company_Creation_Manually__c}">
                                                <apex:outputLink value="{!URLFOR($Action.rdcc__Insight_Company__c.New)}">
                                                    <span class="slds-icon_container slds-icon_container--circle slds-icon-action-new" onclick="closeNav();" title="New" style="margin-left:.5rem;padding: .3rem;">
                                                        <svg class="slds-icon slds-icon--x-small" aria-hidden="true" style="width: 1.2rem;">
                                                            <use xlink:href="{!URLFOR($Resource.SLDS202, '/assets/icons/action-sprite/svg/symbols.svg#new')}"></use>
                                                        </svg>
                                                    </span>
                                                </apex:outputLink>
                                            </apex:outputPanel>
                                        </span>                                        
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--search section-->
            <div class="slds-form-element" style="top:70px;">
                <div class="slds-p-vertical--x-small slds-p-horizontal--small slds-shrink-none slds-theme--shade">
                    <div class="slds-form-element slds-lookup" data-select="single">                        
                        <div class="slds-form-element__control slds-input-has-fixed-addon" style="margin-top:4%;">                        
                            <apex:inputText styleClass="slds-input" 
                                            html-placeholder="Enter Search Text..." 
                                            id="searchText"  
                                            onkeydown="if (event.keyCode == 13){event.preventDefault();this.nextSibling.nextSibling.firstChild.click()}" /> 
                            
                            <span class="slds-form-element__addon">
                                <apex:commandButton id="btnSearch"  
                                                    onclick="onClick();return customSearch();" 
                                                    status="spinnerStatus"
                                                    styleClass="slds-button slds-button--icon-border"
                                                    style="padding:5px;"
                                                    value="Search!" 
                                                    image="{!URLFOR($Resource.rdcc__SLDS202, '/assets/icons/utility/search_60.png')}" />
                                
                            </span>
                        </div>
                    </div>
                </div>
            </div>    
            <!--Search Results section-->
            <apex:outputPanel id="results">
                <apex:outputPanel >
                    <table id="resultTable" class="display">
                        <thead style="display:none;">
                            <tr>
                                <th>Header</th>        
                            </tr>
                        </thead>
                        <tbody>
                            <apex:repeat value="{!companies}" var="company">
                                <tr>   
                                    <td>
                                        
                                        <div>
                                            <apex:repeat value="{!selectedFields}" var="s">
                                                <apex:outputPanel rendered="{!IF(OR(insCompFieldApiNameTypeMap[s.value] == 'DATE',insCompFieldApiNameTypeMap[s.value] == 'DATETIME'),true,false)}">
                                                    <b><apex:outputLabel value="{!s.Label}:"/></b> &nbsp;<apex:outputField value="{!company[s.value]}" /><br/>
                                                </apex:outputPanel>
                                                <apex:outputPanel rendered="{!IF(insCompFieldApiNameTypeMap[s.value]=='CURRENCY',true,false)}">
                                                    <b><apex:outputLabel value="{!s.Label}:"/></b> &nbsp;<apex:outputField value="{!company[s.value]}" style="color:#005fb2;"/><br/>
                                                </apex:outputPanel>
                                                <apex:outputPanel rendered="{!IF(s.value=='Name',true,false)}">
                                                    <a href="/{!company.Id}" style="text-decoration: none; " > <b><apex:outputText value="{!company.name}" style="color:#005fb2;" /></b></a><br/>
                                                </apex:outputPanel>
                                                <apex:outputPanel rendered="{!IF(insCompFieldApiNameTypeMap[s.value]=='REFERENCE',true,false)}">
                                                    <b><apex:outputLabel value="{!s.Label}:"/></b> &nbsp;<a style="text-decoration:none;color:#005fb2;" href="javascript:void(0);" >{!company[insCompLkpFieldsMap[s.value]]}</a><br/>                                                            
                                                </apex:outputPanel>
                                                <apex:outputPanel rendered="{!IF(insCompFieldApiNameTypeMap[s.value]!='REFERENCE' && s.value!='Name' && insCompFieldApiNameTypeMap[s.value]!='DATE' && insCompFieldApiNameTypeMap[s.value]!='DATETIME'  && s.value != 'rdcc__Insight_Participant_Total_Project_Value__c',true,false)}">
                                                    <b><apex:outputLabel value="{!s.Label}:"/></b> &nbsp;<apex:outputText value="{!company[s.value]}"  style="color:#005fb2;"/><br/>
                                                </apex:outputPanel>                                                        
                                            </apex:repeat>
                                        </div>
                                        
                                    </td>
                                </tr>
                            </apex:repeat>
                        </tbody>
                    </table>
                </apex:outputPanel> 
            </apex:outputPanel>
            <!--sidenav-filetrs--->
            <div id="mySidenav" class="sidenav" style="top:80px;">
                <apex:commandLink styleClass="slds-text-link"
                                  style="text-decoration: underline;color: #0070d2;"
                                  value="Clear Filters"
                                  onclick="openNav();uncheckedAllCheckbox();onClick();clearTextFilters();clearFilters();tooglePillsDiv();return customSearch();" />
                <table cellpadding="2" cellspacing="2">
                    <tr>
                        <td><div id="countryStateAccId" style="width: 93%;">
                            
                            
                            <button id="btnCountryStateTS" class="accordion" onclick = "expandSection(this);"  type="button" style="width:80%;">Company Location</button>
                            <div class="tooltip" id="btnCountryStateTSPanel" style="display:none;width:80%;">
                                
                                
                                <div id="header" class="div_header" > 
                                    <apex:outputPanel >
                                        <apex:commandLink value="Clear" styleClass="clear-filter" onclick="return uncheckedAllCheckbox('btnCountryStateTScontent');"/>
                                    </apex:outputPanel>
                                </div>
                                <div id="btnCountryStateTScontent" Style="background-color:white;overflow:auto;max-height: 200px;"> 
                                    <c:Tree_New ts="{!countryStateTS}" layer="3"/>
                                </div> 
                                <div id="footer" class="div_header" style="text-align:center" >
                                    <apex:commandButton value="Apply"  
                                                        onclick="openNav();onClick();createPillsForSelectedLocation('{!$Resource.rdcc__SLDS202}');toogleSelectedLocationDiv();return customSearch();"  
                                                        styleClass="slds-button slds-button--neutral" />
                                    
                                    <input type="button" value = "Cancel" class="slds-button slds-button--neutral" 
                                           onclick="hideExpandedAccordian();onClose('btnCountryStateTScontent','selectedLocationDiv');openNav();" 
                                           oncomplete="resultTableProperties();"/>
                                </div> 
                                
                            </div> 
                            </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td><div id="typeDiv" style="display:{!IF(companyTypeTS.nodes.size == 0, 'none','block')}">
                            
                            
                            <button id="btnCompanyTypeTS" class="accordion" onclick = "expandSection(this);"  type="button" style="width:74.5%;">Company Type</button>
                            <div class="tooltip" id="btnCompanyTypeTSPanel" style="width:74.5%;display:none">
                                
                                <div id="header" class="div_header" > 
                                    <apex:outputPanel >
                                        <apex:commandLink value="Clear" 
                                                          styleClass="clear-filter" 
                                                          onclick="return uncheckedAllCheckbox('btnCompanyTypeTScontent');"/>
                                    </apex:outputPanel>
                                </div>
                                <div id="btnCompanyTypeTScontent" Style="background-color:white;overflow:auto;max-height: 200px;"> 
                                    <c:Tree_New ts="{!companyTypeTS}" layer="2"/>
                                </div> 
                                <div id="footer" class="div_header" style="text-align:center"> 
                                    <apex:commandButton value="Apply" 
                                                        onclick="openNav();onClick();createPillsForSelectedType('{!$Resource.rdcc__SLDS202}');toogleSelectedTypeDiv();return customSearch();" 
                                                        styleClass="slds-button slds-button--neutral" />
                                    
                                    <input type="button" value = "Cancel" class="slds-button slds-button--neutral" 
                                           onclick="hideExpandedAccordian();onClose('btnCompanyTypeTScontent','selectedTypeDiv');openNav();" 
                                           oncomplete="resultTableProperties();"/>
                                </div> 
                            </diV>                                        
                            
                            </div>
                        </td>                                
                    </tr>
                    <tr>
                        <td>
                            <button id="btnProjectEstValue" class="accordion" onclick="expandSectionBelow(this);" type="button" style="width:74.5%;">Open Project Value</button>
                            <div class="panel" id="btnProjectEstValuePanel" style="display:none;width:74.5%;padding:10px;max-height:300px;">
                                <div style="float:right;margin-right:10px;">
                                    <apex:commandLink value="Clear"  onclick="onClick();clearProjectEstValue();return customSearch();" style="text-decoration: underline;color: #0070d2;"/>
                                </div>
                                <div>
                                    <apex:outputLabel styleClass="slds-text-body_regular" for="frmLastUpdated" value="Min" style="margin-right: 30px;font-weight: bold;" />
                                    <apex:inputText id="minProjectValue" label="Min" style="width:100%;margin-top: 5px;" styleClass="slds-input" />
                                </div>
                                <br/>
                                <div>
                                    <apex:outputLabel styleClass="slds-text-body_regular" for="toLastUpdated" value="Max" style="margin-right: 30px;font-weight: bold;" />
                                    <apex:inputText id="maxProjectValue"  label="Min" style="width:100%;margin-top: 5px;" styleClass="slds-input" />
                                </div>
                                <br/>   
                                <div id="errMsgEstValue" style="display:none;color:red"><p>*Please provide value of atleast one field.</p><br/></div>
                                <div style="text-align:center">
                                    <apex:commandButton value="Apply" rerender="results" onclick="onClick();return validateData('page:theForm:minProjectValue','page:theForm:maxProjectValue','errMsgEstValue','Estimated Value','{!$Resource.rdcc__SLDS202}');" status="spinnerStatus" styleClass="slds-button slds-button--neutral" style="margin-bottom: 5px;" />
                                </div>
                            </div>
                        </td>                                
                    </tr>
                    <tr>
                        <td>                                      
                            <div id="selectedLocationContainer" class="slds" style="display:none" >
                                <div id="header" class="div_header" style="margin-left:-7px"> 
                                    <p>
                                        Company Location
                                    </p>
                                </div>
                                <div id="selectedLocationDiv" Style="background-color:white;overflow:auto;max-height: 200px;max-width:200px"/> 
                            </div>                                     
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <div id="selectedTypeContainer" class="slds" style="display:none">
                                <div id="header" class="div_header" style="margin-left:-7px"> 
                                    <p>
                                        Company Type
                                    </p>
                                </div>
                                
                                <div id="selectedTypeDiv" Style="background-color:white;overflow:auto;max-height: 200px;max-width:200px"/>    
                            </div>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="selectedEstValueContainer" class="slds" style="display:none">
                                <div id="header" class="div_header" style="margin-left:-7px"> 
                                    <p>
                                        Open Project Value
                                    </p>
                                </div>
                                
                                <div id="selectedEstValueDiv" Style="background-color:white;overflow:auto;max-height: 200px;max-width:200px"/>    
                            </div>
                            
                        </td>
                    </tr>       
                </table>
            </div>     
        </apex:form>
    </html>
</apex:page>